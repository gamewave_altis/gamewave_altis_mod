ZoneDeRadiaFion = nil;

InRadiaFionZone = {
	ZoneDeRadiaFion = _this select 0;
};

OutRadiaFionZone = {
	ZoneDeRadiaFion = nil;
	"chromAberration" ppEffectEnable false;
	"radialBlur" ppEffectEnable false;
	"FilmGrain" ppEffectEnable false;
	"dynamicBlur" ppEffectEnable false;	
};

CalculateNearNess = {
	private ["_zone", "_unit", "_radius", "_percentNear"];
	_zone = _this select 0;
	_unit = _this select 1;
	_radius = ((triggerArea _zone select 0) + (triggerArea _zone select 1)) / 2;
	_percentNear = ((((_radius - (_zone distance _unit)) / _radius) max 0) min 1) * 100;
	_percentNear
};

_radiaFionZones = ["radiaFion_1","radiaFion_2","radiaFion_3"];
{
	_zone = createTrigger ["EmptyDetector",(getMarkerPos _x)];
	_zone setTriggerArea[500,500,0,false];
	_zone setTriggerActivation["ANY","PRESENT",true];
	_zone setTriggerStatements["player in thislist","[thisTrigger] call InRadiaFionZone;[thisTrigger] spawn life_fnc_radiafion;","[thisTrigger] call OutRadiaFionZone;"];
} foreach _radiaFionZones;