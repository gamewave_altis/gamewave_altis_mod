/* 
	Lucel
	_null=this execVM "pnj\dive.sqf";
*/

_this allowDamage false; 
_this enableSimulation false; 
//_this addUniform "A3L_Dude_Outfit";
//_this setFace "GreekHead_A3_01";

_this enableSimulation false; 
 //removeAllContainers _this;

_this setVariable["realname", "Jacob - Taxi"];

_this addAction["<t color='#AAF200'>Véhicule de taxi</t>",
life_fnc_vehicleShopMenu,["taxi_shop",civilian,["taxi_spawnA_1","taxi_spawnA_2"]
,"taxi","Véhicule de taxi"],0,false,false,"",''];

_this addAction["<t color='#FF9900'>Garage</t>",
{  [[getPlayerUID player,playerSide,"car",player],"TON_fnc_getVehicles",false,false] spawn life_fnc_MP;
life_garage_type = "car";
createDialog "Life_impound_menu";
disableSerialization;
ctrlSetText[2802,"Recherches des véhicules...."];
life_garage_sp = "taxi_spawnA_1";  },"",0,false,false,"",''];
_this addAction["<t color='#FF9900'>Rentrer au garage</t>",life_fnc_storeVehicle,"",0,false,false,"",'!life_garage_store'];

_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€))</t>",["license_civ_taxi"] call life_fnc_varToStr,[(["taxi"] call life_fnc_licensePrice)]
 call life_fnc_numberText],life_fnc_buyLicense,"taxi",0,false,false,"",' !license_civ_taxi && !license_civ_rebel && !license_civ_merc && playerSide == civilian '];
