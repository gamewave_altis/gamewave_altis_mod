/* 
	Lucel
	_null=this execVM "pnj\dive.sqf";
*/

_this allowDamage false; 
_this enableSimulation false; 
//_this addUniform "alfr_coveralls_dep";
//_this setFace "GreekHead_A3_01";

_this enableSimulation false; 
 //removeAllContainers _this;

_this setVariable["realname", "Joe - le mécano"];

_this addAction["<t color='#AAF200'>Véhicule de dépannage</t>",
life_fnc_vehicleShopMenu,["dep_shop",civilian,["dep_spawnA_1","dep_spawnA_2"]
,"dep","Véhicule de dépannage"],90,false,false,"",'playerSide == civilian && license_civ_dep'];

_this addAction["<t color='#AAF200'>Magasin d'uniforme</t>",life_fnc_clothingMenu,"dep",100,false,false,"",'playerSide == civilian && license_civ_dep'];


_this addAction["<t color='#FF9900'>Garage</t>",
{  [[getPlayerUID player,playerSide,"car",player],"TON_fnc_getVehicles",false,false] spawn life_fnc_MP;
life_garage_type = "car";
createDialog "Life_impound_menu";
disableSerialization;
ctrlSetText[2802,"Recherches des véhicules...."];
life_garage_sp = "dep_spawnA_1";  },"",0,false,false,"",''];
_this addAction["<t color='#FF9900'>Rentrer au garage</t>",life_fnc_storeVehicle,"",0,false,false,"",'!life_garage_store'];

_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€)</t>",["license_civ_dep"] call life_fnc_varToStr,[(["dep"] call life_fnc_licensePrice)] call life_fnc_numberText],
  life_fnc_buyLicense,"dep",0,false,false,"",' !license_civ_dep && !license_civ_rebel && !license_civ_merc && playerSide == civilian '];
  
_this addAction["<t color='#AAF200'>Boutique pour dépanneur</t>",life_fnc_virt_menu,"dep_shop",1,false,false,"",'playerSide == civilian && license_civ_dep']; 