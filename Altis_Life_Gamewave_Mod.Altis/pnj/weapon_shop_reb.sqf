/* 
	RebshopLucel
*/

//Setup visuel
_this allowDamage false; 
_this enableSimulation false; 
//_this addUniform "U_BG_Guerilla1_1";
//_this addVest "V_PlateCarrierIA2_dgtl";
//_this addHeadgear "H_Shemag_olive_hs";
//_this addGoggles "G_Shades_Green";
//_this setFace "PersianHead_A3_03";

_this switchmove "acts_PointingLeftUnarmed";
_this setVariable["realname", "Chef Rebelle"];

_this enableSimulation false; 
 //removeAllContainers _this;
_this allowDamage false;  





_this addAction["<t color='#FF0000'>Marché rebelle</t>",life_fnc_virt_menu,"rebmarket",90,false,false,"",'license_civ_rebel && playerSide == civilian'];
_this addAction["<t color='#FF0000'>Magasin général</t>",life_fnc_weaponShopMenu,"genstore",92,false,false,"",'license_civ_rebel && playerSide == civilian'];
_this addAction["<t color='#FF0000'>Magasin d'outillage</t>",life_fnc_weaponShopMenu,"civil_tool",93,false,false,"",'license_civ_rebel && playerSide == civilian'];
_this addAction["<t color='#AAF200'>Magasin de vêtement</t>",life_fnc_clothingMenu,"reb",95,false,false,"",' license_civ_rebel && playerSide == civilian'];
_this addAction["<t color='#FF9900'>Armurerie rebelle</t>",life_fnc_weaponShopMenu,"rebel",100,false,false,"",' license_civ_rebel && playerSide == civilian'];
_this addAction["<t color='#ADFF2F'>Distributeur</t>",life_fnc_atmMenu,"",0,false,false,"",''];

_this addAction[format["<t color='#00ffff'>Achat: %1</t>  <t color='#AAF200'>(%2€)</t>",["license_civ_rebel"] call life_fnc_varToStr,[(["rebel"] call life_fnc_licensePrice)] call life_fnc_numberText],
life_fnc_buyLicense,"rebel",0,false,false,"",' !license_civ_rebel && !license_civ_dep && !license_civ_taxi && !license_civ_medic && playerSide == civilian '];

_this addAction["<t color='#AAF200'>Magasin CMN</t>",life_fnc_clothingMenu,"CMN",150,false,false,"",' _idgrp = group player;
license_civ_rebel && playerSide == civilian && {(_idgrp getVariable "gang_id") == (1079)}'];

_this addAction["<t color='#AAF200'>Magasin RIE</t>",life_fnc_clothingMenu,"RIE",150,false,false,"",' _idgrp = group player;
license_civ_rebel && playerSide == civilian && {(_idgrp getVariable "gang_id") == (1078)}'];

_this addAction["<t color='#AAF200'>Magasin C.E.S.A.R</t>",life_fnc_clothingMenu,"C.E.S.A.R",150,false,false,"",' _idgrp = group player;
license_civ_rebel && playerSide == civilian && {(_idgrp getVariable "gang_id") == (970)}'];

// fin du fichier connard