#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)

class Life_job_management {
	idd = 60000;
	name= "Life_job_management";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "[] spawn life_fnc_jobMenu;";
	
	
class controlsBackground {
	class Life_RscTitleBackground:Life_RscText {
	idc = -1;

	x = 9 * GUI_GRID_W + GUI_GRID_X;
	y = 2 * GUI_GRID_H + GUI_GRID_Y;
	w = 24 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
	colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])","(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
	
	};
		
	class MainBackground:Life_RscText {
	idc = -1;

	x = 9 * GUI_GRID_W + GUI_GRID_X;
	y = 3 * GUI_GRID_H + GUI_GRID_Y;
	w = 24 * GUI_GRID_W;
	h = 12.8 * GUI_GRID_H;
	colorBackground[] = {0,0,0,0.7};
		};
	};
class controls {

class Title: Life_RscTitle
{
	idc = -1;

	text = "Menu des métiers";
	x = 9 * GUI_GRID_W + GUI_GRID_X;
	y = 2 * GUI_GRID_H + GUI_GRID_Y;
	w = 24 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
	colorText[] = {0.95,0.95,0.95,1};
};
class AnimList: Life_RscListBox
{
	idc = 60001;

	x = 9.9 * GUI_GRID_W + GUI_GRID_X;
	y = 3.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 22.4 * GUI_GRID_W;
	h = 9.25 * GUI_GRID_H;
};
class CloseButtonKey: Life_RscButtonMenu
{
	onButtonClick = "closeDialog 0;";

	idc = 1004;
	text = "Fermer";
	x = 9 * GUI_GRID_W + GUI_GRID_X;
	y = 16 * GUI_GRID_H + GUI_GRID_Y;
	w = 24 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
	colorText[] = {1,1,1,1};
	colorBackground[] = {0,0,0,0.8};
};
class trueJob: Life_RscButtonMenu
{
	idc = 27453;
	onButtonClick = "[true] spawn life_fnc_jobState;";

	text = "Prendre service";
	x = 9.5 * GUI_GRID_W + GUI_GRID_X;
	y = 13 * GUI_GRID_H + GUI_GRID_Y;
	w = 23 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
	colorText[] = {1,1,1,1};
	colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
};
class falseJob: Life_RscButtonMenu
{
	idc = 27454;
	onButtonClick = "[false] spawn life_fnc_jobState;";

	text = "Fin du service";
	x = 9.5 * GUI_GRID_W + GUI_GRID_X;
	y = 14.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 23 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
	colorText[] = {1,1,1,1};
	colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
};
};
};
