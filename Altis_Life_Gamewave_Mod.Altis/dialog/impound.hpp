#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)

class Life_impound_menu
{
	idd = 2800;
	name="life_vehicle_shop";
	movingEnabled = 0;
	enableSimulation = 1;
	onLoad = "ctrlShow [2330,false];";
	
	class controlsBackground
	{
	class Life_RscTitleBackground: Life_RscText
	{
		idc = -1;

	x = 0 * GUI_GRID_W + GUI_GRID_X;
	y = -1 * GUI_GRID_H + GUI_GRID_Y;
	w = 40 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])","(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
	};
	class MainBackground: Life_RscText
	{
		idc = -1;

	x = 0 * GUI_GRID_W + GUI_GRID_X;
	y = 0 * GUI_GRID_H + GUI_GRID_Y;
	w = 40 * GUI_GRID_W;
	h = 22 * GUI_GRID_H;
		colorBackground[] = {0,0,0,0.7};
	};
	class Title: Life_RscTitle
	{
		idc = 2801;

		text = "Garage"; //--- ToDo: Localize;
	x = 0 * GUI_GRID_W + GUI_GRID_X;
	y = -1 * GUI_GRID_H + GUI_GRID_Y;
	w = 40 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		colorText[] = {0.95,0.95,0.95,1};
	};
	class VehicleTitleBox: Life_RscText
	{
		idc = -1;

		text = "Votre Garage"; //--- ToDo: Localize;
	x = 0.04 * GUI_GRID_W + GUI_GRID_X;
	y = 0.12 * GUI_GRID_H + GUI_GRID_Y;
	w = 18.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])","(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
	};
	class VehicleInfoHeader: Life_RscText
	{
		idc = 2830;

		text = "Informations"; //--- ToDo: Localize;
	x = 20.94 * GUI_GRID_W + GUI_GRID_X;
	y = 0.14 * GUI_GRID_H + GUI_GRID_Y;
	w = 19 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])","(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
	};
	class CloseBtn: Life_RscButtonMenu
	{
		onButtonClick = "closeDialog 0;";

		idc = -1;
		text = "$STR_Global_Close"; //--- ToDo: Localize;
	x = 33.5 * GUI_GRID_W + GUI_GRID_X;
	y = 22 * GUI_GRID_H + GUI_GRID_Y;
	w = 6.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {0,0,0,0.8};
	};
	class RentCar: Life_RscButtonMenu
	{
		onButtonClick = "[] call life_fnc_unimpound;";

		idc = -1;
		text = "$STR_Global_Retrieve"; //--- ToDo: Localize;
	x = 0 * GUI_GRID_W + GUI_GRID_X;
	y = 22 * GUI_GRID_H + GUI_GRID_Y;
	w = 6.25 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = { 0, 0.47, 0.69, 1 }; 
	};
	class BuyCar: Life_RscButtonMenu
	{
		onButtonClick = "[] call life_fnc_sellGarage;";

		idc = 2805;
		text = "$STR_Global_Sell"; //--- ToDo: Localize;
	x = 33.5 * GUI_GRID_W + GUI_GRID_X;
	y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 6.25 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = { 0.68, 0, 0, 1 };
	};
	class AssurCar: Life_RscButtonMenu
	{
	onButtonClick = "[] call life_fnc_vehicleAssur;";

		idc = 2804;
		text = "Assurance"; //--- ToDo: Localize;
	x = 26.5 * GUI_GRID_W + GUI_GRID_X;
	y = 17.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 6.25 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = { 0.24, 0.41, 0.15, 1 };
};
	};
	class controls
	{
	class VehicleList: Life_RscListBox
	{
		idc = 2802;
		sizeEx = 0.04;
		onLBSelChanged = "_this call life_fnc_garageLBChange;";

	x = 0.39 * GUI_GRID_W + GUI_GRID_X;
	y = 1.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 18 * GUI_GRID_W;
	h = 19.5 * GUI_GRID_H;
		colorBackground[] = {0.1,0.1,0.1,0.9};
	};
	class vehicleInfomationList: Life_RscStructuredText
	{
		idc = 2803;
		sizeEx = 0.035;
	x = 21.04 * GUI_GRID_W + GUI_GRID_X;
	y = 1.72 * GUI_GRID_H + GUI_GRID_Y;
	w = 18.5 * GUI_GRID_W;
	h = 15.5 * GUI_GRID_H;
	};
	
	class MainBackgroundHider: Life_RscText
	{
		idc = 2810;

	x = 0 * GUI_GRID_W + GUI_GRID_X;
	y = 0 * GUI_GRID_H + GUI_GRID_Y;
	w = 40 * GUI_GRID_W;
	h = 23 * GUI_GRID_H;
		colorBackground[] = {0,0,0,1};
	};
	
	class MainHideText: Life_RscText
	{
		idc = 2811;
		text = "En attente de la base données..."; //--- ToDo: Localize;
	x = 3 * GUI_GRID_W + GUI_GRID_X;
	y = 7 * GUI_GRID_H + GUI_GRID_Y;
	w = 33.5 * GUI_GRID_W;
	h = 7 * GUI_GRID_H;
		sizeEx = 2 * GUI_GRID_H;
	};
	};
};