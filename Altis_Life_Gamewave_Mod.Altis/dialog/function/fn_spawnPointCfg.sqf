/*
	File: fn_spawnPointCfg.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration for available spawn points depending on the units side.
	
	Return:
	[Spawn Marker,Spawn Name,Image Path]
	Lucel was here mofo.
*/
private["_side","_ret","_spawnList","_mk","_mkName"];
_side = [_this,0,civilian,[civilian]] call BIS_fnc_param;
_spawnList = [];

//Spawn Marker, Spawn Name, PathToImage
switch (_side) do
{
	case west:
	{
	if(!cop_jail_spawn) then {
	life_cop_respawn_5 = false;
	}else{
	life_cop_respawn_5 = true;
	};
	if(life_cop_respawn_1) then {_spawnList pushBack ["cop_spawn_1","Kavala HQ","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
	if(life_cop_respawn_2) then {_spawnList pushBack ["cop_spawn_2","Athira HQ","\a3\ui_f\data\map\GroupIcons\badge_rotate_0_gs.paa"];};
	if(life_cop_respawn_3) then {_spawnList pushBack ["cop_spawn_3","Pyrgos HQ","\a3\ui_f\data\map\MapControl\fuelstation_ca.paa"];};
	if(life_cop_respawn_4) then {_spawnList pushBack ["cop_spawn_4","Air HQ","\a3\ui_f\data\map\Markers\NATO\b_air.paa"];};
	if(life_cop_respawn_5) then {_spawnList pushBack ["cop_spawn_5","Prison d'Altis","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
	};                                               
	
	case civilian:
	{
		if(license_civ_rebel && playerSide == civilian) then {
		if(life_reb_respawn_1) then {_spawnList pushBack ["reb_spawn_1","Camp Abdera","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_reb_respawn_2) then {_spawnList pushBack ["reb_spawn_2","Camp Selakano","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_reb_respawn_3) then {_spawnList pushBack ["reb_spawn_3","Camp Nidasos","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		};

		if(license_civ_merc && playerSide == civilian) then {
		if(life_merc_respawn_1) then {_spawnList pushBack ["merc_spawn_1","Camp Mercenaire Pyrgos","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_merc_respawn_2) then {_spawnList pushBack ["merc_spawn_2","Camp Mercenaire Kavala","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_merc_respawn_3) then {_spawnList pushBack ["merc_spawn_3","Camp Mercenaire Sofia","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		};
		
		if(!license_civ_rebel && playerSide == civilian) then {
		if(life_civ_respawn_1) then {_spawnList pushBack ["civ_spawn_1","Kavala","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_civ_respawn_2) then {_spawnList pushBack ["civ_spawn_2","Pyrgos","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_civ_respawn_3) then {_spawnList pushBack ["civ_spawn_3","Athira","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_civ_respawn_4) then {_spawnList pushBack ["civ_spawn_4","Sofia","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		if(life_civ_respawn_5) then {_spawnList pushBack ["civ_spawn_5","Neochori","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};	
		};		
		
		if((!license_civ_rebel) && (playerSide == civilian) && ({(group player getVariable "gang_id") == (1011)})) then {
		if(life_Be_respawn_1) then {_spawnList pushBack ["be_spawn","Black Eagle","\a3\ui_f\data\map\MapControl\watertower_ca.paa"];};
		};

		if(count life_houses > 0) then {
			{
				_pos = call compile format["%1",_x select 0];
				_house = nearestBuilding _pos;
				_houseName = getText(configFile >> "CfgVehicles" >> (typeOf _house) >> "displayName");
				
                _spawnList pushBack [format["house_%1",_house getVariable "uid"],_houseName,"\a3\ui_f\data\map\MapControl\lighthouse_ca.paa"];

			} foreach life_houses;
		};	
	};
	
};
_spawnList;
