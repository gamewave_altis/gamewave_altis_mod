class Life_agence_pubMenu
{
	idd = 2550;
	name = "life_agence_pubMenu";
	movingEnabled = false;
	enableSimulation = true;
	
	class controlsBackground {
		class Life_RscTitleBackground:Life_RscText {
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
			idc = -1;
			x = 0.3;
			y = 0.2;
			w = 0.47;
			h = (1 / 25);
		};
		
		class MainBackground:Life_RscText {
			colorBackground[] = {0, 0, 0, 0.7};
			idc = -1;
			x = 0.3;
			y = 0.2 + (11 / 250);
			w = 0.47;
			h = 0.3 - (22 / 250);
		};
	};
	
	class controls 
	{
		class InfoMsg : Life_RscStructuredText
		{
			idc = -1;
			sizeEx = 0.020;
			text = "$STR_PUBMENU_INTRO";
			x = 0.31;
			y = 0.2 + (11 / 250);
			w = 0.5; h = 0.12;
		};

		// class life_gang_browse : Life_RscButtonMenu {
			// idc = -1;
			// text = "$STR_PUBMENU_SUB";
			// colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			// onButtonClick = "closeDialog 0; createDialog ""Life_agence_pubSub""";
			// x = 0.18 + (6.25 / 40) + (1 / 250 / (safezoneW / safezoneH));
			// y = 0.4 - (1 / 25);
			// w = 0.40;
			// h = (1 / 25);
		// };
		
		class life_gang_create_button : Life_RscButtonMenu {
			idc = -1;
			text = "$STR_PUBMENU_CREATE";
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			onButtonClick = "closeDialog 0; createDialog ""Life_agence_pub""";
			x = 0.18 + (6.25 / 40) + (1 / 250 / (safezoneW / safezoneH));
			y = 0.35 - (1 / 25);
			w = 0.40;
			h = (1 / 25);
		};
	};
};