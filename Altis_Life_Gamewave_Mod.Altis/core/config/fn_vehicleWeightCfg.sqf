/*
	File: fn_vehicleWeightCfg.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration for vehicle weight.
*/
private["_className"];
_className = [_this,0,"",[""]] call BIS_fnc_param;

switch (_className) do
{

 
	case "Skyline_Mercedes_C63_01_F": {70};
	case "Skyline_Mercedes_C63_02_F": {70};

	case "Land_CargoBox_V1_F": {5000};
	
	case "Box_IND_Grenades_F": {375};
	case "B_supplyCrate_F": {750};
	//HELIDLC
		//REB
	case "O_Heli_Transport_04_bench_F": {50}; 
	case "O_Heli_Transport_04_covered_F": {210}; 
		//COP
	case "B_Heli_Transport_03_unarmed_F": {100}; 
		//CIV
	case "O_Heli_Transport_04_F": {40}; 
	case "C_Heli_Light_01_civil_F": {30}; 
	
	case "O_Heli_Transport_04_medevac_F": {210}; 
	case "O_Heli_Transport_04_repair_F": {210}; 
	
	
	case "C_Kart_01_Blu_F": {5}; // Kart
	case "C_Kart_01_Fuel_F": {5}; // Kart
	case "C_Kart_01_Red_F": {5}; // Kart
	case "C_Kart_01_Vrana_F": {5}; // Kart

	case "B_Quadbike_01_F": {30}; // Quad
	case "C_Hatchback_01_F": {70}; // Voiture Hayon
	case "C_Hatchback_01_sport_F": {70}; // Voiture Hayon Sport	
	case "C_Offroad_01_F": {95}; //Pickup
	case "C_SUV_01_F": {85}; // SUV 
	
	case "C_Van_01_transport_F": {200}; // Camionnette
	case "C_Van_01_box_F": {250}; // Camionnette couvert 
	case "C_Van_01_Fuel_F": {10}; // Camion Citerne
	
	case "I_Truck_02_transport_F": {285}; // Zamak 
	case "I_Truck_02_covered_F": {340}; // Zamak Couvert	
	case "I_Truck_02_box_F": {10}; // Camion Pegasus 
	
	case "B_Truck_01_transport_F": {575}; // HEMTT 
	case "B_Truck_01_box_F": {750}; // HEMTT Couvert
	
	case "O_Truck_03_device_F": {375}; //Tempest
	
	case "B_G_Offroad_01_F": {110}; //Pickup rebelle
	case "B_G_Offroad_01_armed_F": {90}; //Pickup Arm�
	case "O_MRAP_02_F": {60}; // Ifrit
	
	case "B_MRAP_01_F": {65}; // Hunter
	case "B_MRAP_01_hmg_F": {65}; // Hunter Arm�	
	case "I_MRAP_03_F": {60}; // Strider
	
	case "B_Heli_Light_01_F": {30}; //MH-9 Hummingbird
	case "B_Heli_Attack_01_F": {40}; //Blackfoot
	case "O_Heli_Light_02_unarmed_F": {70}; // Po-30 Orca
	case "I_Heli_Transport_02_F": {125}; // Mohawk
	case "I_Heli_light_03_unarmed_F": {40}; //Hellcat
    case "B_Heli_Transport_01_F": {40}; //Ghost Hawk

	
	case "C_Rubberboat": {90}; // Petit bateau
	case "I_G_Boat_Transport_01_F": {150}; //Canot d'Assaut
	case "C_Boat_Civil_01_F": {200}; // Bateau Civil

	case "B_Boat_Transport_01_F": {90}; //Petit Bateau
	case "C_Boat_Civil_01_police_F": {200}; // Bateau Police
	case "B_Boat_Armed_01_minigun_F": {150}; // Bateau arme
	case "B_SDV_01_F": {30}; // Sous-Marin

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule LOW
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_xlada_red": {70};
case "cl3_xlada_green": {70};
case "cl3_xlada_blue": {70};
case "cl3_xlada_classic": {70};

case "cl3_lada": {60};
case "cl3_lada_red": {60};
case "cl3_lada_white": {60};

case "cl3_volha": {70};
case "cl3_volha_black": {70};
case "cl3_volha_grey": {70};

case "cl3_440cuda": {70};
case "cl3_440cuda_black": {70};
case "cl3_440cuda_black1": {70};
case "cl3_440cuda_blu_ltn": {70};
case "cl3_440cuda_blue": {70};
case "cl3_440cuda_blue_flame": {70};
case "cl3_440cuda_emt_chief": {70};
case "cl3_440cuda_flannery08": {70};
case "cl3_440cuda_flannery12": {70};
case "cl3_440cuda_flannery14": {70};
case "cl3_440cuda_flannery27": {70};
case "cl3_440cuda_flannery51": {70};
case "cl3_440cuda_flannery55": {70};
case "cl3_440cuda_flannery69": {70};
case "cl3_440cuda_flannery70": {70};
case "cl3_440cuda_green": {70};
case "cl3_440cuda_green_Logo": {70};
case "cl3_440cuda_yellow": {70};
case "cl3_440cuda_yellow_stripe": {70};

case "cl3_bounder_beige": {130};

case "cl3_civic_vti": {65};
case "cl3_civic_vti_aqua": {65};
case "cl3_civic_vti_babyblue": {65};
case "cl3_civic_vti_babypink": {65};
case "cl3_civic_vti_black": {65};
case "cl3_civic_vti_blue": {65};
case "cl3_civic_vti_burgundy": {65};
case "cl3_civic_vti_camo": {65};
case "cl3_civic_vti_camo_urban": {65};
case "cl3_civic_vti_cardinal": {65};
case "cl3_civic_vti_dark_green": {65};
case "cl3_civic_vti_gold": {65};
case "cl3_civic_vti_green": {65};
case "cl3_civic_vti_grey": {65};
case "cl3_civic_vti_lavender": {65};
case "cl3_civic_vti_light_blue": {65};
case "cl3_civic_vti_light_yellow": {65};
case "cl3_civic_vti_lime": {65};
case "cl3_civic_vti_marina_blue": {65};
case "cl3_civic_vti_navy_blue": {65};
case "cl3_civic_vti_orange": {65};
case "cl3_civic_vti_purple": {65};
case "cl3_civic_vti_red": {65};
case "cl3_civic_vti_sand": {65};
case "cl3_civic_vti_silver": {65};
case "cl3_civic_vti_violet": {65};
case "cl3_civic_vti_white": {65};
case "cl3_civic_vti_yellow": {65};

case "cl3_crown_victoria": {75};
case "cl3_crown_victoria_aqua": {75};
case "cl3_crown_victoria_babyblue": {75};
case "cl3_crown_victoria_babypink": {75};
case "cl3_crown_victoria_black": {75};
case "cl3_crown_victoria_blue": {75};
case "cl3_crown_victoria_burgundy": {75};
case "cl3_crown_victoria_camo": {75};
case "cl3_crown_victoria_camo_urban": {75};
case "cl3_crown_victoria_cardinal": {75};
case "cl3_crown_victoria_dark_green": {75};
case "cl3_crown_victoria_gold": {75};
case "cl3_crown_victoria_green": {75};
case "cl3_crown_victoria_grey": {75};
case "cl3_crown_victoria_lavender": {75};
case "cl3_crown_victoria_light_blue": {75};
case "cl3_crown_victoria_light_yellow": {75};
case "cl3_crown_victoria_lime": {75};
case "cl3_crown_victoria_marina_blue": {75};
case "cl3_crown_victoria_navy_blue": {75};
case "cl3_crown_victoria_orange": {75};
case "cl3_crown_victoria_purple": {75};
case "cl3_crown_victoria_red": {75};
case "cl3_crown_victoria_sand": {75};
case "cl3_crown_victoria_silver": {75};
case "cl3_crown_victoria_violet": {75};
case "cl3_crown_victoria_white": {75};
case "cl3_crown_victoria_yellow": {75};

case "cl3_defender_110": {110};
case "cl3_defender_110_cammo": {110};
case "cl3_defender_110_red": {110};
case "cl3_defender_110_yellow": {110};

case "cl3_discovery": {110};
case "cl3_discovery_black": {110};
case "cl3_discovery_blue": {110};
case "cl3_discovery_darkorange": {110};
case "cl3_discovery_gold": {110};
case "cl3_discovery_green": {110};
case "cl3_discovery_hellokitty": {110};
case "cl3_discovery_joker": {110};
case "cl3_discovery_pink": {110};
case "cl3_discovery_silver": {110};

case "cl3_golf_mk2": {65};
case "cl3_golf_mk2_aqua": {65};
case "cl3_golf_mk2_babyblue": {65};
case "cl3_golf_mk2_babypink": {65};
case "cl3_golf_mk2_black": {65};
case "cl3_golf_mk2_blue": {65};
case "cl3_golf_mk2_burgundy": {65};
case "cl3_golf_mk2_camo": {65};
case "cl3_golf_mk2_camo_urban": {65};
case "cl3_golf_mk2_cardinal": {65};
case "cl3_golf_mk2_dark_green": {65};
case "cl3_golf_mk2_gold": {65};
case "cl3_golf_mk2_green": {65};
case "cl3_golf_mk2_grey": {65};
case "cl3_golf_mk2_lavender": {65};
case "cl3_golf_mk2_light_blue": {65};
case "cl3_golf_mk2_light_yellow": {65};
case "cl3_golf_mk2_lime": {65};
case "cl3_golf_mk2_marina_blue": {65};
case "cl3_golf_mk2_navy_blue": {65};
case "cl3_golf_mk2_orange": {65};
case "cl3_golf_mk2_purple": {65};
case "cl3_golf_mk2_red": {65};
case "cl3_golf_mk2_sand": {65};
case "cl3_golf_mk2_silver": {65};
case "cl3_golf_mk2_violet": {65};
case "cl3_golf_mk2_white": {65};
case "cl3_golf_mk2_yellow": {65};

case "cl3_polo_gti": {65};
case "cl3_polo_gti_aqua": {65};
case "cl3_polo_gti_babyblue": {65};
case "cl3_polo_gti_babypink": {65};
case "cl3_polo_gti_black": {65};
case "cl3_polo_gti_blue": {65};
case "cl3_polo_gti_burgundy": {65};
case "cl3_polo_gti_camo": {65};
case "cl3_polo_gti_camo_urban": {65};
case "cl3_polo_gti_cardinal": {65};
case "cl3_polo_gti_dark_green": {65};
case "cl3_polo_gti_gold": {65};
case "cl3_polo_gti_green": {65};
case "cl3_polo_gti_grey": {65};
case "cl3_polo_gti_lavender": {65};
case "cl3_polo_gti_light_blue": {65};
case "cl3_polo_gti_light_yellow": {65};
case "cl3_polo_gti_lime": {65};
case "cl3_polo_gti_marina_blue": {65};
case "cl3_polo_gti_navy_blue": {65};
case "cl3_polo_gti_orange": {65};
case "cl3_polo_gti_purple": {65};
case "cl3_polo_gti_red": {65};
case "cl3_polo_gti_sand": {65};
case "cl3_polo_gti_silver": {65};
case "cl3_polo_gti_violet": {65};
case "cl3_polo_gti_white": {65};
case "cl3_polo_gti_yellow": {65};


case "beetle_custom": {95};
case "beetle_bleufonce": {95};
case "beetle_bleupetrole": {95};
case "beetle_red": {95};
case "beetle_vert": {95};
case "beetle_violet": {95};
case "beetle_white": {95};
case "beetle_psycha": {95};
case "beetle_psycha1": {95};
case "beetle_coci": {95};
case "beetle_camo": {95};
case "beetle": {95};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule Medium
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "clpd_mondeo": {80};
case "civ_mondeo_Aqua": {80};
case "civ_mondeo_BabyBlue": {80};
case "civ_mondeo_BabyPink": {80};
case "civ_mondeo_Black": {80};
case "civ_mondeo_Burgundy": {80};
case "civ_mondeo_CardinalBurgundy": {80};
case "civ_mondeo_DarkGreen": {80};
case "civ_mondeo_Gold": {80};
case "civ_mondeo_Green": {80};
case "civ_mondeo_Grey": {80};
case "civ_mondeo_Lavendel": {80};
case "civ_mondeo_LightBlue": {80};
case "civ_mondeo_LightYellow": {80};
case "civ_mondeo_Lime": {80};
case "civ_mondeo_MarineBlue": {80};
case "civ_mondeo_NavyBlue": {80};
case "civ_mondeo_Orange": {80};
case "civ_mondeo_Pink": {80};
case "civ_mondeo_Purple": {80};
case "civ_mondeo_Red": {80};
case "civ_mondeo_Silver": {80};
case "civ_mondeo_SkyBlue": {80};
case "civ_mondeo_Violet": {80};
case "civ_mondeo_White": {80};
case "civ_mondeo_Yellow": {80};

case "cl3_challenger_2009": {70};
case "cl3_challenger_2009_black": {70};
case "cl3_challenger_2009_blue": {70};
case "cl3_challenger_2009_green": {70};
case "cl3_challenger_2009_red": {70};
case "cl3_challenger_2009_silver": {70};
case "cl3_challenger_2009_yellow": {70};
case "cl3_challenger_aquablue": {70};
case "cl3_challenger_beige": {70};
case "cl3_challenger_beige2": {70};
case "cl3_challenger_blue": {70};
case "cl3_challenger_brown": {70};
case "cl3_challenger_darksilver": {70};
case "cl3_challenger_green": {70};
case "cl3_challenger_lime": {70};
case "cl3_challenger_orange": {70};
case "cl3_challenger_orangeflame": {70};
case "cl3_challenger_pink": {70};
case "cl3_challenger_purple": {70};
case "cl3_challenger_red": {70};
case "cl3_challenger_yellow": {70};

case "cl3_dodge_charger_patrol": {70};
case "cl3_dodge_charger_cn": {70};
case "cl3_dodge_charger_emt": {70};
case "cl3_dodge_charger_emtcg": {70};
case "cl3_dodge_charger_emtfd": {70};
case "cl3_dodge_charger_emtmr": {70};
case "cl3_dodge_charger_emtpa": {70};
case "cl3_dodge_charger_etu": {70};
case "cl3_dodge_charger_k9": {70};
case "cl3_dodge_charger_patrol": {70};
case "cl3_dodge_charger_patrol2": {70};
case "cl3_dodge_charger_s_black ": {70};
case "cl3_dodge_charger_s_blue": {70};
case "cl3_dodge_charger_s_camo": {70};
case "cl3_dodge_charger_s_camourban": {70};
case "cl3_dodge_charger_s_darkgreen": {70};
case "cl3_dodge_charger_s_darkred": {70};
case "cl3_dodge_charger_s_green": {70};
case "cl3_dodge_charger_s_grey": {70};
case "cl3_dodge_charger_s_lime": {70};
case "cl3_dodge_charger_s_orange": {70};
case "cl3_dodge_charger_s_pink": {70};
case "cl3_dodge_charger_s_purple": {70};
case "cl3_dodge_charger_s_red": {70};
case "cl3_dodge_charger_s_white": {70};
case "cl3_dodge_charger_s_yellow": {70};

case "cl3_e60_m5": {70};
case "cl3_e60_m5_aqua": {70};
case "cl3_e60_m5_babyblue": {70};
case "cl3_e60_m5_babypink": {70};
case "cl3_e60_m5_black": {70};
case "cl3_e60_m5_blue": {70};
case "cl3_e60_m5_burgundy": {70};
case "cl3_e60_m5_camo": {70};
case "cl3_e60_m5_camo_urban": {70};
case "cl3_e60_m5_cardinal": {70};
case "cl3_e60_m5_dark_green": {70};
case "cl3_e60_m5_gold": {70};
case "cl3_e60_m5_green": {70};
case "cl3_e60_m5_grey": {70};
case "cl3_e60_m5_lavender": {70};
case "cl3_e60_m5_light_blue": {70};
case "cl3_e60_m5_light_yellow": {70};
case "cl3_e60_m5_lime": {70};
case "cl3_e60_m5_marina_blue": {70};
case "cl3_e60_m5_navy_blue": {70};
case "cl3_e60_m5_orange": {70};
case "cl3_e60_m5_purple": {70};
case "cl3_e60_m5_red": {70};
case "cl3_e60_m5_sand": {70};
case "cl3_e60_m5_silver": {70};
case "cl3_e60_m5_violet": {70};
case "cl3_e60_m5_white": {70};
case "cl3_e60_m5_yellow": {70};

case "cl3_e63_amg": {80};
case "cl3_e63_amg_aqua": {80};
case "cl3_e63_amg_babyblue": {80};
case "cl3_e63_amg_babypink": {80};
case "cl3_e63_amg_black": {80};
case "cl3_e63_amg_blue": {80};
case "cl3_e63_amg_burgundy ": {80};
case "cl3_e63_amg_camo": {80};
case "cl3_e63_amg_camo_urban": {80};
case "cl3_e63_amg_cardinal": {80};
case "cl3_e63_amg_dark_green": {80};
case "cl3_e63_amg_gold": {80};
case "cl3_e63_amg_green": {80};
case "cl3_e63_amg_grey": {80};
case "cl3_e63_amg_lavender": {80};
case "cl3_e63_amg_light_blue": {80};
case "cl3_e63_amg_light_yellow": {80};
case "cl3_e63_amg_lime": {80};
case "cl3_e63_amg_marina_blue": {80};
case "cl3_e63_amg_navy_blue": {80};
case "cl3_e63_amg_orange": {80};
case "cl3_e63_amg_purple": {80};
case "cl3_e63_amg_red": {80};
case "cl3_e63_amg_sand": {80};
case "cl3_e63_amg_silver": {80};
case "cl3_e63_amg_violet": {80};
case "cl3_e63_amg_white": {80};
case "cl3_e63_amg_yellow": {80};

case "cl3_escalade": {110};
case "cl3_escalade_aqua": {110};
case "cl3_escalade_babyblue": {110};
case "cl3_escalade_babypink": {110};
case "cl3_escalade_black": {110};
case "cl3_escalade_blue": {110};
case "cl3_escalade_burgundy": {110};
case "cl3_escalade_camo": {110};
case "cl3_escalade_camo_urban": {110};
case "cl3_escalade_cardinal": {110};
case "cl3_escalade_cg": {110};
case "cl3_escalade_cn": {110};
case "cl3_escalade_dark_green": {110};
case "cl3_escalade_etu": {110};
case "cl3_escalade_fd": {110};
case "cl3_escalade_gold": {110};
case "cl3_escalade_green": {110};
case "cl3_escalade_grey": {110};
case "cl3_escalade_k9": {110};
case "cl3_escalade_lavender": {110};
case "cl3_escalade_light_blue": {110};
case "cl3_escalade_light_yellow": {110};
case "cl3_escalade_lime": {110};
case "cl3_escalade_marina_blue": {110};
case "cl3_escalade_mr": {110};
case "cl3_escalade_navy_blue": {110};
case "cl3_escalade_orange": {110};
case "cl3_escalade_patrolbw": {110};
case "cl3_escalade_patrolwb": {110};
case "cl3_escalade_pd": {110};
case "cl3_escalade_pm": {110};
case "cl3_escalade_purple": {110};
case "cl3_escalade_red": {110};
case "cl3_escalade_sand": {110};
case "cl3_escalade_silver": {110};
case "cl3_escalade_traffic": {110};
case "cl3_escalade_violet": {110};
case "cl3_escalade_white": {110};
case "cl3_escalade_yellow": {110};
case "cl3_escalade_dep": {110};

case "cl3_q7": {90};
case "cl3_q7_aqua": {90};
case "cl3_q7_babyblue": {90};
case "cl3_q7_babypink": {90};
case "cl3_q7_black": {90};
case "cl3_q7_blue": {90};
case "cl3_q7_burgundy": {90};
case "cl3_q7_camo": {90};
case "cl3_q7_camo_urban": {90};
case "cl3_q7_cardinal": {90};
case "cl3_q7_clpd_cn": {90};
case "cl3_q7_clpd_etu": {90};
case "cl3_q7_clpd_patrol": {90};
case "cl3_q7_dark_green": {90};
case "cl3_q7_gold": {90};
case "cl3_q7_green": {90};
case "cl3_q7_grey": {90};
case "cl3_q7_lavender": {90};
case "cl3_q7_light_blue": {90};
case "cl3_q7_light_yellow": {90};
case "cl3_q7_lime": {90};
case "cl3_q7_marina_blue": {90};
case "cl3_q7_navy_blue": {90};
case "cl3_q7_orange": {90};
case "cl3_q7_purple": {90};
case "cl3_q7_red": {90};
case "cl3_q7_sand": {90};
case "cl3_q7_silver": {90};
case "cl3_q7_violet": {90};
case "cl3_q7_white": {90};
case "cl3_q7_yellow": {90};

case "cl3_s5": {80};
case "cl3_s5_aqua": {80};
case "cl3_s5_babyblue": {80};
case "cl3_s5_babypink": {80};
case "cl3_s5_black": {80};
case "cl3_s5_blue": {80};
case "cl3_s5_burgundy": {80};
case "cl3_s5_camo": {80};
case "cl3_s5_camo_urban": {80};
case "cl3_s5_cardinal": {80};
case "cl3_s5_dark_green": {80};
case "cl3_s5_gold": {80};
case "cl3_s5_green": {80};
case "cl3_s5_grey": {80};
case "cl3_s5_lavender": {80};
case "cl3_s5_light_blue": {80};
case "cl3_s5_light_yellow": {80};
case "cl3_s5_lime": {80};
case "cl3_s5_marina_blue": {80};
case "cl3_s5_navy_blue": {80};
case "cl3_s5_orange": {80};
case "cl3_s5_purple": {80};
case "cl3_s5_red": {80};
case "cl3_s5_sand": {80};
case "cl3_s5_silver": {80};
case "cl3_s5_violet": {80};
case "cl3_s5_white": {80};
case "cl3_s5_yellow": {80};

case "cl3_suv": {110};
case "cl3_suv_black": {110};
case "cl3_suv_emt": {110};
case "cl3_suv_taxi": {110};

case "cl3_taurus": {80};
case "cl3_taurus_aqua": {80};
case "cl3_taurus_babyblue": {80};
case "cl3_taurus_babypink": {80};
case "cl3_taurus_black": {80};
case "cl3_taurus_blue": {80};
case "cl3_taurus_burgundy": {80};
case "cl3_taurus_camo": {80};
case "cl3_taurus_camo_urban": {80};
case "cl3_taurus_cardinal": {80};
case "cl3_taurus_dark_green": {80};
case "cl3_taurus_gold": {80};
case "cl3_taurus_green": {80};
case "cl3_taurus_grey": {80};
case "cl3_taurus_lavender": {80};
case "cl3_taurus_light_blue": {80};
case "cl3_taurus_light_yellow": {80};
case "cl3_taurus_lime": {80};
case "cl3_taurus_marina_blue": {80};
case "cl3_taurus_navy_blue": {80};
case "cl3_taurus_orange": {80};
case "cl3_taurus_purple": {80};
case "cl3_taurus_red": {80};
case "cl3_taurus_sand": {80};
case "cl3_taurus_silver": {80};
case "cl3_taurus_violet": {80};
case "cl3_taurus_white": {80};
case "cl3_taurus_yellow": {80};

case "cl3_transit": {170};
case "cl3_transit_aqua": {170};
case "cl3_transit_babyblue": {170};
case "cl3_transit_babypink": {170};
case "cl3_transit_black": {170};
case "cl3_transit_blue": {170};
case "cl3_transit_burgundy": {170};
case "cl3_transit_camo": {170};
case "cl3_transit_camo_urban": {170};
case "cl3_transit_cardinal": {170};
case "cl3_transit_civ": {170};
case "cl3_transit_cop": {170};
case "cl3_transit_dark_green": {170};
case "cl3_transit_gold": {170};
case "cl3_transit_green": {170};
case "cl3_transit_grey": {170};
case "cl3_transit_lavender": {170};
case "cl3_transit_light_blue": {170};
case "cl3_transit_light_yellow": {170};
case "cl3_transit_lime": {170};
case "cl3_transit_marina_blue": {170};
case "cl3_transit_navy_blue": {170};
case "cl3_transit_orange": {170};
case "cl3_transit_purple": {170};
case "cl3_transit_red": {170};
case "cl3_transit_sand": {170};
case "cl3_transit_silver": {170};
case "cl3_transit_violet": {170};
case "cl3_transit_white": {170};
case "cl3_transit_yellow": {170};
case "cl3_transitemt": {170};
case "cl3_transitk9": {170};
case "cl3_transitNews": {170};
case "cl3_transitpatrol": {170};

case "cl3_z4_2008": {50};
case "cl3_z4_2008_aqua": {50};
case "cl3_z4_2008_babyblue": {50};
case "cl3_z4_2008_babypink": {50};
case "cl3_z4_2008_black": {50};
case "cl3_z4_2008_blue": {50};
case "cl3_z4_2008_burgundy": {50};
case "cl3_z4_2008_camo": {50};
case "cl3_z4_2008_camo_urban": {50};
case "cl3_z4_2008_cardinal": {50};
case "cl3_z4_2008_dark_green": {50};
case "cl3_z4_2008_gold": {50};
case "cl3_z4_2008_green": {50};
case "cl3_z4_2008_grey": {50};
case "cl3_z4_2008_lavender": {50};
case "cl3_z4_2008_light_blue": {50};
case "cl3_z4_2008_light_yellow": {50};
case "cl3_z4_2008_lime": {50};
case "cl3_z4_2008_marina_blue": {50};
case "cl3_z4_2008_navy_blue": {50};
case "cl3_z4_2008_orange": {50};
case "cl3_z4_2008_purple": {50};
case "cl3_z4_2008_red": {50};
case "cl3_z4_2008_sand": {50};
case "cl3_z4_2008_silver": {50};
case "cl3_z4_2008_violet": {50};
case "cl3_z4_2008_white": {50};
case "cl3_z4_2008_yellow": {50};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule HIGH
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_458": {30};
case "cl3_458_2tone1": {30};
case "cl3_458_2tone2": {30};
case "cl3_458_2tone3": {30};
case "cl3_458_2tone4": {30};
case "cl3_458_2tone5": {30};
case "cl3_458_aqua": {30};
case "cl3_458_babyblue": {30};
case "cl3_458_babypink": {30};
case "cl3_458_black": {30};
case "cl3_458_blue": {30};
case "cl3_458_burgundy": {30};
case "cl3_458_camo": {30};
case "cl3_458_camo_urban": {30};
case "cl3_458_cardinal": {30};
case "cl3_458_dark_green": {30};
case "cl3_458_flame": {30};
case "cl3_458_flame1": {30};
case "cl3_458_flame2": {30};
case "cl3_458_gold": {30};
case "cl3_458_green": {30};
case "cl3_458_grey": {30};
case "cl3_458_lavender": {30};
case "cl3_458_light_blue": {30};
case "cl3_458_light_yellow": {30};
case "cl3_458_lime": {30};
case "cl3_458_marina_blue": {30};
case "cl3_458_navy_blue": {30};
case "cl3_458_orange": {30};
case "cl3_458_purple": {30};
case "cl3_458_red": {30};
case "cl3_458_sand": {30};
case "cl3_458_silver": {30};
case "cl3_458_violet": {30};
case "cl3_458_white": {30};
case "cl3_458_yellow": {30};

case "cl3_aventador_lp7004": {30};
case "cl3_aventador_lp7004_2tone1": {30};
case "cl3_aventador_lp7004_2tone2": {30};
case "cl3_aventador_lp7004_2tone3": {30};
case "cl3_aventador_lp7004_2tone4": {30};
case "cl3_aventador_lp7004_2tone5": {30};
case "cl3_aventador_lp7004_aqua": {30};
case "cl3_aventador_lp7004_babyblue": {30};
case "cl3_aventador_lp7004_babypink": {30};
case "cl3_aventador_lp7004_black": {30};
case "cl3_aventador_lp7004_blue": {30};
case "cl3_aventador_lp7004_burgundy": {30};
case "cl3_aventador_lp7004_camo": {30};
case "cl3_aventador_lp7004_camo_urban": {30};
case "cl3_aventador_lp7004_cardinal": {30};
case "cl3_aventador_lp7004_dark_green": {30};
case "cl3_aventador_lp7004_flame": {30};
case "cl3_aventador_lp7004_flame1": {30};
case "cl3_aventador_lp7004_flame2": {30};
case "cl3_aventador_lp7004_gold": {30};
case "cl3_aventador_lp7004_green": {30};
case "cl3_aventador_lp7004_grey": {30};
case "cl3_aventador_lp7004_lavender": {30};
case "cl3_aventador_lp7004_light_blue": {30};
case "cl3_aventador_lp7004_light_yellow": {30};
case "cl3_aventador_lp7004_lime": {30};
case "cl3_aventador_lp7004_marina_blue": {30};
case "cl3_aventador_lp7004_navy_blue": {30};
case "cl3_aventador_lp7004_orange": {30};
case "cl3_aventador_lp7004_purple": {30};
case "cl3_aventador_lp7004_red": {30};
case "cl3_aventador_lp7004_sand": {30};
case "cl3_aventador_lp7004_silver": {30};
case "cl3_aventador_lp7004_violet": {30};
case "cl3_aventador_lp7004_white": {30};
case "cl3_aventador_lp7004_yellow": {30};

case "cl3_carrera_gt": {30};
case "cl3_carrera_gt_aqua": {30};
case "cl3_carrera_gt_babyblue": {30};
case "cl3_carrera_gt_babypink": {30};
case "cl3_carrera_gt_black": {30};
case "cl3_carrera_gt_blue": {30};
case "cl3_carrera_gt_burgundy": {30};
case "cl3_carrera_gt_camo": {30};
case "cl3_carrera_gt_camo_urban": {30};
case "cl3_carrera_gt_cardinal": {30};
case "cl3_carrera_gt_dark_green": {30};
case "cl3_carrera_gt_gold": {30};
case "cl3_carrera_gt_green": {30};
case "cl3_carrera_gt_grey": {30};
case "cl3_carrera_gt_lavender": {30};
case "cl3_carrera_gt_light_blue": {30};
case "cl3_carrera_gt_light_yellow": {30};
case "cl3_carrera_gt_lime": {30};
case "cl3_carrera_gt_marina_blue": {30};
case "cl3_carrera_gt_navy_blue": {30};
case "cl3_carrera_gt_orange": {30};
case "cl3_carrera_gt_purple": {30};
case "cl3_carrera_gt_red": {30};
case "cl3_carrera_gt_sand": {30};
case "cl3_carrera_gt_silver": {30};
case "cl3_carrera_gt_violet": {30};
case "cl3_carrera_gt_white": {30};
case "cl3_carrera_gt_yellow": {30};

case "cl3_dbs_volante": {40};
case "cl3_dbs_volante_aqua": {40};
case "cl3_dbs_volante_babyblue": {40};
case "cl3_dbs_volante_babypink": {40};
case "cl3_dbs_volante_black": {40};
case "cl3_dbs_volante_blue": {40};
case "cl3_dbs_volante_burgundy": {40};
case "cl3_dbs_volante_camo": {40};
case "cl3_dbs_volante_camo_urban": {40};
case "cl3_dbs_volante_cardinal": {40};
case "cl3_dbs_volante_dark_green": {40};
case "cl3_dbs_volante_flame": {40};
case "cl3_dbs_volante_flame1": {40};
case "cl3_dbs_volante_flame2": {40};
case "cl3_dbs_volante_gold": {40};
case "cl3_dbs_volante_green": {40};
case "cl3_dbs_volante_grey": {40};
case "cl3_dbs_volante_lavender": {40};
case "cl3_dbs_volante_light_blue": {40};
case "cl3_dbs_volante_light_yellow": {40};
case "cl3_dbs_volante_lime": {40};
case "cl3_dbs_volante_marina_blue": {40};
case "cl3_dbs_volante_navy_blue": {40};
case "cl3_dbs_volante_orange": {40};
case "cl3_dbs_volante_purple": {40};
case "cl3_dbs_volante_red": {40};
case "cl3_dbs_volante_sand": {40};
case "cl3_dbs_volante_silver": {40};
case "cl3_dbs_volante_violet": {40};
case "cl3_dbs_volante_white": {40};
case "cl3_dbs_volante_yellow": {40};

case "cl3_impreza_rally": {60};
case "cl3_impreza_rally_aqua": {60};
case "cl3_impreza_rally_babyblue": {60};
case "cl3_impreza_rally_babypink": {60};
case "cl3_impreza_rally_black": {60};
case "cl3_impreza_rally_blue": {60};
case "cl3_impreza_rally_burgundy": {60};
case "cl3_impreza_rally_camo": {60};
case "cl3_impreza_rally_camo_urban": {60};
case "cl3_impreza_rally_cardinal": {60};
case "cl3_impreza_rally_dark_green": {60};
case "cl3_impreza_rally_flame": {60};
case "cl3_impreza_rally_flame1": {60};
case "cl3_impreza_rally_flame2": {60};
case "cl3_impreza_rally_gold": {60};
case "cl3_impreza_rally_green": {60};
case "cl3_impreza_rally_grey": {60};
case "cl3_impreza_rally_lavender": {60};
case "cl3_impreza_rally_light_blue": {60};
case "cl3_impreza_rally_light_yellow": {60};
case "cl3_impreza_rally_lime": {60};
case "cl3_impreza_rally_marina_blue": {60};
case "cl3_impreza_rally_navy_blue": {60};
case "cl3_impreza_rally_orange": {60};
case "cl3_impreza_rally_purple": {60};
case "cl3_impreza_rally_red": {60};
case "cl3_impreza_rally_sand": {60};
case "cl3_impreza_rally_silver": {60};
case "cl3_impreza_rally_violet": {60};
case "cl3_impreza_rally_white": {60};
case "cl3_impreza_rally_yellow": {60};

case "cl3_impreza_road": {60};
case "cl3_impreza_road_aqua": {60};
case "cl3_impreza_road_babyblue": {60};
case "cl3_impreza_road_babypink": {60};
case "cl3_impreza_road_black": {60};
case "cl3_impreza_road_blue": {60};
case "cl3_impreza_road_burgundy": {60};
case "cl3_impreza_road_camo": {60};
case "cl3_impreza_road_camo_urban": {60};
case "cl3_impreza_road_cardinal": {60};
case "cl3_impreza_road_dark_green": {60};
case "cl3_impreza_road_flame": {60};
case "cl3_impreza_road_flame1": {60};
case "cl3_impreza_road_flame2": {60};
case "cl3_impreza_road_gold": {60};
case "cl3_impreza_road_green": {60};
case "cl3_impreza_road_grey": {60};
case "cl3_impreza_road_lavender": {60};
case "cl3_impreza_road_light_blue": {60};
case "cl3_impreza_road_light_yellow": {60};
case "cl3_impreza_road_lime": {60};
case "cl3_impreza_road_livery1": {60};
case "cl3_impreza_road_livery2": {60};
case "cl3_impreza_road_livery3": {60};
case "cl3_impreza_road_livery4": {60};
case "cl3_impreza_road_livery5": {60};
case "cl3_impreza_road_marina_blue": {60};
case "cl3_impreza_road_navy_blue": {60};
case "cl3_impreza_road_orange": {60};
case "cl3_impreza_road_purple": {60};
case "cl3_impreza_road_red": {60};
case "cl3_impreza_road_sand": {60};
case "cl3_impreza_road_silver": {60};
case "cl3_impreza_road_violet": {60};
case "cl3_impreza_road_white": {60};
case "cl3_impreza_road_yellow": {60};

case "cl3_insignia": {80};
case "cl3_insignia_aqua": {80};
case "cl3_insignia_babyblue": {80};
case "cl3_insignia_babypink": {80};
case "cl3_insignia_black": {80};
case "cl3_insignia_blue": {80};
case "cl3_insignia_burgundy": {80};
case "cl3_insignia_camo": {80};
case "cl3_insignia_camo_urban": {80};
case "cl3_insignia_cardinal": {80};
case "cl3_insignia_dark_green": {80};
case "cl3_insignia_gold": {80};
case "cl3_insignia_green": {80};
case "cl3_insignia_grey": {80};
case "cl3_insignia_lavender": {80};
case "cl3_insignia_light_blue": {80};
case "cl3_insignia_light_yellow": {80};
case "cl3_insignia_lime": {80};
case "cl3_insignia_marina_blue": {80};
case "cl3_insignia_navy_blue": {80};
case "cl3_insignia_orange": {80};
case "cl3_insignia_purple": {80};
case "cl3_insignia_red": {80};
case "cl3_insignia_sand": {80};
case "cl3_insignia_silver": {80};
case "cl3_insignia_violet": {80};
case "cl3_insignia_white": {80};
case "cl3_insignia_yellow": {80};

case "cl3_lamborghini_gt1": {30};
case "cl3_lamborghini_gt1_2tone1": {30};
case "cl3_lamborghini_gt1_2tone2": {30};
case "cl3_lamborghini_gt1_2tone3": {30};
case "cl3_lamborghini_gt1_2tone4": {30};
case "cl3_lamborghini_gt1_2tone5": {30};
case "cl3_lamborghini_gt1_aqua": {30};
case "cl3_lamborghini_gt1_babyblue": {30};
case "cl3_lamborghini_gt1_babypink": {30};
case "cl3_lamborghini_gt1_black": {30};
case "cl3_lamborghini_gt1_blue": {30};
case "cl3_lamborghini_gt1_burgundy": {30};
case "cl3_lamborghini_gt1_camo": {30};
case "cl3_lamborghini_gt1_camo_urban": {30};
case "cl3_lamborghini_gt1_cardinal": {30};
case "cl3_lamborghini_gt1_dark_green": {30};
case "cl3_lamborghini_gt1_flame": {30};
case "cl3_lamborghini_gt1_flame1": {30};
case "cl3_lamborghini_gt1_flame2": {30};
case "cl3_lamborghini_gt1_gold": {30};
case "cl3_lamborghini_gt1_green": {30};
case "cl3_lamborghini_gt1_grey": {30};
case "cl3_lamborghini_gt1_lavender": {30};
case "cl3_lamborghini_gt1_light_blue": {30};
case "cl3_lamborghini_gt1_light_yellow": {30};
case "cl3_lamborghini_gt1_lime": {30};
case "cl3_lamborghini_gt1_marina_blue": {30};
case "cl3_lamborghini_gt1_navy_blue": {30};
case "cl3_lamborghini_gt1_orange": {30};
case "cl3_lamborghini_gt1_purple": {30};
case "cl3_lamborghini_gt1_red": {30};
case "cl3_lamborghini_gt1_sand": {30};
case "cl3_lamborghini_gt1_silver": {30};
case "cl3_lamborghini_gt1_violet": {30};
case "cl3_lamborghini_gt1_white": {30};
case "cl3_lamborghini_gt1_yellow": {30};

case "cl3_murcielago": {30};
case "cl3_murcielago_2tone1": {30};
case "cl3_murcielago_2tone2": {30};
case "cl3_murcielago_2tone3": {30};
case "cl3_murcielago_2tone4": {30};
case "cl3_murcielago_2tone5": {30};
case "cl3_murcielago_aqua": {30};
case "cl3_murcielago_babyblue": {30};
case "cl3_murcielago_babypink": {30};
case "cl3_murcielago_black": {30};
case "cl3_murcielago_blue": {30};
case "cl3_murcielago_burgundy": {30};
case "cl3_murcielago_camo": {30};
case "cl3_murcielago_camo_urban": {30};
case "cl3_murcielago_cardinal": {30};
case "cl3_murcielago_dark_green": {30};
case "cl3_murcielago_flame": {30};
case "cl3_murcielago_flame1": {30};
case "cl3_murcielago_flame2": {30};
case "cl3_murcielago_gold": {30};
case "cl3_murcielago_green": {30};
case "cl3_murcielago_grey": {30};
case "cl3_murcielago_lavender": {30};
case "cl3_murcielago_light_blue": {30};
case "cl3_murcielago_light_yellow": {30};
case "cl3_murcielago_lime": {30};
case "cl3_murcielago_marina_blue": {30};
case "cl3_murcielago_navy_blue": {30};
case "cl3_murcielago_orange": {30};
case "cl3_murcielago_purple": {30};
case "cl3_murcielago_red": {30};
case "cl3_murcielago_sand": {30};
case "cl3_murcielago_silver": {30};
case "cl3_murcielago_violet": {30};
case "cl3_murcielago_white": {30};
case "cl3_murcielago_yellow": {30};

case "cl3_r8_spyder": {30};
case "cl3_r8_spyder_2tone1": {30};
case "cl3_r8_spyder_2tone2": {30};
case "cl3_r8_spyder_2tone3": {30};
case "cl3_r8_spyder_2tone4": {30};
case "cl3_r8_spyder_2tone5": {30};
case "cl3_r8_spyder_aqua": {30};
case "cl3_r8_spyder_babyblue": {30};
case "cl3_r8_spyder_babypink": {30};
case "cl3_r8_spyder_black": {30};
case "cl3_r8_spyder_blue": {30};
case "cl3_r8_spyder_burgundy": {30};
case "cl3_r8_spyder_camo": {30};
case "cl3_r8_spyder_camo_urban": {30};
case "cl3_r8_spyder_cardinal": {30};
case "cl3_r8_spyder_dark_green": {30};
case "cl3_r8_spyder_flame": {30};
case "cl3_r8_spyder_flame1": {30};
case "cl3_r8_spyder_flame2": {30};
case "cl3_r8_spyder_gold": {30};
case "cl3_r8_spyder_green": {30};
case "cl3_r8_spyder_grey": {30};
case "cl3_r8_spyder_lavender": {30};
case "cl3_r8_spyder_light_blue": {30};
case "cl3_r8_spyder_light_yellow": {30};
case "cl3_r8_spyder_lime": {30};
case "cl3_r8_spyder_marina_blue": {30};
case "cl3_r8_spyder_navy_blue": {30};
case "cl3_r8_spyder_orange": {30};
case "cl3_r8_spyder_purple": {30};
case "cl3_r8_spyder_red": {30};
case "cl3_r8_spyder_sand": {30};
case "cl3_r8_spyder_silver": {30};
case "cl3_r8_spyder_violet": {30};
case "cl3_r8_spyder_white": {30};
case "cl3_r8_spyder_yellow": {30};

case "cl3_reventon": {30};
case "cl3_reventon_2tone1": {30};
case "cl3_reventon_2tone2": {30};
case "cl3_reventon_2tone3": {30};
case "cl3_reventon_2tone4": {30};
case "cl3_reventon_2tone5": {30};
case "cl3_reventon_aqua": {30};
case "cl3_reventon_babyblue": {30};
case "cl3_reventon_babypink": {30};
case "cl3_reventon_black": {30};
case "cl3_reventon_blue": {30};
case "cl3_reventon_burgundy": {30};
case "cl3_reventon_camo": {30};
case "cl3_reventon_camo_urban": {30};
case "cl3_reventon_cardinal": {30};
case "cl3_reventon_clpd": {30};
case "cl3_reventon_clpd_traf": {30};
case "cl3_reventon_dark_green": {30};
case "cl3_reventon_flame": {30};
case "cl3_reventon_flame1": {30};
case "cl3_reventon_flame2": {30};
case "cl3_reventon_gold": {30};
case "cl3_reventon_green": {30};
case "cl3_reventon_grey": {30};
case "cl3_reventon_lavender": {30};
case "cl3_reventon_light_blue": {30};
case "cl3_reventon_light_yellow": {30};
case "cl3_reventon_lime": {30};
case "cl3_reventon_marina_blue": {30};
case "cl3_reventon_navy_blue": {30};
case "cl3_reventon_orange": {30};
case "cl3_reventon_purple": {30};
case "cl3_reventon_red": {30};
case "cl3_reventon_sand": {30};
case "cl3_reventon_silver": {30};
case "cl3_reventon_violet": {30};
case "cl3_reventon_white": {30};
case "cl3_reventon_yellow": {30};

case "cl3_veyron": {30};
case "cl3_veyron_black": {30};
case "cl3_veyron_blk_wht": {30};
case "cl3_veyron_brn_blk": {30};
case "cl3_veyron_lblue_dblue": {30};
case "cl3_veyron_lblue_lblue": {30};
case "cl3_veyron_red_red": {30};
case "cl3_veyron_wht_blu": {30};
case "cl3_veyron_wht_lwht": {30};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Motos
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_enduro": {10};
case "cl3_enduro_aqua": {10};
case "cl3_enduro_babyblue": {10};
case "cl3_enduro_babypink": {10};
case "cl3_enduro_black": {10};
case "cl3_enduro_blue": {10};
case "cl3_enduro_burgundy": {10};
case "cl3_enduro_camo": {10};
case "cl3_enduro_camo_urban": {10};
case "cl3_enduro_cardinal": {10};
case "cl3_enduro_dark_green": {10};
case "cl3_enduro_emt": {10};
case "cl3_enduro_flame": {10};
case "cl3_enduro_flame1": {10};
case "cl3_enduro_flame2": {10};
case "cl3_enduro_gold": {10};
case "cl3_enduro_green": {10};
case "cl3_enduro_grey": {10};
case "cl3_enduro_lavender": {10};
case "cl3_enduro_light_blue": {10};
case "cl3_enduro_light_yellow": {10};
case "cl3_enduro_lime": {10};
case "cl3_enduro_marina_blue": {10};
case "cl3_enduro_navy_blue": {10};
case "cl3_enduro_orange": {10};
case "cl3_enduro_police": {10};
case "cl3_enduro_purple": {10};
case "cl3_enduro_red": {10};
case "cl3_enduro_sand": {10};
case "cl3_enduro_silver": {10};
case "cl3_enduro_violet": {10};
case "cl3_enduro_white": {10};
case "cl3_enduro_yellow": {10};

case "cl3_xr_1000": {20};
case "cl3_xr_1000_aqua": {20};
case "cl3_xr_1000_babyblue": {20};
case "cl3_xr_1000_babypink": {20};
case "cl3_xr_1000_black": {20};
case "cl3_xr_1000_blue": {20};
case "cl3_xr_1000_burgundy": {20};
case "cl3_xr_1000_camo": {20};
case "cl3_xr_1000_camo_urban": {20};
case "cl3_xr_1000_cardinal": {20};
case "cl3_xr_1000_dark_green": {20};
case "cl3_xr_1000_emt": {20};
case "cl3_xr_1000_flame": {20};
case "cl3_xr_1000_flame1": {20};
case "cl3_xr_1000_flame2": {20};
case "cl3_xr_1000_gold": {20};
case "cl3_xr_1000_green": {20};
case "cl3_xr_1000_grey": {20};
case "cl3_xr_1000_lavender": {20};
case "cl3_xr_1000_light_blue": {20};
case "cl3_xr_1000_light_yellow": {20};
case "cl3_xr_1000_lime": {20};
case "cl3_xr_1000_marina_blue": {20};
case "cl3_xr_1000_navy_blue": {20};
case "cl3_xr_1000_orange": {20};
case "cl3_xr_1000_police": {20};
case "cl3_xr_1000_purple": {20};
case "cl3_xr_1000_red": {20};
case "cl3_xr_1000_sand": {20};
case "cl3_xr_1000_silver": {20};
case "cl3_xr_1000_violet": {20};
case "cl3_xr_1000_white": {20};

case "cl3_chopper": {10};
case "cl3_chopper_blue": {10};
case "cl3_chopper_gold": {10};
case "cl3_chopper_green": {10};
case "cl3_chopper_red": {10};
case "cl3_chopper_silver": {10};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//AVIATION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_GNT_C185_fuse1": {110};
case "cl3_GNT_C185_fuse2": {110};
case "cl3_GNT_C185_fuse3": {110};
case "cl3_GNT_C185_fuse4": {110};
case "cl3_GNT_C185_fuse5": {110};
case "cl3_GNT_C185_fuse6": {110};
case "IVORY_ERJ135_1":{1500};
case "IVORY_CRJ200_1":{1800};
case "GNT_C185F": {110};
case "C130J_Cargo": {800};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DEPANNEUR
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_f150repo_black": {95};
case "cl3_f150repo_blue": {95};
case "cl3_f150repo_gray": {95};
case "cl3_f150repo_green": {95};
case "cl3_f150repo_orange": {95};
case "cl3_f150repo_red": {95};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//BUS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "CL3_bus_cl_black": {190};
case "CL3_bus_cl_blue": {190};
case "CL3_bus_cl_green": {190};
case "CL3_bus_cl_grey": {190};
case "CL3_bus_cl_jail": {190};
case "CL3_bus_cl_purple": {190};
case "CL3_bus_cl_yellow": {190};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//RIGOLOL
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_batmobile": {50};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Trucks
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_mackr_del": {900}; //Mack R
case "cl3_mackr_del_american": {900};
case "cl3_mackr_del_black": {900};
case "cl3_mackr_del_black_gold": {900};
case "cl3_mackr_del_black_white": {900};
case "cl3_mackr_del_blue": {900};
case "cl3_mackr_del_brown_camo": {900};
case "cl3_mackr_del_forest_camo": {900};
case "cl3_mackr_del_green_white": {900};
case "cl3_mackr_del_multi_color": {900};
case "cl3_mackr_del_optimus": {900};
case "cl3_mackr_del_orange_white": { 900};
case "cl3_mackr_del_purple_white": {900};
case "cl3_mackr_del_red_white" : {900};
case "cl3_mackr_del_silver": {900};

default {-1};
};