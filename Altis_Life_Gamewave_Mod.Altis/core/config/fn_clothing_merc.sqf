/*
	File: fn_clothing_reb.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for Reb shop.
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Tenue mercenaire"];

switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[
			["U_O_CombatUniform_oucamo","Mercenaire",5000],
			["alfr_b_combat_merc",nil,5000],
			["alfr_b_combat_merc2",nil,5000],
			["alfr_polopants_merc_1",nil,5000],
			["alfr_polopants_merc_2",nil,5000],
			["alfr_polopants_merc_3",nil,5000],
			["cl3_pilot_CGP_uniform","Combinaison antirad Merc",15000],
			["U_I_GhillieSuit",nil,5000],
			["U_O_Wetsuit","Tenue de plongée",5000]
		];
	};
	
	//Hats
	case 1:
	{
		[

			["H_Cap_blu",nil,150],
			["H_Cap_grn",nil,150],
			["H_Cap_blk_CMMG",nil,150],
			["H_Watchcap_blk",nil,150],
			["H_MilCap_gry",nil,150],
			["H_Bandanna_surfer",nil,150],
			["H_Bandanna_khk_hs",nil,150],
			["H_HelmetB",nil,150],
			["H_HelmetB_light_sand",nil,150],
			["H_HelmetB_sand",nil,150],
			["H_Shemag_khk",nil,150],
			["H_PilotHelmetFighter_O","Masque a Gaz",500]

		];
	};
	
	//Glasses
	case 2:
	{
		[
			["G_Shades_Black",nil,25],
			["G_Shades_Blue",nil,20],
			["G_Sport_Blackred",nil,20],
			["G_Sport_Checkered",nil,20],
			["G_Sport_Blackyellow",nil,20],
			["G_Sport_BlackWhite",nil,20],
			["G_Squares",nil,10],
			["G_Aviator",nil,100],
			["G_Lady_Mirror",nil,150],
			["G_Lady_Dark",nil,150],
			["G_Lady_Blue",nil,150],
			["G_Lowprofile",nil,30],
			["G_Combat",nil,55],
			["G_Diving",nil,150],
			["G_Balaclava_blk",nil,650]
		];
	};
	
	//Vest
	case 3:
	{
		[
			["V_RebreatherIA","Gilet de plongée",5000],
			["V_Rangemaster_belt",nil,12500],
			["V_BandollierB_blk",nil,12500],
			["V_Chestrig_blk",nil,12500],
			["V_TacVestIR_blk",nil,12500],
			["V_TacVest_blk",nil,12500],
			["V_PlateCarrier1_blk",nil,12500]

		];
	};
	
	//Backpacks
	case 4:
	{
		[
			["B_FieldPack_blk",nil,2500],
			["B_OutdoorPack_blk",nil,2500],
			["B_AssaultPack_blk",nil,2500],
			["B_Carryall_oucamo",nil,5000],
			["B_AssaultPack_cbr","Sac à Dos 44 Slots",2500],
			["B_FieldPack_ocamo","Sac à Dos 49 Slots",3000],
			["B_TacticalPack_oli","Sac à Dos 54 Slots",3500],
			["B_Kitbag_mcamo","Sac à Dos 59 Slots",4500],
			["B_Bergen_sgg","Sac à Dos 59 Slots",4500],
			["B_Kitbag_cbr","Sac à Dos 59 Slots",4500],
			["B_Carryall_oli","Sac à Dos 64 Slots",5000],
			["B_Carryall_khk","Sac à Dos 64 Slots",5000]
		];
	};
};
//[] call life_fnc_updateClothing;