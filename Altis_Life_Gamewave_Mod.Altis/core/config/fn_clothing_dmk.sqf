/*
	File: fn_clothing_pam.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Shop dédié a l'entreprise de médecins DMK
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Tenue DMK"];

switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[
		["cl3_emt_uniform","Tenue DMK",3000],
		["cl_pepperspray","Spray au poivre",2000],
        ["cl_PepperMag","Recharge spray au poivre",50]
		];
	};
	
	//Hats
	case 1:
	{
		[
		];
	};
	
	//Glasses
	case 2:
	{
		[
		];
	};
	
	//Vest
	case 3:
	{
		[
		["cl3_police_vest_pm",nil,1000]
		];
	};
	
	//Backpacks
	case 4:
	{
		[
		["cl3_emt_tacticalbelt_black",nil,3000],
		["cl3_emt_tacticalbelt",nil,3000]
		];
	};
};
//[] call life_fnc_updateClothing;