#include <macro.h>
/*
	File: fn_vehicleListCfg.sqf
	Modif Nonoxs for GameWave
	
	Description:
	Vendeurs Véhicules
*/
private["_shop","_return"];
_shop = [_this,0,"",[""]] call BIS_fnc_param;
if(_shop == "") exitWith {[]};
_return = [];
switch (_shop) do
{
case "civ_Pegasus_1":
{
	_return =
	[
	["I_Truck_02_box_F",50000] //Camion Pegasus
	];	
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Police Car
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	case "cop_car":
	{
		_return =
		[
			["cl3_bike_bmx",2500],
			["cl3_bike_mountain",4000],
			["cl3_bike_Road",2000],
			["B_Quadbike_01_F",7500],
			["C_Offroad_01_F",55250],
			["C_SUV_01_F",50000]
		];
			if(__GETC__(life_coplevel) > 1) then //lvl 2 - Brigadier/Sergent -

			{	
				_return pushBack["B_MRAP_01_F",1000000];
				_return pushBack["cl3_xr_1000",350200];
				_return pushBack["cl3_escalade",600000];
				_return pushBack["cl3_dodge_charger_patrol",300000];
				_return pushBack["cl3_transit",150000];
				_return pushBack["cl3_suv",450110];
			};
				
				if(__GETC__(life_coplevel) > 2) then //lvl 3 - Adjudant/Adjudant-chef -
				{
					_return pushBack["B_G_Offroad_01_armed_F",1500000];	
				    _return pushBack["CL3_bus_cl",90250];
					
					if(__GETC__(life_donator) > 1) then
					{
					_return pushBack["C_Hatchback_01_sport_F",500000];
					_return pushBack["cl3_reventon",1200000];
					};	
				};

			
					if(__GETC__(life_coplevel) > 3) then //lvl 4 - Major/Aspirant -
					{
						_return pushBack["I_MRAP_03_F",1200000];
						
					};

										


		};

	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Police Air
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	case "cop_air":
	{
			if(__GETC__(life_coplevel) > 1) then //lvl 2 - Brigadier/Sergent 
			{
				_return pushBack["B_Heli_Light_01_F",1125000];	
				_return pushBack["O_Heli_Light_02_unarmed_F",1500000];	
			};
				
			if(__GETC__(life_coplevel) > 2) then //lvl 3
			{
				_return pushBack["I_Heli_light_03_unarmed_F",2100000];	
			};
						
			if(__GETC__(life_coplevel) > 4) then //lvl 5 - Lieutenant
			{
				_return pushBack["B_Heli_Transport_01_F",3000000];	
				_return pushBack["B_Heli_Transport_03_unarmed_F",3000000];
				_return pushBack["O_Heli_Transport_04_bench_F",2750000];
				
					if(__GETC__(life_donator) > 1) then
					{
					_return pushBack["O_Heli_Transport_04_F",3000000];
					};
			};

			if(__GETC__(life_coplevel) > 6) then //lvl 7 - Commandant -
			{
				_return pushBack["B_Heli_Attack_01_F",4000000];	
			};					
	};
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Police Bateau
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	case "cop_ship":
	{
		_return =
		[
			["B_Boat_Transport_01_F",75000]
		];

			if(__GETC__(life_coplevel) > 1) then //lvl 2 + Hors-Bord Police
			{
				_return pushBack["C_Boat_Civil_01_police_F",150000];	
			};
			
				if(__GETC__(life_coplevel) > 3) then //lvl 4 + Sous-Marin+ Bateau Armé
				{
					_return pushBack["B_SDV_01_F",329688];
					_return pushBack["B_Boat_Armed_01_minigun_F",620000];	
				};

	};
		
	
	
		
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Vélo BMX VTT
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "civ_bike":
{
_return =
[
["cl3_bike_bmx",2500],
//["cl3_bike_mountain",4000],
["cl3_bike_Road",2000]
];			
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Moto
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "civ_moto":
{
_return =
[
["cl3_enduro",250000],
["cl3_chopper",250000],
["cl3_xr_1000",350200]
];			
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil KART
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "kart_shop":
{
_return =
[
["C_Kart_01_Blu_F",15000],
["C_Kart_01_Fuel_F",15000],
["C_Kart_01_Red_F",15000],
["C_Kart_01_Vrana_F",15000]
];		
};	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule low
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "civ_car_low":
{
_return =
[
["B_Quadbike_01_F",4500],
["C_Hatchback_01_F",25000],
["C_Offroad_01_F",30250],
["cl3_lada",15000],
["cl3_volha",20000],
["cl3_440cuda",110000],
["cl3_civic_vti",85000],
["cl3_crown_victoria",150000],
["beetle_custom",500000],
["cl3_defender_110_red",300000],
["cl3_discovery",200000],
["cl3_golf_mk2",45000],
["cl3_polo_gti",90000]
];		
if(__GETC__(life_donator) > 1) then
{
	_return pushBack ["cl3_bounder_beige",80000];
};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule medium
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "civ_car_medium":
{
_return =
[
["C_SUV_01_F",50000],
["clpd_mondeo",250000],
["cl3_dodge_charger_patrol",300000],
["cl3_e60_m5",350000],
["cl3_e63_amg",380000],
["cl3_escalade",600000],
["cl3_q7",350000],
["cl3_s5",320000],
["cl3_suv",400000],
["cl3_taurus",280000],
["cl3_transit",150000],
["cl3_z4_2008",350000]
];
	if(__GETC__(life_donator) > 1) then
	{
		_return pushBack ["cl3_challenger_2009",400000];
	};		
};	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule High
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "civ_car_high":
{
_return =
[
["cl3_458",1800000],
["cl3_carrera_gt",1600000],
["cl3_dbs_volante",900000],
["cl3_impreza_rally",480000],
["cl3_impreza_road",400000],
["cl3_insignia",300000],
//["cl3_lamborghini_gt1",1600000],
["cl3_murcielago",1300000],
["cl3_r8_spyder",700000],
["cl3_veyron",2200000],
["Skyline_Mercedes_C63_02_F",1400000]
];		
	if(__GETC__(life_donator) > 1) then
	{
		_return pushBack ["Skyline_Mercedes_C63_01_F",1700000];
		_return pushBack ["C_Hatchback_01_sport_F",500000];
		_return pushBack ["cl3_aventador_lp7004",1400000];
		_return pushBack ["cl3_reventon",1200000];
		_return pushBack ["cl3_batmobile",2000000];
		
	};
};	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Camion
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "civ_truck":
{
_return =
[
["C_Van_01_box_F",200000],
["I_Truck_02_transport_F",300000],
["I_Truck_02_covered_F",600000],
["B_Truck_01_transport_F",1500000],
["B_Truck_01_box_F",2000000],
["O_Truck_03_device_F",1500000],
["I_Truck_02_box_F",50000], //Camion Pegasus
["cl3_mackr_del",2000000] 
];
};	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//AVIATION CIV
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "civ_plane":
{
	_return =
	[
	["cl3_GNT_C185_base",1100000],
	//["GNT_C185F",1200000],
	["IVORY_ERJ135_1",4000000],
	["IVORY_CRJ200_1",4500000]
	
	//["C130J_Cargo",110000]
	];
	if(__GETC__(life_adminlevel) > 1) then	
	{
	_return pushBack ["cl3_jetpack",1000000];
	};
};	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//HELICO CIV
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "civ_air":
{
	_return =
	[
	["B_Heli_Light_01_F",1125000],
	["C_Heli_Light_01_civil_F",1200000],
	["O_Heli_Light_02_unarmed_F",1500000],
	["O_Heli_Transport_04_covered_F",1500000]
	];	

	if(__GETC__(life_donator) > 1) then
	{
	_return pushBack ["I_Heli_Transport_02_F",2250000];
	_return pushBack ["O_Heli_Transport_04_F",3000000];
	};

};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//BATEAU CIV
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "civ_ship":
{
	_return =
	[
	["C_Rubberboat",5000],
	["C_Boat_Civil_01_F",22000],
	["I_G_Boat_Transport_01_F",15000]
	];
	
	if(__GETC__(life_donator) > 1) then
	{
	_return pushBack ["B_SDV_01_F",329688];
	};	
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule FUN
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "civ_fun":
{
	_return =
	[
	["cl_skatea",4000],
	["cl_skateb",4000],
	["cl_skatec",4000],
	["cl_skated",4000],
	["cl_skatee",4000],
	["cl_skatef",4000],
	["cl_skateg",4000],
	["cl_skateh",4000],
	["cl_skatei",4000],
	["cl_skatej",4000],
	["cl_skatek",4000],
	["cl_skatel",4000],
	["cl_skatem",4000],
	["cl_skaten",4000],
	["cl_skateo",4000],
	["cl_skatep",4000],
	["cl_skateq",4000],
	["cl_skater",4000],
	["cl_skates",4000],
	["cl_skatet",4000],
	["cl_skateu",4000],
	["cl_skatev",4000],
	["cl_skatex",4000],
	["cl_skatey",4000]
	];
	if(__GETC__(life_donator) > 1) then	
	{
	_return pushBack ["A3L_Hoverboard_bull",12000];
	_return pushBack ["A3L_Hoverboard",10000];
	};
	if(__GETC__(life_adminlevel) > 1) then	
	{
	_return pushBack ["A3L_MonsterTruck",120000];
	};
};	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//REBELLE CARS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "reb_car":
{
	_return =
	[
		["cl3_bike_Road",2000],
		["cl3_bike_mountain",4000],
		["cl3_bike_bmx",2500],
		["cl3_enduro",250000],
		["cl3_xr_1000",350200],
		["B_Quadbike_01_F",7500],
		["C_Offroad_01_F",55250],
		["O_MRAP_02_F",1000000],
		["B_G_Offroad_01_F",35000],
		["B_G_Offroad_01_armed_F",1500000],
		["cl3_impreza_rally",480000],
		["cl3_bike_Road",2000],
		["cl3_bike_mountain",4000],
		["cl3_bike_bmx",2500],
		["cl3_civic_vti",85000],
		["cl3_crown_victoria",150000],
		["cl3_golf_mk2",45000],
		["cl3_polo_gti",90000],
		["cl3_dodge_charger_patrol",300000],
		["cl3_e60_m5",350000],
		["cl3_e63_amg",380000],
		["cl3_escalade",600000],
		["cl3_q7",350000],
		["cl3_s5",320000],
		["cl3_taurus",280000],
		["beetle_custom",500000],
		//["cl3_transit",150000],
		["cl3_z4_2008",350000]
	];	
	if(__GETC__(life_donator) > 1) then
	{
		_return pushBack["C_Hatchback_01_sport_F",500000];
		_return pushBack["cl3_reventon",1200000];
		_return pushBack["cl3_aventador_lp7004",1400000];
	};
	
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//REBELLE AIR
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "reb_air":
{
_return =
[
	["B_Heli_Light_01_F",1125000],
	["O_Heli_Light_02_unarmed_F",1500000],
	["I_Heli_light_03_unarmed_F",2100000],
	["O_Heli_Transport_04_bench_F",2750000],
	["O_Heli_Transport_04_covered_F",1500000]
];
if(__GETC__(life_donator) > 1) then	
{
_return pushBack ["I_Heli_Transport_02_F",2250000];
_return pushBack ["O_Heli_Transport_04_F",3000000];
};					
};	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DONATEUR SHOP
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	case "donator":
	{	
		if(__GETC__(life_donator) > 1) then
		{
			_return =
			[
				["A3L_Hoverboard_bull",12000],
				["A3L_Hoverboard",12000],
				
				["cl3_bike_bmx",2500],
				["cl3_bike_mountain",4000],
				["cl3_bike_Road",2000],
				
				["cl3_enduro",250000],
				["cl3_chopper",250000],
				["cl3_xr_1000",350200],
				
				["B_Quadbike_01_F",7500],
				["C_Hatchback_01_F",25000],
				["C_Offroad_01_F",55250],
				["cl3_lada",15000],
				["cl3_volha",20000],
				["cl3_bounder_beige",80000],
				["cl3_440cuda",110000],
				["cl3_civic_vti",85000],
				["cl3_crown_victoria",150000],
				["cl3_defender_110_red",300000],
				["cl3_discovery",200000],
				["cl3_golf_mk2",45000],
				["cl3_polo_gti",90000],
				
				["clpd_mondeo",250000],
				["cl3_dodge_charger_patrol",300000],
				["cl3_challenger_2009",400000],
				["cl3_e60_m5",350000],
				["cl3_e63_amg",380000],
				["beetle_custom",500000],
				["cl3_escalade",600000],
				["cl3_q7",350000],
				["cl3_s5",320000],
				//["cl3_suv",400000],
				["cl3_taurus",280000],
				["cl3_transit",150000],
				["cl3_z4_2008",350000],
				
				["C_Hatchback_01_sport_F",500000],
				["cl3_458",1800000],
				["cl3_aventador_lp7004",1400000],
				["cl3_carrera_gt",1600000],
				["cl3_dbs_volante",900000],
				["cl3_impreza_rally",480000],
				["cl3_impreza_road",400000],
				["cl3_insignia",300000],
				//["cl3_lamborghini_gt1",1600000],
				["cl3_murcielago",1300000],
				["cl3_r8_spyder",700000],
				["cl3_reventon",1200000],
				["cl3_veyron",2200000],
				["cl3_batmobile",2000000]
			];
			if(license_civ_truck) then
			{
				_return pushBack ["C_Van_01_transport_F",100000];
				_return pushBack ["C_Van_01_box_F",200000];
				_return pushBack ["I_Truck_02_transport_F",300000];
				_return pushBack ["I_Truck_02_covered_F",600000];
				_return pushBack ["B_Truck_01_transport_F",1500000];
				_return pushBack ["O_Truck_03_device_F",1500000];
				_return pushBack ["B_Truck_01_box_F",2000000];
				_return pushBack ["cl3_mackr_del",2000000];
			};	
	
			if(license_civ_air) then
			{
				_return pushBack ["B_Heli_Light_01_F",1125000]; // littlebird
				_return pushBack ["O_Heli_Light_02_unarmed_F",1500000]; // orca
				_return pushBack ["I_Heli_Transport_02_F",2250000]; // mohak
				_return pushBack ["O_Heli_Transport_04_F",3000000]; // Taru
			};	

			if(license_civ_rebel) then
			{
				_return pushBack ["B_G_Offroad_01_F",50000];
				_return pushBack ["B_G_Offroad_01_armed_F",1500000];
				_return pushBack ["O_MRAP_02_F",1000000];
				_return pushBack ["I_Heli_light_03_unarmed_F",2100000]; //hellcat
			};			
		};
	};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Dépanneur Shop
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "dep_shop":
{
	_return =
	[
		["C_Offroad_01_F",55250],
		["cl3_escalade_dep",350250],
		["C_Van_01_Fuel_F",100000]
	];
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Dépanneur Air
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "dep_air":
	{

		_return =
			[
			["O_Heli_Transport_04_repair_F",1800000]
			];
		if(__GETC__(life_donator) > 1) then
		{
			_return pushBack ["I_Heli_Transport_02_F",2250000];
			_return pushBack ["O_Heli_Transport_04_F",2500000];
		};				
	};	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Taxi Shop
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "taxi_shop":
{

	_return =
	[
		["C_SUV_01_F",50000],
		["cl3_suv",400000],
		["C_Offroad_01_F",55250],
		["CL3_bus_cl",90250]
	];
			
};
	

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Medic Cars
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "med_shop":
	{
	_return =
		[
			["C_SUV_01_F",50000],
			["C_Hatchback_01_F",25000],
			["cl3_dodge_charger_patrol",300000],
			["cl3_suv",400000],
			["cl3_transit",150000],
			["cl3_escalade",600000]
		];
		if(__GETC__(life_donator) > 1) then
		{
		_return pushBack ["C_Hatchback_01_sport_F",500000];
		};	
	};	


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Medic Air
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	case "med_air_hs":
	{

		_return =
			[
			["B_Heli_Light_01_F",1125000],
			["O_Heli_Light_02_unarmed_F",1500000],
			["O_Heli_Transport_04_medevac_F",1800000]
			];
		
		if(__GETC__(life_donator) > 1) then
		{
			_return pushBack ["I_Heli_Transport_02_F",2250000];
			_return pushBack ["O_Heli_Transport_04_F",3000000];
		};	

	};
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Black Eagle Car
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	case "BlackEagle":
	{

		_return =
			[
			["C_Heli_Light_01_civil_F",1125000]
			];
		
		if(__GETC__(life_donator) > 1) then
		{
			_return pushBack ["I_Heli_Transport_02_F",2250000];
			_return pushBack ["O_Heli_Transport_04_F",3000000];
		};	

	};
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Mercenaire Car
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "merc_v":
	{
		_return =
		[
				["B_Quadbike_01_F",7500],
				["B_MRAP_01_F",1310000],
				["I_MRAP_03_F",1820000],
				["B_G_Offroad_01_F",55250],
				["B_G_Offroad_01_armed_F",1500000],
				["C_SUV_01_F",50000]

		];
		if(__GETC__(life_donator) > 1) then
		{
		_return pushBack ["C_Hatchback_01_sport_F",500000];
		_return pushBack ["cl3_reventon",1200000];
		};		
	};
	
	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Mercenaire Air
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "merc_air":
	{
	_return =
	[
	["B_Heli_Light_01_F",1125000],
	["O_Heli_Light_02_unarmed_F",1500000], 
	["I_Heli_light_03_unarmed_F",2100000], 
	["O_Heli_Transport_04_bench_F",2750000],
	["O_Heli_Transport_04_covered_F",3000000]
	];	
	if(__GETC__(life_donator) > 1) then
	{
		_return pushBack ["I_Heli_Transport_02_F",2250000];
		_return pushBack ["O_Heli_Transport_04_F",3000000];
	};			
	};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Mercenaire Bateau
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "merc_ship":
	{

	_return =
			[
			["C_Rubberboat",7500],
			["C_Boat_Civil_01_F",33000],
			["I_G_Boat_Transport_01_F",22500]
			];
		
		if(__GETC__(life_donator) > 1) then
		{
			_return pushBack ["B_SDV_01_F",329688];
		};
		
};	


};


_return;


