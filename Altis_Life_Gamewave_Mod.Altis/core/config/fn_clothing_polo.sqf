/*
	File: fn_clothing_bruce.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for Bruce's Outback Outfits.
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Polos"];

switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[
		["alfr_poloshirt_abstract_1",nil,250],
		["alfr_poloshirt_believe",nil,1000],
		["alfr_poloshirt_bluescreen",nil,250],
		["alfr_poloshirt_sceau",nil,250],
		["alfr_poloshirt_Nightmare",nil,250],
		["alfr_poloshirt_invader",nil,250],
		["alfr_poloshirt_booo",nil,250],
		["alfr_poloshirt_crane",nil,250],
		["alfr_poloshirt_carotte",nil,250],
		["alfr_poloshirt_cartemere",nil,250],
		["alfr_poloshirt_crazycat",nil,1500],
		["alfr_poloshirt_government",nil,1000],
		["alfr_poloshirt_happy",nil,1000],
		["alfr_poloshirt_legalize",nil,1000],
		["alfr_poloshirt_megaman",nil,1200],
		["alfr_richardhotlinemiami",nil,1500],
		["alfr_poloshirt_miaou",nil,500],
		["alfr_poloshirt_mouse",nil,250],
		["alfr_poloshirt_optic",nil,750],
		["alfr_poloshirt_piment",nil,250],
		["alfr_poloshirt_skyhelmet",nil,1500],
		["alfr_poloshirt_smokemon",nil,250],
		["alfr_poloshirt_thisismybest",nil,250],
		["alfr_poloshirt_thisismybestfriend",nil,250],
		["alfr_poloshirt_toiletdownloading",nil,250],
		["alfr_poloshirt_torse",nil,500],
		["alfr_poloshirt_twitch",nil,750],
		["alfr_poloshirt_zef",nil,1200],
		["alfr_poloshirt_zerg",nil,1000],
		["alfr_poloshirt_pryapisme1",nil,250],
		["alfr_poloshirt_pryapisme2",nil,250],
		["cl3_c_poloshirt_10_uniform",nil,250],
		["cl3_c_poloshirt_11_uniform",nil,250],
		["cl3_c_poloshirt_12_uniform",nil,250],
		["cl3_c_poloshirt_13_uniform",nil,250],
		//["cl3_c_poloshirt_14_uniform",nil,250],
		//["cl3_c_poloshirt_15_uniform",nil,250],
		["cl3_c_poloshirt_16_uniform",nil,250],
		["cl3_c_poloshirt_17_uniform",nil,250],
		["cl3_c_poloshirt_18_uniform",nil,250],
		["cl3_c_poloshirt_19_uniform",nil,250],
		["cl3_c_poloshirt_1_uniform",nil,250],
		["cl3_c_poloshirt_20_uniform",nil,250],
		//["cl3_c_poloshirt_21_uniform",nil,250],
		["cl3_c_poloshirt_22_uniform",nil,250],
		["cl3_c_poloshirt_23_uniform",nil,250],
		["cl3_c_poloshirt_2_uniform",nil,250],
		["cl3_c_poloshirt_3_uniform",nil,250],
		["cl3_c_poloshirt_4_uniform",nil,250],
		["cl3_c_poloshirt_5_uniform",nil,250],
		["cl3_c_poloshirt_6_uniform",nil,250],
		["cl3_c_poloshirt_7_uniform",nil,250],
		["cl3_c_poloshirt_8_uniform",nil,250],
		//["cl3_c_poloshirt_9_uniform",nil,250],
		["cl3_c_poloshirt_funk_co_uniform",nil,250],
		["cl3_c_poloshirt_gravity_co_uniform",nil,250],
		//["cl3_c_poloshirtpants_10_uniform",nil,250],
		//["cl3_c_poloshirtpants_10_uniform",nil,250],
		["cl3_c_poloshirtpants_11_uniform",nil,250],
		["cl3_c_poloshirtpants_12_uniform",nil,250],
		//["cl3_c_poloshirtpants_13_uniform",nil,250],
		//["cl3_c_poloshirtpants_13_uniform",nil,250],
		//["cl3_c_poloshirtpants_14_uniform",nil,250], //pizza
		
		["cl3_c_poloshirtpants_15_uniform",nil,250],
		
		//["cl3_c_poloshirtpants_16_uniform",nil,250], //security
		//["cl3_c_poloshirtpants_17_uniform",nil,250],
		//["cl3_c_poloshirtpants_18_uniform",nil,250],
		
		//["cl3_c_poloshirtpants_19_uniform",nil,250],
		//["cl3_c_poloshirtpants_1_uniform",nil,250],
		["cl3_c_poloshirtpants_20_uniform",nil,250],
		["cl3_c_poloshirtpants_21_uniform",nil,250],
		["cl3_c_poloshirtpants_22_uniform",nil,250],
		["cl3_c_poloshirtpants_23_uniform",nil,250],
		["cl3_c_poloshirtpants_24_uniform",nil,250],
		["cl3_c_poloshirtpants_25_uniform",nil,250],
		["cl3_c_poloshirtpants_26_uniform",nil,250],
		["cl3_c_poloshirtpants_2_uniform",nil,250],
		["cl3_c_poloshirtpants_3_uniform",nil,250],
		["cl3_c_poloshirtpants_4_uniform",nil,250],
		["cl3_c_poloshirtpants_5_uniform",nil,250],
		["cl3_c_poloshirtpants_5_uniform",nil,250],
		["cl3_c_poloshirtpants_5_uniform",nil,250],
		["cl3_c_poloshirtpants_6_uniform",nil,250],
		//["cl3_c_poloshirtpants_7_uniform",nil,250],
		//["cl3_c_poloshirtpants_8_uniform",nil,250],
		//["cl3_c_poloshirtpants_9_uniform",nil,250],
		
		//["cl3_c_poloshirtpants_bus_uniform",nil,250],
		//["cl3_c_poloshirtpants_CG_uniform",nil,250],
		//["cl3_c_poloshirtpants_CHIEF_uniform",nil,250],
		//["cl3_c_poloshirtpants_CLN_uniform",nil,250],
		//["cl3_c_poloshirtpants_clpdcom_uniform",nil,250],
		//["cl3_c_poloshirtpants_clpdpatrol_uniform",nil,250],
		//["cl3_c_poloshirtpants_clpdtraffic_uniform",nil,250],
		//["cl3_c_poloshirtpants_FD_uniform",nil,250],
		//["cl3_c_poloshirtpants_MR_uniform",nil,250],
		//["cl3_c_poloshirtpants_PM_uniform",nil,250],
		
		
		
		["cl3_citizen2_10_uniform",nil,250],
		["cl3_citizen2_11_uniform",nil,250],
		["cl3_citizen2_12_uniform",nil,250],
		["cl3_citizen2_13_uniform",nil,250],
		["cl3_citizen2_13_uniform",nil,250],
		["cl3_citizen2_14_uniform",nil,250],
		["cl3_citizen2_15_uniform",nil,250],
		["cl3_citizen2_16_uniform",nil,250],
		["cl3_citizen2_17_uniform",nil,250],
		["cl3_citizen2_18_uniform",nil,250],
		["cl3_citizen2_19_uniform",nil,250],
		["cl3_citizen2_1_uniform",nil,250],
		["cl3_citizen2_20_uniform",nil,250],
		["cl3_citizen2_21_uniform",nil,250],
		["cl3_citizen2_22_uniform",nil,250],
		["cl3_citizen2_23_uniform",nil,250],
		["cl3_citizen2_24_uniform",nil,250],
		["cl3_citizen2_25_uniform",nil,250],
		["cl3_citizen2_2_uniform",nil,250],
		["cl3_citizen2_2_uniform",nil,250],
		["cl3_citizen2_3_uniform",nil,250],
		["cl3_citizen2_4_uniform",nil,250],
		["cl3_citizen2_4_uniform",nil,250],
		["cl3_citizen2_5_uniform",nil,250],
		["cl3_citizen2_6_uniform",nil,250],
		["cl3_citizen2_7_uniform",nil,250],
		["cl3_citizen2_8_uniform",nil,250],
		["cl3_citizen2_9_uniform",nil,250],
		["cl3_citizen3_10_uniform",nil,250],
		["cl3_citizen3_11_uniform",nil,250],
		["cl3_citizen3_12_uniform",nil,250],
		["cl3_citizen3_13_uniform",nil,250],
		["cl3_citizen3_14_uniform",nil,250],
		["cl3_citizen3_15_uniform",nil,250],
		["cl3_citizen3_16_uniform",nil,250],
		["cl3_citizen3_17_uniform",nil,250],
		["cl3_citizen3_18_uniform",nil,250],
		["cl3_citizen3_19_uniform",nil,250],
		["cl3_citizen3_1_uniform",nil,250],
		["cl3_citizen3_20_uniform",nil,250],
		["cl3_citizen3_21_uniform",nil,250],
		["cl3_citizen3_22_uniform",nil,250],
		["cl3_citizen3_23_uniform",nil,250],
		["cl3_citizen3_24_uniform",nil,250],
		["cl3_citizen3_25_uniform",nil,250],
		["cl3_citizen3_2_uniform",nil,250],
		["cl3_citizen3_3_uniform",nil,250],
		["cl3_citizen3_4_uniform",nil,250],
		["cl3_citizen3_5_uniform",nil,250],
		["cl3_citizen3_6_uniform",nil,250],
		["cl3_citizen3_7_uniform",nil,250],
		["cl3_citizen3_7_uniform",nil,250],
		["cl3_citizen3_8_uniform",nil,250],
		["cl3_citizen3_9_uniform",nil,250],
		["cl3_citizen3_9_uniform",nil,250],
		["cl3_citizen4_10_uniform",nil,250],
		["cl3_citizen4_11_uniform",nil,250],
		["cl3_citizen4_12_uniform",nil,250],
		["cl3_citizen4_13_uniform",nil,250],
		["cl3_citizen4_14_uniform",nil,250],
		["cl3_citizen4_15_uniform",nil,250],
		["cl3_citizen4_16_uniform",nil,250],
		["cl3_citizen4_17_uniform",nil,250],
		["cl3_citizen4_18_uniform",nil,250],
		["cl3_citizen4_19_uniform",nil,250],
		["cl3_citizen4_1_uniform",nil,250],
		["cl3_citizen4_20_uniform",nil,250],
		["cl3_citizen4_21_uniform",nil,250],
		["cl3_citizen4_22_uniform",nil,250],
		["cl3_citizen4_22_uniform",nil,250],
		["cl3_citizen4_23_uniform",nil,250],
		["cl3_citizen4_24_uniform",nil,250],
		["cl3_citizen4_25_uniform",nil,250],
		["cl3_citizen4_2_uniform",nil,250],
		["cl3_citizen4_3_uniform",nil,250],
		["cl3_citizen4_4_uniform",nil,250],
		["cl3_citizen4_4_uniform",nil,250],
		["cl3_citizen4_5_uniform",nil,250],
		["cl3_citizen4_6_uniform",nil,250],
		["cl3_citizen4_7_uniform",nil,250],
		["cl3_citizen4_8_uniform",nil,250],
		["cl3_citizen4_9_uniform",nil,250],
		["cl3_citizen4_9_uniform",nil,250]	
		];
	};
	
	//Hats
	case 1:
	{
		[
		
		];
	};
	
	//Glasses
	case 2:
	{
		[

		];
	};
	
	//Vest
	case 3:
	{
		[
		];
	};
	
	//Backpacks
	case 4:
	{
		[

		];
	};
};