/*
	File: fn_clothing_bruce.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for Bruce's Outback Outfits.
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Magasin de vêtement d'Altis"];

switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[
		["U_C_Poloshirt_blue","Poloshirt bleu",250],
		["U_C_Poloshirt_burgundy","Poloshirt pourpre",250],
		["U_C_Poloshirt_redwhite","Poloshirt rouge et blanc",250],
		["U_C_Poloshirt_salmon","Poloshirt Saumon",250],
		["U_C_Poloshirt_stripped","Poloshirt à rayure",250],
		["U_C_Poloshirt_tricolour","Poloshirt Tricolore",250],
		["U_C_Poor_2","Habits Made in China",250],
		["U_Competitor","Pilote",650],
		["U_IG_Guerilla2_2","tee shirt verte à rayure",650],
		["U_IG_Guerilla3_1","Veste brune",735],
		["U_IG_Guerilla2_3","Le randonneur",1200],
		["U_BG_Guerilla2_1","Polo Bleu",1200],
		["U_C_HunterBody_grn","Le chasseur",1500],
		//["U_C_WorkerCoveralls","Le garagiste",2500],
		["U_OrestesBody","Le surfer",1100],
		["U_NikosBody","Chemise",2100],
		["U_C_Journalist","Costume de journaliste",3100],
		["U_NikosAgedBody","Costume Cravate",5100],
		["A3L_Worker_Outfit","Tenue de travailleur",2100],
		["A3L_Dude_Outfit","Tenue à la cool",2100],
		["A3L_Farmer_Outfit","Tenue de fermier",2100]
		
	
		];
	};
	
	//Hats
	case 1:
	{
		[
			["H_Bandanna_camo","Camo Bandanna",120],
			["H_Bandanna_camo","Camo Bandanna",120],
			["H_Bandanna_surfer","Surfer Bandanna",130],
			["H_Bandanna_gry","Bandanna Gris",150],
			["H_Bandanna_khk","Bandanna vert",145],
			["H_Bandanna_sgg","Sage Bandanna",160],
			["H_StrawHat","La fedora",225],
			["H_Hat_grey","Chapeau",225],
			["H_Hat_blue","Chapeau Bleu",225],
			["H_StrawHat_dark","Chapeau Marron",225],
			["H_Hat_tan","Chapeau Tan",225],
			["H_Hat_checker","Chapeau Dammier",225],
			["H_Cap_press","Chapeau Journaliste",225],
			["H_BandMask_blk","Chapeau et bandanna",300],
			["H_Bandanna_cbr",nil,165],
			["H_Booniehat_tan",nil,425],
			["H_Hat_brown",nil,276],
			["H_Cap_blu",nil,150],
			["H_Cap_grn",nil,150],
			["H_Cap_grn_BI",nil,150],
			["H_Cap_oli",nil,150],
			["H_Cap_red",nil,150],
			["H_Cap_tan",nil,150],
			["alfr_casquette_rm",nil,1000],
			["alfr_casquette_gamewave",nil,750],
			["alfr_casquette_altisfr",nil,750],
			["cl3_Headgear_Civcap1",nil,750],
			["cl3_Headgear_Civcap2",nil,750],
			["cl3_Headgear_Civcap3",nil,750],
			["cl3_Headgear_Civcap4",nil,750],
			["cl3_Headgear_bandanna",nil,750],
			["cl3_Headgear_BaseballCap2black",nil,750],
			["cl3_Headgear_BaseballCap2green",nil,750],
			["cl3_Headgear_BaseballCap2Navyblue",nil,750],
			["cl3_Headgear_BaseballCap2pink",nil,750],
			["cl3_Headgear_BaseballCap2red",nil,750],
			["cl3_Headgear_BaseballCap2silver",nil,750],
			["H_RacingHelmet_1_red_F",nil,1000],
			["H_RacingHelmet_1_white_F",nil,1000],
			["H_RacingHelmet_1_blue_F",nil,1000],
			["H_RacingHelmet_1_yellow_F",nil,1000],
			["H_RacingHelmet_1_green_F",nil,1000],
			["H_RacingHelmet_1_F",nil,2500],
			["H_RacingHelmet_2_F",nil,2500],
			["H_RacingHelmet_3_F",nil,2500],
			["H_RacingHelmet_4_F",nil,2500],
			["cl3_Headgear_cigar",nil,2500]
			];
	};
	
	//Glasses
	case 2:
	{
		[
			["G_Shades_Black",nil,25],
			["G_Shades_Blue",nil,20],
			["G_Sport_Blackred",nil,20],
			["G_Sport_Checkered",nil,20],
			["G_Sport_Blackyellow",nil,20],
			["G_Sport_BlackWhite",nil,20],
			["G_Squares",nil,10],
			["G_Aviator",nil,100],
			["G_Lady_Mirror",nil,150],
			["G_Lady_Dark",nil,150],
			["G_Lady_Blue",nil,150],
			["G_Lowprofile",nil,30],
			["G_Combat",nil,55],
			["SFG_Tac_BeardD","Barbe",30],
			["SFG_Tac_BeardB","Barbe",30],
			["SFG_Tac_BeardG","Barbe",30],
			["SFG_Tac_BeardO","Barbe",30],
			["SFG_Tac_smallBeardD","Barbe",30],
			["SFG_Tac_smallBeardB","Barbe",30],
			["SFG_Tac_smallBeardG","Barbe",30],
			["SFG_Tac_smallBeardO","Barbe",30]
		];
	};
	
	//Vest
	case 3:
	{
		[
		];
	};
	
	//Backpacks
	case 4:
	{
		[
			["B_AssaultPack_cbr","Sac à Dos 44 Slots",2500],
			["cl3_civ_tacticalbelt","Ceinture 49 Slots",3000],
			["cl3_civ_tacticalbelt_black","Ceinture noir 49 Slots",3000],
			["B_FieldPack_ocamo","Sac à Dos 49 Slots",3000],
			["B_TacticalPack_oli","Sac à Dos 54 Slots",3500],
			["B_Kitbag_mcamo","Sac à Dos 59 Slots",4500],
			["B_Bergen_sgg","Sac à Dos 59 Slots",4500],
			["B_Kitbag_cbr","Sac à Dos 59 Slots",4500],
			["B_Carryall_oli","Sac à Dos 64 Slots",5000],
			["B_Carryall_khk","Sac à Dos 64 Slots",5000],
			["CL3_ParachutePack","Parachute",2500],
			["CL3_ParachutePackCptAmer","Parachute Captin America",2500],
			["CL3_ParachutePackPhyco","Parachute Psycho",2500],
			["CL3_ParachutePackAmerica","Parachute America",2500],
			["CL3_ParachutePackRedEye","Parachute Red Eye",2500],
			["CL3_ParachutePackToxic","Parachute Toxic",2500],
			["CL3_ParachutePackGoblin","Parachute Goblin",2500],
			["CL3_ParachutePackVista","Parachute Vista",2500],
			["CL3_ParachutePackBlueFire","Parachute Blue Fire",2500],
			["CL3_ParachutePackASU","Parachute ASU",2500]
		];
	};
};