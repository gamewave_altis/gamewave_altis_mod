/*
*/
private["_vehicle","_color","_index"];
_vehicle = [_this,0,"",[""]] call BIS_fnc_param;
_index = [_this,1,-1,[0]] call BIS_fnc_param;
_color = "";

switch (_vehicle) do
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Bike
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_bike_bmx" :
{
switch (_index) do
{
case 0: {_color = "bleu/vert";};
case 1: {_color = "violet/noir";};
case 2: {_color = "turquoise/blanc";};
case 3: {_color = "vert/jaune";};
case 4: {_color = "orange/jaune";};
case 5: {_color = "aqua";};
case 6: {_color = "bleu vif";};
case 7: {_color = "rose vif";};
case 8: {_color = "bordeaux";};
case 9: {_color = "carbone";};
case 10: {_color = "rouge cardinal";};
case 11: {_color = "vert sombre";};
case 12: {_color = "or";};
case 13: {_color = "vert";};
case 14: {_color = "gris";};
case 15: {_color = "lavende";};
case 16: {_color = "bleu clair";};
case 17: {_color = "jaune clair";};
case 18: {_color = "Jaune citron";};
case 19: {_color = "bleu marine";};
case 20: {_color = "bleu profond";};
case 21: {_color = "orange";};
case 22: {_color = "rose";};
case 23: {_color = "violet";};
case 24: {_color = "rouge";};
case 25: {_color = "sable";};
case 26: {_color = "argent";};
case 27: {_color = "noir";};
case 28: {_color = "bleu sombre";};
case 29: {_color = "camouflage";};
case 30: {_color = "camouflage urbain";};
case 31: {_color = "vert sombre";};
case 32: {_color = "rouge sombre";};
case 33: {_color = "flamme";};
case 34: {_color = "flamme2";};
case 35: {_color = "flamme3";};
case 36: {_color = "vert";};
case 37: {_color = "orange";};
case 38: {_color = "rose";};
case 39: {_color = "violet";};
case 40: {_color = "rouge";};
case 41: {_color = "argent";};
case 42: {_color = "blanc";};
case 43: {_color = "jaune";};
case 44: {_color = "bleu sombre";};
case 45: {_color = "fushia";};
case 46: {_color = "jaune";};
};
};

case "cl3_bike_mountain" :
{
switch (_index) do
{
case 0: {_color = "bleu/vert";};
case 1: {_color = "violet/noir";};
case 2: {_color = "turquoise/blanc";};
case 3: {_color = "vert/jaune";};
case 4: {_color = "orange/jaune";};
case 5: {_color = "aqua";};
case 6: {_color = "bleu vif";};
case 7: {_color = "rose vif";};
case 8: {_color = "bordeaux";};
case 9: {_color = "carbone";};
case 10: {_color = "rouge cardinal";};
case 11: {_color = "vert sombre";};
case 12: {_color = "or";};
case 13: {_color = "vert";};
case 14: {_color = "gris";};
case 15: {_color = "lavende";};
case 16: {_color = "bleu clair";};
case 17: {_color = "jaune clair";};
case 18: {_color = "Jaune citron";};
case 19: {_color = "bleu marine";};
case 20: {_color = "bleu profond";};
case 21: {_color = "orange";};
case 22: {_color = "rose";};
case 23: {_color = "violet";};
case 24: {_color = "rouge";};
case 25: {_color = "sable";};
case 26: {_color = "argent";};
case 27: {_color = "noir";};
case 28: {_color = "bleu sombre";};
case 29: {_color = "camouflage";};
case 30: {_color = "camouflage urbain";};
case 31: {_color = "vert sombre";};
case 32: {_color = "rouge sombre";};
case 33: {_color = "flamme";};
case 34: {_color = "flamme2";};
case 35: {_color = "flamme3";};
case 36: {_color = "vert";};
case 37: {_color = "orange";};
case 38: {_color = "rose";};
case 39: {_color = "violet";};
case 40: {_color = "rouge";};
case 41: {_color = "argent";};
case 42: {_color = "blanc";};
case 43: {_color = "jaune";};
case 44: {_color = "bleu sombre";};
case 45: {_color = "fushia";};
case 46: {_color = "jaune";};
};
};

case "cl3_bike_Road" :
{
switch (_index) do
{
case 0: {_color = "bleu/vert";};
case 1: {_color = "violet/noir";};
case 2: {_color = "turquoise/blanc";};
case 3: {_color = "vert/jaune";};
case 4: {_color = "orange/jaune";};
case 5: {_color = "aqua";};
case 6: {_color = "bleu vif";};
case 7: {_color = "rose vif";};
case 8: {_color = "bordeaux";};
case 9: {_color = "carbone";};
case 10: {_color = "rouge cardinal";};
case 11: {_color = "vert sombre";};
case 12: {_color = "or";};
case 13: {_color = "vert";};
case 14: {_color = "gris";};
case 15: {_color = "lavende";};
case 16: {_color = "bleu clair";};
case 17: {_color = "jaune clair";};
case 18: {_color = "Jaune citron";};
case 19: {_color = "bleu marine";};
case 20: {_color = "bleu profond";};
case 21: {_color = "orange";};
case 22: {_color = "rose";};
case 23: {_color = "violet";};
case 24: {_color = "rouge";};
case 25: {_color = "sable";};
case 26: {_color = "argent";};
case 27: {_color = "noir";};
case 28: {_color = "bleu sombre";};
case 29: {_color = "camouflage";};
case 30: {_color = "camouflage urbain";};
case 31: {_color = "vert sombre";};
case 32: {_color = "rouge sombre";};
case 33: {_color = "flamme";};
case 34: {_color = "flamme2";};
case 35: {_color = "flamme3";};
case 36: {_color = "vert";};
case 37: {_color = "orange";};
case 38: {_color = "rose";};
case 39: {_color = "violet";};
case 40: {_color = "rouge";};
case 41: {_color = "argent";};
case 42: {_color = "blanc";};
case 43: {_color = "jaune";};
case 44: {_color = "bleu sombre";};
case 45: {_color = "fushia";};
case 46: {_color = "jaune";};
};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Moto
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_enduro" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu clair";};
case 2: {_color ="rose clair";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="médecin";};
case 11: {_color ="flamme";};
case 12: {_color ="flamme2";};
case 13: {_color ="flamme3";};
case 14: {_color ="or";};
case 15: {_color ="vert";};
case 16: {_color ="gris";};
case 17: {_color ="lavende";};
case 18: {_color ="bleu vif";};
case 19: {_color ="jaune vif";};
case 20: {_color ="jaune citron";};
case 21: {_color ="bleu marine";};
case 22: {_color ="bleu navy";};
case 23: {_color ="orange";};
case 24: {_color ="Police";};
case 25: {_color ="violet";};
case 26: {_color ="rouge";};
case 27: {_color ="sable";};
case 28: {_color ="argent";};
case 29: {_color ="fushia";};
case 30: {_color ="blanc";};
case 31: {_color ="jaune";};
};
};

case "cl3_xr_1000" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu clair";};
case 2: {_color ="rose clair";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="médecin";};
case 11: {_color ="flamme";};
case 12: {_color ="flamme2";};
case 13: {_color ="flamme3";};
case 14: {_color ="or";};
case 15: {_color ="vert";};
case 16: {_color ="gris";};
case 17: {_color ="lavende";};
case 18: {_color ="bleu vif";};
case 19: {_color ="jaune vif";};
case 20: {_color ="jaune citron";};
case 21: {_color ="bleu marine";};
case 22: {_color ="bleu navy";};
case 23: {_color ="orange";};
case 24: {_color ="Police";};
case 25: {_color ="violet";};
case 26: {_color ="rouge";};
case 27: {_color ="sable";};
case 28: {_color ="argent";};
case 29: {_color ="fushia";};
case 30: {_color ="blanc";};
};
};

case "cl3_chopper" :
{
switch (_index) do
{

case 0: {_color ="bleu sombre";};
case 1: {_color ="or";};
case 2: {_color ="vert";};
case 3: {_color ="rouge";};
case 4: {_color ="argent";};
};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule low
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_440cuda" :
{
switch (_index) do
{
case 0: {_color ="noir";};
case 1: {_color ="noir";};
case 2: {_color ="bleu léger";};
case 3: {_color ="bleu sombre";};
case 4: {_color ="bleu flamme";};
case 5: {_color ="médecin";};
case 6: {_color ="12";};
case 7: {_color ="14";};
case 8: {_color ="27";};
case 9: {_color ="51";};
case 10: {_color ="55";};
case 11: {_color ="69";};
case 12: {_color ="70";};
case 13: {_color ="70";};
case 14: {_color ="vert";};
case 15: {_color ="vert logo";};
case 16: {_color ="jaune";};
case 17: {_color ="rayure jaune";};
};
};
case "cl3_civic_vti" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu clair";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="orange";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};
case "cl3_crown_victoria" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu clair";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="orange";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};
case "cl3_defender_110" :
{
switch (_index) do
{
case 0: {_color = "camouflage";};
case 1: {_color = "rouge";};
case 2: {_color = "jaune";};
};
};
case "cl3_lada" :
{
switch (_index) do
{
case 0: {_color = "rouge";};
case 1: {_color = "blanc";};
};
};
case "cl3_volha" :
{
switch (_index) do
{
case 0: {_color = "noir";};
case 1: {_color = "gris";};
};
};
case "cl3_discovery" :
{
switch (_index) do
{
case 0: {_color ="noir";};
case 1: {_color ="bleu sombre";};
case 2: {_color ="orange sombre";};
case 3: {_color ="or";};
case 4: {_color ="vert";};
case 5: {_color ="hello kitty";};
case 6: {_color ="joker";};
case 7: {_color ="rose";};
case 8: {_color ="argent";};
};
};
case "cl3_golf_mk2" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu clair";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="orange";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};
case "cl3_polo_gti" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu clair";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="orange";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};
case "beetle_custom" :
{
switch (_index) do
{
case 0: {_color ="bleu sombre";};
case 1: {_color ="Bleu Pétrole";};
case 2: {_color ="¨Rouge";};
case 3: {_color ="Vert";};
case 4: {_color ="Violet";};
case 5: {_color ="Blanc";};
case 6: {_color ="Ligne Psyché";};
case 7: {_color ="Art Psyché";};
case 8: {_color ="Coccinelle";};
case 9: {_color ="Camouflage";};
case 10: {_color ="Noir";};
};
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule Medium
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "clpd_mondeo" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="rouge bordeaux";};
case 5: {_color ="rouge cardinal";};
case 6: {_color ="vert sombre";};
case 7: {_color ="or";};
case 8: {_color ="vert";};
case 9: {_color ="gris";};
case 10: {_color ="lavende";};
case 11: {_color ="bleu clair";};
case 12: {_color ="jaune clair";};
case 13: {_color ="jaune citron";};
case 14: {_color ="bleu marine";};
case 15: {_color ="bleu navy";};
case 16: {_color ="orange";};
case 17: {_color ="rose";};
case 18: {_color ="violet";};
case 19: {_color ="rouge";};
case 20: {_color ="argent";};
case 21: {_color ="bleu ciel";};
case 22: {_color ="fushia";};
case 23: {_color ="blanc";};
case 24: {_color ="jaune";};
};
};
case "cl3_challenger_2009" :
{
switch (_index) do
{
case 0: {_color ="noir";};
case 1: {_color ="bleu sombre";};
case 2: {_color ="vert";};
case 3: {_color ="rouge";};
case 4: {_color ="argent";};
case 5: {_color ="jaune";};
case 6: {_color ="aqua";};
case 7: {_color ="beige";};
case 8: {_color ="beige2";};
case 9: {_color ="bleu sombre";};
case 10: {_color ="marron";};
case 11: {_color ="argent";};
case 12: {_color ="vert";};
case 13: {_color ="jaune citron";};
case 14: {_color ="orange";};
case 15: {_color ="orange flamme";};
case 16: {_color ="rose";};
case 17: {_color ="violet";};
case 18: {_color ="rouge";};
case 19: {_color ="jaune";};

};
};
case "cl3_dodge_charger_patrol" :
{
switch (_index) do
{
case 0: {_color ="CN";};
case 1: {_color ="EMT";};
case 2: {_color ="EMTCG";};
case 3: {_color ="EMTFD";};
case 4: {_color ="EMTMR";};
case 5: {_color ="EMTPA";};
case 6: {_color ="ETU";};
case 7: {_color ="Police K9";};
case 8: {_color ="Police blanc/noir";};
case 9: {_color ="Police noir/blanc";};
case 10: {_color ="noir";};
case 11: {_color ="bleu sombre";};
case 12: {_color ="camouflage";};
case 13: {_color ="camouflage urbain";};
case 14: {_color ="vert sombre";};
case 15: {_color ="rouge sombre";};
case 16: {_color ="vert sombre";};
case 17: {_color ="gris";};
case 18: {_color ="jaune citron";};
case 19: {_color ="orange";};
case 20: {_color ="rose";};
case 21: {_color ="violet";};
case 22: {_color ="rouge";};
case 23: {_color ="blanc";};
case 24: {_color ="jaune";};
};
};

case "cl3_e60_m5" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu clair";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="beige";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};

case "cl3_e63_amg" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu sombre";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="orange";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};

case "cl3_escalade" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="CG";};
case 10: {_color ="CN";};
case 11: {_color ="vert sombre";};
case 12: {_color ="ETU";};
case 13: {_color ="FD";};
case 14: {_color ="or";};
case 15: {_color ="vert";};
case 16: {_color ="gris";};
case 17: {_color ="Police K9";};
case 18: {_color ="lavende";};
case 19: {_color ="bleu clair";};
case 20: {_color ="jaune clair";};
case 21: {_color ="jaune citron";};
case 22: {_color ="bleu marine";};
case 23: {_color ="MR";};
case 24: {_color ="bleu navy";};
case 25: {_color ="orange";};
case 26: {_color ="Police noir/blanc";};
case 27: {_color ="Police blanc/noir";};
case 28: {_color ="PD";};
case 29: {_color ="PM";};
case 30: {_color ="violet";};
case 31: {_color ="rouge";};
case 32: {_color ="sable";};
case 33: {_color ="argent";};
case 34: {_color ="traffic";};
case 35: {_color ="violet";};
case 36: {_color ="blanc";};
case 37: {_color ="jaune";};
case 38: {_color ="Dépanneur";};
};
};
case "cl3_q7" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="Police";};
case 10: {_color ="ETU";};
case 11: {_color ="Police";};
case 12: {_color ="vert sombre";};
case 13: {_color ="orange";};
case 14: {_color ="vert sombre";};
case 15: {_color ="gris";};
case 16: {_color ="lavende";};
case 17: {_color ="bleu clair";};
case 18: {_color ="jaune clair";};
case 19: {_color ="jaune citron";};
case 20: {_color ="bleu marine";};
case 21: {_color ="bleu navy";};
case 22: {_color ="orange";};
case 23: {_color ="violet";};
case 24: {_color ="rouge";};
case 25: {_color ="sable";};
case 26: {_color ="argent";};
case 27: {_color ="fushia";};
case 28: {_color ="blanc";};
case 29: {_color ="jaune";};
};
};

case "cl3_s5" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu clair";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="orange";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};

case "cl3_suv" :
{
switch (_index) do
{
case 0: {_color ="noir";};
case 1: {_color ="médecin";};
case 2: {_color ="taxi";};
};
};

case "cl3_taurus" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu clair";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="orange";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};

case "cl3_transit" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="civil";};
case 10: {_color ="Police";};
case 11: {_color ="vert sombre";};
case 12: {_color ="or";};
case 13: {_color ="vert";};
case 14: {_color ="gris";};
case 15: {_color ="lavende";};
case 16: {_color ="bleu clair";};
case 17: {_color ="jaune clair";};
case 18: {_color ="jaune citron";};
case 19: {_color ="bleu marine";};
case 20: {_color ="bleu navy";};
case 21: {_color ="orange";};
case 22: {_color ="violet";};
case 23: {_color ="rouge";};
case 24: {_color ="sable";};
case 25: {_color ="argent";};
case 26: {_color ="fushia";};
case 27: {_color ="blanc";};
case 28: {_color ="jaune";};
case 29: {_color ="médecin";};
case 30: {_color ="Police K9";};
case 31: {_color ="AltisNews";};
case 32: {_color ="Police";};
};
};

case "cl3_z4_2008" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu clair";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="orange";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule High
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_458" :
{
switch (_index) do
{
case 0: {_color ="bleu/vert";};
case 1: {_color ="violet/noir";};
case 2: {_color ="turquoise/blanc";};
case 3: {_color ="vert/jaune";};
case 4: {_color ="orange/jaune";};
case 5: {_color ="aqua";};
case 6: {_color ="bleu vif";};
case 7: {_color ="rose vif";};
case 8: {_color ="noir";};
case 9: {_color ="bleu sombre";};
case 10: {_color ="rouge bordeaux";};
case 11: {_color ="camouflage";};
case 12: {_color ="camouflage urbain";};
case 13: {_color ="rouge cardinal";};
case 14: {_color ="vert sombre";};
case 15: {_color ="flamme";};
case 16: {_color ="flamme2";};
case 17: {_color ="flamme3";};
case 18: {_color ="or";};
case 19: {_color ="vert";};
case 20: {_color ="gri";};
case 21: {_color ="lavende";};
case 22: {_color ="bleu clair";};
case 23: {_color ="jaune clair";};
case 24: {_color ="jaune citron";};
case 25: {_color ="bleu marine";};
case 26: {_color ="bleu navy";};
case 27: {_color ="orange";};
case 28: {_color ="violet";};
case 29: {_color ="rouge";};
case 30: {_color ="sable";};
case 31: {_color ="argent";};
case 32: {_color ="fushia";};
case 33: {_color ="blanc";};
case 34: {_color ="jaune";};
};
};
case "cl3_aventador_lp7004" :
{
switch (_index) do
{
case 0: {_color ="bleu/vert";};
case 1: {_color ="violet/noir";};
case 2: {_color ="turquoise/blanc";};
case 3: {_color ="vert/jaune";};
case 4: {_color ="orange/jaune";};
case 5: {_color ="aqua";};
case 6: {_color ="bleu vif";};
case 7: {_color ="rose vif";};
case 8: {_color ="noir";};
case 9: {_color ="bleu sombre";};
case 10: {_color ="rouge bordeaux";};
case 11: {_color ="camouflage";};
case 12: {_color ="camouflage urbain";};
case 13: {_color ="rouge cardinal";};
case 14: {_color ="vert sombre";};
case 15: {_color ="flamme";};
case 16: {_color ="flamme2";};
case 17: {_color ="flamme3";};
case 18: {_color ="or";};
case 19: {_color ="vert";};
case 20: {_color ="gri";};
case 21: {_color ="lavende";};
case 22: {_color ="bleu clair";};
case 23: {_color ="jaune clair";};
case 24: {_color ="jaune citron";};
case 25: {_color ="bleu marine";};
case 26: {_color ="bleu navy";};
case 27: {_color ="orange";};
case 28: {_color ="violet";};
case 29: {_color ="rouge";};
case 30: {_color ="sable";};
case 31: {_color ="argent";};
case 32: {_color ="fushia";};
case 33: {_color ="blanc";};
case 34: {_color ="jaune";};
};
};
case "cl3_carrera_gt" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu clair";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="orange";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};
case "cl3_dbs_volante" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="flamme";};
case 11: {_color ="flamme2";};
case 12: {_color ="flamme3";};
case 13: {_color ="or";};
case 14: {_color ="vert";};
case 15: {_color ="gris";};
case 16: {_color ="lavende";};
case 17: {_color ="bleu clair";};
case 18: {_color ="jaune clair";};
case 19: {_color ="jaune citron";};
case 20: {_color ="bleu marine";};
case 21: {_color ="bleu navy";};
case 22: {_color ="orange";};
case 23: {_color ="violet";};
case 24: {_color ="rouge";};
case 25: {_color ="sable";};
case 26: {_color ="argent";};
case 27: {_color ="fushia";};
case 28: {_color ="blanc";};
case 29: {_color ="jaune";};
};
};
case "cl3_impreza_rally" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="flamme";};
case 11: {_color ="flamme2";};
case 12: {_color ="flamme3";};
case 13: {_color ="or";};
case 14: {_color ="vert";};
case 15: {_color ="gris";};
case 16: {_color ="lavende";};
case 17: {_color ="bleu clair";};
case 18: {_color ="jaune clair";};
case 19: {_color ="jaune citron";};
case 20: {_color ="bleu marine";};
case 21: {_color ="bleu navy";};
case 22: {_color ="orange";};
case 23: {_color ="violet";};
case 24: {_color ="rouge";};
case 25: {_color ="sable";};
case 26: {_color ="argent";};
case 27: {_color ="fushia";};
case 28: {_color ="blanc";};
case 29: {_color ="jaune";};
};
};
case "cl3_impreza_road" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="flamme";};
case 11: {_color ="flamme2";};
case 12: {_color ="flamme3";};
case 13: {_color ="or";};
case 14: {_color ="vert";};
case 15: {_color ="gris";};
case 16: {_color ="lavende";};
case 17: {_color ="bleu clair";};
case 18: {_color ="jaune clair";};
case 19: {_color ="jaune citron";};
case 20: {_color ="livraison 1";};
case 21: {_color ="livraison 2";};
case 22: {_color ="livraison 3";};
case 23: {_color ="livraison 4";};
case 24: {_color ="livraison 5";};
case 25: {_color ="bleu marine";};
case 26: {_color ="bleu navy";};
case 27: {_color ="orange";};
case 28: {_color ="violet";};
case 29: {_color ="rouge";};
case 30: {_color ="sable";};
case 31: {_color ="argent";};
case 32: {_color ="fushia";};
case 33: {_color ="blanc";};
case 34: {_color ="jaune";};
};
};
case "cl3_insignia" :
{
switch (_index) do
{
case 0: {_color ="aqua";};
case 1: {_color ="bleu vif";};
case 2: {_color ="rose vif";};
case 3: {_color ="noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge bordeaux";};
case 6: {_color ="camouflage";};
case 7: {_color ="camouflage urbain";};
case 8: {_color ="rouge cardinal";};
case 9: {_color ="vert sombre";};
case 10: {_color ="orange";};
case 11: {_color ="vert sombre";};
case 12: {_color ="gris";};
case 13: {_color ="lavende";};
case 14: {_color ="bleu clair";};
case 15: {_color ="jaune clair";};
case 16: {_color ="jaune citron";};
case 17: {_color ="bleu marine";};
case 18: {_color ="bleu navy";};
case 19: {_color ="orange";};
case 20: {_color ="violet";};
case 21: {_color ="rouge";};
case 22: {_color ="sable";};
case 23: {_color ="argent";};
case 24: {_color ="fushia";};
case 25: {_color ="blanc";};
case 26: {_color ="jaune";};
};
};
case "cl3_lamborghini_gt1" :
{
switch (_index) do
{
case 0: {_color ="bleu/vert";};
case 1: {_color ="violet/noir";};
case 2: {_color ="turquoise/blanc";};
case 3: {_color ="vert/jaune";};
case 4: {_color ="orange/jaune";};
case 5: {_color ="aqua";};
case 6: {_color ="bleu vif";};
case 7: {_color ="rose vif";};
case 8: {_color ="noir";};
case 9: {_color ="bleu sombre";};
case 10: {_color ="rouge bordeaux";};
case 11: {_color ="camouflage";};
case 12: {_color ="camouflage urbain";};
case 13: {_color ="rouge cardinal";};
case 14: {_color ="vert sombre";};
case 15: {_color ="flamme";};
case 16: {_color ="flamme2";};
case 17: {_color ="flamme3";};
case 18: {_color ="or";};
case 19: {_color ="vert";};
case 20: {_color ="gri";};
case 21: {_color ="lavende";};
case 22: {_color ="bleu clair";};
case 23: {_color ="jaune clair";};
case 24: {_color ="jaune citron";};
case 25: {_color ="bleu marine";};
case 26: {_color ="bleu navy";};
case 27: {_color ="orange";};
case 28: {_color ="violet";};
case 29: {_color ="rouge";};
case 30: {_color ="sable";};
case 31: {_color ="argent";};
case 32: {_color ="fushia";};
case 33: {_color ="blanc";};
case 34: {_color ="jaune";};
};
};

case "cl3_murcielago" :
{
switch (_index) do
{
case 0: {_color ="bleu/vert";};
case 1: {_color ="violet/noir";};
case 2: {_color ="turquoise/blanc";};
case 3: {_color ="vert/jaune";};
case 4: {_color ="orange/jaune";};
case 5: {_color ="aqua";};
case 6: {_color ="bleu vif";};
case 7: {_color ="rose vif";};
case 8: {_color ="noir";};
case 9: {_color ="bleu sombre";};
case 10: {_color ="rouge bordeaux";};
case 11: {_color ="camouflage";};
case 12: {_color ="camouflage urbain";};
case 13: {_color ="rouge cardinal";};
case 14: {_color ="vert sombre";};
case 15: {_color ="flamme";};
case 16: {_color ="flamme2";};
case 17: {_color ="flamme3";};
case 18: {_color ="or";};
case 19: {_color ="vert";};
case 20: {_color ="gri";};
case 21: {_color ="lavende";};
case 22: {_color ="bleu clair";};
case 23: {_color ="jaune clair";};
case 24: {_color ="jaune citron";};
case 25: {_color ="bleu marine";};
case 26: {_color ="bleu navy";};
case 27: {_color ="orange";};
case 28: {_color ="violet";};
case 29: {_color ="rouge";};
case 30: {_color ="sable";};
case 31: {_color ="argent";};
case 32: {_color ="fushia";};
case 33: {_color ="blanc";};
case 34: {_color ="jaune";};
};
};

case "cl3_r8_spyder" :
{
switch (_index) do
{
case 0: {_color ="bleu/vert";};
case 1: {_color ="violet/noir";};
case 2: {_color ="turquoise/blanc";};
case 3: {_color ="vert/jaune";};
case 4: {_color ="orange/jaune";};
case 5: {_color ="aqua";};
case 6: {_color ="bleu vif";};
case 7: {_color ="rose vif";};
case 8: {_color ="noir";};
case 9: {_color ="bleu sombre";};
case 10: {_color ="rouge bordeaux";};
case 11: {_color ="camouflage";};
case 12: {_color ="camouflage urbain";};
case 13: {_color ="rouge cardinal";};
case 14: {_color ="vert sombre";};
case 15: {_color ="flamme";};
case 16: {_color ="flamme2";};
case 17: {_color ="flamme3";};
case 18: {_color ="or";};
case 19: {_color ="vert";};
case 20: {_color ="gri";};
case 21: {_color ="lavende";};
case 22: {_color ="bleu clair";};
case 23: {_color ="jaune clair";};
case 24: {_color ="jaune citron";};
case 25: {_color ="bleu marine";};
case 26: {_color ="bleu navy";};
case 27: {_color ="orange";};
case 28: {_color ="violet";};
case 29: {_color ="rouge";};
case 30: {_color ="sable";};
case 31: {_color ="argent";};
case 32: {_color ="fushia";};
case 33: {_color ="blanc";};
case 34: {_color ="jaune";};
};
};

case "cl3_reventon" :
{
switch (_index) do
{
case 0: {_color ="bleu/vert";};
case 1: {_color ="violet/noir";};
case 2: {_color ="turquoise/blanc";};
case 3: {_color ="vert/jaune";};
case 4: {_color ="orange/jaune";};
case 5: {_color ="aqua";};
case 6: {_color ="bleu vif";};
case 7: {_color ="rose vif";};
case 8: {_color ="noir";};
case 9: {_color ="bleu sombre";};
case 10: {_color ="rouge bordeaux";};
case 11: {_color ="camouflage";};
case 12: {_color ="camouflage urbain";};
case 13: {_color ="rouge cardinal";};
case 14: {_color ="Police";};
case 15: {_color ="Police trafic";};
case 16: {_color ="vert sombre";};
case 17: {_color ="flamme";};
case 18: {_color ="flamme2";};
case 19: {_color ="flamme3";};
case 20: {_color ="or";};
case 21: {_color ="vert";};
case 22: {_color ="gri";};
case 23: {_color ="lavende";};
case 24: {_color ="bleu clair";};
case 25: {_color ="jaune clair";};
case 26: {_color ="jaune citron";};
case 27: {_color ="bleu marine";};
case 28: {_color ="bleu navy";};
case 29: {_color ="orange";};
case 30: {_color ="violet";};
case 31: {_color ="rouge";};
case 32: {_color ="sable";};
case 33: {_color ="argent";};
case 34: {_color ="fushia";};
case 35: {_color ="blanc";};
case 36: {_color ="jaune";};
};
};
case "cl3_veyron" :
{
switch (_index) do
{
case 0: {_color ="noir";};
case 1: {_color ="blanc/noir";};
case 2: {_color ="rouge/noir";};
case 3: {_color ="bleu/noir";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="rouge";};
case 6: {_color ="turquoise/blanc";};
case 7: {_color ="rouge bordeaux";};
};
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule FUN
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule FUN
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "CL3_bus_cl" :
{
switch (_index) do
{
case 0: {_color ="noir";};
case 1: {_color ="bleu sombre";};
case 2: {_color ="vert";};
case 3: {_color ="gris";};
case 4: {_color ="prison";};
case 5: {_color ="violet";};
case 6: {_color ="jaune";};
};
};

case "cl3_mackr_del" :
{
switch (_index) do
{
case 0: {_color ="americain";};
case 1: {_color ="noir";};
case 2: {_color ="noir/or";};
case 3: {_color ="noir/blanc";};
case 4: {_color ="bleu sombre";};
case 5: {_color ="marron camouflage";};
case 6: {_color ="forêt camouflage";};
case 7: {_color ="vert/blanc";};
case 8: {_color ="multi couleurs";};
case 9: {_color ="optimus prime";};
case 10: {_color ="orange/blanc";};
case 11: {_color ="violet/blanc";};
case 12: {_color ="rouge/blanc";};
case 13: {_color ="argent";};
};
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Aviation
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_GNT_C185_base" :
{
switch (_index) do
{
case 0: {_color ="blanc";};
case 1: {_color ="jaune/rouge";};
case 2: {_color ="jaune";};
case 3: {_color ="blanc/bleu";};
case 4: {_color ="pale";};
case 5: {_color ="jaune camouflage";};
};
};
case "GNT_C185F" :
{
switch (_index) do
{
case 0: {_color ="Base";};
};
};

case "IVORY_CRJ200_1" :
{
switch (_index) do
{
case 0: {_color ="Base";};
};
};

case "IVORY_ERJ135_1" :
{
switch (_index) do
{
case 0: {_color ="Base";};
};
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Depanneur
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_f150repo" :
{
switch (_index) do
{
case 0: {_color ="noir";};
case 1: {_color ="bleu sombre";};
case 2: {_color ="gris";};
case 3: {_color ="vert";};
case 4: {_color ="orange";};
case 5: {_color ="rouge";};
};
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Rigolol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_batmobile" :
{
switch (_index) do
{
case 0: {_color ="Batmobile";};
};
};





};

_color;