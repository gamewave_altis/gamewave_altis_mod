/*
*/
private["_vehicletype","_ret","_path"];
_vehicletype = [_this,0,"",[""]] call BIS_fnc_param;
if(_vehicletype == "") exitWith {[]};
_ret = [];

switch (_vehicletype) do
{
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Bike
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_bike_bmx":
{
_ret =
[
["cl3_bike_bmx_Road2tone1","civ"],
["cl3_bike_bmx_Road2tone2","civ"],
["cl3_bike_bmx_Road2tone3","civ"],
["cl3_bike_bmx_Road2tone4","civ"],
["cl3_bike_bmx_Road2tone5","civ"],
["cl3_bike_bmx_Roadaqua","civ"],
["cl3_bike_bmx_Roadbaby_blue","civ"],
["cl3_bike_bmx_Roadbaby_pink","civ"],
["cl3_bike_bmx_Roadburgundy","civ"],
["cl3_bike_bmx_Roadcarbon","civ"],
["cl3_bike_bmx_Roadcardinal","civ"],
["cl3_bike_bmx_Roaddark_green","civ"],
["cl3_bike_bmx_Roadgold","civ"],
["cl3_bike_bmx_Roadgreen","civ"],
["cl3_bike_bmx_Roadgrey","civ"],
["cl3_bike_bmx_Roadlavender","civ"],
["cl3_bike_bmx_Roadlight_blue","civ"],
["cl3_bike_bmx_Roadlight_yellow","civ"],
["cl3_bike_bmx_Roadlime","civ"],
["cl3_bike_bmx_Roadmarina_blue","civ"],
["cl3_bike_bmx_Roadnavy_blue","civ"],
["cl3_bike_bmx_Roadorange","civ"],
["cl3_bike_bmx_Roadpink","civ"],
["cl3_bike_bmx_Roadpurple","civ"],
["cl3_bike_bmx_Roadred","civ"],
["cl3_bike_bmx_Roadsand","civ"],
["cl3_bike_bmx_Roadsilver","civ"],
["cl3_bike_bmx_Roadskin_black","cop"],
["cl3_bike_bmx_Roadskin_blue","civ"],
["cl3_bike_bmx_Roadskin_camo","reb"],
["cl3_bike_bmx_Roadskin_camo_urban","merc"],
["cl3_bike_bmx_Roadskin_darkgreen","civ"],
["cl3_bike_bmx_Roadskin_darkred","civ"],
["cl3_bike_bmx_Roadskin_flame","civ"],
["cl3_bike_bmx_Roadskin_flame1","civ"],
["cl3_bike_bmx_Roadskin_flame2","civ"],
["cl3_bike_bmx_Roadskin_green","civ"],
["cl3_bike_bmx_Roadskin_orange","civ"],
["cl3_bike_bmx_Roadskin_pink","civ"],
["cl3_bike_bmx_Roadskin_purple","civ"],
["cl3_bike_bmx_Roadskin_red","civ"],
["cl3_bike_bmx_Roadskin_silver","civ"],
["cl3_bike_bmx_Roadskin_white","civ"],
["cl3_bike_bmx_Roadskin_yellow","civ"],
["cl3_bike_bmx_Roadsky_blue","civ"],
["cl3_bike_bmx_Roadviolet","civ"],
["cl3_bike_bmx_Roadyellow","civ"]
];
};
case "cl3_bike_mountain":
{
_ret =
[
["cl3_bike_mountain2tone1","civ"],
["cl3_bike_mountain2tone2","civ"],
["cl3_bike_mountain2tone3","civ"],
["cl3_bike_mountain2tone4","civ"],
["cl3_bike_mountain2tone5","civ"],
["cl3_bike_mountainaqua","civ"],
["cl3_bike_mountainbaby_blue","civ"],
["cl3_bike_mountainbaby_pink","civ"],
["cl3_bike_mountainburgundy","civ"],
["cl3_bike_mountaincarbon","civ"],
["cl3_bike_mountaincardinal","civ"],
["cl3_bike_mountaindark_green","civ"],
["cl3_bike_mountaingold","civ"],
["cl3_bike_mountaingreen","civ"],
["cl3_bike_mountaingrey","civ"],
["cl3_bike_mountainlavender","civ"],
["cl3_bike_mountainlight_blue","civ"],
["cl3_bike_mountainlight_yellow","civ"],
["cl3_bike_mountainlime","civ"],
["cl3_bike_mountainmarina_blue","civ"],
["cl3_bike_mountainnavy_blue","civ"],
["cl3_bike_mountainorange","civ"],
["cl3_bike_mountainpink","civ"],
["cl3_bike_mountainpurple","civ"],
["cl3_bike_mountainred","civ"],
["cl3_bike_mountainsand","civ"],
["cl3_bike_mountainsilver","civ"],
["cl3_bike_mountainskin_black","cop"],
["cl3_bike_mountainskin_blue","civ"],
["cl3_bike_mountainskin_camo","reb"],
["cl3_bike_mountainskin_camo_urban","merc"],
["cl3_bike_mountainskin_darkgreen","civ"],
["cl3_bike_mountainskin_darkred","civ"],
["cl3_bike_mountainskin_flame","civ"],
["cl3_bike_mountainskin_flame1","civ"],
["cl3_bike_mountainskin_flame2","civ"],
["cl3_bike_mountainskin_green","civ"],
["cl3_bike_mountainskin_orange","civ"],
["cl3_bike_mountainskin_pink","civ"],
["cl3_bike_mountainskin_purple","civ"],
["cl3_bike_mountainskin_red","civ"],
["cl3_bike_mountainskin_silver","civ"],
["cl3_bike_mountainskin_white","civ"],
["cl3_bike_mountainskin_yellow","civ"],
["cl3_bike_mountainsky_blue","civ"],
["cl3_bike_mountainviolet","civ"],
["cl3_bike_mountainyellow","civ"]
];
};

case "cl3_bike_Road":
{
_ret =
[
["cl3_bike_Road2tone1","civ"],
["cl3_bike_Road2tone2","civ"],
["cl3_bike_Road2tone3","civ"],
["cl3_bike_Road2tone4","civ"],
["cl3_bike_Road2tone5","civ"],
["cl3_bike_Roadaqua","civ"],
["cl3_bike_Roadbaby_blue","civ"],
["cl3_bike_Roadbaby_pink","civ"],
["cl3_bike_Roadburgundy","civ"],
["cl3_bike_Roadcarbon","civ"],
["cl3_bike_Roadcardinal","civ"],
["cl3_bike_Roaddark_green","civ"],
["cl3_bike_Roadgold","civ"],
["cl3_bike_Roadgreen","civ"],
["cl3_bike_Roadgrey","civ"],
["cl3_bike_Roadlavender","civ"],
["cl3_bike_Roadlight_blue","civ"],
["cl3_bike_Roadlight_yellow","civ"],
["cl3_bike_Roadlime","civ"],
["cl3_bike_Roadmarina_blue","civ"],
["cl3_bike_Roadnavy_blue","civ"],
["cl3_bike_Roadorange","civ"],
["cl3_bike_Roadpink","civ"],
["cl3_bike_Roadpurple","civ"],
["cl3_bike_Roadred","civ"],
["cl3_bike_Roadsand","civ"],
["cl3_bike_Roadsilver","civ"],
["cl3_bike_Roadskin_black","cop"],
["cl3_bike_Roadskin_blue","civ"],
["cl3_bike_Roadskin_camo","reb"],
["cl3_bike_Roadskin_camo_urban","merc"],
["cl3_bike_Roadskin_darkgreen","civ"],
["cl3_bike_Roadskin_darkred","civ"],
["cl3_bike_Roadskin_flame","civ"],
["cl3_bike_Roadskin_flame1","civ"],
["cl3_bike_Roadskin_flame2","civ"],
["cl3_bike_Roadskin_green","civ"],
["cl3_bike_Roadskin_orange","civ"],
["cl3_bike_Roadskin_pink","civ"],
["cl3_bike_Roadskin_purple","civ"],
["cl3_bike_Roadskin_red","civ"],
["cl3_bike_Roadskin_silver","civ"],
["cl3_bike_Roadskin_white","civ"],
["cl3_bike_Roadskin_yellow","civ"],
["cl3_bike_Roadsky_blue","civ"],
["cl3_bike_Roadviolet","civ"],
["cl3_bike_Roadyellow","civ"]
];
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Moto
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_enduro":
{
_ret =
[
["cl3_enduro_aqua","civ"],
["cl3_enduro_babyblue","civ"],
["cl3_enduro_babypink","civ"],
["cl3_enduro_black","civ"],
["cl3_enduro_blue","civ"],
["cl3_enduro_burgundy","civ"],
["cl3_enduro_camo","reb"],
["cl3_enduro_camo_urban","merc"],
["cl3_enduro_cardinal","civ"],
["cl3_enduro_dark_green","civ"],
["cl3_enduro_emt","civ"],
["cl3_enduro_flame","civ"],
["cl3_enduro_flame1","civ"],
["cl3_enduro_flame2","civ"],
["cl3_enduro_gold","civ"],
["cl3_enduro_green","civ"],
["cl3_enduro_grey","civ"],
["cl3_enduro_lavender","civ"],
["cl3_enduro_light_blue","civ"],
["cl3_enduro_light_yellow","civ"],
["cl3_enduro_lime","civ"],
["cl3_enduro_marina_blue","civ"],
["cl3_enduro_navy_blue","civ"],
["cl3_enduro_orange","civ"],
["cl3_enduro_police","cop"],
["cl3_enduro_purple","civ"],
["cl3_enduro_red","civ"],
["cl3_enduro_sand","civ"],
["cl3_enduro_silver","civ"],
["cl3_enduro_violet","civ"],
["cl3_enduro_white","civ"],
["cl3_enduro_yellow","civ"]
];

};
case "cl3_xr_1000":
{
_ret =
[
["cl3_xr_1000_aqua","civ"],
["cl3_xr_1000_babyblue","civ"],
["cl3_xr_1000_babypink","civ"],
["cl3_xr_1000_black","civ"],
["cl3_xr_1000_blue","civ"],
["cl3_xr_1000_burgundy","civ"],
["cl3_xr_1000_camo","reb"],
["cl3_xr_1000_camo_urban","merc"],
["cl3_xr_1000_cardinal","civ"],
["cl3_xr_1000_dark_green","civ"],
["cl3_xr_1000_emt","civ"],
["cl3_xr_1000_flame","civ"],
["cl3_xr_1000_flame1","civ"],
["cl3_xr_1000_flame2","civ"],
["cl3_xr_1000_gold","civ"],
["cl3_xr_1000_green","civ"],
["cl3_xr_1000_grey","civ"],
["cl3_xr_1000_lavender","civ"],
["cl3_xr_1000_light_blue","civ"],
["cl3_xr_1000_light_yellow","civ"],
["cl3_xr_1000_lime","civ"],
["cl3_xr_1000_marina_blue","civ"],
["cl3_xr_1000_navy_blue","civ"],
["cl3_xr_1000_orange","civ"],
["cl3_xr_1000_police","cop"],
["cl3_xr_1000_purple","civ"],
["cl3_xr_1000_red","civ"],
["cl3_xr_1000_sand","civ"],
["cl3_xr_1000_silver","civ"],
["cl3_xr_1000_violet","civ"],
["cl3_xr_1000_white","civ"]
];
};

case "cl3_chopper":
{
_ret =
[
["cl3_chopper_blue","civ"],
["cl3_chopper_gold","civ"],
["cl3_chopper_green","civ"],
["cl3_chopper_red","civ"],
["cl3_chopper_silver","civ"]
];
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule low
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_440cuda":
{
_ret =
[
["cl3_440cuda_black","civ"],
["cl3_440cuda_black1","civ"],
["cl3_440cuda_blu_ltn","civ"],
["cl3_440cuda_blue","civ"],
["cl3_440cuda_blue_flame","civ"],
["cl3_440cuda_emt_chief","civ"],
["cl3_440cuda_flannery08","civ"],
["cl3_440cuda_flannery12","civ"],
["cl3_440cuda_flannery14","civ"],
["cl3_440cuda_flannery27","civ"],
["cl3_440cuda_flannery51","civ"],
["cl3_440cuda_flannery55","civ"],
["cl3_440cuda_flannery69","civ"],
["cl3_440cuda_flannery70","civ"],
["cl3_440cuda_green","civ"],
["cl3_440cuda_green_Logo","civ"],
["cl3_440cuda_yellow","civ"],
["cl3_440cuda_yellow_stripe","civ"]
];
};

case "cl3_civic_vti":
{
_ret =
[
["cl3_civic_vti_aqua","civ"],
["cl3_civic_vti_babyblue","civ"],
["cl3_civic_vti_babypink","civ"],
["cl3_civic_vti_black","civ"],
["cl3_civic_vti_blue","civ"],
["cl3_civic_vti_burgundy","civ"],
["cl3_civic_vti_camo","reb"],
["cl3_civic_vti_camo_urban","merc"],
["cl3_civic_vti_cardinal","civ"],
["cl3_civic_vti_dark_green","civ"],
["cl3_civic_vti_gold","civ"],
["cl3_civic_vti_green","civ"],
["cl3_civic_vti_grey","civ"],
["cl3_civic_vti_lavender","civ"],
["cl3_civic_vti_light_blue","civ"],
["cl3_civic_vti_light_yellow","civ"],
["cl3_civic_vti_lime","civ"],
["cl3_civic_vti_marina_blue","civ"],
["cl3_civic_vti_navy_blue","civ"],
["cl3_civic_vti_orange","civ"],
["cl3_civic_vti_purple","civ"],
["cl3_civic_vti_red","civ"],
["cl3_civic_vti_sand","civ"],
["cl3_civic_vti_silver","civ"],
["cl3_civic_vti_violet","civ"],
["cl3_civic_vti_white","civ"],
["cl3_civic_vti_yellow","civ"]
];
};

case "cl3_crown_victoria":
{
_ret =
[
["cl3_crown_victoria_aqua","civ"],
["cl3_crown_victoria_babyblue","civ"],
["cl3_crown_victoria_babypink","civ"],
["cl3_crown_victoria_black","civ"],
["cl3_crown_victoria_blue","civ"],
["cl3_crown_victoria_burgundy","civ"],
["cl3_crown_victoria_camo","reb"],
["cl3_crown_victoria_camo_urban","merc"],
["cl3_crown_victoria_cardinal","civ"],
["cl3_crown_victoria_dark_green","civ"],
["cl3_crown_victoria_gold","civ"],
["cl3_crown_victoria_green","civ"],
["cl3_crown_victoria_grey","civ"],
["cl3_crown_victoria_lavender","civ"],
["cl3_crown_victoria_light_blue","civ"],
["cl3_crown_victoria_light_yellow","civ"],
["cl3_crown_victoria_lime","civ"],
["cl3_crown_victoria_marina_blue","civ"],
["cl3_crown_victoria_navy_blue","civ"],
["cl3_crown_victoria_orange","civ"],
["cl3_crown_victoria_purple","civ"],
["cl3_crown_victoria_red","civ"],
["cl3_crown_victoria_sand","civ"],
["cl3_crown_victoria_silver","civ"],
["cl3_crown_victoria_violet","civ"],
["cl3_crown_victoria_white","civ"],
["cl3_crown_victoria_yellow","civ"]
];
};

case "cl3_defender_100":
{
_ret =
[
["cl3_defender_110_cammo","civ"],
["cl3_defender_110_red","civ"],
["cl3_defender_110_yellow","civ"]
];
};
case "cl3_lada":
{
_ret =
[
["cl3_lada_red","civ"],
["cl3_lada_white","civ"]
];
};
case "cl3_volha":
{
_ret =
[
["cl3_volha_black","civ"],
["cl3_volha_grey","civ"]
];
};

case "cl3_discovery":
{
_ret =
[
["cl3_discovery_black","civ"],
["cl3_discovery_blue","civ"],
["cl3_discovery_darkorange","civ"],
["cl3_discovery_gold","civ"],
["cl3_discovery_green","civ"],
["cl3_discovery_hellokitty","unkown"],
["cl3_discovery_joker","unkown"],
["cl3_discovery_pink","civ"],
["cl3_discovery_silver","civ"]
];
};
case "cl3_golf_mk2":
{
_ret =
[
["cl3_golf_mk2_aqua","civ"],
["cl3_golf_mk2_babyblue","civ"],
["cl3_golf_mk2_babypink","civ"],
["cl3_golf_mk2_black","civ"],
["cl3_golf_mk2_blue","civ"],
["cl3_golf_mk2_burgundy","civ"],
["cl3_golf_mk2_camo","reb"],
["cl3_golf_mk2_camo_urban","merc"],
["cl3_golf_mk2_cardinal","civ"],
["cl3_golf_mk2_dark_green","civ"],
["cl3_golf_mk2_gold","civ"],
["cl3_golf_mk2_green","civ"],
["cl3_golf_mk2_grey","civ"],
["cl3_golf_mk2_lavender","civ"],
["cl3_golf_mk2_light_blue","civ"],
["cl3_golf_mk2_light_yellow","civ"],
["cl3_golf_mk2_lime","civ"],
["cl3_golf_mk2_marina_blue","civ"],
["cl3_golf_mk2_navy_blue","civ"],
["cl3_golf_mk2_orange","civ"],
["cl3_golf_mk2_purple","civ"],
["cl3_golf_mk2_red","civ"],
["cl3_golf_mk2_sand","civ"],
["cl3_golf_mk2_silver","civ"],
["cl3_golf_mk2_violet","civ"],
["cl3_golf_mk2_white","civ"],
["cl3_golf_mk2_yellow","civ"]
];
};
case "cl3_polo_gti":
{
_ret =
[
["cl3_polo_gti_aqua","civ"],
["cl3_polo_gti_babyblue","civ"],
["cl3_polo_gti_babypink","civ"],
["cl3_polo_gti_black","civ"],
["cl3_polo_gti_blue","civ"],
["cl3_polo_gti_burgundy","civ"],
["cl3_polo_gti_camo","reb"],
["cl3_polo_gti_camo_urban","merc"],
["cl3_polo_gti_cardinal","civ"],
["cl3_polo_gti_dark_green","civ"],
["cl3_polo_gti_gold","civ"],
["cl3_polo_gti_green","civ"],
["cl3_polo_gti_grey","civ"],
["cl3_polo_gti_lavender","civ"],
["cl3_polo_gti_light_blue","civ"],
["cl3_polo_gti_light_yellow","civ"],
["cl3_polo_gti_lime","civ"],
["cl3_polo_gti_marina_blue","civ"],
["cl3_polo_gti_navy_blue","civ"],
["cl3_polo_gti_orange","civ"],
["cl3_polo_gti_purple","civ"],
["cl3_polo_gti_red","civ"],
["cl3_polo_gti_sand","civ"],
["cl3_polo_gti_silver","civ"],
["cl3_polo_gti_violet","civ"],
["cl3_polo_gti_white","civ"],
["cl3_polo_gti_yellow","civ"]
];
};

case "beetle_custom":
{
_ret =
[
["beetle_bleufonce","civ"],
["beetle_bleupetrole","civ"],
["beetle_red","civ"],
["beetle_vert","civ"],
["beetle_violet","civ"],
["beetle_white","civ"],
["beetle_psycha","civ"],
["beetle_psycha1","civ"],
["beetle_coci","civ"],
["beetle_camo","reb"],
["beetle","civ"]
];
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule medium
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "clpd_mondeo":
{
_ret =
[
["civ_mondeo_Aqua","civ"],
["civ_mondeo_BabyBlue","civ"],
["civ_mondeo_BabyPink","civ"],
["civ_mondeo_Black","civ"],
["civ_mondeo_Burgundy","civ"],
["civ_mondeo_CardinalBurgundy","civ"],
["civ_mondeo_DarkGreen","civ"],
["civ_mondeo_Gold","civ"],
["civ_mondeo_Green","civ"],
["civ_mondeo_Grey","civ"],
["civ_mondeo_Lavendel","civ"],
["civ_mondeo_LightBlue","civ"],
["civ_mondeo_LightYellow","civ"],
["civ_mondeo_Lime","civ"],
["civ_mondeo_MarineBlue","civ"],
["civ_mondeo_NavyBlue","civ"],
["civ_mondeo_Orange","civ"],
["civ_mondeo_Pink","civ"],
["civ_mondeo_Purple","civ"],
["civ_mondeo_Red","civ"],
["civ_mondeo_Silver","civ"],
["civ_mondeo_SkyBlue","civ"],
["civ_mondeo_Violet","civ"],
["civ_mondeo_White","civ"],
["civ_mondeo_Yellow","civ"]
];
};
case "cl3_challenger_2009":
{
_ret =
[
["cl3_challenger_2009_black","civ"],
["cl3_challenger_2009_blue","civ"],
["cl3_challenger_2009_green","civ"],
["cl3_challenger_2009_red","civ"],
["cl3_challenger_2009_silver","civ"],
["cl3_challenger_2009_yellow","civ"],
["cl3_challenger_aquablue","civ"],
["cl3_challenger_beige","civ"],
["cl3_challenger_beige2","civ"],
["cl3_challenger_blue","civ"],
["cl3_challenger_brown","civ"],
["cl3_challenger_darksilver","civ"],
["cl3_challenger_green","civ"],
["cl3_challenger_lime","civ"],
["cl3_challenger_orange","civ"],
["cl3_challenger_orangeflame","civ"],
["cl3_challenger_pink","civ"],
["cl3_challenger_purple","civ"],
["cl3_challenger_red","civ"],
["cl3_challenger_yellow","civ"]
];
};
case "cl3_dodge_charger_patrol":
{
_ret =
[
["cl3_dodge_charger_cn","unknow"],
["cl3_dodge_charger_emt","unknow"],
["cl3_dodge_charger_emtcg","unknow"],
["cl3_dodge_charger_emtfd","unknow"],
["cl3_dodge_charger_emtmr","unknow"],
["cl3_dodge_charger_emtpa","medic"],
["cl3_dodge_charger_etu","unknow"],

["cl3_dodge_charger_k9","cop"],

["cl3_dodge_charger_patrol","cop"],
["cl3_dodge_charger_patrol2","cop"],

["cl3_dodge_charger_s_black","civ"],
["cl3_dodge_charger_s_blue","civ"],
["cl3_dodge_charger_s_camo","reb"],
["cl3_dodge_charger_s_camourban","merc"],
["cl3_dodge_charger_s_darkgreen","civ"],
["cl3_dodge_charger_s_darkred","civ"],
["cl3_dodge_charger_s_green","civ"],
["cl3_dodge_charger_s_grey","civ"],
["cl3_dodge_charger_s_lime","civ"],
["cl3_dodge_charger_s_orange","civ"],
["cl3_dodge_charger_s_pink","civ"],
["cl3_dodge_charger_s_purple","civ"],
["cl3_dodge_charger_s_red","civ"],
["cl3_dodge_charger_s_white","civ"],
["cl3_dodge_charger_s_yellow","civ"]
];
};
case "cl3_e60_m5":
{
_ret =
[
["cl3_e60_m5_aqua","civ"],
["cl3_e60_m5_babyblue","civ"],
["cl3_e60_m5_babypink","civ"],
["cl3_e60_m5_black","civ"],
["cl3_e60_m5_blue","civ"],
["cl3_e60_m5_burgundy","civ"],
["cl3_e60_m5_camo","reb"],
["cl3_e60_m5_camo_urban","merc"],
["cl3_e60_m5_cardinal","civ"],
["cl3_e60_m5_dark_green","civ"],
["cl3_e60_m5_gold","civ"],
["cl3_e60_m5_green","civ"],
["cl3_e60_m5_grey","civ"],
["cl3_e60_m5_lavender","civ"],
["cl3_e60_m5_light_blue","civ"],
["cl3_e60_m5_light_yellow","civ"],
["cl3_e60_m5_lime","civ"],
["cl3_e60_m5_marina_blue","civ"],
["cl3_e60_m5_navy_blue","civ"],
["cl3_e60_m5_orange","civ"],
["cl3_e60_m5_purple","civ"],
["cl3_e60_m5_red","civ"],
["cl3_e60_m5_sand","civ"],
["cl3_e60_m5_silver","civ"],
["cl3_e60_m5_violet","civ"],
["cl3_e60_m5_white","civ"],
["cl3_e60_m5_yellow","civ"]
];
};
case "cl3_e63_amg":
{
_ret =
[
["cl3_e63_amg_aqua","civ"],
["cl3_e63_amg_babyblue","civ"],
["cl3_e63_amg_babypink","civ"],
["cl3_e63_amg_black","civ"],
["cl3_e63_amg_blue","civ"],
["cl3_e63_amg_burgundy","civ"],
["cl3_e63_amg_camo","reb"],
["cl3_e63_amg_camo_urban","merc"],
["cl3_e63_amg_cardinal","civ"],
["cl3_e63_amg_dark_green","civ"],
["cl3_e63_amg_gold","civ"],
["cl3_e63_amg_green","civ"],
["cl3_e63_amg_grey","civ"],
["cl3_e63_amg_lavender","civ"],
["cl3_e63_amg_light_blue","civ"],
["cl3_e63_amg_light_yellow","civ"],
["cl3_e63_amg_lime","civ"],
["cl3_e63_amg_marina_blue","civ"],
["cl3_e63_amg_navy_blue","civ"],
["cl3_e63_amg_orange","civ"],
["cl3_e63_amg_purple","civ"],
["cl3_e63_amg_red","civ"],
["cl3_e63_amg_sand","civ"],
["cl3_e63_amg_silver","civ"],
["cl3_e63_amg_violet","civ"],
["cl3_e63_amg_white","civ"],
["cl3_e63_amg_yellow","civ"]
];
};
case "cl3_escalade":
{
_ret =
[
["cl3_escalade_aqua","civ"],
["cl3_escalade_babyblue","civ"],
["cl3_escalade_babypink","civ"],
["cl3_escalade_black","civ"],
["cl3_escalade_blue","civ"],
["cl3_escalade_burgundy","civ"],
["cl3_escalade_camo","reb"],
["cl3_escalade_camo_urban","merc"],
["cl3_escalade_cardinal","civ"],
["cl3_escalade_cg","unknow"],
["cl3_escalade_cn","unknow"],
["cl3_escalade_dark_green","civ"],
["cl3_escalade_etu","unknow"],
["cl3_escalade_fd","unknow"],
["cl3_escalade_gold","civ"],
["cl3_escalade_green","civ"],
["cl3_escalade_grey","civ"],
["cl3_escalade_k9","unknow"],
["cl3_escalade_lavender","civ"],
["cl3_escalade_light_blue","civ"],
["cl3_escalade_light_yellow","civ"],
["cl3_escalade_lime","civ"],
["cl3_escalade_marina_blue","civ"],
["cl3_escalade_mr","unknow"],
["cl3_escalade_navy_blue","civ"],
["cl3_escalade_orange","civ"],
["cl3_escalade_patrolbw","cop"],
["cl3_escalade_patrolwb","cop"],
["cl3_escalade_pd","unknow"],
["cl3_escalade_pm","medic"],
["cl3_escalade_purple","civ"],
["cl3_escalade_red","civ"],
["cl3_escalade_sand","civ"],
["cl3_escalade_silver","civ"],
["cl3_escalade_traffic","cop"],
["cl3_escalade_violet","civ"],
["cl3_escalade_white","civ"],
["cl3_escalade_yellow","civ"],
["cl3_escalade_dep","dep"]
];
};
case "cl3_q7":
{
_ret =
[
["cl3_q7_aqua","civ"],
["cl3_q7_babyblue","civ"],
["cl3_q7_babypink","civ"],
["cl3_q7_black","civ"],
["cl3_q7_blue","civ"],
["cl3_q7_burgundy","civ"],
["cl3_q7_camo","reb"],
["cl3_q7_camo_urban","merc"],
["cl3_q7_cardinal","civ"],
["cl3_q7_clpd_cn","unknow"],
["cl3_q7_clpd_etu","unknow"],
["cl3_q7_clpd_patrol","cop"],
["cl3_q7_dark_green","civ"],
["cl3_q7_gold","civ"],
["cl3_q7_green","civ"],
["cl3_q7_grey","civ"],
["cl3_q7_lavender","civ"],
["cl3_q7_light_blue","civ"],
["cl3_q7_light_yellow","civ"],
["cl3_q7_lime","civ"],
["cl3_q7_marina_blue","civ"],
["cl3_q7_navy_blue","civ"],
["cl3_q7_orange","civ"],
["cl3_q7_purple","civ"],
["cl3_q7_red","civ"],
["cl3_q7_sand","civ"],
["cl3_q7_silver","civ"],
["cl3_q7_violet","civ"],
["cl3_q7_white","civ"],
["cl3_q7_yellow","civ"]
];
};
case "cl3_s5":
{
_ret =
[
["cl3_s5_aqua","civ"],
["cl3_s5_babyblue","civ"],
["cl3_s5_babypink","civ"],
["cl3_s5_black","civ"],
["cl3_s5_blue","civ"],
["cl3_s5_burgundy","civ"],
["cl3_s5_camo","reb"],
["cl3_s5_camo_urban","merc"],
["cl3_s5_cardinal","civ"],
["cl3_s5_dark_green","civ"],
["cl3_s5_gold","civ"],
["cl3_s5_green","civ"],
["cl3_s5_grey","civ"],
["cl3_s5_lavender","civ"],
["cl3_s5_light_blue","civ"],
["cl3_s5_light_yellow","civ"],
["cl3_s5_lime","civ"],
["cl3_s5_marina_blue","civ"],
["cl3_s5_navy_blue","civ"],
["cl3_s5_orange","civ"],
["cl3_s5_purple","civ"],
["cl3_s5_red","civ"],
["cl3_s5_sand","civ"],
["cl3_s5_silver","civ"],
["cl3_s5_violet","civ"],
["cl3_s5_white","civ"],
["cl3_s5_yellow","civ"]
];
};
case "cl3_suv":
{
_ret =
[
["cl3_suv_black","cop"],
["cl3_suv_emt","medic"],
["cl3_suv_taxi","taxi"]
];
};
case "cl3_taurus":
{
_ret =
[
["cl3_taurus_aqua","civ"],
["cl3_taurus_babyblue","civ"],
["cl3_taurus_babypink","civ"],
["cl3_taurus_black","civ"],
["cl3_taurus_blue","civ"],
["cl3_taurus_burgundy","civ"],
["cl3_taurus_camo","reb"],
["cl3_taurus_camo_urban","merc"],
["cl3_taurus_cardinal","civ"],
["cl3_taurus_dark_green","civ"],
["cl3_taurus_gold","civ"],
["cl3_taurus_green","civ"],
["cl3_taurus_grey","civ"],
["cl3_taurus_lavender","civ"],
["cl3_taurus_light_blue","civ"],
["cl3_taurus_light_yellow","civ"],
["cl3_taurus_lime","civ"],
["cl3_taurus_marina_blue","civ"],
["cl3_taurus_navy_blue","civ"],
["cl3_taurus_orange","civ"],
["cl3_taurus_purple","civ"],
["cl3_taurus_red","civ"],
["cl3_taurus_sand","civ"],
["cl3_taurus_silver","civ"],
["cl3_taurus_violet","civ"],
["cl3_taurus_white","civ"],
["cl3_taurus_yellow","civ"]
];
};
case "cl3_transit":
{
_ret =
[
["cl3_transit_aqua","civ"],
["cl3_transit_babyblue","civ"],
["cl3_transit_babypink","civ"],
["cl3_transit_black","cop"],
["cl3_transit_blue","civ"],
["cl3_transit_burgundy","civ"],
["cl3_transit_camo","reb"],
["cl3_transit_camo_urban","merc"],
["cl3_transit_cardinal","civ"],
["cl3_transit_civ","fion"],
["cl3_transit_cop","cop"],
["cl3_transit_dark_green","civ"],
["cl3_transit_gold","civ"],
["cl3_transit_green","civ"],
["cl3_transit_grey","civ"],
["cl3_transit_lavender","civ"],
["cl3_transit_light_blue","civ"],
["cl3_transit_light_yellow","civ"],
["cl3_transit_lime","civ"],
["cl3_transit_marina_blue","civ"],
["cl3_transit_navy_blue","civ"],
["cl3_transit_orange","civ"],
["cl3_transit_purple","civ"],
["cl3_transit_red","civ"],
["cl3_transit_sand","civ"],
["cl3_transit_silver","civ"],
["cl3_transit_violet","civ"],
["cl3_transit_white","civ"],
["cl3_transit_yellow","civ"],
["cl3_transitemt","medic"],
["cl3_transitk9","cop"],
["cl3_transitNews","civ"],
["cl3_transitpatrol","cop"]
];
};
case "cl3_z4_2008":
{
_ret =
[
["cl3_z4_2008_aqua","civ"],
["cl3_z4_2008_babyblue","civ"],
["cl3_z4_2008_babypink","civ"],
["cl3_z4_2008_black","civ"],
["cl3_z4_2008_blue","civ"],
["cl3_z4_2008_burgundy","civ"],
["cl3_z4_2008_camo","reb"],
["cl3_z4_2008_camo_urban","merc"],
["cl3_z4_2008_cardinal","civ"],
["cl3_z4_2008_dark_green","civ"],
["cl3_z4_2008_gold","civ"],
["cl3_z4_2008_green","civ"],
["cl3_z4_2008_grey","civ"],
["cl3_z4_2008_lavender","civ"],
["cl3_z4_2008_light_blue","civ"],
["cl3_z4_2008_light_yellow","civ"],
["cl3_z4_2008_lime","civ"],
["cl3_z4_2008_marina_blue","civ"],
["cl3_z4_2008_navy_blue","civ"],
["cl3_z4_2008_orange","civ"],
["cl3_z4_2008_purple","civ"],
["cl3_z4_2008_red","civ"],
["cl3_z4_2008_sand","civ"],
["cl3_z4_2008_silver","civ"],
["cl3_z4_2008_violet","civ"],
["cl3_z4_2008_white","civ"],
["cl3_z4_2008_yellow","civ"]
];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule high
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_458":
{
_ret =
[
["cl3_458_2tone1","civ"],
["cl3_458_2tone2","civ"],
["cl3_458_2tone3","civ"],
["cl3_458_2tone4","civ"],
["cl3_458_2tone5","civ"],
["cl3_458_aqua","civ"],
["cl3_458_babyblue","civ"],
["cl3_458_babypink","civ"],
["cl3_458_black","civ"],
["cl3_458_blue","civ"],
["cl3_458_burgundy","civ"],
["cl3_458_camo","civ"],
["cl3_458_camo_urban","civ"],
["cl3_458_cardinal","civ"],
["cl3_458_dark_green","civ"],
["cl3_458_flame","civ"],
["cl3_458_flame1","civ"],
["cl3_458_flame2","civ"],
["cl3_458_gold","civ"],
["cl3_458_green","civ"],
["cl3_458_grey","civ"],
["cl3_458_lavender","civ"],
["cl3_458_light_blue","civ"],
["cl3_458_light_yellow","civ"],
["cl3_458_lime","civ"],
["cl3_458_marina_blue","civ"],
["cl3_458_navy_blue","civ"],
["cl3_458_orange","civ"],
["cl3_458_purple","civ"],
["cl3_458_red","civ"],
["cl3_458_sand","civ"],
["cl3_458_silver","civ"],
["cl3_458_violet","civ"],
["cl3_458_white","civ"],
["cl3_458_yellow","civ"]
];
};
case "cl3_aventador_lp7004":
{
_ret =
[
["cl3_aventador_lp7004_2tone1","civ"],
["cl3_aventador_lp7004_2tone2","civ"],
["cl3_aventador_lp7004_2tone3","civ"],
["cl3_aventador_lp7004_2tone4","civ"],
["cl3_aventador_lp7004_2tone5","civ"],
["cl3_aventador_lp7004_aqua","civ"],
["cl3_aventador_lp7004_babyblue","civ"],
["cl3_aventador_lp7004_babypink","civ"],
["cl3_aventador_lp7004_black","civ"],
["cl3_aventador_lp7004_blue","civ"],
["cl3_aventador_lp7004_burgundy","civ"],
["cl3_aventador_lp7004_camo","reb"],
["cl3_aventador_lp7004_camo_urban","merc"],
["cl3_aventador_lp7004_cardinal","civ"],
["cl3_aventador_lp7004_dark_green","civ"],
["cl3_aventador_lp7004_flame","civ"],
["cl3_aventador_lp7004_flame1","civ"],
["cl3_aventador_lp7004_flame2","civ"],
["cl3_aventador_lp7004_gold","civ"],
["cl3_aventador_lp7004_green","civ"],
["cl3_aventador_lp7004_grey","civ"],
["cl3_aventador_lp7004_lavender","civ"],
["cl3_aventador_lp7004_light_blue","civ"],
["cl3_aventador_lp7004_light_yellow","civ"],
["cl3_aventador_lp7004_lime","civ"],
["cl3_aventador_lp7004_marina_blue","civ"],
["cl3_aventador_lp7004_navy_blue","civ"],
["cl3_aventador_lp7004_orange","civ"],
["cl3_aventador_lp7004_purple","civ"],
["cl3_aventador_lp7004_red","civ"],
["cl3_aventador_lp7004_sand","civ"],
["cl3_aventador_lp7004_silver","civ"],
["cl3_aventador_lp7004_violet","civ"],
["cl3_aventador_lp7004_white","civ"],
["cl3_aventador_lp7004_yellow","civ"]
];
};

case "cl3_carrera_gt":
{
_ret =
[
["cl3_carrera_gt_aqua","civ"],
["cl3_carrera_gt_babyblue","civ"],
["cl3_carrera_gt_babypink","civ"],
["cl3_carrera_gt_black","civ"],
["cl3_carrera_gt_blue","civ"],
["cl3_carrera_gt_burgundy","civ"],
["cl3_carrera_gt_camo","civ"],
["cl3_carrera_gt_camo_urban","civ"],
["cl3_carrera_gt_cardinal","civ"],
["cl3_carrera_gt_dark_green","civ"],
["cl3_carrera_gt_gold","civ"],
["cl3_carrera_gt_green","civ"],
["cl3_carrera_gt_grey","civ"],
["cl3_carrera_gt_lavender","civ"],
["cl3_carrera_gt_light_blue","civ"],
["cl3_carrera_gt_light_yellow","civ"],
["cl3_carrera_gt_lime","civ"],
["cl3_carrera_gt_marina_blue","civ"],
["cl3_carrera_gt_navy_blue","civ"],
["cl3_carrera_gt_orange","civ"],
["cl3_carrera_gt_purple","civ"],
["cl3_carrera_gt_red","civ"],
["cl3_carrera_gt_sand","civ"],
["cl3_carrera_gt_silver","civ"],
["cl3_carrera_gt_violet","civ"],
["cl3_carrera_gt_white","civ"],
["cl3_carrera_gt_yellow","civ"]
];
};
case "cl3_dbs_volante":
{
_ret =
[
["cl3_dbs_volante_aqua","civ"],
["cl3_dbs_volante_babyblue","civ"],
["cl3_dbs_volante_babypink","civ"],
["cl3_dbs_volante_black","civ"],
["cl3_dbs_volante_blue","civ"],
["cl3_dbs_volante_burgundy","civ"],
["cl3_dbs_volante_camo","civ"],
["cl3_dbs_volante_camo_urban","civ"],
["cl3_dbs_volante_cardinal","civ"],
["cl3_dbs_volante_dark_green","civ"],
["cl3_dbs_volante_flame","civ"],
["cl3_dbs_volante_flame1","civ"],
["cl3_dbs_volante_flame2","civ"],
["cl3_dbs_volante_gold","civ"],
["cl3_dbs_volante_green","civ"],
["cl3_dbs_volante_grey","civ"],
["cl3_dbs_volante_lavender","civ"],
["cl3_dbs_volante_light_blue","civ"],
["cl3_dbs_volante_light_yellow","civ"],
["cl3_dbs_volante_lime","civ"],
["cl3_dbs_volante_marina_blue","civ"],
["cl3_dbs_volante_navy_blue","civ"],
["cl3_dbs_volante_orange","civ"],
["cl3_dbs_volante_purple","civ"],
["cl3_dbs_volante_red","civ"],
["cl3_dbs_volante_sand","civ"],
["cl3_dbs_volante_silver","civ"],
["cl3_dbs_volante_violet","civ"],
["cl3_dbs_volante_white","civ"],
["cl3_dbs_volante_yellow","civ"]
];
};

case "cl3_impreza_rally":
{
_ret =
[
["cl3_impreza_rally_aqua","civ"],
["cl3_impreza_rally_babyblue","civ"],
["cl3_impreza_rally_babypink","civ"],
["cl3_impreza_rally_black","civ"],
["cl3_impreza_rally_blue","civ"],
["cl3_impreza_rally_burgundy","civ"],
["cl3_impreza_rally_camo","reb"],
["cl3_impreza_rally_camo_urban","merc"],
["cl3_impreza_rally_cardinal","civ"],
["cl3_impreza_rally_dark_green","civ"],
["cl3_impreza_rally_flame","civ"],
["cl3_impreza_rally_flame1","civ"],
["cl3_impreza_rally_flame2","civ"],
["cl3_impreza_rally_gold","civ"],
["cl3_impreza_rally_green","civ"],
["cl3_impreza_rally_grey","civ"],
["cl3_impreza_rally_lavender","civ"],
["cl3_impreza_rally_light_blue","civ"],
["cl3_impreza_rally_light_yellow","civ"],
["cl3_impreza_rally_lime","civ"],
["cl3_impreza_rally_marina_blue","civ"],
["cl3_impreza_rally_navy_blue","civ"],
["cl3_impreza_rally_orange","civ"],
["cl3_impreza_rally_purple","civ"],
["cl3_impreza_rally_red","civ"],
["cl3_impreza_rally_sand","civ"],
["cl3_impreza_rally_silver","civ"],
["cl3_impreza_rally_violet","civ"],
["cl3_impreza_rally_white","civ"],
["cl3_impreza_rally_yellow","civ"]
];
};

case "cl3_impreza_road":
{
_ret =
[
["cl3_impreza_road_aqua","civ"],
["cl3_impreza_road_babyblue","civ"],
["cl3_impreza_road_babypink","civ"],
["cl3_impreza_road_black","civ"],
["cl3_impreza_road_blue","civ"],
["cl3_impreza_road_burgundy","civ"],
["cl3_impreza_road_camo","civ"],
["cl3_impreza_road_camo_urban","civ"],
["cl3_impreza_road_cardinal","civ"],
["cl3_impreza_road_dark_green","civ"],
["cl3_impreza_road_flame","civ"],
["cl3_impreza_road_flame1","civ"],
["cl3_impreza_road_flame2","civ"],
["cl3_impreza_road_gold","civ"],
["cl3_impreza_road_green","civ"],
["cl3_impreza_road_grey","civ"],
["cl3_impreza_road_lavender","civ"],
["cl3_impreza_road_light_blue","civ"],
["cl3_impreza_road_light_yellow","civ"],
["cl3_impreza_road_lime","civ"],
["cl3_impreza_road_livery1","civ"],
["cl3_impreza_road_livery2","civ"],
["cl3_impreza_road_livery3","civ"],
["cl3_impreza_road_livery4","civ"],
["cl3_impreza_road_livery5","civ"],
["cl3_impreza_road_marina_blue","civ"],
["cl3_impreza_road_navy_blue","civ"],
["cl3_impreza_road_orange","civ"],
["cl3_impreza_road_purple","civ"],
["cl3_impreza_road_red","civ"],
["cl3_impreza_road_sand","civ"],
["cl3_impreza_road_silver","civ"],
["cl3_impreza_road_violet","civ"],
["cl3_impreza_road_white","civ"],
["cl3_impreza_road_yellow","civ"]
];
};

case "cl3_insignia":
{
_ret =
[
["cl3_insignia_aqua","civ"],
["cl3_insignia_babyblue","civ"],
["cl3_insignia_babypink","civ"],
["cl3_insignia_black","civ"],
["cl3_insignia_blue","civ"],
["cl3_insignia_burgundy","civ"],
["cl3_insignia_camo","civ"],
["cl3_insignia_camo_urban","civ"],
["cl3_insignia_cardinal","civ"],
["cl3_insignia_dark_green","civ"],
["cl3_insignia_gold","civ"],
["cl3_insignia_green","civ"],
["cl3_insignia_grey","civ"],
["cl3_insignia_lavender","civ"],
["cl3_insignia_light_blue","civ"],
["cl3_insignia_light_yellow","civ"],
["cl3_insignia_lime","civ"],
["cl3_insignia_marina_blue","civ"],
["cl3_insignia_navy_blue","civ"],
["cl3_insignia_orange","civ"],
["cl3_insignia_purple","civ"],
["cl3_insignia_red","civ"],
["cl3_insignia_sand","civ"],
["cl3_insignia_silver","civ"],
["cl3_insignia_violet","civ"],
["cl3_insignia_white","civ"],
["cl3_insignia_yellow","civ"]
];
};

case "cl3_lamborghini_gt1":
{
_ret =
[
["cl3_lamborghini_gt1_2tone1","civ"],
["cl3_lamborghini_gt1_2tone2","civ"],
["cl3_lamborghini_gt1_2tone3","civ"],
["cl3_lamborghini_gt1_2tone4","civ"],
["cl3_lamborghini_gt1_2tone5","civ"],
["cl3_lamborghini_gt1_aqua","civ"],
["cl3_lamborghini_gt1_babyblue","civ"],
["cl3_lamborghini_gt1_babypink","civ"],
["cl3_lamborghini_gt1_black","civ"],
["cl3_lamborghini_gt1_blue","civ"],
["cl3_lamborghini_gt1_burgundy","civ"],
["cl3_lamborghini_gt1_camo","civ"],
["cl3_lamborghini_gt1_camo_urban","civ"],
["cl3_lamborghini_gt1_cardinal","civ"],
["cl3_lamborghini_gt1_dark_green","civ"],
["cl3_lamborghini_gt1_flame","civ"],
["cl3_lamborghini_gt1_flame1","civ"],
["cl3_lamborghini_gt1_flame2","civ"],
["cl3_lamborghini_gt1_gold","civ"],
["cl3_lamborghini_gt1_green","civ"],
["cl3_lamborghini_gt1_grey","civ"],
["cl3_lamborghini_gt1_lavender","civ"],
["cl3_lamborghini_gt1_light_blue","civ"],
["cl3_lamborghini_gt1_light_yellow","civ"],
["cl3_lamborghini_gt1_lime","civ"],
["cl3_lamborghini_gt1_marina_blue","civ"],
["cl3_lamborghini_gt1_navy_blue","civ"],
["cl3_lamborghini_gt1_orangeciv"],
["cl3_lamborghini_gt1_purpleciv"],
["cl3_lamborghini_gt1_redciv"],
["cl3_lamborghini_gt1_sandciv"],
["cl3_lamborghini_gt1_silverciv"],
["cl3_lamborghini_gt1_violetciv"],
["cl3_lamborghini_gt1_whiteciv"],
["cl3_lamborghini_gt1_yellowciv"]
];
};
case "cl3_murcielago":
{
_ret =
[
["cl3_murcielago_2tone1","civ"],
["cl3_murcielago_2tone2","civ"],
["cl3_murcielago_2tone3","civ"],
["cl3_murcielago_2tone4","civ"],
["cl3_murcielago_2tone5","civ"],
["cl3_murcielago_aqua","civ"],
["cl3_murcielago_babyblue","civ"],
["cl3_murcielago_babypink","civ"],
["cl3_murcielago_black","civ"],
["cl3_murcielago_blue","civ"],
["cl3_murcielago_burgundy","civ"],
["cl3_murcielago_camo","civ"],
["cl3_murcielago_camo_urban","civ"],
["cl3_murcielago_cardinal","civ"],
["cl3_murcielago_dark_green","civ"],
["cl3_murcielago_flame","civ"],
["cl3_murcielago_flame1","civ"],
["cl3_murcielago_flame2","civ"],
["cl3_murcielago_gold","civ"],
["cl3_murcielago_green","civ"],
["cl3_murcielago_grey","civ"],
["cl3_murcielago_lavender","civ"],
["cl3_murcielago_light_blue","civ"],
["cl3_murcielago_light_yellow","civ"],
["cl3_murcielago_lime","civ"],
["cl3_murcielago_marina_blue","civ"],
["cl3_murcielago_navy_blue","civ"],
["cl3_murcielago_orangeciv"],
["cl3_murcielago_purpleciv"],
["cl3_murcielago_redciv"],
["cl3_murcielago_sandciv"],
["cl3_murcielago_silverciv"],
["cl3_murcielago_violetciv"],
["cl3_murcielago_whiteciv"],
["cl3_murcielago_yellowciv"]
];
};

case "cl3_r8_spyder":
{
_ret =
[
["cl3_r8_spyder_2tone1","civ"],
["cl3_r8_spyder_2tone2","civ"],
["cl3_r8_spyder_2tone3","civ"],
["cl3_r8_spyder_2tone4","civ"],
["cl3_r8_spyder_2tone5","civ"],
["cl3_r8_spyder_aqua","civ"],
["cl3_r8_spyder_babyblue","civ"],
["cl3_r8_spyder_babypink","civ"],
["cl3_r8_spyder_black","civ"],
["cl3_r8_spyder_blue","civ"],
["cl3_r8_spyder_burgundy","civ"],
["cl3_r8_spyder_camo","civ"],
["cl3_r8_spyder_camo_urban","civ"],
["cl3_r8_spyder_cardinal","civ"],
["cl3_r8_spyder_dark_green","civ"],
["cl3_r8_spyder_flame","civ"],
["cl3_r8_spyder_flame1","civ"],
["cl3_r8_spyder_flame2","civ"],
["cl3_r8_spyder_gold","civ"],
["cl3_r8_spyder_green","civ"],
["cl3_r8_spyder_grey","civ"],
["cl3_r8_spyder_lavender","civ"],
["cl3_r8_spyder_light_blue","civ"],
["cl3_r8_spyder_light_yellow","civ"],
["cl3_r8_spyder_lime","civ"],
["cl3_r8_spyder_marina_blue","civ"],
["cl3_r8_spyder_navy_blue","civ"],
["cl3_r8_spyder_orangeciv"],
["cl3_r8_spyder_purpleciv"],
["cl3_r8_spyder_redciv"],
["cl3_r8_spyder_sandciv"],
["cl3_r8_spyder_silverciv"],
["cl3_r8_spyder_violetciv"],
["cl3_r8_spyder_whiteciv"],
["cl3_r8_spyder_yellowciv"]
];
};

case "cl3_reventon":
{
_ret =
[
["cl3_reventon_2tone1","civ"],
["cl3_reventon_2tone2","civ"],
["cl3_reventon_2tone3","civ"],
["cl3_reventon_2tone4","civ"],
["cl3_reventon_2tone5","civ"],
["cl3_reventon_aqua","civ"],
["cl3_reventon_babyblue","civ"],
["cl3_reventon_babypink","civ"],
["cl3_reventon_black","civ"],
["cl3_reventon_blue","civ"],
["cl3_reventon_burgundy","civ"],
["cl3_reventon_camo","reb"],
["cl3_reventon_camo_urban","merc"],
["cl3_reventon_cardinal","civ"],
["cl3_reventon_clpd","cop"],
["cl3_reventon_clpd_traf","cop"],
["cl3_reventon_dark_green","civ"],
["cl3_reventon_flame","civ"],
["cl3_reventon_flame1","civ"],
["cl3_reventon_flame2","civ"],
["cl3_reventon_gold","civ"],
["cl3_reventon_green","civ"],
["cl3_reventon_grey","civ"],
["cl3_reventon_lavender","civ"],
["cl3_reventon_light_blue","civ"],
["cl3_reventon_light_yellow","civ"],
["cl3_reventon_lime","civ"],
["cl3_reventon_marina_blue","civ"],
["cl3_reventon_navy_blue","civ"],
["cl3_reventon_orange","civ"],
["cl3_reventon_purple","civ"],
["cl3_reventon_red","civ"],
["cl3_reventon_sand","civ"],
["cl3_reventon_silver","civ"],
["cl3_reventon_violet","civ"],
["cl3_reventon_white","civ"],
["cl3_reventon_yellow","civ"]
];
};

case "cl3_veyron":
{
_ret =
[
["cl3_veyron_black","civ"],
["cl3_veyron_blk_wht","civ"],
["cl3_veyron_brn_blk","civ"],
["cl3_veyron_lblue_dblue","civ"],
["cl3_veyron_lblue_lblue","civ"],
["cl3_veyron_red_red","civ"],
["cl3_veyron_wht_blu","civ"],
["cl3_veyron_wht_lwht","civ"]
];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule FUN
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil TRUCK
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


case "cl3_mackr_del":
{
_ret =
[
["cl3_mackr_del_american","civ"],
["cl3_mackr_del_black","civ"],
["cl3_mackr_del_black_gold","civ"],
["cl3_mackr_del_black_white","civ"],
["cl3_mackr_del_blue","civ"],
["cl3_mackr_del_brown_camo","civ"],
["cl3_mackr_del_forest_camo","civ"],
["cl3_mackr_del_green_white","civ"],
["cl3_mackr_del_multi_color","civ"],
["cl3_mackr_del_optimus","civ"],
["cl3_mackr_del_orange_white","civ"],
["cl3_mackr_del_purple_white","civ"],
["cl3_mackr_del_red_white","civ"],
["cl3_mackr_del_silver","civ"]
];
};


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//AVIATION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_GNT_C185_base":
{
_ret =
[
["cl3_GNT_C185_fuse1","civ"],
["cl3_GNT_C185_fuse2","civ"],
["cl3_GNT_C185_fuse3","civ"],
["cl3_GNT_C185_fuse4","civ"],
["cl3_GNT_C185_fuse5","civ"],
["cl3_GNT_C185_fuse6","civ"]
];
};

case "GNT_C185F":
{
_ret =
[
["GNT_C185F","civ"]
];
};

case "IVORY_ERJ135_1":
{
_ret =
[
["IVORY_ERJ135_1","civ"]
];
};
case "IVORY_CRJ200_1":
{
_ret =
[
["IVORY_CRJ200_1","civ"]
];
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DEPANNEUR
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_f150repo":
{
_ret =
[
["cl3_f150repo_black","dep"],
["cl3_f150repo_blue","dep"],
["cl3_f150repo_gray","dep"],
["cl3_f150repo_green","dep"],
["cl3_f150repo_orange","dep"],
["cl3_f150repo_red","dep"]
];
};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//BUS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "CL3_bus_cl":
{
_ret =
[
["CL3_bus_cl_black","taxi"],
["CL3_bus_cl_blue","taxi"],
["CL3_bus_cl_green","taxi"],
["CL3_bus_cl_grey","taxi"],
["CL3_bus_cl_jail","cop"],
["CL3_bus_cl_purple","taxi"],
["CL3_bus_cl_yellow","taxi"]
];
};

/////////////////////////////////////////////////////////////////////////////////
//RIGOLOL
/////////////////////////////////////////////////////////////////////////////////


case "cl3_batmobile":
{
_ret =
[
["cl3_batmobile","civ"]
];
};
case "cl3_jetpack":
{
_ret =
[
["cl3_jetpack","civ"]
];
};





};


_ret;