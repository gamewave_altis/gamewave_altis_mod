/*
	File: fn_clothing_bruce.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for Bruce's Outback Outfits.
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Tenues"];

switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[		
		["cl3_citizen2_10_uniform",nil,250],
		["cl3_citizen2_11_uniform",nil,250],
		["cl3_citizen2_12_uniform",nil,250],
		["cl3_citizen2_13_uniform",nil,250],
		["cl3_citizen2_13_uniform",nil,250],
		["cl3_citizen2_14_uniform",nil,250],
		["cl3_citizen2_15_uniform",nil,250],
		["cl3_citizen2_16_uniform",nil,250],
		["cl3_citizen2_17_uniform",nil,250],
		["cl3_citizen2_18_uniform",nil,250],
		["cl3_citizen2_19_uniform",nil,250],
		["cl3_citizen2_1_uniform",nil,250],
		["cl3_citizen2_20_uniform",nil,250],
		["cl3_citizen2_21_uniform",nil,250],
		["cl3_citizen2_22_uniform",nil,250],
		["cl3_citizen2_23_uniform",nil,250],
		["cl3_citizen2_24_uniform",nil,250],
		["cl3_citizen2_25_uniform",nil,250],
		["cl3_citizen2_2_uniform",nil,250],
		["cl3_citizen2_2_uniform",nil,250],
		["cl3_citizen2_3_uniform",nil,250],
		["cl3_citizen2_4_uniform",nil,250],
		["cl3_citizen2_4_uniform",nil,250],
		["cl3_citizen2_5_uniform",nil,250],
		["cl3_citizen2_6_uniform",nil,250],
		["cl3_citizen2_7_uniform",nil,250],
		["cl3_citizen2_8_uniform",nil,250],
		["cl3_citizen2_9_uniform",nil,250],
		["cl3_citizen3_10_uniform",nil,250],
		["cl3_citizen3_11_uniform",nil,250],
		["cl3_citizen3_12_uniform",nil,250],
		["cl3_citizen3_13_uniform",nil,250],
		["cl3_citizen3_14_uniform",nil,250],
		["cl3_citizen3_15_uniform",nil,250],
		["cl3_citizen3_16_uniform",nil,250],
		["cl3_citizen3_17_uniform",nil,250],
		["cl3_citizen3_18_uniform",nil,250],
		["cl3_citizen3_19_uniform",nil,250],
		["cl3_citizen3_1_uniform",nil,250],
		["cl3_citizen3_20_uniform",nil,250],
		["cl3_citizen3_21_uniform",nil,250],
		["cl3_citizen3_22_uniform",nil,250],
		["cl3_citizen3_23_uniform",nil,250],
		["cl3_citizen3_24_uniform",nil,250],
		["cl3_citizen3_25_uniform",nil,250],
		["cl3_citizen3_2_uniform",nil,250],
		["cl3_citizen3_3_uniform",nil,250],
		["cl3_citizen3_4_uniform",nil,250],
		["cl3_citizen3_5_uniform",nil,250],
		["cl3_citizen3_6_uniform",nil,250],
		["cl3_citizen3_7_uniform",nil,250],
		["cl3_citizen3_7_uniform",nil,250],
		["cl3_citizen3_8_uniform",nil,250],
		["cl3_citizen3_9_uniform",nil,250],
		["cl3_citizen3_9_uniform",nil,250],
		["cl3_citizen4_10_uniform",nil,250],
		["cl3_citizen4_11_uniform",nil,250],
		["cl3_citizen4_12_uniform",nil,250],
		["cl3_citizen4_13_uniform",nil,250],
		["cl3_citizen4_14_uniform",nil,250],
		["cl3_citizen4_15_uniform",nil,250],
		["cl3_citizen4_16_uniform",nil,250],
		["cl3_citizen4_17_uniform",nil,250],
		["cl3_citizen4_18_uniform",nil,250],
		["cl3_citizen4_19_uniform",nil,250],
		["cl3_citizen4_1_uniform",nil,250],
		["cl3_citizen4_20_uniform",nil,250],
		["cl3_citizen4_21_uniform",nil,250],
		["cl3_citizen4_22_uniform",nil,250],
		["cl3_citizen4_22_uniform",nil,250],
		["cl3_citizen4_23_uniform",nil,250],
		["cl3_citizen4_24_uniform",nil,250],
		["cl3_citizen4_25_uniform",nil,250],
		["cl3_citizen4_2_uniform",nil,250],
		["cl3_citizen4_3_uniform",nil,250],
		["cl3_citizen4_4_uniform",nil,250],
		["cl3_citizen4_4_uniform",nil,250],
		["cl3_citizen4_5_uniform",nil,250],
		["cl3_citizen4_6_uniform",nil,250],
		["cl3_citizen4_7_uniform",nil,250],
		["cl3_citizen4_8_uniform",nil,250],
		["cl3_citizen4_9_uniform",nil,250],
		["cl3_citizen4_9_uniform",nil,250]	
		];
	};
	
	//Hats
	case 1:
	{
		[

		];
	};
	
	//Glasses
	case 2:
	{
		[

		];
	};
	
	//Vest
	case 3:
	{
		[
		];
	};
	
	//Backpacks
	case 4:
	{
		[

		];
	};
};