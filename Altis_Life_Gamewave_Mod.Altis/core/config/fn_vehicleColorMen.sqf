/*
File: fn_vehicleColorMen.sqf
Author: For Gamewave

*/
private["_vehicle","_lvmg","_index"];
_vehicle = [_this,0,"",[""]] call BIS_fnc_param;
_index = [_this,1,-1,[0]] call BIS_fnc_param;
_lvmg = "";

switch (_vehicle) do
{

case "Skyline_Mercedes_C63_01_F":
{
switch (_index) do
{

default {_lvmg = "Mercedes-Benz C63 AMG Custom"};

};
};
case "Skyline_Mercedes_C63_02_F":
{
switch (_index) do
{

default {_lvmg = "Mercedes-Benz C63 AMG"};
};
};



case "O_Heli_Transport_04_bench_F":
{
switch (_index) do
{
case 0: {_lvmg = "Mi-290 Taru Transport : Rebelle"};
case 1: {_lvmg = "Mi-290 Taru Transport : Mercenaire"};
case 2: {_lvmg = "Mi-290 Taru Transport : Police"};
default {_lvmg = "Mi-290 Taru Transport : Classique"};
};
};

case "O_Heli_Transport_04_covered_F":
{
switch (_index) do
{
case 0: {_lvmg = "Mi-290 Taru Couvert : Rebelle"};
case 1: {_lvmg = "Mi-290 Taru Couvert : Mercenaire"};
default {_lvmg = "Mi-290 Taru Couvert : Classique"};
};
};

case "B_Heli_Transport_03_unarmed_F":
{
switch (_index) do
{
default {_lvmg = "CH-67 Huron : Black"};
};
};

case "O_Heli_Transport_04_F":
{
switch (_index) do
{
case 0: {_lvmg = "Mi-290 Taru : Hook"};
case 1: {_lvmg = "Mi-290 Taru : Hook Police"};
case 2: {_lvmg = "Mi-290 Taru : Black Eagle"};
case 2: {_lvmg = "Mi-290 Taru : Dépanneur"};
default {_lvmg = "Mi-290 Taru : Hook"};
};
};

case "C_Heli_Light_01_civil_F":
{
switch (_index) do
{
default {_lvmg = "M-900"};
};
};

case "O_Heli_Transport_04_medevac_F":
{
switch (_index) do
{
default {_lvmg = "Mi-290 Taru : Secouriste"};
};
};

case "O_Heli_Transport_04_repair_F":
{
switch (_index) do
{
default {_lvmg = "Mi-290 Taru : Dépanneur"};
};
};

case "C_Kart_01_Blu_F": //Type Bleu
{
switch (_index) do
{
default {_lvmg = "Kart : BluKing"};
};
};
case "C_Kart_01_Fuel_F": //Type Kart
{
switch (_index) do
{

default {_lvmg = "Kart : Fuel"};
};
};
case "C_Kart_01_Vrana_F": //Type Kart
{
switch (_index) do
{

default {_lvmg = "Kart : Vrana"};
};
};

case "C_Kart_01_Red_F": //Type Kart
{
switch (_index) do
{

default {_lvmg = "Kart : RedStone"};
};
};

case "B_Quadbike_01_F": //Type Quad
{
switch (_index) do
{
case 0: {_lvmg = "Quad : Brun"};
case 1: {_lvmg = "Quad : Camo"};
case 2: {_lvmg = "Quad : Noir"};
case 3: {_lvmg = "Quad : Bleu"};
case 4: {_lvmg = "Quad : Rouge"};
case 5: {_lvmg = "Quad : Blanc"};
case 6: {_lvmg = "Quad : Vert Camo"};
case 7: {_lvmg = "Quad : Camo Hunter"};
case 8: {_lvmg = "Quad : Camo"};
case 9: {_lvmg = "Quad : Police"};
default {_lvmg = "Quad" };
};
};
case "C_Hatchback_01_F": //Type Hatchback
{
switch (_index) do
{
case 0: {_lvmg = "Hatchback : Beige";};
case 1: {_lvmg = "Hatchback : Vert";};
case 2: {_lvmg = "Hatchback : Bleu";};
case 3: {_lvmg = "Hatchback : Bleu nuit";};
case 4: {_lvmg = "Hatchback : Jaune";};
case 5: {_lvmg = "Hatchback : Blanc"};
case 6: {_lvmg = "Hatchback : Gris"};
case 7: {_lvmg = "Hatchback : Noir"};
case 8: {_lvmg = "Hatchback : La Coccinelle"};
case 9: {_lvmg = "Hatchback : Secouriste"};
case 10: {_lvmg = "Hatchback : Noel"};
default {_lvmg = "Hatchback" };
};
};
case "C_Hatchback_01_sport_F": //Type Voiture Hayon S 
{
switch(_index) do
{
case 0: {_lvmg = "Hatchback S : Rouge"};
case 1: {_lvmg = "Hatchback S : Bleu Nuit"};
case 2: {_lvmg = "Hatchback S : Orange"};
case 3: {_lvmg = "Hatchback S : Noir et Blanc"};
case 4: {_lvmg = "Hatchback S : Tan"};
case 5: {_lvmg = "Hatchback S : Vert"};
case 6: {_lvmg = "Hatchback S : La Coccinelle"};
case 7: {_lvmg = "Hatchback S : Police"};
case 8: {_lvmg = "Hatchback S : Vert Tribal"};
case 9: {_lvmg = "Hatchback S : Orange Sport"};
case 10: {_lvmg = "Hatchback S : Monster"};
case 11: {_lvmg = "Hatchback S : Secouriste"};
case 12: {_lvmg = "Hatchback S : Mercenaire"};
case 13: {_lvmg = "Hatchback S : Noel"};
default {_lvmg = "Hatchback S" };
};
};
case "C_Offroad_01_F": //Type Pick-Up
{
switch (_index) do
{
case 0: {_lvmg = "Pick-Up : Rouge";};
case 1: {_lvmg = "Pick-Up : Jaune";};
case 2: {_lvmg = "Pick-Up : Blanc";};
case 3: {_lvmg = "Pick-Up : Bleu";};
case 4: {_lvmg = "Pick-Up : Pourpre";};
case 5: {_lvmg = "Pick-Up : Bleu et Blanc"};
case 6: {_lvmg = "Pick-Up : Noir"};
case 7: {_lvmg = "Pick-Up : Police"};
case 8: {_lvmg = "Pick-Up : Taxi"};
case 9: {_lvmg = "Pick-Up : Depanneuse"};
default {_lvmg = "Pick-Up" };
};
};
case "C_SUV_01_F": //Type SUV
{
switch (_index) do
{
case 0: {_lvmg = "SUV : Rouge pourpre";};
case 1: {_lvmg = "SUV : Noir";};
case 2: {_lvmg = "SUV : Argent";};
case 3: {_lvmg = "SUV : Orange";};
case 4: {_lvmg = "SUV : Police";};
case 5: {_lvmg = "SUV : Taxi";};
case 6: {_lvmg = "SUV : Rebelle";};
case 7: {_lvmg = "SUV : Secouriste";};
case 8: {_lvmg = "SUV : Mercenaire"};
default {_lvmg = "SUV" };
};
};
case "C_Van_01_transport_F": //Type Camion-Benne
{
switch (_index) do
{
case 0: {_lvmg = "Camion-Benne : Blanc"};
case 1: {_lvmg = "Camion-Benne : Rouge"};
default {_lvmg = "Camion-Benne" };
};
};
case "C_Van_01_box_F": //Type Fourgon
{
switch (_index) do
{
case 0: {_lvmg = "Fourgon : Blanc"};
case 1: {_lvmg = "Fourgon : Rouge"};
default {_lvmg = "Fourgon" };
};
};
case "C_Van_01_Fuel_F": //Type Camion Citerne
{
switch (_index) do
{
case 0: {_lvmg = "Camion Citerne : Blanc"};
case 1: {_lvmg = "Camion Citerne : Rouge"};
default {_lvmg = "Camion Citerne" };
};
};
case "I_Truck_02_transport_F": //Type Zamak Transporteur
{
switch (_index) do
{
case 0: {_lvmg = "Zamak Transport : Orange"};
case 1: {_lvmg = "Zamak Transport : Noir"};
default {_lvmg = "Zamak Transport" };
};
};
case "I_Truck_02_covered_F": //Type Zamak Transporteur Couvert
{
switch (_index) do
{
case 0: {_lvmg = "Zamak Transport Couvert : Orange"};
case 1: {_lvmg = "Zamak Transport Couvert : Noir"};
default {_lvmg = "Zamak Transport Couvert" };
};
};
case "I_Truck_02_box_F": //Type Zamak Ammo
{
switch (_index) do
{
case 0: {_lvmg = "Camion Pegasus"};
default {_lvmg = "Camion Pegasus" };
};
};
case "B_Truck_01_transport_F": //Type HEMTT Transport
{
switch (_index) do
{
default {_lvmg = "HEMTT Transport" };
};
};
case "B_Truck_01_box_F": //Type HEMTT Cargaison
{
switch (_index) do
{
default {_lvmg = "HEMTT Cargaison" };
};
};
case "O_Truck_03_device_F": //Type Tempest
{
switch (_index) do
{
default {_lvmg = "Tempest" };
};
};

// Hélicos

case "B_Heli_Light_01_F": //Type MH-9 "Hummingbird"
{
switch (_index) do
{
case 0: {_lvmg = "MH-9 : Sheriff"};
case 1: {_lvmg = "MH-9 : Noir"};
case 2: {_lvmg = "MH-9 : Bleu Clair"};
case 3: {_lvmg = "MH-9 : Rouge Clair"};
case 4: {_lvmg = "MH-9 : Vert Camo"};
case 5: {_lvmg = "MH-9 : Bleu"};
case 6: {_lvmg = "MH-9 : Elliptical"};
case 7: {_lvmg = "MH-9 : Furious"};
case 8: {_lvmg = "MH-9 : Jeans Blue"};
case 9: {_lvmg = "MH-9 : Speedy Redline"};
case 10: {_lvmg = "MH-9 : Sunset"};
case 11: {_lvmg = "MH-9 : Vrana"};
case 12: {_lvmg = "MH-9 : Waves Blue"};
case 13: {_lvmg = "MH-9: Rebel Digital"};
case 14: {_lvmg = "MH-9: Secouriste"};
case 15: {_lvmg = "MH-9: Mercenaire"};
case 16: {_lvmg = "MH-9 : Wasp"};
default {_lvmg = "MH-9" };
};
};
case "C_Heli_Light_01_civil_F":
{
switch (_index) do
{
case 0: {_lvmg  = "M900 : Blueline"};
case 1: {_lvmg  = "M900 : Elliptical"};
case 2: {_lvmg = "M900 : Furious"};
case 3: {_lvmg  = "M900 : Graywatcher"};
case 4: {_lvmg  = "M900 : Jeans"};
case 5: {_lvmg  = "M900 : Light"};
case 6: {_lvmg  = "M900 : Shadow"};
case 7: {_lvmg  = "M900 : Sheriff"};
case 8: {_lvmg  = "M900 : Speedy"};
case 9: {_lvmg  = "M900 : Sunset"};
case 10: {_lvmg  = "M900 : Vrana"};
case 11: {_lvmg  = "M900 : Wasp"};
case 12: {_lvmg = "M900 : Wave"};
case 13:	{_lvmg = "Black Eagle"};
default {_lvmg = "M-900"};
};
};
case "B_Heli_Transport_01_F": //Type Ghost Hawk
{
switch (_index) do
{
default {_lvmg = "Ghost Hawk" };
};
};
case "B_Heli_Attack_01_F": //Type Blackfoot
{
switch (_index) do
{
default {_lvmg = "Blackfoot" };
};
};
case "O_Heli_Light_02_unarmed_F": //Type Orca
{
switch (_index) do
{
case 0: {_lvmg = "Orca : Noir"};
case 1: {_lvmg = "Orca : Blanc et Bleu"};
case 2: {_lvmg = "Orca : Camo Vert"};
case 3: {_lvmg = "Orca : Camo desert"};
case 4: {_lvmg = "Orca : Secouriste"};
default {_lvmg = "Orca" };
};
};
case "I_Heli_Transport_02_F": //Type Mohawk
{
switch (_index) do
{
case 0: {_lvmg = "Mohawk : Ion"};
case 1: {_lvmg = "Mohawk : Dahoman"};
case 2: {_lvmg = "Black Eagle"};
default {_lvmg = "Mohawk" };
};
};
case "I_Heli_light_03_unarmed_F": //Type Hellcat
{
switch (_index) do
{
case 0: {_lvmg = "Hellcat : Rebelle"};
case 1: {_lvmg = "Hellcat :Police Noir"};
case 2: {_lvmg = "Hellcat : Mercenaire"};
case 3: {_lvmg = "Hellcat : Rebelle Camo"};
default {_lvmg = "Hellcat" };
};
};

// Blindés

case "B_G_Offroad_01_F": //Type Pick-Up
{
switch (_index) do
{
case 0: {_lvmg = "Pick-Up : Rebelle" };
case 1: {_lvmg = "Pick-Up : Mercenaire" };

default {_lvmg = "Pick-Up" };
};
};
case "B_G_Offroad_01_armed_F": //Type Pick-Up Armé
{
switch (_index) do
{
case 0: {_lvmg = "Pick-Up Armé : Rebelle";};
case 1: {_lvmg = "Pick-Up Armé : Police";};
case 2: {_lvmg = "Pick-Up Armé : Mercenaire";};
default {_lvmg = "Pick-Up Armé" };
};
};
case "O_MRAP_02_F": //Type Ifrit
{
switch (_index) do
{
case 0: {_lvmg = "Ifrit : Classique";};
case 1: {_lvmg = "Ifrit : Camo vert";};
default {_lvmg = "Ifrit" };
};
};
case "B_MRAP_01_F": //Type Hunter
{
switch (_index) do
{
case 0: {_lvmg = "Hunter : Police noir"};
case 1: {_lvmg = "Hunter : Mercenaire"};
default {_lvmg = "Hunter" };
};
};
case "B_MRAP_01_hmg_F": //Type Hunter Armé
{
switch (_index) do
{
default {_lvmg = "Hunter HMG" };
};
};
case "I_MRAP_03_F": //Type Strider
{
switch (_index) do
{
case 0: {_lvmg = "Strider : Police"};
case 1: {_lvmg = "Strider : Mercenaire"};
default {_lvmg = "Strider" };
};
};

// Bateaux

case "C_Rubberboat": //Type Canot de sauvetage
{
switch (_index) do
{
default {_lvmg = "Canot de sauvetage" };
};
};
case "C_Boat_Civil_01_F": //Type Hors-bord
{
switch (_index) do
{
default {_lvmg = "Hors-bord" };
};
};
case "I_G_Boat_Transport_01_F": //Type Canot d'Assaut
{
switch (_index) do
{
default {_lvmg = "Canot d'Assaut" };
};
};


case "B_Boat_Transport_01_F": //Type Canot d'Assaut
{
switch (_index) do
{
default {_lvmg = "Canot d'Assaut"};
};
};
case "C_Boat_Civil_01_police_F": //Type Hors-bord
{
switch (_index) do
{
default {_lvmg = "Hors-bord Police"};
};
};
case "B_Boat_Armed_01_minigun_F": //Type Bateau d'assaut Minigun
{
switch (_index) do
{
default {_lvmg = "Bateau d'assaut Minigun"};
};
};
case "B_SDV_01_F": //Type Sous-marin
{
switch (_index) do
{
default {_lvmg = "Sous-marin" };
};
};

/////////////////////////////////////////////////////////////////////////////////
//Vélo
/////////////////////////////////////////////////////////////////////////////////
case "cl3_bike_bmx":{_lvmg="BMX"};
case "cl3_bike_bmx_Road2tone1":{_lvmg="BMX bleu/vert"};
case "cl3_bike_bmx_Road2tone2":{_lvmg="BMX violet/noir"};
case "cl3_bike_bmx_Road2tone3":{_lvmg="BMX turquoise/blanc"};
case "cl3_bike_bmx_Road2tone4":{_lvmg="BMX vert/jaune"};
case "cl3_bike_bmx_Road2tone5":{_lvmg="BMX orange/jaune"};
case "cl3_bike_bmx_Roadaqua":{_lvmg="BMX aqua"};
case "cl3_bike_bmx_Roadbaby_blue":{_lvmg="BMX bleu vif"};
case "cl3_bike_bmx_Roadbaby_pink":{_lvmg="BMX rose vif"};
case "cl3_bike_bmx_Roadburgundy":{_lvmg="BMX bordeaux"};
case "cl3_bike_bmx_Roadcarbon":{_lvmg="BMX carbone"};
case "cl3_bike_bmx_Roadcardinal":{_lvmg="BMX rouge cardinal"};
case "cl3_bike_bmx_Roaddark_green":{_lvmg="BMX vert sombre"};
case "cl3_bike_bmx_Roadgold":{_lvmg="BMX or"};
case "cl3_bike_bmx_Roadgreen":{_lvmg="BMX vert"};
case "cl3_bike_bmx_Roadgrey":{_lvmg="BMX gris"};
case "cl3_bike_bmx_Roadlavender":{_lvmg="BMX lavende"};
case "cl3_bike_bmx_Roadlight_blue":{_lvmg="BMX bleu clair"};
case "cl3_bike_bmx_Roadlight_yellow":{_lvmg="BMX jaune clair"};
case "cl3_bike_bmx_Roadlime":{_lvmg="BMX Jaune citron"};
case "cl3_bike_bmx_Roadmarina_blue":{_lvmg="BMX bleu marine"};
case "cl3_bike_bmx_Roadnavy_blue":{_lvmg="BMX bleu profond"};
case "cl3_bike_bmx_Roadorange":{_lvmg="BMX orange"};
case "cl3_bike_bmx_Roadpink":{_lvmg="BMX rose"};
case "cl3_bike_bmx_Roadpurple":{_lvmg="BMX violet"};
case "cl3_bike_bmx_Roadred":{_lvmg="BMX rouge"};
case "cl3_bike_bmx_Roadsand":{_lvmg="BMX sable"};
case "cl3_bike_bmx_Roadsilver":{_lvmg="BMX argent"};
case "cl3_bike_bmx_Roadskin_black":{_lvmg="BMX noir"};
case "cl3_bike_bmx_Roadskin_blue":{_lvmg="BMX bleu"};
case "cl3_bike_bmx_Roadskin_camo":{_lvmg="BMX camouflage"};
case "cl3_bike_bmx_Roadskin_camo_urban":{_lvmg="BMX camouflage urbain"};
case "cl3_bike_bmx_Roadskin_darkgreen":{_lvmg="BMX vert sombre"};
case "cl3_bike_bmx_Roadskin_darkred":{_lvmg="BMX rouge sombre"};
case "cl3_bike_bmx_Roadskin_flame":{_lvmg="BMX flamme"};
case "cl3_bike_bmx_Roadskin_flame1":{_lvmg="BMX flamme2"};
case "cl3_bike_bmx_Roadskin_flame2":{_lvmg="BMX flamme3"};
case "cl3_bike_bmx_Roadskin_green":{_lvmg="BMX vert"};
case "cl3_bike_bmx_Roadskin_orange":{_lvmg="BMX orange"};
case "cl3_bike_bmx_Roadskin_pink":{_lvmg="BMX rose"};
case "cl3_bike_bmx_Roadskin_purple":{_lvmg="BMX violet"};
case "cl3_bike_bmx_Roadskin_red":{_lvmg="BMX rouge"};
case "cl3_bike_bmx_Roadskin_silver":{_lvmg="BMX argent"};
case "cl3_bike_bmx_Roadskin_white":{_lvmg="BMX blanc"};
case "cl3_bike_bmx_Roadskin_yellow":{_lvmg="BMX jaune"};
case "cl3_bike_bmx_Roadsky_blue":{_lvmg="BMX bleu"};
case "cl3_bike_bmx_Roadviolet":{_lvmg="BMX fushia"};
case "cl3_bike_bmx_Roadyellow":{_lvmg="BMX jaune"};

case "cl3_bike_mountain": {_lvmg="VTT"};
case "cl3_bike_mountain2tone1": {_lvmg="VTT bleu/vert"};
case "cl3_bike_mountain2tone2":{_lvmg="VTT violet/noir"};
case "cl3_bike_mountain2tone3":{_lvmg="VTT turquoise/blanc"};
case "cl3_bike_mountain2tone4":{_lvmg="VTT vert/jaune"};
case "cl3_bike_mountain2tone5":{_lvmg="VTT orange/jaune"};
case "cl3_bike_mountainaqua":{_lvmg="VTT aqua"};
case "cl3_bike_mountainbaby_blue":{_lvmg="VTT bleu vif"};
case "cl3_bike_mountainbaby_pink":{_lvmg="VTT rose vif"};
case "cl3_bike_mountainburgundy":{_lvmg="VTT bordeaux"};
case "cl3_bike_mountaincarbon":{_lvmg="VTT carbone"};
case "cl3_bike_mountaincardinal":{_lvmg="VTT rouge cardinal"};
case "cl3_bike_mountaindark_green":{_lvmg="VTT vert sombre"};
case "cl3_bike_mountaingold":{_lvmg="VTT or"};
case "cl3_bike_mountaingreen":{_lvmg="VTT vert"};
case "cl3_bike_mountaingrey":{_lvmg="VTT gris"};
case "cl3_bike_mountainlavender":{_lvmg="VTT lavende"};
case "cl3_bike_mountainlight_blue":{_lvmg="VTT bleu clair"};
case "cl3_bike_mountainlight_yellow":{_lvmg="VTT jaune clair"};
case "cl3_bike_mountainlime":{_lvmg="VTT Jaune citron"};
case "cl3_bike_mountainmarina_blue":{_lvmg="VTT bleu marine"};
case "cl3_bike_mountainnavy_blue":{_lvmg="VTT bleu profond"};
case "cl3_bike_mountainorange":{_lvmg="VTT orange"};
case "cl3_bike_mountainpink":{_lvmg="VTT rose"};
case "cl3_bike_mountainpurple":{_lvmg="VTT violet"};
case "cl3_bike_mountainred":{_lvmg="VTT rouge"};
case "cl3_bike_mountainsand":{_lvmg="VTT sable"};
case "cl3_bike_mountainsilver":{_lvmg="VTT argent"};
case "cl3_bike_mountainskin_black":{_lvmg="VTT noir"};
case "cl3_bike_mountainskin_blue":{_lvmg="VTT bleu"};
case "cl3_bike_mountainskin_camo":{_lvmg="VTT camouflage"};
case "cl3_bike_mountainskin_camo_urban":{_lvmg="VTT camouflage urbain"};
case "cl3_bike_mountainskin_darkgreen":{_lvmg="VTT vert sombre"};
case "cl3_bike_mountainskin_darkred":{_lvmg="VTT rouge sombre"};
case "cl3_bike_mountainskin_flame":{_lvmg="VTT flamme"};
case "cl3_bike_mountainskin_flame1":{_lvmg="VTT flamme2"};
case "cl3_bike_mountainskin_flame2":{_lvmg="VTT flamme3"};
case "cl3_bike_mountainskin_green":{_lvmg="VTT vert"};
case "cl3_bike_mountainskin_orange":{_lvmg="VTT orange"};
case "cl3_bike_mountainskin_pink":{_lvmg="VTT rose"};
case "cl3_bike_mountainskin_purple":{_lvmg="VTT violet"};
case "cl3_bike_mountainskin_red":{_lvmg="VTT rouge"};
case "cl3_bike_mountainskin_silver":{_lvmg="VTT argent"};
case "cl3_bike_mountainskin_white":{_lvmg="VTT blanc"};
case "cl3_bike_mountainskin_yellow":{_lvmg="VTT jaune"};
case "cl3_bike_mountainsky_blue":{_lvmg="VTT bleu"};
case "cl3_bike_mountainviolet":{_lvmg="VTT fushia"};
case "cl3_bike_mountainyellow":{_lvmg="VTT jaune"};

case "cl3_bike_Road":{_lvmg="Bicyclette"};
case "cl3_bike_Road2tone1":{_lvmg="Bicyclette bleu/vert"};
case "cl3_bike_Road2tone2":{_lvmg="Bicyclette violet/noir"};
case "cl3_bike_Road2tone3":{_lvmg="Bicyclette turquoise/blanc"};
case "cl3_bike_Road2tone4":{_lvmg="Bicyclette vert/jaune"};
case "cl3_bike_Road2tone5":{_lvmg="Bicyclette orange/jaune"};
case "cl3_bike_Roadaqua":{_lvmg="Bicyclette aqua"};
case "cl3_bike_Roadbaby_blue":{_lvmg="Bicyclette bleu vif"};
case "cl3_bike_Roadbaby_pink":{_lvmg="Bicyclette rose vif"};
case "cl3_bike_Roadburgundy":{_lvmg="Bicyclette rouge bordeaux"};
case "cl3_bike_Roadcarbon":{_lvmg="Bicyclette carbone"};
case "cl3_bike_Roadcardinal":{_lvmg="Bicyclette rouge cardinal"};
case "cl3_bike_Roaddark_green":{_lvmg="Bicyclette vert sombre"};
case "cl3_bike_Roadgold":{_lvmg="Bicyclette or"};
case "cl3_bike_Roadgreen":{_lvmg="Bicyclette vert"};
case "cl3_bike_Roadgrey":{_lvmg="Bicyclette gris"};
case "cl3_bike_Roadlavender":{_lvmg="Bicyclette lavende"};
case "cl3_bike_Roadlight_blue":{_lvmg="Bicyclette bleu clair"};
case "cl3_bike_Roadlight_yellow":{_lvmg="Bicyclette jaune clair"};
case "cl3_bike_Roadlime":{_lvmg="Bicyclette Jaune citron"};
case "cl3_bike_Roadmarina_blue":{_lvmg="Bicyclette bleu marine"};
case "cl3_bike_Roadnavy_blue":{_lvmg="Bicyclette bleu profond"};
case "cl3_bike_Roadorange":{_lvmg="Bicyclette orange"};
case "cl3_bike_Roadpink":{_lvmg="Bicyclette rose"};
case "cl3_bike_Roadpurple":{_lvmg="Bicyclette violet"};
case "cl3_bike_Roadred":{_lvmg="Bicyclette rouge"};
case "cl3_bike_Roadsand":{_lvmg="Bicyclette sable"};
case "cl3_bike_Roadsilver":{_lvmg="Bicyclette argent"};
case "cl3_bike_Roadskin_black":{_lvmg="Bicyclette noir"};
case "cl3_bike_Roadskin_blue":{_lvmg="Bicyclette bleu"};
case "cl3_bike_Roadskin_camo":{_lvmg="Bicyclette camouflage"};
case "cl3_bike_Roadskin_camo_urban":{_lvmg="Bicyclette camouflage urbain"};
case "cl3_bike_Roadskin_darkgreen":{_lvmg="Bicyclette vert sombre"};
case "cl3_bike_Roadskin_darkred":{_lvmg="Bicyclette rouge sombre"};
case "cl3_bike_Roadskin_flame":{_lvmg="Bicyclette flamme"};
case "cl3_bike_Roadskin_flame1":{_lvmg="Bicyclette flamme2"};
case "cl3_bike_Roadskin_flame2":{_lvmg="Bicyclette flamme3"};
case "cl3_bike_Roadskin_green":{_lvmg="Bicyclette vert"};
case "cl3_bike_Roadskin_orange":{_lvmg="Bicyclette orange"};
case "cl3_bike_Roadskin_pink":{_lvmg="Bicyclette rose"};
case "cl3_bike_Roadskin_purple":{_lvmg="Bicyclette violet"};
case "cl3_bike_Roadskin_red":{_lvmg="Bicyclette rouge"};
case "cl3_bike_Roadskin_silver":{_lvmg="Bicyclette argent"};
case "cl3_bike_Roadskin_white":{_lvmg="Bicyclette blanc"};
case "cl3_bike_Roadskin_yellow":{_lvmg="Bicyclette jaune"};
case "cl3_bike_Roadsky_blue":{_lvmg="Bicyclette bleu"};
case "cl3_bike_Roadviolet":{_lvmg="Bicyclette violet"};
case "cl3_bike_Roadyellow":{_lvmg="Bicyclette jaune"};


/////////////////////////////////////////////////////////////////////////////////
//CIV_Moto
/////////////////////////////////////////////////////////////////////////////////

case "cl3_enduro":{_lvmg="Enduro"};
case "cl3_enduro_aqua":{_lvmg="Enduro - aqua"};
case "cl3_enduro_babyblue":{_lvmg="Enduro - bleu clair"};
case "cl3_enduro_babypink":{_lvmg="Enduro - rose clair"};
case "cl3_enduro_black":{_lvmg="Enduro - noir"};
case "cl3_enduro_blue":{_lvmg="Enduro - bleu"};
case "cl3_enduro_burgundy":{_lvmg="Enduro - rouge bordeaux"};
case "cl3_enduro_camo":{_lvmg="Enduro - camouflage"};
case "cl3_enduro_camo_urban":{_lvmg="Enduro - camouflage urbain"};
case "cl3_enduro_cardinal":{_lvmg="Enduro - rouge cardinal"};
case "cl3_enduro_dark_green":{_lvmg="Enduro - vert sombre"};
case "cl3_enduro_emt":{_lvmg="Enduro - médecin"};
case "cl3_enduro_flame":{_lvmg="Enduro - flamme"};
case "cl3_enduro_flame1":{_lvmg="Enduro - flamme2"};
case "cl3_enduro_flame2":{_lvmg="Enduro - flamme3"};
case "cl3_enduro_gold":{_lvmg="Enduro - or"};
case "cl3_enduro_green":{_lvmg="Enduro - vert"};
case "cl3_enduro_grey":{_lvmg="Enduro - gris"};
case "cl3_enduro_lavender":{_lvmg="Enduro - lavende"};
case "cl3_enduro_light_blue":{_lvmg="Enduro - bleu vif"};
case "cl3_enduro_light_yellow":{_lvmg="Enduro - jaune vif"};
case "cl3_enduro_lime":{_lvmg="Enduro - jaune citron"};
case "cl3_enduro_marina_blue":{_lvmg="Enduro - bleu marine"};
case "cl3_enduro_navy_blue":{_lvmg="Enduro - bleu navy"};
case "cl3_enduro_orange":{_lvmg="Enduro - orange"};
case "cl3_enduro_police":{_lvmg="Enduro - Police"};
case "cl3_enduro_purple":{_lvmg="Enduro - violet"};
case "cl3_enduro_red":{_lvmg="Enduro - rouge"};
case "cl3_enduro_sand":{_lvmg="Enduro - sable"};
case "cl3_enduro_silver":{_lvmg="Enduro - argent"};
case "cl3_enduro_violet":{_lvmg="Enduro - fushia"};
case "cl3_enduro_white":{_lvmg="Enduro - blanc"};
case "cl3_enduro_yellow":{_lvmg="Enduro - jaune"};

case "cl3_xr_1000":{_lvmg="XR 1000"};
case "cl3_xr_1000_aqua":{_lvmg="XR 1000 - aqua"};
case "cl3_xr_1000_babyblue":{_lvmg="XR 1000 - bleu clair"};
case "cl3_xr_1000_babypink":{_lvmg="XR 1000 - rose clair"};
case "cl3_xr_1000_black":{_lvmg="XR 1000 - noir"};
case "cl3_xr_1000_blue":{_lvmg="XR 1000 - bleu"};
case "cl3_xr_1000_burgundy":{_lvmg="XR 1000 - rouge bordeaux"};
case "cl3_xr_1000_camo":{_lvmg="XR 1000 - camouflage"};
case "cl3_xr_1000_camo_urban":{_lvmg="XR 1000 - camouflage urbain"};
case "cl3_xr_1000_cardinal":{_lvmg="XR 1000 - rouge cardinal"};
case "cl3_xr_1000_dark_green":{_lvmg="XR 1000 - vert sombre"};
case "cl3_xr_1000_emt":{_lvmg="XR 1000 - médecin"};
case "cl3_xr_1000_flame":{_lvmg="XR 1000 - flamme"};
case "cl3_xr_1000_flame1":{_lvmg="XR 1000 - flamme2"};
case "cl3_xr_1000_flame2":{_lvmg="XR 1000 - flamme3"};
case "cl3_xr_1000_gold":{_lvmg="XR 1000 - or"};
case "cl3_xr_1000_green":{_lvmg="XR 1000 - vert"};
case "cl3_xr_1000_grey":{_lvmg="XR 1000 - gris"};
case "cl3_xr_1000_lavender":{_lvmg="XR 1000 - lavende"};
case "cl3_xr_1000_light_blue":{_lvmg="XR 1000 - bleu vif"};
case "cl3_xr_1000_light_yellow":{_lvmg="XR 1000 - jaune vif"};
case "cl3_xr_1000_lime":{_lvmg="XR 1000 - jaune citron"};
case "cl3_xr_1000_marina_blue":{_lvmg="XR 1000 - bleu marine"};
case "cl3_xr_1000_navy_blue":{_lvmg="XR 1000 - bleu navy"};
case "cl3_xr_1000_orange":{_lvmg="XR 1000 - orange"};
case "cl3_xr_1000_police":{_lvmg="XR 1000 - Police"};
case "cl3_xr_1000_purple":{_lvmg="XR 1000 - violet"};
case "cl3_xr_1000_red":{_lvmg="XR 1000 - rouge"};
case "cl3_xr_1000_sand":{_lvmg="XR 1000 - sable"};
case "cl3_xr_1000_silver":{_lvmg="XR 1000 - argent"};
case "cl3_xr_1000_violet":{_lvmg="XR 1000 - fushia"};
case "cl3_xr_1000_white":{_lvmg="XR 1000 - blanc"};

case "cl3_chopper":{_lvmg="Chopper"};
case "cl3_chopper_blue":{_lvmg="Chopper - bleu"};
case "cl3_chopper_gold":{_lvmg="Chopper - or"};
case "cl3_chopper_green":{_lvmg="Chopper - vert"};
case "cl3_chopper_red":{_lvmg="Chopper - rouge"};
case "cl3_chopper_silver":{_lvmg="Chopper - argent"};

/////////////////////////////////////////////////////////////////////////////////
//CIV_LOW
/////////////////////////////////////////////////////////////////////////////////

case "cl3_lada":{_lvmg="Lada"};
case "cl3_lada_red":{_lvmg="Lada - rouge"};
case "cl3_lada_white":{_lvmg="Lada - blanc"};

case "cl3_volha":{_lvmg="Volha"};
case "cl3_volha_black":{_lvmg="Volha - noir"};
case "cl3_volha_grey":{_lvmg="Volha - gris"};

case "cl3_440cuda":{_lvmg="Plymouth"};
case "cl3_440cuda_black":{_lvmg="Plymouth - noir"};
case "cl3_440cuda_black1":{_lvmg="Plymouth - noir"};
case "cl3_440cuda_blu_ltn":{_lvmg="Plymouth - bleu léger"};
case "cl3_440cuda_blue":{_lvmg="Plymouth - bleu"};
case "cl3_440cuda_blue_flame":{_lvmg="Plymouth - bleu flamme"};
case "cl3_440cuda_emt_chief":{_lvmg="Plymouth - médecin"};
case "cl3_440cuda_flannery08":{_lvmg="Plymouth - 12"};
case "cl3_440cuda_flannery12":{_lvmg="Plymouth - 14"};
case "cl3_440cuda_flannery14":{_lvmg="Plymouth - 27"};
case "cl3_440cuda_flannery27":{_lvmg="Plymouth - 51"};
case "cl3_440cuda_flannery51":{_lvmg="Plymouth - 55"};
case "cl3_440cuda_flannery55":{_lvmg="Plymouth - 69"};
case "cl3_440cuda_flannery69":{_lvmg="Plymouth - 70"};
case "cl3_440cuda_flannery70":{_lvmg="Plymouth - 70"};
case "cl3_440cuda_green":{_lvmg="Plymouth - vert"};
case "cl3_440cuda_green_Logo":{_lvmg="Plymouth - vert logo"};
case "cl3_440cuda_yellow":{_lvmg="Plymouth - jaune"};
case "cl3_440cuda_yellow_stripe":{_lvmg="Plymouth - rayure jaune"};

case "cl3_bounder_beige":{_lvmg="Camping car"};

case "cl3_civic_vti":{_lvmg="Honda civic"};
case "cl3_civic_vti_aqua":{_lvmg="Honda civic - aqua"};
case "cl3_civic_vti_babyblue":{_lvmg="Honda civic - bleu vif"};
case "cl3_civic_vti_babypink":{_lvmg="Honda civic - rose vif"};
case "cl3_civic_vti_black":{_lvmg="Honda civic - noir"};
case "cl3_civic_vti_blue":{_lvmg="Honda civic - bleu"};
case "cl3_civic_vti_burgundy":{_lvmg="Honda civic - rouge bordeaux"};
case "cl3_civic_vti_camo":{_lvmg="Honda civic - camouflage"};
case "cl3_civic_vti_camo_urban":{_lvmg="Honda civic - camouflage urbain"};
case "cl3_civic_vti_cardinal":{_lvmg="Honda civic - rouge cardinal"};
case "cl3_civic_vti_dark_green":{_lvmg="Honda civic - vert sombre"};
case "cl3_civic_vti_gold":{_lvmg="Honda civic - orange"};
case "cl3_civic_vti_green":{_lvmg="Honda civic - vert sombre"};
case "cl3_civic_vti_grey":{_lvmg="Honda civic - gris"};
case "cl3_civic_vti_lavender":{_lvmg="Honda civic - lavende"};
case "cl3_civic_vti_light_blue":{_lvmg="Honda civic - bleu clair"};
case "cl3_civic_vti_light_yellow":{_lvmg="Honda civic - jaune clair"};
case "cl3_civic_vti_lime":{_lvmg="Honda civic - jaune citron"};
case "cl3_civic_vti_marina_blue":{_lvmg="Honda civic - bleu marine"};
case "cl3_civic_vti_navy_blue":{_lvmg="Honda civic - bleu navy"};
case "cl3_civic_vti_orange":{_lvmg="Honda civic - orange"};
case "cl3_civic_vti_purple":{_lvmg="Honda civic - violet"};
case "cl3_civic_vti_red":{_lvmg="Honda civic - rouge"};
case "cl3_civic_vti_sand":{_lvmg="Honda civic - sable"};
case "cl3_civic_vti_silver":{_lvmg="Honda civic - argent"};
case "cl3_civic_vti_violet":{_lvmg="Honda civic - fushia"};
case "cl3_civic_vti_white":{_lvmg="Honda civic - blanc"};
case "cl3_civic_vti_yellow":{_lvmg="Honda civic - jaune"};

case "cl3_crown_victoria":{_lvmg="Ford Crown Victoria"};
case "cl3_crown_victoria_aqua":{_lvmg="Ford Crown Victoria - aqua"};
case "cl3_crown_victoria_babyblue":{_lvmg="Ford Crown Victoria - bleu vif"};
case "cl3_crown_victoria_babypink":{_lvmg="Ford Crown Victoria - rose vif"};
case "cl3_crown_victoria_black":{_lvmg="Ford Crown Victoria - noir"};
case "cl3_crown_victoria_blue":{_lvmg="Ford Crown Victoria - bleu"};
case "cl3_crown_victoria_burgundy":{_lvmg="Ford Crown Victoria - rouge bordeaux"};
case "cl3_crown_victoria_camo":{_lvmg="Ford Crown Victoria - camouflage"};
case "cl3_crown_victoria_camo_urban":{_lvmg="Ford Crown Victoria - camouflage urbain"};
case "cl3_crown_victoria_cardinal":{_lvmg="Ford Crown Victoria - rouge cardinal"};
case "cl3_crown_victoria_dark_green":{_lvmg="Ford Crown Victoria - vert sombre"};
case "cl3_crown_victoria_gold":{_lvmg="Ford Crown Victoria - orange"};
case "cl3_crown_victoria_green":{_lvmg="Ford Crown Victoria - vert sombre"};
case "cl3_crown_victoria_grey":{_lvmg="Ford Crown Victoria - gris"};
case "cl3_crown_victoria_lavender":{_lvmg="Ford Crown Victoria - lavende"};
case "cl3_crown_victoria_light_blue":{_lvmg="Ford Crown Victoria - bleu clair"};
case "cl3_crown_victoria_light_yellow":{_lvmg="Ford Crown Victoria - jaune clair"};
case "cl3_crown_victoria_lime":{_lvmg="Ford Crown Victoria - jaune citron"};
case "cl3_crown_victoria_marina_blue":{_lvmg="Ford Crown Victoria - bleu marine"};
case "cl3_crown_victoria_navy_blue":{_lvmg="Ford Crown Victoria - bleu navy"};
case "cl3_crown_victoria_orange":{_lvmg="Ford Crown Victoria - orange"};
case "cl3_crown_victoria_purple":{_lvmg="Ford Crown Victoria - violet"};
case "cl3_crown_victoria_red":{_lvmg="Ford Crown Victoria - rouge"};
case "cl3_crown_victoria_sand":{_lvmg="Ford Crown Victoria - sable"};
case "cl3_crown_victoria_silver":{_lvmg="Ford Crown Victoria - argent"};
case "cl3_crown_victoria_violet":{_lvmg="Ford Crown Victoria - fushia"};
case "cl3_crown_victoria_white":{_lvmg="Ford Crown Victoria - blanc"};
case "cl3_crown_victoria_yellow":{_lvmg="Ford Crown Victoria - jaune"};

case "cl3_defender_110":{_lvmg="Land Rover"};
case "cl3_defender_110_cammo":{_lvmg="Land Rover - camouflage"};
case "cl3_defender_110_red":{_lvmg="Land Rover - rouge"};
case "cl3_defender_110_yellow":{_lvmg="Land Rover - jaune"};

case "cl3_discovery":{_lvmg="Range Rover"};
case "cl3_discovery_black":{_lvmg="Range Rover - noir"};
case "cl3_discovery_blue":{_lvmg="Range Rover - bleu"};
case "cl3_discovery_darkorange":{_lvmg="Range Rover - orange sombre"};
case "cl3_discovery_gold":{_lvmg="Range Rover - or"};
case "cl3_discovery_green":{_lvmg="Range Rover - vert"};
case "cl3_discovery_hellokitty":{_lvmg="Range Rover - hello kitty"};
case "cl3_discovery_joker":{_lvmg="Range Rover - joker"};
case "cl3_discovery_pink":{_lvmg="Range Rover - rose"};
case "cl3_discovery_silver":{_lvmg="Range Rover - argent"};

case "cl3_golf_mk2":{_lvmg="Golf mk2"};
case "cl3_golf_mk2_aqua":{_lvmg="Golf mk2 -aqua"};
case "cl3_golf_mk2_babyblue":{_lvmg="Golf mk2 -bleu vif"};
case "cl3_golf_mk2_babypink":{_lvmg="Golf mk2 -rose vif"};
case "cl3_golf_mk2_black":{_lvmg="Golf mk2 -noir"};
case "cl3_golf_mk2_blue":{_lvmg="Golf mk2 -bleu"};
case "cl3_golf_mk2_burgundy":{_lvmg="Golf mk2 -rouge bordeaux"};
case "cl3_golf_mk2_camo":{_lvmg="Golf mk2 -camouflage"};
case "cl3_golf_mk2_camo_urban":{_lvmg="Golf mk2 -camouflage urbain"};
case "cl3_golf_mk2_cardinal":{_lvmg="Golf mk2 -rouge cardinal"};
case "cl3_golf_mk2_dark_green":{_lvmg="Golf mk2 -vert sombre"};
case "cl3_golf_mk2_gold":{_lvmg="Golf mk2 -orange"};
case "cl3_golf_mk2_green":{_lvmg="Golf mk2 -vert sombre"};
case "cl3_golf_mk2_grey":{_lvmg="Golf mk2 -gris"};
case "cl3_golf_mk2_lavender":{_lvmg="Golf mk2 -lavende"};
case "cl3_golf_mk2_light_blue":{_lvmg="Golf mk2 -bleu clair"};
case "cl3_golf_mk2_light_yellow":{_lvmg="Golf mk2 -jaune clair"};
case "cl3_golf_mk2_lime":{_lvmg="Golf mk2 -jaune citron"};
case "cl3_golf_mk2_marina_blue":{_lvmg="Golf mk2 -bleu marine"};
case "cl3_golf_mk2_navy_blue":{_lvmg="Golf mk2 -bleu navy"};
case "cl3_golf_mk2_orange":{_lvmg="Golf mk2 -orange"};
case "cl3_golf_mk2_purple":{_lvmg="Golf mk2 -violet"};
case "cl3_golf_mk2_red":{_lvmg="Golf mk2 -rouge"};
case "cl3_golf_mk2_sand":{_lvmg="Golf mk2 -sable"};
case "cl3_golf_mk2_silver":{_lvmg="Golf mk2 -argent"};
case "cl3_golf_mk2_violet":{_lvmg="Golf mk2 -fushia"};
case "cl3_golf_mk2_white":{_lvmg="Golf mk2 -blanc"};
case "cl3_golf_mk2_yellow":{_lvmg="Golf mk2 -jaune"};

case "cl3_polo_gti":{_lvmg="Polo GTI"};
case "cl3_polo_gti_aqua":{_lvmg="Polo GTI - aqua"};
case "cl3_polo_gti_babyblue":{_lvmg="Polo GTI - bleu vif"};
case "cl3_polo_gti_babypink":{_lvmg="Polo GTI - rose vif"};
case "cl3_polo_gti_black":{_lvmg="Polo GTI - noir"};
case "cl3_polo_gti_blue":{_lvmg="Polo GTI - bleu"};
case "cl3_polo_gti_burgundy":{_lvmg="Polo GTI - rouge bordeaux"};
case "cl3_polo_gti_camo":{_lvmg="Polo GTI - camouflage"};
case "cl3_polo_gti_camo_urban":{_lvmg="Polo GTI - camouflage urbain"};
case "cl3_polo_gti_cardinal":{_lvmg="Polo GTI - rouge cardinal"};
case "cl3_polo_gti_dark_green":{_lvmg="Polo GTI - vert sombre"};
case "cl3_polo_gti_gold":{_lvmg="Polo GTI - orange"};
case "cl3_polo_gti_green":{_lvmg="Polo GTI - vert sombre"};
case "cl3_polo_gti_grey":{_lvmg="Polo GTI - gris"};
case "cl3_polo_gti_lavender":{_lvmg="Polo GTI - lavende"};
case "cl3_polo_gti_light_blue":{_lvmg="Polo GTI - bleu clair"};
case "cl3_polo_gti_light_yellow":{_lvmg="Polo GTI - jaune clair"};
case "cl3_polo_gti_lime":{_lvmg="Polo GTI - jaune citron"};
case "cl3_polo_gti_marina_blue":{_lvmg="Polo GTI - bleu marine"};
case "cl3_polo_gti_navy_blue":{_lvmg="Polo GTI - bleu navy"};
case "cl3_polo_gti_orange":{_lvmg="Polo GTI - orange"};
case "cl3_polo_gti_purple":{_lvmg="Polo GTI - violet"};
case "cl3_polo_gti_red":{_lvmg="Polo GTI - rouge"};
case "cl3_polo_gti_sand":{_lvmg="Polo GTI - sable"};
case "cl3_polo_gti_silver":{_lvmg="Polo GTI - argent"};
case "cl3_polo_gti_violet":{_lvmg="Polo GTI - fushia"};
case "cl3_polo_gti_white":{_lvmg="Polo GTI - blanc"};
case "cl3_polo_gti_yellow":{_lvmg="Polo GTI - jaune"};

case "beetle_custom":{_lvmg="Beetle 4X4"};
case "beetle_bleufonce":{_lvmg="Beetle 4X4 - Bleu"};
case "beetle_bleupetrole":{_lvmg="Beetle 4X4 - Bleu Pétrole"};
case "beetle_red":{_lvmg="Beetle 4X4 - Rouge"};
case "beetle_vert":{_lvmg="Beetle 4X4 - Vert"};
case "beetle_violet":{_lvmg="Beetle 4X4 - Violet"};
case "beetle_white":{_lvmg="Beetle 4X4 - Blanc"};
case "beetle_psycha":{_lvmg="Beetle 4X4 - Ligne Psyché"};
case "beetle_psycha1":{_lvmg="Beetle 4X4 - Art Psyché"};
case "beetle_coci":{_lvmg="Beetle 4X4 - Coccinelle"};
case "beetle_camo":{_lvmg="Beetle 4X4 - Camo"};
case "beetle":{_lvmg="Beetle 4X4 - noir"};

/////////////////////////////////////////////////////////////////////////////////
//CIV_MED
/////////////////////////////////////////////////////////////////////////////////


case "clpd_mondeo":{_lvmg="Ford Mondeo"};
case "civ_mondeo_Aqua":{_lvmg="Ford Mondeo - aqua"};
case "civ_mondeo_BabyBlue":{_lvmg="Ford Mondeo - bleu vif"};
case "civ_mondeo_BabyPink":{_lvmg="Ford Mondeo - rose vif"};
case "civ_mondeo_Black":{_lvmg="Ford Mondeo - noir"};
case "civ_mondeo_Burgundy":{_lvmg="Ford Mondeo - rouge bordeaux"};
case "civ_mondeo_CardinalBurgundy":{_lvmg="Ford Mondeo - rouge cardinal"};
case "civ_mondeo_DarkGreen":{_lvmg="Ford Mondeo - vert sombre"};
case "civ_mondeo_Gold":{_lvmg="Ford Mondeo - or"};
case "civ_mondeo_Green":{_lvmg="Ford Mondeo - vert"};
case "civ_mondeo_Grey":{_lvmg="Ford Mondeo - gris"};
case "civ_mondeo_Lavendel":{_lvmg="Ford Mondeo - lavende"};
case "civ_mondeo_LightBlue":{_lvmg="Ford Mondeo - bleu clair"};
case "civ_mondeo_LightYellow":{_lvmg="Ford Mondeo - jaune clair"};
case "civ_mondeo_Lime":{_lvmg="Ford Mondeo - jaune citron"};
case "civ_mondeo_MarineBlue":{_lvmg="Ford Mondeo - bleu marine"};
case "civ_mondeo_NavyBlue":{_lvmg="Ford Mondeo - bleu navy"};
case "civ_mondeo_Orange":{_lvmg="Ford Mondeo - orange"};
case "civ_mondeo_Pink":{_lvmg="Ford Mondeo - rose"};
case "civ_mondeo_Purple":{_lvmg="Ford Mondeo - violet"};
case "civ_mondeo_Red":{_lvmg="Ford Mondeo - rouge"};
case "civ_mondeo_Silver":{_lvmg="Ford Mondeo - argent"};
case "civ_mondeo_SkyBlue":{_lvmg="Ford Mondeo - bleu ciel"};
case "civ_mondeo_Violet":{_lvmg="Ford Mondeo - fushia"};
case "civ_mondeo_White":{_lvmg="Ford Mondeo - blanc"};
case "civ_mondeo_Yellow":{_lvmg="Ford Mondeo - jaune"};

case "cl3_challenger_2009":{_lvmg="Dodge Challenger"};
case "cl3_challenger_2009_black":{_lvmg="Dodge Challenger - noir"};
case "cl3_challenger_2009_blue":{_lvmg="Dodge Challenger - bleu"};
case "cl3_challenger_2009_green":{_lvmg="Dodge Challenger - vert"};
case "cl3_challenger_2009_red":{_lvmg="Dodge Challenger - rouge"};
case "cl3_challenger_2009_silver":{_lvmg="Dodge Challenger - argent"};
case "cl3_challenger_2009_yellow":{_lvmg="Dodge Challenger - jaune"};
case "cl3_challenger_aquablue":{_lvmg="Dodge Challenger - aqua"};
case "cl3_challenger_beige":{_lvmg="Dodge Challenger - beige"};
case "cl3_challenger_beige2":{_lvmg="Dodge Challenger - beige2"};
case "cl3_challenger_blue":{_lvmg="Dodge Challenger - bleu"};
case "cl3_challenger_brown":{_lvmg="Dodge Challenger - marron"};
case "cl3_challenger_darksilver":{_lvmg="Dodge Challenger - argent"};
case "cl3_challenger_green":{_lvmg="Dodge Challenger - vert"};
case "cl3_challenger_lime":{_lvmg="Dodge Challenger - jaune citron"};
case "cl3_challenger_orange":{_lvmg="Dodge Challenger - orange"};
case "cl3_challenger_orangeflame":{_lvmg="Dodge Challenger - orange flamme"};
case "cl3_challenger_pink":{_lvmg="Dodge Challenger - rose"};
case "cl3_challenger_purple":{_lvmg="Dodge Challenger - violet"};
case "cl3_challenger_red":{_lvmg="Dodge Challenger - rouge"};
case "cl3_challenger_yellow":{_lvmg="Dodge Challenger - jaune"};

case "cl3_dodge_charger_patrol":{_lvmg="Dodge Charger"};
case "cl3_dodge_charger_cn":{_lvmg="Dodge Charger - CN"};
case "cl3_dodge_charger_emt":{_lvmg="Dodge Charger - EMT"};
case "cl3_dodge_charger_emtcg":{_lvmg="Dodge Charger - EMTCG"};
case "cl3_dodge_charger_emtfd":{_lvmg="Dodge Charger - EMTFD"};
case "cl3_dodge_charger_emtmr":{_lvmg="Dodge Charger - EMTMR"};
case "cl3_dodge_charger_emtpa":{_lvmg="Dodge Charger - EMTPA"};
case "cl3_dodge_charger_etu":{_lvmg="Dodge Charger - ETU"};
case "cl3_dodge_charger_k9":{_lvmg="Dodge Charger - Police"};
case "cl3_dodge_charger_patrol":{_lvmg="Dodge Charger - Police blanc/noir"};
case "cl3_dodge_charger_patrol2":{_lvmg="Dodge Charger - Police noir/blanc"};
case "cl3_dodge_charger_s_black":{_lvmg="Dodge Charger - noir"};
case "cl3_dodge_charger_s_blue":{_lvmg="Dodge Charger - bleu"};
case "cl3_dodge_charger_s_camo":{_lvmg="Dodge Charger - camouflage"};
case "cl3_dodge_charger_s_camourban":{_lvmg="Dodge Charger - camouflage urbain"};
case "cl3_dodge_charger_s_darkgreen":{_lvmg="Dodge Charger - vert sombre"};
case "cl3_dodge_charger_s_darkred":{_lvmg="Dodge Charger - rouge sombre"};
case "cl3_dodge_charger_s_green":{_lvmg="Dodge Charger - vert sombre"};
case "cl3_dodge_charger_s_grey":{_lvmg="Dodge Charger - gris"};
case "cl3_dodge_charger_s_lime":{_lvmg="Dodge Charger - jaune citron"};
case "cl3_dodge_charger_s_orange":{_lvmg="Dodge Charger - orange"};
case "cl3_dodge_charger_s_pink":{_lvmg="Dodge Charger - rose"};
case "cl3_dodge_charger_s_purple":{_lvmg="Dodge Charger - violet"};
case "cl3_dodge_charger_s_red":{_lvmg="Dodge Charger - rouge"};
case "cl3_dodge_charger_s_white":{_lvmg="Dodge Charger - blanc"};
case "cl3_dodge_charger_s_yellow":{_lvmg="Dodge Charger - jaune"};

case "cl3_e60_m5":{_lvmg="BMW M5 E60"};
case "cl3_e60_m5_aqua":{_lvmg="BMW M5 E60 - aqua"};
case "cl3_e60_m5_babyblue":{_lvmg="BMW M5 E60 - bleu vif"};
case "cl3_e60_m5_babypink":{_lvmg="BMW M5 E60 - rose vif"};
case "cl3_e60_m5_black":{_lvmg="BMW M5 E60 - noir"};
case "cl3_e60_m5_blue":{_lvmg="BMW M5 E60 - bleu"};
case "cl3_e60_m5_burgundy":{_lvmg="BMW M5 E60 - rouge bordeaux"};
case "cl3_e60_m5_camo":{_lvmg="BMW M5 E60 - camouflage"};
case "cl3_e60_m5_camo_urban":{_lvmg="BMW M5 E60 - camouflage urbain"};
case "cl3_e60_m5_cardinal":{_lvmg="BMW M5 E60 - rouge cardinal"};
case "cl3_e60_m5_dark_green":{_lvmg="BMW M5 E60 - vert sombre"};
case "cl3_e60_m5_gold":{_lvmg="BMW M5 E60 - orange"};
case "cl3_e60_m5_green":{_lvmg="BMW M5 E60 - vert sombre"};
case "cl3_e60_m5_grey":{_lvmg="BMW M5 E60 - gris"};
case "cl3_e60_m5_lavender":{_lvmg="BMW M5 E60 - lavende"};
case "cl3_e60_m5_light_blue":{_lvmg="BMW M5 E60 - bleu clair"};
case "cl3_e60_m5_light_yellow":{_lvmg="BMW M5 E60 - jaune clair"};
case "cl3_e60_m5_lime":{_lvmg="BMW M5 E60 - jaune citron"};
case "cl3_e60_m5_marina_blue":{_lvmg="BMW M5 E60 - bleu marine"};
case "cl3_e60_m5_navy_blue":{_lvmg="BMW M5 E60 - bleu navy"};
case "cl3_e60_m5_orange":{_lvmg="BMW M5 E60 - orange"};
case "cl3_e60_m5_purple":{_lvmg="BMW M5 E60 - violet"};
case "cl3_e60_m5_red":{_lvmg="BMW M5 E60 - rouge"};
case "cl3_e60_m5_sand":{_lvmg="BMW M5 E60 - sable"};
case "cl3_e60_m5_silver":{_lvmg="BMW M5 E60 - argent"};
case "cl3_e60_m5_violet":{_lvmg="BMW M5 E60 - fushia"};
case "cl3_e60_m5_white":{_lvmg="BMW M5 E60 - blanc"};
case "cl3_e60_m5_yellow":{_lvmg="BMW M5 E60 - jaune"};

case "cl3_e63_amg":{_lvmg="Mercedes E63 AMG"};
case "cl3_e63_amg_aqua":{_lvmg="Mercedes E63 AMG - aqua"};
case "cl3_e63_amg_babyblue":{_lvmg="Mercedes E63 AMG - bleu vif"};
case "cl3_e63_amg_babypink":{_lvmg="Mercedes E63 AMG - rose vif"};
case "cl3_e63_amg_black":{_lvmg="Mercedes E63 AMG - noir"};
case "cl3_e63_amg_blue":{_lvmg="Mercedes E63 AMG - bleu"};
case "cl3_e63_amg_burgundy":{_lvmg="Mercedes E63 AMG - rouge bordeaux"};
case "cl3_e63_amg_camo":{_lvmg="Mercedes E63 AMG - camouflage"};
case "cl3_e63_amg_camo_urban":{_lvmg="Mercedes E63 AMG - camouflage urbain"};
case "cl3_e63_amg_cardinal":{_lvmg="Mercedes E63 AMG - rouge cardinal"};
case "cl3_e63_amg_dark_green":{_lvmg="Mercedes E63 AMG - vert sombre"};
case "cl3_e63_amg_gold":{_lvmg="Mercedes E63 AMG - orange"};
case "cl3_e63_amg_green":{_lvmg="Mercedes E63 AMG - vert sombre"};
case "cl3_e63_amg_grey":{_lvmg="Mercedes E63 AMG - gris"};
case "cl3_e63_amg_lavender":{_lvmg="Mercedes E63 AMG - lavende"};
case "cl3_e63_amg_light_blue":{_lvmg="Mercedes E63 AMG - bleu clair"};
case "cl3_e63_amg_light_yellow":{_lvmg="Mercedes E63 AMG - jaune clair"};
case "cl3_e63_amg_lime":{_lvmg="Mercedes E63 AMG - jaune citron"};
case "cl3_e63_amg_marina_blue":{_lvmg="Mercedes E63 AMG - bleu marine"};
case "cl3_e63_amg_navy_blue":{_lvmg="Mercedes E63 AMG - bleu navy"};
case "cl3_e63_amg_orange":{_lvmg="Mercedes E63 AMG - orange"};
case "cl3_e63_amg_purple":{_lvmg="Mercedes E63 AMG - violet"};
case "cl3_e63_amg_red":{_lvmg="Mercedes E63 AMG - rouge"};
case "cl3_e63_amg_sand":{_lvmg="Mercedes E63 AMG - sable"};
case "cl3_e63_amg_silver":{_lvmg="Mercedes E63 AMG - argent"};
case "cl3_e63_amg_violet":{_lvmg="Mercedes E63 AMG - fushia"};
case "cl3_e63_amg_white":{_lvmg="Mercedes E63 AMG - blanc"};
case "cl3_e63_amg_yellow":{_lvmg="Mercedes E63 AMG - jaune"};

case "cl3_escalade":{_lvmg="Cadillac Escalade"};
case "cl3_escalade_aqua":{_lvmg="Cadillac Escalade - aqua"};
case "cl3_escalade_babyblue":{_lvmg="Cadillac Escalade - bleu vif"};
case "cl3_escalade_babypink":{_lvmg="Cadillac Escalade - rose vif"};
case "cl3_escalade_black":{_lvmg="Cadillac Escalade - noir"};
case "cl3_escalade_blue":{_lvmg="Cadillac Escalade - bleu"};
case "cl3_escalade_burgundy":{_lvmg="Cadillac Escalade - rouge bordeaux"};
case "cl3_escalade_camo":{_lvmg="Cadillac Escalade - camouflage"};
case "cl3_escalade_camo_urban":{_lvmg="Cadillac Escalade - camouflage urbain"};
case "cl3_escalade_cardinal":{_lvmg="Cadillac Escalade - rouge cardinal"};
case "cl3_escalade_cg":{_lvmg="Cadillac Escalade - CG"};
case "cl3_escalade_cn":{_lvmg="Cadillac Escalade - CN"};
case "cl3_escalade_dark_green":{_lvmg="Cadillac Escalade - vert sombre"};
case "cl3_escalade_etu":{_lvmg="Cadillac Escalade - ETU"};
case "cl3_escalade_fd":{_lvmg="Cadillac Escalade - FD"};
case "cl3_escalade_gold":{_lvmg="Cadillac Escalade - or"};
case "cl3_escalade_green":{_lvmg="Cadillac Escalade - vert"};
case "cl3_escalade_grey":{_lvmg="Cadillac Escalade - gris"};
case "cl3_escalade_k9":{_lvmg="Cadillac Escalade - Police"};
case "cl3_escalade_lavender":{_lvmg="Cadillac Escalade - lavende"};
case "cl3_escalade_light_blue":{_lvmg="Cadillac Escalade - bleu clair"};
case "cl3_escalade_light_yellow":{_lvmg="Cadillac Escalade - jaune clair"};
case "cl3_escalade_lime":{_lvmg="Cadillac Escalade - jaune citron"};
case "cl3_escalade_marina_blue":{_lvmg="Cadillac Escalade - bleu marine"};
case "cl3_escalade_mr":{_lvmg="Cadillac Escalade - MR"};
case "cl3_escalade_navy_blue":{_lvmg="Cadillac Escalade - bleu navy"};
case "cl3_escalade_orange":{_lvmg="Cadillac Escalade - orange"};
case "cl3_escalade_patrolbw":{_lvmg="Cadillac Escalade - Police noir/blanc"};
case "cl3_escalade_patrolwb":{_lvmg="Cadillac Escalade - Police blanc/noir"};
case "cl3_escalade_pd":{_lvmg="Cadillac Escalade - PD"};
case "cl3_escalade_pm":{_lvmg="Cadillac Escalade - PM"};
case "cl3_escalade_purple":{_lvmg="Cadillac Escalade - violet"};
case "cl3_escalade_red":{_lvmg="Cadillac Escalade - rouge"};
case "cl3_escalade_sand":{_lvmg="Cadillac Escalade - sable"};
case "cl3_escalade_silver":{_lvmg="Cadillac Escalade - argent"};
case "cl3_escalade_traffic":{_lvmg="Cadillac Escalade - traffic"};
case "cl3_escalade_violet":{_lvmg="Cadillac Escalade - violet"};
case "cl3_escalade_white":{_lvmg="Cadillac Escalade - blanc"};
case "cl3_escalade_yellow":{_lvmg="Cadillac Escalade - jaune"};
case "cl3_escalade_dep":{_lvmg="Cadillac Escalade - Depanneur"};

case "cl3_q7":{_lvmg="Audi Q7"};
case "cl3_q7_aqua":{_lvmg="Audi Q7 - aqua"};
case "cl3_q7_babyblue":{_lvmg="Audi Q7 - bleu vif"};
case "cl3_q7_babypink":{_lvmg="Audi Q7 - rose vif"};
case "cl3_q7_black":{_lvmg="Audi Q7 - noir"};
case "cl3_q7_blue":{_lvmg="Audi Q7 - bleu"};
case "cl3_q7_burgundy":{_lvmg="Audi Q7 - rouge bordeaux"};
case "cl3_q7_camo":{_lvmg="Audi Q7 - camouflage"};
case "cl3_q7_camo_urban":{_lvmg="Audi Q7 - camouflage urbain"};
case "cl3_q7_cardinal":{_lvmg="Audi Q7 - rouge cardinal"};
case "cl3_q7_clpd_cn":{_lvmg="Audi Q7 - Police"};
case "cl3_q7_clpd_etu":{_lvmg="Audi Q7 - ETU"};
case "cl3_q7_clpd_patrol":{_lvmg="Audi Q7 - Police"};
case "cl3_q7_dark_green":{_lvmg="Audi Q7 - vert sombre"};
case "cl3_q7_gold":{_lvmg="Audi Q7 - orange"};
case "cl3_q7_green":{_lvmg="Audi Q7 - vert sombre"};
case "cl3_q7_grey":{_lvmg="Audi Q7 - gris"};
case "cl3_q7_lavender":{_lvmg="Audi Q7 - lavende"};
case "cl3_q7_light_blue":{_lvmg="Audi Q7 - bleu clair"};
case "cl3_q7_light_yellow":{_lvmg="Audi Q7 - jaune clair"};
case "cl3_q7_lime":{_lvmg="Audi Q7 - jaune citron"};
case "cl3_q7_marina_blue":{_lvmg="Audi Q7 - bleu marine"};
case "cl3_q7_navy_blue":{_lvmg="Audi Q7 - bleu navy"};
case "cl3_q7_orange":{_lvmg="Audi Q7 - orange"};
case "cl3_q7_purple":{_lvmg="Audi Q7 - violet"};
case "cl3_q7_red":{_lvmg="Audi Q7 - rouge"};
case "cl3_q7_sand":{_lvmg="Audi Q7 - sable"};
case "cl3_q7_silver":{_lvmg="Audi Q7 - argent"};
case "cl3_q7_violet":{_lvmg="Audi Q7 - fushia"};
case "cl3_q7_white":{_lvmg="Audi Q7 - blanc"};
case "cl3_q7_yellow":{_lvmg="Audi Q7 - jaune"};

case "cl3_s5":{_lvmg="Audi S5"};
case "cl3_s5_aqua":{_lvmg="Audi S5 - aqua"};
case "cl3_s5_babyblue":{_lvmg="Audi S5 - bleu vif"};
case "cl3_s5_babypink":{_lvmg="Audi S5 - rose vif"};
case "cl3_s5_black":{_lvmg="Audi S5 - noir"};
case "cl3_s5_blue":{_lvmg="Audi S5 - bleu"};
case "cl3_s5_burgundy":{_lvmg="Audi S5 - rouge bordeaux"};
case "cl3_s5_camo":{_lvmg="Audi S5 - camouflage"};
case "cl3_s5_camo_urban":{_lvmg="Audi S5 - camouflage urbain"};
case "cl3_s5_cardinal":{_lvmg="Audi S5 - rouge cardinal"};
case "cl3_s5_dark_green":{_lvmg="Audi S5 - vert sombre"};
case "cl3_s5_gold":{_lvmg="Audi S5 - orange"};
case "cl3_s5_green":{_lvmg="Audi S5 - vert sombre"};
case "cl3_s5_grey":{_lvmg="Audi S5 - gris"};
case "cl3_s5_lavender":{_lvmg="Audi S5 - lavende"};
case "cl3_s5_light_blue":{_lvmg="Audi S5 - bleu clair"};
case "cl3_s5_light_yellow":{_lvmg="Audi S5 - jaune clair"};
case "cl3_s5_lime":{_lvmg="Audi S5 - jaune citron"};
case "cl3_s5_marina_blue":{_lvmg="Audi S5 - bleu marine"};
case "cl3_s5_navy_blue":{_lvmg="Audi S5 - bleu navy"};
case "cl3_s5_orange":{_lvmg="Audi S5 - orange"};
case "cl3_s5_purple":{_lvmg="Audi S5 - violet"};
case "cl3_s5_red":{_lvmg="Audi S5 - rouge"};
case "cl3_s5_sand":{_lvmg="Audi S5 - sable"};
case "cl3_s5_silver":{_lvmg="Audi S5 - argent"};
case "cl3_s5_violet":{_lvmg="Audi S5 - fushia"};
case "cl3_s5_white":{_lvmg="Audi S5 - blanc"};
case "cl3_s5_yellow":{_lvmg="Audi S5 - jaune"};

case "cl3_suv":{_lvmg="SUV A2"};
case "cl3_suv_black":{_lvmg="SUV A2 - noir"};
case "cl3_suv_emt":{_lvmg="SUV A2 - médecin"};
case "cl3_suv_taxi":{_lvmg="SUV A2 - taxi"};

case "cl3_taurus":{_lvmg="Ford Taurus"};
case "cl3_taurus_aqua":{_lvmg="Ford Taurus - aqua"};
case "cl3_taurus_babyblue":{_lvmg="Ford Taurus - bleu vif"};
case "cl3_taurus_babypink":{_lvmg="Ford Taurus - rose vif"};
case "cl3_taurus_black":{_lvmg="Ford Taurus - noir"};
case "cl3_taurus_blue":{_lvmg="Ford Taurus - bleu"};
case "cl3_taurus_burgundy":{_lvmg="Ford Taurus - rouge bordeaux"};
case "cl3_taurus_camo":{_lvmg="Ford Taurus - camouflage"};
case "cl3_taurus_camo_urban":{_lvmg="Ford Taurus - camouflage urbain"};
case "cl3_taurus_cardinal":{_lvmg="Ford Taurus - rouge cardinal"};
case "cl3_taurus_dark_green":{_lvmg="Ford Taurus - vert sombre"};
case "cl3_taurus_gold":{_lvmg="Ford Taurus - orange"};
case "cl3_taurus_green":{_lvmg="Ford Taurus - vert sombre"};
case "cl3_taurus_grey":{_lvmg="Ford Taurus - gris"};
case "cl3_taurus_lavender":{_lvmg="Ford Taurus - lavende"};
case "cl3_taurus_light_blue":{_lvmg="Ford Taurus - bleu clair"};
case "cl3_taurus_light_yellow":{_lvmg="Ford Taurus - jaune clair"};
case "cl3_taurus_lime":{_lvmg="Ford Taurus - jaune citron"};
case "cl3_taurus_marina_blue":{_lvmg="Ford Taurus - bleu marine"};
case "cl3_taurus_navy_blue":{_lvmg="Ford Taurus - bleu navy"};
case "cl3_taurus_orange":{_lvmg="Ford Taurus - orange"};
case "cl3_taurus_purple":{_lvmg="Ford Taurus - violet"};
case "cl3_taurus_red":{_lvmg="Ford Taurus - rouge"};
case "cl3_taurus_sand":{_lvmg="Ford Taurus - sable"};
case "cl3_taurus_silver":{_lvmg="Ford Taurus - argent"};
case "cl3_taurus_violet":{_lvmg="Ford Taurus - fushia"};
case "cl3_taurus_white":{_lvmg="Ford Taurus - blanc"};
case "cl3_taurus_yellow":{_lvmg="Ford Taurus - jaune"};

case "cl3_transit":{_lvmg="Ford Transit"};
case "cl3_transit_aqua":{_lvmg="Ford Transit - aqua"};
case "cl3_transit_babyblue":{_lvmg="Ford Transit - bleu vif"};
case "cl3_transit_babypink":{_lvmg="Ford Transit - rose vif"};
case "cl3_transit_black":{_lvmg="Ford Transit - noir"};
case "cl3_transit_blue":{_lvmg="Ford Transit - bleu"};
case "cl3_transit_burgundy":{_lvmg="Ford Transit - rouge bordeaux"};
case "cl3_transit_camo":{_lvmg="Ford Transit - camouflage"};
case "cl3_transit_camo_urban":{_lvmg="Ford Transit - camouflage urbain"};
case "cl3_transit_cardinal":{_lvmg="Ford Transit - rouge cardinal"};
case "cl3_transit_civ":{_lvmg="Ford Transit - civil"};
case "cl3_transit_cop":{_lvmg="Ford Transit - Police"};
case "cl3_transit_dark_green":{_lvmg="Ford Transit - vert sombre"};
case "cl3_transit_gold":{_lvmg="Ford Transit - or"};
case "cl3_transit_green":{_lvmg="Ford Transit - vert"};
case "cl3_transit_grey":{_lvmg="Ford Transit - gris"};
case "cl3_transit_lavender":{_lvmg="Ford Transit - lavende"};
case "cl3_transit_light_blue":{_lvmg="Ford Transit - bleu clair"};
case "cl3_transit_light_yellow":{_lvmg="Ford Transit - jaune clair"};
case "cl3_transit_lime":{_lvmg="Ford Transit - jaune citron"};
case "cl3_transit_marina_blue":{_lvmg="Ford Transit - bleu marine"};
case "cl3_transit_navy_blue":{_lvmg="Ford Transit - bleu navy"};
case "cl3_transit_orange":{_lvmg="Ford Transit - orange"};
case "cl3_transit_purple":{_lvmg="Ford Transit - violet"};
case "cl3_transit_red":{_lvmg="Ford Transit - rouge"};
case "cl3_transit_sand":{_lvmg="Ford Transit - sable"};
case "cl3_transit_silver":{_lvmg="Ford Transit - argent"};
case "cl3_transit_violet":{_lvmg="Ford Transit - fushia"};
case "cl3_transit_white":{_lvmg="Ford Transit - blanc"};
case "cl3_transit_yellow":{_lvmg="Ford Transit - jaune"};
case "cl3_transitemt":{_lvmg="Ford Transit - médecin"};
case "cl3_transitk9":{_lvmg="Ford Transit - Police"};
case "cl3_transitNews":{_lvmg="Ford Transit - AltisNews"};
case "cl3_transitpatrol":{_lvmg="Ford Transit - Police"};

case "cl3_z4_2008":{_lvmg="BMW Z4"};
case "cl3_z4_2008_aqua":{_lvmg="BMW Z4 - aqua"};
case "cl3_z4_2008_babyblue":{_lvmg="BMW Z4 - bleu vif"};
case "cl3_z4_2008_babypink":{_lvmg="BMW Z4 - rose vif"};
case "cl3_z4_2008_black":{_lvmg="BMW Z4 - noir"};
case "cl3_z4_2008_blue":{_lvmg="BMW Z4 - bleu"};
case "cl3_z4_2008_burgundy":{_lvmg="BMW Z4 - rouge bordeaux"};
case "cl3_z4_2008_camo":{_lvmg="BMW Z4 - camouflage"};
case "cl3_z4_2008_camo_urban":{_lvmg="BMW Z4 - camouflage urbain"};
case "cl3_z4_2008_cardinal":{_lvmg="BMW Z4 - rouge cardinal"};
case "cl3_z4_2008_dark_green":{_lvmg="BMW Z4 - vert sombre"};
case "cl3_z4_2008_gold":{_lvmg="BMW Z4 - orange"};
case "cl3_z4_2008_green":{_lvmg="BMW Z4 - vert sombre"};
case "cl3_z4_2008_grey":{_lvmg="BMW Z4 - gris"};
case "cl3_z4_2008_lavender":{_lvmg="BMW Z4 - lavende"};
case "cl3_z4_2008_light_blue":{_lvmg="BMW Z4 - bleu clair"};
case "cl3_z4_2008_light_yellow":{_lvmg="BMW Z4 - jaune clair"};
case "cl3_z4_2008_lime":{_lvmg="BMW Z4 - jaune citron"};
case "cl3_z4_2008_marina_blue":{_lvmg="BMW Z4 - bleu marine"};
case "cl3_z4_2008_navy_blue":{_lvmg="BMW Z4 - bleu navy"};
case "cl3_z4_2008_orange":{_lvmg="BMW Z4 - orange"};
case "cl3_z4_2008_purple":{_lvmg="BMW Z4 - violet"};
case "cl3_z4_2008_red":{_lvmg="BMW Z4 - rouge"};
case "cl3_z4_2008_sand":{_lvmg="BMW Z4 - sable"};
case "cl3_z4_2008_silver":{_lvmg="BMW Z4 - argent"};
case "cl3_z4_2008_violet":{_lvmg="BMW Z4 - fushia"};
case "cl3_z4_2008_white":{_lvmg="BMW Z4 - blanc"};
case "cl3_z4_2008_yellow":{_lvmg="BMW Z4 - jaune"};

/////////////////////////////////////////////////////////////////////////////////
//CIV_HIGH
/////////////////////////////////////////////////////////////////////////////////

case "cl3_458":{_lvmg="Ferrari 458"};
case "cl3_458_2tone1":{_lvmg="Ferrari 458 - bleu/vert"};
case "cl3_458_2tone2":{_lvmg="Ferrari 458 - violet/noir"};
case "cl3_458_2tone3":{_lvmg="Ferrari 458 - turquoise/blanc"};
case "cl3_458_2tone4":{_lvmg="Ferrari 458 - vert/jaune"};
case "cl3_458_2tone5":{_lvmg="Ferrari 458 - orange/jaune"};
case "cl3_458_aqua":{_lvmg="Ferrari 458 - aqua"};
case "cl3_458_babyblue":{_lvmg="Ferrari 458 - bleu vif"};
case "cl3_458_babypink":{_lvmg="Ferrari 458 - rose vif"};
case "cl3_458_black":{_lvmg="Ferrari 458 - noir"};
case "cl3_458_blue":{_lvmg="Ferrari 458 - bleu"};
case "cl3_458_burgundy":{_lvmg="Ferrari 458 - rouge bordeaux"};
case "cl3_458_camo":{_lvmg="Ferrari 458 - camouflage"};
case "cl3_458_camo_urban":{_lvmg="Ferrari 458 - camouflage urbain"};
case "cl3_458_cardinal":{_lvmg="Ferrari 458 - rouge cardinal"};
case "cl3_458_dark_green":{_lvmg="Ferrari 458 - vert sombre"};
case "cl3_458_flame":{_lvmg="Ferrari 458 - flamme"};
case "cl3_458_flame1":{_lvmg="Ferrari 458 - flamme2"};
case "cl3_458_flame2":{_lvmg="Ferrari 458 - flamme3"};
case "cl3_458_gold":{_lvmg="Ferrari 458 - or"};
case "cl3_458_green":{_lvmg="Ferrari 458 - vert"};
case "cl3_458_grey":{_lvmg="Ferrari 458 - gri"};
case "cl3_458_lavender":{_lvmg="Ferrari 458 - lavende"};
case "cl3_458_light_blue":{_lvmg="Ferrari 458 - bleu clair"};
case "cl3_458_light_yellow":{_lvmg="Ferrari 458 - jaune clair"};
case "cl3_458_lime":{_lvmg="Ferrari 458 - jaune citron"};
case "cl3_458_marina_blue":{_lvmg="Ferrari 458 - bleu marine"};
case "cl3_458_navy_blue":{_lvmg="Ferrari 458 - bleu navy"};
case "cl3_458_orange":{_lvmg="Ferrari 458 - orange"};
case "cl3_458_purple":{_lvmg="Ferrari 458 - violet"};
case "cl3_458_red":{_lvmg="Ferrari 458 - rouge"};
case "cl3_458_sand":{_lvmg="Ferrari 458 - sable"};
case "cl3_458_silver":{_lvmg="Ferrari 458 - argent"};
case "cl3_458_violet":{_lvmg="Ferrari 458 - fushia"};
case "cl3_458_white":{_lvmg="Ferrari 458 - blanc"};
case "cl3_458_yellow":{_lvmg="Ferrari 458 - jaune"};

case "cl3_aventador_lp7004":{_lvmg="Lamborghini Aventador"};
case "cl3_aventador_lp7004_2tone1":{_lvmg="Lamborghini Aventador - bleu/vert"};
case "cl3_aventador_lp7004_2tone2":{_lvmg="Lamborghini Aventador - violet/noir"};
case "cl3_aventador_lp7004_2tone3":{_lvmg="Lamborghini Aventador - turquoise/blanc"};
case "cl3_aventador_lp7004_2tone4":{_lvmg="Lamborghini Aventador - vert/jaune"};
case "cl3_aventador_lp7004_2tone5":{_lvmg="Lamborghini Aventador - orange/jaune"};
case "cl3_aventador_lp7004_aqua":{_lvmg="Lamborghini Aventador - aqua"};
case "cl3_aventador_lp7004_babyblue":{_lvmg="Lamborghini Aventador - bleu vif"};
case "cl3_aventador_lp7004_babypink":{_lvmg="Lamborghini Aventador - rose vif"};
case "cl3_aventador_lp7004_black":{_lvmg="Lamborghini Aventador - noir"};
case "cl3_aventador_lp7004_blue":{_lvmg="Lamborghini Aventador - bleu"};
case "cl3_aventador_lp7004_burgundy":{_lvmg="Lamborghini Aventador - rouge bordeaux"};
case "cl3_aventador_lp7004_camo":{_lvmg="Lamborghini Aventador - camouflage"};
case "cl3_aventador_lp7004_camo_urban":{_lvmg="Lamborghini Aventador - camouflage urbain"};
case "cl3_aventador_lp7004_cardinal":{_lvmg="Lamborghini Aventador - rouge cardinal"};
case "cl3_aventador_lp7004_dark_green":{_lvmg="Lamborghini Aventador - vert sombre"};
case "cl3_aventador_lp7004_flame":{_lvmg="Lamborghini Aventador - flamme"};
case "cl3_aventador_lp7004_flame1":{_lvmg="Lamborghini Aventador - flamme2"};
case "cl3_aventador_lp7004_flame2":{_lvmg="Lamborghini Aventador - flamme3"};
case "cl3_aventador_lp7004_gold":{_lvmg="Lamborghini Aventador - or"};
case "cl3_aventador_lp7004_green":{_lvmg="Lamborghini Aventador - vert"};
case "cl3_aventador_lp7004_grey":{_lvmg="Lamborghini Aventador - gri"};
case "cl3_aventador_lp7004_lavender":{_lvmg="Lamborghini Aventador - lavende"};
case "cl3_aventador_lp7004_light_blue":{_lvmg="Lamborghini Aventador - bleu clair"};
case "cl3_aventador_lp7004_light_yellow":{_lvmg="Lamborghini Aventador - jaune clair"};
case "cl3_aventador_lp7004_lime":{_lvmg="Lamborghini Aventador - jaune citron"};
case "cl3_aventador_lp7004_marina_blue":{_lvmg="Lamborghini Aventador - bleu marine"};
case "cl3_aventador_lp7004_navy_blue":{_lvmg="Lamborghini Aventador - bleu navy"};
case "cl3_aventador_lp7004_orange":{_lvmg="Lamborghini Aventador - orange"};
case "cl3_aventador_lp7004_purple":{_lvmg="Lamborghini Aventador - violet"};
case "cl3_aventador_lp7004_red":{_lvmg="Lamborghini Aventador - rouge"};
case "cl3_aventador_lp7004_sand":{_lvmg="Lamborghini Aventador - sable"};
case "cl3_aventador_lp7004_silver":{_lvmg="Lamborghini Aventador - argent"};
case "cl3_aventador_lp7004_violet":{_lvmg="Lamborghini Aventador - fushia"};
case "cl3_aventador_lp7004_white":{_lvmg="Lamborghini Aventador - blanc"};
case "cl3_aventador_lp7004_yellow":{_lvmg="Lamborghini Aventador - jaune"};

case "cl3_carrera_gt":{_lvmg="Porche Carrera GT"};
case "cl3_carrera_gt_aqua":{_lvmg="Porche Carrera GT - aqua"};
case "cl3_carrera_gt_babyblue":{_lvmg="Porche Carrera GT - bleu vif"};
case "cl3_carrera_gt_babypink":{_lvmg="Porche Carrera GT - rose vif"};
case "cl3_carrera_gt_black":{_lvmg="Porche Carrera GT - noir"};
case "cl3_carrera_gt_blue":{_lvmg="Porche Carrera GT - bleu"};
case "cl3_carrera_gt_burgundy":{_lvmg="Porche Carrera GT - rouge bordeaux"};
case "cl3_carrera_gt_camo":{_lvmg="Porche Carrera GT - camouflage"};
case "cl3_carrera_gt_camo_urban":{_lvmg="Porche Carrera GT - camouflage urbain"};
case "cl3_carrera_gt_cardinal":{_lvmg="Porche Carrera GT - rouge cardinal"};
case "cl3_carrera_gt_dark_green":{_lvmg="Porche Carrera GT - vert sombre"};
case "cl3_carrera_gt_gold":{_lvmg="Porche Carrera GT - orange"};
case "cl3_carrera_gt_green":{_lvmg="Porche Carrera GT - vert sombre"};
case "cl3_carrera_gt_grey":{_lvmg="Porche Carrera GT - gris"};
case "cl3_carrera_gt_lavender":{_lvmg="Porche Carrera GT - lavende"};
case "cl3_carrera_gt_light_blue":{_lvmg="Porche Carrera GT - bleu clair"};
case "cl3_carrera_gt_light_yellow":{_lvmg="Porche Carrera GT - jaune clair"};
case "cl3_carrera_gt_lime":{_lvmg="Porche Carrera GT - jaune citron"};
case "cl3_carrera_gt_marina_blue":{_lvmg="Porche Carrera GT - bleu marine"};
case "cl3_carrera_gt_navy_blue":{_lvmg="Porche Carrera GT - bleu navy"};
case "cl3_carrera_gt_orange":{_lvmg="Porche Carrera GT - orange"};
case "cl3_carrera_gt_purple":{_lvmg="Porche Carrera GT - violet"};
case "cl3_carrera_gt_red":{_lvmg="Porche Carrera GT - rouge"};
case "cl3_carrera_gt_sand":{_lvmg="Porche Carrera GT - sable"};
case "cl3_carrera_gt_silver":{_lvmg="Porche Carrera GT - argent"};
case "cl3_carrera_gt_violet":{_lvmg="Porche Carrera GT - fushia"};
case "cl3_carrera_gt_white":{_lvmg="Porche Carrera GT - blanc"};
case "cl3_carrera_gt_yellow":{_lvmg="Porche Carrera GT - jaune"};

case "cl3_dbs_volante":{_lvmg="Aston Martin dbs Volante"};
case "cl3_dbs_volante_aqua":{_lvmg="Aston Martin dbs Volante - aqua"};
case "cl3_dbs_volante_babyblue":{_lvmg="Aston Martin dbs Volante - bleu vif"};
case "cl3_dbs_volante_babypink":{_lvmg="Aston Martin dbs Volante - rose vif"};
case "cl3_dbs_volante_black":{_lvmg="Aston Martin dbs Volante - noir"};
case "cl3_dbs_volante_blue":{_lvmg="Aston Martin dbs Volante - bleu"};
case "cl3_dbs_volante_burgundy":{_lvmg="Aston Martin dbs Volante - rouge bordeaux"};
case "cl3_dbs_volante_camo":{_lvmg="Aston Martin dbs Volante - camouflage"};
case "cl3_dbs_volante_camo_urban":{_lvmg="Aston Martin dbs Volante - camouflage urbain"};
case "cl3_dbs_volante_cardinal":{_lvmg="Aston Martin dbs Volante - rouge cardinal"};
case "cl3_dbs_volante_dark_green":{_lvmg="Aston Martin dbs Volante - vert sombre"};
case "cl3_dbs_volante_flame":{_lvmg="Aston Martin dbs Volante - flamme"};
case "cl3_dbs_volante_flame1":{_lvmg="Aston Martin dbs Volante - flamme2"};
case "cl3_dbs_volante_flame2":{_lvmg="Aston Martin dbs Volante - flamme3"};
case "cl3_dbs_volante_gold":{_lvmg="Aston Martin dbs Volante - or"};
case "cl3_dbs_volante_green":{_lvmg="Aston Martin dbs Volante - vert"};
case "cl3_dbs_volante_grey":{_lvmg="Aston Martin dbs Volante - gris"};
case "cl3_dbs_volante_lavender":{_lvmg="Aston Martin dbs Volante - lavende"};
case "cl3_dbs_volante_light_blue":{_lvmg="Aston Martin dbs Volante - bleu clair"};
case "cl3_dbs_volante_light_yellow":{_lvmg="Aston Martin dbs Volante - jaune clair"};
case "cl3_dbs_volante_lime":{_lvmg="Aston Martin dbs Volante - jaune citron"};
case "cl3_dbs_volante_marina_blue":{_lvmg="Aston Martin dbs Volante - bleu marine"};
case "cl3_dbs_volante_navy_blue":{_lvmg="Aston Martin dbs Volante - bleu navy"};
case "cl3_dbs_volante_orange":{_lvmg="Aston Martin dbs Volante - orange"};
case "cl3_dbs_volante_purple":{_lvmg="Aston Martin dbs Volante - violet"};
case "cl3_dbs_volante_red":{_lvmg="Aston Martin dbs Volante - rouge"};
case "cl3_dbs_volante_sand":{_lvmg="Aston Martin dbs Volante - sable"};
case "cl3_dbs_volante_silver":{_lvmg="Aston Martin dbs Volante - argent"};
case "cl3_dbs_volante_violet":{_lvmg="Aston Martin dbs Volante - fushia"};
case "cl3_dbs_volante_white":{_lvmg="Aston Martin dbs Volante - blanc"};
case "cl3_dbs_volante_yellow":{_lvmg="Aston Martin dbs Volante - jaune"};

case "cl3_impreza_rally":{_lvmg="Subaru Impreza Rally"};
case "cl3_impreza_rally_aqua":{_lvmg="Subaru Impreza Rally - aqua"};
case "cl3_impreza_rally_babyblue":{_lvmg="Subaru Impreza Rally - bleu vif"};
case "cl3_impreza_rally_babypink":{_lvmg="Subaru Impreza Rally - rose vif"};
case "cl3_impreza_rally_black":{_lvmg="Subaru Impreza Rally - noir"};
case "cl3_impreza_rally_blue":{_lvmg="Subaru Impreza Rally - bleu"};
case "cl3_impreza_rally_burgundy":{_lvmg="Subaru Impreza Rally - rouge bordeaux"};
case "cl3_impreza_rally_camo":{_lvmg="Subaru Impreza Rally - camouflage"};
case "cl3_impreza_rally_camo_urban":{_lvmg="Subaru Impreza Rally - camouflage urbain"};
case "cl3_impreza_rally_cardinal":{_lvmg="Subaru Impreza Rally - rouge cardinal"};
case "cl3_impreza_rally_dark_green":{_lvmg="Subaru Impreza Rally - vert sombre"};
case "cl3_impreza_rally_flame":{_lvmg="Subaru Impreza Rally - flamme"};
case "cl3_impreza_rally_flame1":{_lvmg="Subaru Impreza Rally - flamme2"};
case "cl3_impreza_rally_flame2":{_lvmg="Subaru Impreza Rally - flamme3"};
case "cl3_impreza_rally_gold":{_lvmg="Subaru Impreza Rally - or"};
case "cl3_impreza_rally_green":{_lvmg="Subaru Impreza Rally - vert"};
case "cl3_impreza_rally_grey":{_lvmg="Subaru Impreza Rally - gris"};
case "cl3_impreza_rally_lavender":{_lvmg="Subaru Impreza Rally - lavende"};
case "cl3_impreza_rally_light_blue":{_lvmg="Subaru Impreza Rally - bleu clair"};
case "cl3_impreza_rally_light_yellow":{_lvmg="Subaru Impreza Rally - jaune clair"};
case "cl3_impreza_rally_lime":{_lvmg="Subaru Impreza Rally - jaune citron"};
case "cl3_impreza_rally_marina_blue":{_lvmg="Subaru Impreza Rally - bleu marine"};
case "cl3_impreza_rally_navy_blue":{_lvmg="Subaru Impreza Rally - bleu navy"};
case "cl3_impreza_rally_orange":{_lvmg="Subaru Impreza Rally - orange"};
case "cl3_impreza_rally_purple":{_lvmg="Subaru Impreza Rally - violet"};
case "cl3_impreza_rally_red":{_lvmg="Subaru Impreza Rally - rouge"};
case "cl3_impreza_rally_sand":{_lvmg="Subaru Impreza Rally - sable"};
case "cl3_impreza_rally_silver":{_lvmg="Subaru Impreza Rally - argent"};
case "cl3_impreza_rally_violet":{_lvmg="Subaru Impreza Rally - fushia"};
case "cl3_impreza_rally_white":{_lvmg="Subaru Impreza Rally - blanc"};
case "cl3_impreza_rally_yellow":{_lvmg="Subaru Impreza Rally - jaune"};

case "cl3_impreza_road":{_lvmg="Subaru Impreza WRX"};
case "cl3_impreza_road_aqua":{_lvmg="Subaru Impreza WRX - aqua"};
case "cl3_impreza_road_babyblue":{_lvmg="Subaru Impreza WRX - bleu vif"};
case "cl3_impreza_road_babypink":{_lvmg="Subaru Impreza WRX - rose vif"};
case "cl3_impreza_road_black":{_lvmg="Subaru Impreza WRX - noir"};
case "cl3_impreza_road_blue":{_lvmg="Subaru Impreza WRX - bleu"};
case "cl3_impreza_road_burgundy":{_lvmg="Subaru Impreza WRX - rouge bordeaux"};
case "cl3_impreza_road_camo":{_lvmg="Subaru Impreza WRX - camouflage"};
case "cl3_impreza_road_camo_urban":{_lvmg="Subaru Impreza WRX - camouflage urbain"};
case "cl3_impreza_road_cardinal":{_lvmg="Subaru Impreza WRX - rouge cardinal"};
case "cl3_impreza_road_dark_green":{_lvmg="Subaru Impreza WRX - vert sombre"};
case "cl3_impreza_road_flame":{_lvmg="Subaru Impreza WRX - flamme"};
case "cl3_impreza_road_flame1":{_lvmg="Subaru Impreza WRX - flamme2"};
case "cl3_impreza_road_flame2":{_lvmg="Subaru Impreza WRX - flamme3"};
case "cl3_impreza_road_gold":{_lvmg="Subaru Impreza WRX - or"};
case "cl3_impreza_road_green":{_lvmg="Subaru Impreza WRX - vert"};
case "cl3_impreza_road_grey":{_lvmg="Subaru Impreza WRX - gris"};
case "cl3_impreza_road_lavender":{_lvmg="Subaru Impreza WRX - lavende"};
case "cl3_impreza_road_light_blue":{_lvmg="Subaru Impreza WRX - bleu clair"};
case "cl3_impreza_road_light_yellow":{_lvmg="Subaru Impreza WRX - jaune clair"};
case "cl3_impreza_road_lime":{_lvmg="Subaru Impreza WRX - jaune citron"};
case "cl3_impreza_road_livery1":{_lvmg="Subaru Impreza WRX - livraison 1"};
case "cl3_impreza_road_livery2":{_lvmg="Subaru Impreza WRX - livraison 2"};
case "cl3_impreza_road_livery3":{_lvmg="Subaru Impreza WRX - livraison 3"};
case "cl3_impreza_road_livery4":{_lvmg="Subaru Impreza WRX - livraison 4"};
case "cl3_impreza_road_livery5":{_lvmg="Subaru Impreza WRX - livraison 5"};
case "cl3_impreza_road_marina_blue":{_lvmg="Subaru Impreza WRX - bleu marine"};
case "cl3_impreza_road_navy_blue":{_lvmg="Subaru Impreza WRX - bleu navy"};
case "cl3_impreza_road_orange":{_lvmg="Subaru Impreza WRX - orange"};
case "cl3_impreza_road_purple":{_lvmg="Subaru Impreza WRX - violet"};
case "cl3_impreza_road_red":{_lvmg="Subaru Impreza WRX - rouge"};
case "cl3_impreza_road_sand":{_lvmg="Subaru Impreza WRX - sable"};
case "cl3_impreza_road_silver":{_lvmg="Subaru Impreza WRX - argent"};
case "cl3_impreza_road_violet":{_lvmg="Subaru Impreza WRX - fushia"};
case "cl3_impreza_road_white":{_lvmg="Subaru Impreza WRX - blanc"};
case "cl3_impreza_road_yellow":{_lvmg="Subaru Impreza WRX - jaune"};

case "cl3_insignia":{_lvmg="Vauxhall Insignia"};
case "cl3_insignia_aqua":{_lvmg="Vauxhall Insignia - aqua"};
case "cl3_insignia_babyblue":{_lvmg="Vauxhall Insignia - bleu vif"};
case "cl3_insignia_babypink":{_lvmg="Vauxhall Insignia - rose vif"};
case "cl3_insignia_black":{_lvmg="Vauxhall Insignia - noir"};
case "cl3_insignia_blue":{_lvmg="Vauxhall Insignia - bleu"};
case "cl3_insignia_burgundy":{_lvmg="Vauxhall Insignia - rouge bordeaux"};
case "cl3_insignia_camo":{_lvmg="Vauxhall Insignia - camouflage"};
case "cl3_insignia_camo_urban":{_lvmg="Vauxhall Insignia - camouflage urbain"};
case "cl3_insignia_cardinal":{_lvmg="Vauxhall Insignia - rouge cardinal"};
case "cl3_insignia_dark_green":{_lvmg="Vauxhall Insignia - vert sombre"};
case "cl3_insignia_gold":{_lvmg="Vauxhall Insignia - orange"};
case "cl3_insignia_green":{_lvmg="Vauxhall Insignia - vert sombre"};
case "cl3_insignia_grey":{_lvmg="Vauxhall Insignia - gris"};
case "cl3_insignia_lavender":{_lvmg="Vauxhall Insignia - lavende"};
case "cl3_insignia_light_blue":{_lvmg="Vauxhall Insignia - bleu clair"};
case "cl3_insignia_light_yellow":{_lvmg="Vauxhall Insignia - jaune clair"};
case "cl3_insignia_lime":{_lvmg="Vauxhall Insignia - jaune citron"};
case "cl3_insignia_marina_blue":{_lvmg="Vauxhall Insignia - bleu marine"};
case "cl3_insignia_navy_blue":{_lvmg="Vauxhall Insignia - bleu navy"};
case "cl3_insignia_orange":{_lvmg="Vauxhall Insignia - orange"};
case "cl3_insignia_purple":{_lvmg="Vauxhall Insignia - violet"};
case "cl3_insignia_red":{_lvmg="Vauxhall Insignia - rouge"};
case "cl3_insignia_sand":{_lvmg="Vauxhall Insignia - sable"};
case "cl3_insignia_silver":{_lvmg="Vauxhall Insignia - argent"};
case "cl3_insignia_violet":{_lvmg="Vauxhall Insignia - fushia"};
case "cl3_insignia_white":{_lvmg="Vauxhall Insignia - blanc"};
case "cl3_insignia_yellow":{_lvmg="Vauxhall Insignia - jaune"};

case "cl3_lamborghini_gt1":{_lvmg="Lamborghini GT1"};
case "cl3_lamborghini_gt1_2tone1":{_lvmg="Lamborghini GT1 - bleu/vert"};
case "cl3_lamborghini_gt1_2tone2":{_lvmg="Lamborghini GT1 - violet/noir"};
case "cl3_lamborghini_gt1_2tone3":{_lvmg="Lamborghini GT1 - turquoise/blanc"};
case "cl3_lamborghini_gt1_2tone4":{_lvmg="Lamborghini GT1 - vert/jaune"};
case "cl3_lamborghini_gt1_2tone5":{_lvmg="Lamborghini GT1 - orange/jaune"};
case "cl3_lamborghini_gt1_aqua":{_lvmg="Lamborghini GT1 - aqua"};
case "cl3_lamborghini_gt1_babyblue":{_lvmg="Lamborghini GT1 - bleu vif"};
case "cl3_lamborghini_gt1_babypink":{_lvmg="Lamborghini GT1 - rose vif"};
case "cl3_lamborghini_gt1_black":{_lvmg="Lamborghini GT1 - noir"};
case "cl3_lamborghini_gt1_blue":{_lvmg="Lamborghini GT1 - bleu"};
case "cl3_lamborghini_gt1_burgundy":{_lvmg="Lamborghini GT1 - rouge bordeaux"};
case "cl3_lamborghini_gt1_camo":{_lvmg="Lamborghini GT1 - camouflage"};
case "cl3_lamborghini_gt1_camo_urban":{_lvmg="Lamborghini GT1 - camouflage urbain"};
case "cl3_lamborghini_gt1_cardinal":{_lvmg="Lamborghini GT1 - rouge cardinal"};
case "cl3_lamborghini_gt1_dark_green":{_lvmg="Lamborghini GT1 - vert sombre"};
case "cl3_lamborghini_gt1_flame":{_lvmg="Lamborghini GT1 - flamme"};
case "cl3_lamborghini_gt1_flame1":{_lvmg="Lamborghini GT1 - flamme2"};
case "cl3_lamborghini_gt1_flame2":{_lvmg="Lamborghini GT1 - flamme3"};
case "cl3_lamborghini_gt1_gold":{_lvmg="Lamborghini GT1 - or"};
case "cl3_lamborghini_gt1_green":{_lvmg="Lamborghini GT1 - vert"};
case "cl3_lamborghini_gt1_grey":{_lvmg="Lamborghini GT1 - gri"};
case "cl3_lamborghini_gt1_lavender":{_lvmg="Lamborghini GT1 - lavende"};
case "cl3_lamborghini_gt1_light_blue":{_lvmg="Lamborghini GT1 - bleu clair"};
case "cl3_lamborghini_gt1_light_yellow":{_lvmg="Lamborghini GT1 - jaune clair"};
case "cl3_lamborghini_gt1_lime":{_lvmg="Lamborghini GT1 - jaune citron"};
case "cl3_lamborghini_gt1_marina_blue":{_lvmg="Lamborghini GT1 - bleu marine"};
case "cl3_lamborghini_gt1_navy_blue":{_lvmg="Lamborghini GT1 - bleu navy"};
case "cl3_lamborghini_gt1_orange":{_lvmg="Lamborghini GT1 - orange"};
case "cl3_lamborghini_gt1_purple":{_lvmg="Lamborghini GT1 - violet"};
case "cl3_lamborghini_gt1_red":{_lvmg="Lamborghini GT1 - rouge"};
case "cl3_lamborghini_gt1_sand":{_lvmg="Lamborghini GT1 - sable"};
case "cl3_lamborghini_gt1_silver":{_lvmg="Lamborghini GT1 - argent"};
case "cl3_lamborghini_gt1_violet":{_lvmg="Lamborghini GT1 - fushia"};
case "cl3_lamborghini_gt1_white":{_lvmg="Lamborghini GT1 - blanc"};
case "cl3_lamborghini_gt1_yellow":{_lvmg="Lamborghini GT1 - jaune"};

case "cl3_murcielago":{_lvmg="Lamborghini Murcielago"};
case "cl3_murcielago_2tone1":{_lvmg="Lamborghini Murcielago - bleu/vert"};
case "cl3_murcielago_2tone2":{_lvmg="Lamborghini Murcielago - violet/noir"};
case "cl3_murcielago_2tone3":{_lvmg="Lamborghini Murcielago - turquoise/blanc"};
case "cl3_murcielago_2tone4":{_lvmg="Lamborghini Murcielago - vert/jaune"};
case "cl3_murcielago_2tone5":{_lvmg="Lamborghini Murcielago - orange/jaune"};
case "cl3_murcielago_aqua":{_lvmg="Lamborghini Murcielago - aqua"};
case "cl3_murcielago_babyblue":{_lvmg="Lamborghini Murcielago - bleu vif"};
case "cl3_murcielago_babypink":{_lvmg="Lamborghini Murcielago - rose vif"};
case "cl3_murcielago_black":{_lvmg="Lamborghini Murcielago - noir"};
case "cl3_murcielago_blue":{_lvmg="Lamborghini Murcielago - bleu"};
case "cl3_murcielago_burgundy":{_lvmg="Lamborghini Murcielago - rouge bordeaux"};
case "cl3_murcielago_camo":{_lvmg="Lamborghini Murcielago - camouflage"};
case "cl3_murcielago_camo_urban":{_lvmg="Lamborghini Murcielago - camouflage urbain"};
case "cl3_murcielago_cardinal":{_lvmg="Lamborghini Murcielago - rouge cardinal"};
case "cl3_murcielago_dark_green":{_lvmg="Lamborghini Murcielago - vert sombre"};
case "cl3_murcielago_flame":{_lvmg="Lamborghini Murcielago - flamme"};
case "cl3_murcielago_flame1":{_lvmg="Lamborghini Murcielago - flamme2"};
case "cl3_murcielago_flame2":{_lvmg="Lamborghini Murcielago - flamme3"};
case "cl3_murcielago_gold":{_lvmg="Lamborghini Murcielago - or"};
case "cl3_murcielago_green":{_lvmg="Lamborghini Murcielago - vert"};
case "cl3_murcielago_grey":{_lvmg="Lamborghini Murcielago - gri"};
case "cl3_murcielago_lavender":{_lvmg="Lamborghini Murcielago - lavende"};
case "cl3_murcielago_light_blue":{_lvmg="Lamborghini Murcielago - bleu clair"};
case "cl3_murcielago_light_yellow":{_lvmg="Lamborghini Murcielago - jaune clair"};
case "cl3_murcielago_lime":{_lvmg="Lamborghini Murcielago - jaune citron"};
case "cl3_murcielago_marina_blue":{_lvmg="Lamborghini Murcielago - bleu marine"};
case "cl3_murcielago_navy_blue":{_lvmg="Lamborghini Murcielago - bleu navy"};
case "cl3_murcielago_orange":{_lvmg="Lamborghini Murcielago - orange"};
case "cl3_murcielago_purple":{_lvmg="Lamborghini Murcielago - violet"};
case "cl3_murcielago_red":{_lvmg="Lamborghini Murcielago - rouge"};
case "cl3_murcielago_sand":{_lvmg="Lamborghini Murcielago - sable"};
case "cl3_murcielago_silver":{_lvmg="Lamborghini Murcielago - argent"};
case "cl3_murcielago_violet":{_lvmg="Lamborghini Murcielago - fushia"};
case "cl3_murcielago_white":{_lvmg="Lamborghini Murcielago - blanc"};
case "cl3_murcielago_yellow":{_lvmg="Lamborghini Murcielago - jaune"};

case "cl3_r8_spyder":{_lvmg="Audi R8 Spyder"};
case "cl3_r8_spyder_2tone1":{_lvmg="Audi R8 Spyder - bleu/vert"};
case "cl3_r8_spyder_2tone2":{_lvmg="Audi R8 Spyder - violet/noir"};
case "cl3_r8_spyder_2tone3":{_lvmg="Audi R8 Spyder - turquoise/blanc"};
case "cl3_r8_spyder_2tone4":{_lvmg="Audi R8 Spyder - vert/jaune"};
case "cl3_r8_spyder_2tone5":{_lvmg="Audi R8 Spyder - orange/jaune"};
case "cl3_r8_spyder_aqua":{_lvmg="Audi R8 Spyder - aqua"};
case "cl3_r8_spyder_babyblue":{_lvmg="Audi R8 Spyder - bleu vif"};
case "cl3_r8_spyder_babypink":{_lvmg="Audi R8 Spyder - rose vif"};
case "cl3_r8_spyder_black":{_lvmg="Audi R8 Spyder - noir"};
case "cl3_r8_spyder_blue":{_lvmg="Audi R8 Spyder - bleu"};
case "cl3_r8_spyder_burgundy":{_lvmg="Audi R8 Spyder - rouge bordeaux"};
case "cl3_r8_spyder_camo":{_lvmg="Audi R8 Spyder - camouflage"};
case "cl3_r8_spyder_camo_urban":{_lvmg="Audi R8 Spyder - camouflage urbain"};
case "cl3_r8_spyder_cardinal":{_lvmg="Audi R8 Spyder - rouge cardinal"};
case "cl3_r8_spyder_dark_green":{_lvmg="Audi R8 Spyder - vert sombre"};
case "cl3_r8_spyder_flame":{_lvmg="Audi R8 Spyder - flamme"};
case "cl3_r8_spyder_flame1":{_lvmg="Audi R8 Spyder - flamme2"};
case "cl3_r8_spyder_flame2":{_lvmg="Audi R8 Spyder - flamme3"};
case "cl3_r8_spyder_gold":{_lvmg="Audi R8 Spyder - or"};
case "cl3_r8_spyder_green":{_lvmg="Audi R8 Spyder - vert"};
case "cl3_r8_spyder_grey":{_lvmg="Audi R8 Spyder - gri"};
case "cl3_r8_spyder_lavender":{_lvmg="Audi R8 Spyder - lavende"};
case "cl3_r8_spyder_light_blue":{_lvmg="Audi R8 Spyder - bleu clair"};
case "cl3_r8_spyder_light_yellow":{_lvmg="Audi R8 Spyder - jaune clair"};
case "cl3_r8_spyder_lime":{_lvmg="Audi R8 Spyder - jaune citron"};
case "cl3_r8_spyder_marina_blue":{_lvmg="Audi R8 Spyder - bleu marine"};
case "cl3_r8_spyder_navy_blue":{_lvmg="Audi R8 Spyder - bleu navy"};
case "cl3_r8_spyder_orange":{_lvmg="Audi R8 Spyder - orange"};
case "cl3_r8_spyder_purple":{_lvmg="Audi R8 Spyder - violet"};
case "cl3_r8_spyder_red":{_lvmg="Audi R8 Spyder - rouge"};
case "cl3_r8_spyder_sand":{_lvmg="Audi R8 Spyder - sable"};
case "cl3_r8_spyder_silver":{_lvmg="Audi R8 Spyder - argent"};
case "cl3_r8_spyder_violet":{_lvmg="Audi R8 Spyder - fushia"};
case "cl3_r8_spyder_white":{_lvmg="Audi R8 Spyder - blanc"};
case "cl3_r8_spyder_yellow":{_lvmg="Audi R8 Spyder - jaune"};

case "cl3_reventon":{_lvmg="Lamborghini Reventon"};
case "cl3_reventon_2tone1":{_lvmg="Lamborghini Reventon - bleu/vert"};
case "cl3_reventon_2tone2":{_lvmg="Lamborghini Reventon - violet/noir"};
case "cl3_reventon_2tone3":{_lvmg="Lamborghini Reventon - turquoise/blanc"};
case "cl3_reventon_2tone4":{_lvmg="Lamborghini Reventon - vert/jaune"};
case "cl3_reventon_2tone5":{_lvmg="Lamborghini Reventon - orange/jaune"};
case "cl3_reventon_aqua":{_lvmg="Lamborghini Reventon - aqua"};
case "cl3_reventon_babyblue":{_lvmg="Lamborghini Reventon - bleu vif"};
case "cl3_reventon_babypink":{_lvmg="Lamborghini Reventon - rose vif"};
case "cl3_reventon_black":{_lvmg="Lamborghini Reventon - noir"};
case "cl3_reventon_blue":{_lvmg="Lamborghini Reventon - bleu"};
case "cl3_reventon_burgundy":{_lvmg="Lamborghini Reventon - rouge bordeaux"};
case "cl3_reventon_camo":{_lvmg="Lamborghini Reventon - camouflage"};
case "cl3_reventon_camo_urban":{_lvmg="Lamborghini Reventon - camouflage urbain"};
case "cl3_reventon_cardinal":{_lvmg="Lamborghini Reventon - rouge cardinal"};
case "cl3_reventon_clpd":{_lvmg="Lamborghini Reventon - Police"};
case "cl3_reventon_clpd_traf":{_lvmg="Lamborghini Reventon - Police trafic"};
case "cl3_reventon_dark_green":{_lvmg="Lamborghini Reventon - vert sombre"};
case "cl3_reventon_flame":{_lvmg="Lamborghini Reventon - flamme"};
case "cl3_reventon_flame1":{_lvmg="Lamborghini Reventon - flamme2"};
case "cl3_reventon_flame2":{_lvmg="Lamborghini Reventon - flamme3"};
case "cl3_reventon_gold":{_lvmg="Lamborghini Reventon - or"};
case "cl3_reventon_green":{_lvmg="Lamborghini Reventon - vert"};
case "cl3_reventon_grey":{_lvmg="Lamborghini Reventon - gri"};
case "cl3_reventon_lavender":{_lvmg="Lamborghini Reventon - lavende"};
case "cl3_reventon_light_blue":{_lvmg="Lamborghini Reventon - bleu clair"};
case "cl3_reventon_light_yellow":{_lvmg="Lamborghini Reventon - jaune clair"};
case "cl3_reventon_lime":{_lvmg="Lamborghini Reventon - jaune citron"};
case "cl3_reventon_marina_blue":{_lvmg="Lamborghini Reventon - bleu marine"};
case "cl3_reventon_navy_blue":{_lvmg="Lamborghini Reventon - bleu navy"};
case "cl3_reventon_orange":{_lvmg="Lamborghini Reventon - orange"};
case "cl3_reventon_purple":{_lvmg="Lamborghini Reventon - violet"};
case "cl3_reventon_red":{_lvmg="Lamborghini Reventon - rouge"};
case "cl3_reventon_sand":{_lvmg="Lamborghini Reventon - sable"};
case "cl3_reventon_silver":{_lvmg="Lamborghini Reventon - argent"};
case "cl3_reventon_violet":{_lvmg="Lamborghini Reventon - fushia"};
case "cl3_reventon_white":{_lvmg="Lamborghini Reventon - blanc"};
case "cl3_reventon_yellow":{_lvmg="Lamborghini Reventon - jaune"};

case "cl3_veyron":{_lvmg="Bugatti Veyron"};
case "cl3_veyron_black":{_lvmg="Bugatti Veyron - noir"};
case "cl3_veyron_blk_wht":{_lvmg="Bugatti Veyron - blanc/noir"};
case "cl3_veyron_brn_blk":{_lvmg="Bugatti Veyron - rouge/noir"};
case "cl3_veyron_lblue_dblue":{_lvmg="Bugatti Veyron - bleu/noir"};
case "cl3_veyron_lblue_lblue":{_lvmg="Bugatti Veyron - bleu"};
case "cl3_veyron_red_red":{_lvmg="Bugatti Veyron - rouge"};
case "cl3_veyron_wht_blu":{_lvmg="Bugatti Veyron - turquoise/blanc"};
case "cl3_veyron_wht_lwht":{_lvmg="Bugatti Veyron - rouge bordeaux"};

/////////////////////////////////////////////////////////////////////////////////
//CIV_FUN
/////////////////////////////////////////////////////////////////////////////////

case "A3L_MonsterTruck":{_lvmg="Monster Truck"};
case "A3L_Hoverboard_bull":{_lvmg="Hoverboard Bull"};
case "A3L_Hoverboard":{_lvmg="Hoverboard Mattel"};
case "cl_skatea":{_lvmg="Skateboard - A"};
case "cl_skateb":{_lvmg="Skateboard - B"};
case "cl_skatec":{_lvmg="Skateboard - C"};
case "cl_skated":{_lvmg="Skateboard - D"};
case "cl_skatee":{_lvmg="Skateboard - E"};
case "cl_skatef":{_lvmg="Skateboard - F"};
case "cl_skateg":{_lvmg="Skateboard - G"};
case "cl_skateh":{_lvmg="Skateboard - H"};
case "cl_skatei":{_lvmg="Skateboard - I"};
case "cl_skatej":{_lvmg="Skateboard - J"};
case "cl_skatek":{_lvmg="Skateboard - K"};
case "cl_skatel":{_lvmg="Skateboard - L"};
case "cl_skatem":{_lvmg="Skateboard - M"};
case "cl_skaten":{_lvmg="Skateboard - N"};
case "cl_skateo":{_lvmg="Skateboard - O"};
case "cl_skatep":{_lvmg="Skateboard - P"};
case "cl_skateq":{_lvmg="Skateboard - Q"};
case "cl_skater":{_lvmg="Skateboard - R"};
case "cl_skates":{_lvmg="Skateboard - S"};
case "cl_skatet":{_lvmg="Skateboard - T"};
case "cl_skateu":{_lvmg="Skateboard - U"};
case "cl_skatev":{_lvmg="Skateboard - V"};
case "cl_skatex":{_lvmg="Skateboard - X"};
case "cl_skatey":{_lvmg="Skateboard - Y"};

/////////////////////////////////////////////////////////////////////////////////
//CIV_TRUCKS
/////////////////////////////////////////////////////////////////////////////////


case "CL3_bus_cl":{_lvmg="Bus"};
case "CL3_bus_cl_black":{_lvmg="Bus - noir"};
case "CL3_bus_cl_blue":{_lvmg="Bus - bleu"};
case "CL3_bus_cl_green":{_lvmg="Bus - vert"};
case "CL3_bus_cl_grey":{_lvmg="Bus - gris"};
case "CL3_bus_cl_jail":{_lvmg="Bus - prison"};
case "CL3_bus_cl_purple":{_lvmg="Bus - violet"};
case "CL3_bus_cl_yellow":{_lvmg="Bus - jaune"};


case "cl3_mackr_del":{_lvmg="Mack R"};
case "cl3_mackr_del_american":{_lvmg="Mack R - americain"};
case "cl3_mackr_del_black":{_lvmg="Mack R - noir"};
case "cl3_mackr_del_black_gold":{_lvmg="Mack R - noir/or"};
case "cl3_mackr_del_black_white":{_lvmg="Mack R - noir/blanc"};
case "cl3_mackr_del_blue":{_lvmg="Mack R - bleu"};
case "cl3_mackr_del_brown_camo":{_lvmg="Mack R - marron camouflage"};
case "cl3_mackr_del_forest_camo":{_lvmg="Mack R - forêt camouflage"};
case "cl3_mackr_del_green_white":{_lvmg="Mack R - vert/blanc"};
case "cl3_mackr_del_multi_color":{_lvmg="Mack R - multi couleurs"};
case "cl3_mackr_del_optimus":{_lvmg="Mack R - optimus prime"};
case "cl3_mackr_del_orange_white":{_lvmg="Mack R - orange/blanc"};
case "cl3_mackr_del_purple_white":{_lvmg="Mack R - violet/blanc"};
case "cl3_mackr_del_red_white":{_lvmg="Mack R - rouge/blanc"};
case "cl3_mackr_del_silver":{_lvmg="Mack R - argent"};


/////////////////////////////////////////////////////////////////////////////////
//AVIATION
/////////////////////////////////////////////////////////////////////////////////

case "cl3_GNT_C185_base":{_lvmg="Cessna 185 Skywagon"};
case "cl3_GNT_C185_fuse1":{_lvmg="Cessna 185 Skywagon - blanc"};
case "cl3_GNT_C185_fuse2":{_lvmg="Cessna 185 Skywagon - jaune/rouge"};
case "cl3_GNT_C185_fuse3":{_lvmg="Cessna 185 Skywagon - jaune"};
case "cl3_GNT_C185_fuse4":{_lvmg="Cessna 185 Skywagon - blanc/bleu"};
case "cl3_GNT_C185_fuse5":{_lvmg="Cessna 185 Skywagon - pale"};
case "cl3_GNT_C185_fuse6":{_lvmg="Cessna 185 Skywagon - jaune camouflage"};
case "GNT_C185F":{_lvmg="Cessna 185 Hydravion"};

case "C130J_Cargo":{_lvmg="C130J - Transport"};

case "IVORY_CRJ200_1":{_lvmg="CRJ200"};
case "IVORY_ERJ135_1":{_lvmg="ERJ135"};

/////////////////////////////////////////////////////////////////////////////////
//DEPANNEUR
/////////////////////////////////////////////////////////////////////////////////

case "cl3_f150repo":{_lvmg="Dépanneuse"};
case "cl3_f150repo_black":{_lvmg="Dépanneuse - noir"};
case "cl3_f150repo_blue":{_lvmg="Dépanneuse - bleu"};
case "cl3_f150repo_gray":{_lvmg="Dépanneuse - gris"};
case "cl3_f150repo_green":{_lvmg="Dépanneuse - vert"};
case "cl3_f150repo_orange":{_lvmg="Dépanneuse - orange"};
case "cl3_f150repo_red":{_lvmg="Dépanneuse - rouge"};

/////////////////////////////////////////////////////////////////////////////////
//RIGOLOL
/////////////////////////////////////////////////////////////////////////////////

case "cl3_batmobile":{_lvmg="Batmobile"};
case "cl3_jetpack":{_lvmg="Jetpack"};




};
_lvmg;