/*
	File: fn_clothing_reb.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for Reb shop.
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Tenue Maras"];

switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[
		["alfr_guerilla_cesar","Tenue C.E.S.A.R",10000]
		];
	};
	
	//Hats
	case 1:
	{
		[


		];
	};
	
	//Glasses
	case 2:
	{
		[
			
		];
	};
	
	//Vest
	case 3:
	{
		[
		

		];
	};
	
	//Backpacks
	case 4:
	{
		[
		["B_Carryall_mcamo","Backpack C.E.S.A.R",5000]

		];
	};
};
//[] call life_fnc_updateClothing;