/*
	File: fn_clothing_bruce.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for Bruce's Outback Outfits.
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Chapeau Rigolo"];

switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[
		
	
		];
	};
	
	//Hats
	case 1:
	{
		[
		["cl3_Headgear_AfricanMask","Masque Africain",250],
		["cl3_Headgear_alienMask","Masque Alien",250],
		["cl3_Headgear_devilMask","Masque Diable",250],
		["cl3_Headgear_pigMask","Masque Cochon",250],
		["cl3_Headgear_vampireMask","Masque Vampire",250],
		["cl3_Headgear_PieHat","Chapeau Porkpie",250],
		["cl3_Headgear_BaseballCap2black","Casquette Baseball noir",250],
		["cl3_Headgear_BaseballCap2green","Casquette Baseball verte",250],
		["cl3_Headgear_BaseballCap2Navyblue","Casquette Baseball bleu",250],
		["cl3_Headgear_BaseballCap2pink","Casquette Baseball rose",250],
		["cl3_Headgear_BaseballCap2red","Casquette Baseball rouge",250],
		["cl3_Headgear_BaseballCap2silver","Casquette Baseball argent",250],
		//["cl3_Headgear_busdriverhat",nil,250], // Taxi 
		["cl3_Headgear_chin","Paire de burnes",250],
		["cl3_Headgear_cigar","Cigare",250],
		["cl3_Headgear_Civcap1","Casquette China",250],
		["cl3_Headgear_Civcap2","Casquette W",250],
		["cl3_Headgear_Civcap3","Casquette Cowboy",250],
		["cl3_Headgear_Civcap4","Casquette Stella",250],
		["cl3_Headgear_cowboyHat","Chapeau de CowBoy",250],
		["cl3_Headgear_DarthVader","Masque de Dark Vador",250],
		
			//["cl3_Headgear_emtcap",nil,250],
			//["cl3_Headgear_emtcapCG",nil,250],
			//["cl3_Headgear_emtcapCG",nil,250],
			//["cl3_Headgear_emtcapFD",nil,250],
			//["cl3_Headgear_emtcapMR",nil,250],
			//["cl3_Headgear_emtcapPM",nil,250],
			//["cl3_Headgear_emtcapsenior",nil,250], //casquette EMT
		    //
			//["cl3_Headgear_emthelmet",nil,250], // Casque pompier
			//["cl3_Headgear_emthelmet_asschief",nil,250],
			//["cl3_Headgear_emthelmet_chief",nil,250],
			//["cl3_Headgear_emthelmet_dchief",nil,250],
			//["cl3_Headgear_emthelmet_Divisional",nil,250],
			//["cl3_Headgear_emthelmet_prob",nil,250],
			//["cl3_Headgear_emthelmet_senior",nil,250],
			//["cl3_Headgear_emthelmet_station",nil,250],
		
		
		["cl3_Headgear_F1helmet","Casque F1",250],
		["cl3_Headgear_F1helmetviserless","Casque F1 sans visière",250],
		
			//["cl3_headgear_fire_helmet_black",nil,250],
			//["cl3_headgear_fire_helmet_black",nil,250],
			//["cl3_headgear_fire_helmet_yellow",nil,250], // Pompier2
		
		["cl3_Headgear_goblin",nil,250],
		
	
		
		["cl3_Headgear_helmu","Daft Punk",250],
		
		["cl3_Headgear_Imbatman",nil,250],
		
		["cl3_Headgear_motorbike_helmet","Casque Moto Cross",250],

		
		["cl3_Headgear_paperbaghappy","Tête de sac content",250],
		["cl3_Headgear_paperbagsad","Tête de sac pas content",250],
		

		
		
		["cl3_Headgear_predator","Masque de prédator",250],
		["cl3_Headgear_Santahat","Chapeau de père Noël",250],
		["cl3_Headgear_skull_helmetBlack","Masque de squelette noir",250],
		["cl3_Headgear_skull_helmetblue","Masque de squelette bleu",250],
		["cl3_Headgear_skull_helmetcarbon","Masque de squelette carbonne",250],
		["cl3_Headgear_skull_helmetchrome","Masque de squelette chromé",250],
		
		["cl3_Headgear_sombrero",nil,250],
		//["cl3_Headgear_spliff",nil,250],
		["cl3_Headgear_viking","Casque de viking",250],
		["cl3_Headgear_Work_helmet","Casque de sécurité",250]
		];
	};
	
	//Glasses
	case 2:
	{
		[

		];
	};
	
	//Vest
	case 3:
	{
		[
		];
	};
	
	//Backpacks
	case 4:
	{
		[

		];
	};
};