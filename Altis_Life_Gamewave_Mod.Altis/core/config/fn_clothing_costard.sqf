/*
	File: fn_clothing_bruce.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for Bruce's Outback Outfits.
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Costumes"];

switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[
["cl3_suit_black_black_black",nil,10000],
["cl3_suit_black_blue_black",nil,10000],
["cl3_suit_black_blue_brown",nil,10000],
["cl3_suit_black_red_black",nil,10000],
["cl3_suit_brown_blue_black",nil,10000],
["cl3_suit_gray_black_black",nil,10000],
["cl3_suit_pink_pink_black",nil,10000],
["cl3_suit_purple_white_black",nil,10000],
["cl3_suit_red_red_black",nil,10000],
["cl3_suit_white_white_black",nil,10000],
["cl3_suit_black_gold_black",nil,10000],
["cl3_Suit_pimp",nil,10000],
["cl3_suit_blue_black_black",nil,10000],
["cl3_Suit_white_black_black",nil,10000],
["cl3_Suit_green_blue_brown",nil,10000],
["cl3_Suit_black_pink_brown",nil,10000],
["cl3_Suit_grey_stripe_black",nil,10000],
["cl3_Suit_blue_purple_black",nil,10000],
["cl3_Suit_red_black_black",nil,10000],
["cl3_suit_black_black_black",nil,10000],
["cl3_suit_black_blue_black",nil,10000],
["cl3_suit_black_blue_brown",nil,10000],
["cl3_suit_black_red_black",nil,10000],
["cl3_suit_brown_blue_black",nil,10000],
["cl3_suit_gray_black_black",nil,10000],
["cl3_suit_pink_pink_black",nil,10000],
["cl3_suit_purple_white_black",nil,10000],
["cl3_suit_red_red_black",nil,10000],
["cl3_suit_white_white_black",nil,10000],
["cl3_suit_black_gold_black",nil,10000],
["cl3_Suit_pimp",nil,10000],
["cl3_suit_blue_black_black",nil,10000],
["cl3_Suit_white_black_black",nil,10000],
["cl3_Suit_green_blue_brown",nil,10000],
["cl3_Suit_black_pink_brown",nil,10000],
["cl3_Suit_grey_stripe_black",nil,10000],
["cl3_Suit_blue_purple_black",nil,10000],
["cl3_Suit_red_black_black",nil,10000]
		
	
		];
	};
	
	//Hats
	case 1:
	{
		[

		];
	};
	
	//Glasses
	case 2:
	{
		[

		];
	};
	
	//Vest
	case 3:
	{
		[
		];
	};
	
	//Backpacks
	case 4:
	{
		[

		];
	};
};