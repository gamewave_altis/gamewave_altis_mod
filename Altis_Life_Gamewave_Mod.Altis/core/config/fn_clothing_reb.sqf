/*
	File: fn_clothing_reb.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Master configuration file for Reb shop.
*/
private["_filter"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price

//Shop Title Name
ctrlSetText[3103,"Mohammed's Jihadi Shop"];

switch (_filter) do
{
	//Uniforms
	case 0:
	{
		[
			["U_IG_Guerilla1_1",nil,5000],
			["U_I_G_Story_Protagonist_F",nil,7500],
			["U_I_G_resistanceLeader_F",nil,11500],
			["U_O_SpecopsUniform_ocamo",nil,17500],
			["U_B_PilotCoveralls",nil,17500],
			["U_O_PilotCoveralls",nil,15610],
			["U_IG_leader","Guerilla Leader",15340],
			["U_I_CombatUniform",nil,11500],
			["U_O_CombatUniform_oucamo","Uniforme de combat",15000],
			["alfr_b_combat_merc","Uniforme de combat 2",17000],
			["alfr_b_combat_merc2","Uniforme de combat 3",17000],
			["U_O_OfficerUniform_ocamo",nil,20000],
			["U_O_Wetsuit",nil,25000],
			["U_I_GhillieSuit","Ghillie camo vert",50000],
			["U_O_GhillieSuit",nil,50000]
			
		];
	};
	
	//Hats
	case 1:
	{
		[
			["H_ShemagOpen_tan",nil,850],
			["H_Shemag_olive",nil,850],
			["H_ShemagOpen_khk",nil,800],
			["H_MilCap_oucamo",nil,1200],
			["H_Shemag_tan",nil,1200],
			["H_Bandanna_camo",nil,650],
			["H_Booniehat_dgtl",nil,650],
			["H_Watchcap_sgg",nil,650],
			["H_MilCap_dgtl",nil,700],
			["H_HelmetO_ocamo",nil,2500],
			["H_HelmetLeaderO_ocamo",nil,2500]
		];
	};
	
	//Glasses
	case 2:
	{
		[
			["G_Shades_Black",nil,250],
			["G_Shades_Blue",nil,200],
			["G_mas_wpn_bala_gog_b",nil,500],
			["G_mas_wpn_bala_mask_b",nil,500],
			["G_mas_wpn_bala_b",nil,500],
			["G_mas_wpn_bala_t",nil,500],
			["G_mas_wpn_shemag_r",nil,500],
			["G_mas_wpn_shemag_mask",nil,500],
			["G_mas_wpn_shemag_w",nil,500],
			["G_mas_wpn_shemag",nil,500],
			["G_mas_wpn_wrap_mask_b",nil,500],
			["G_mas_wpn_wrap_mask_g",nil,500],
			["G_mas_wpn_wrap_mask_t",nil,500],
			["G_mas_wpn_wrap",nil,500],
			["G_mas_wpn_wrap_c",nil,500],
			["G_mas_wpn_wrap_gog",nil,500],
			["G_Sport_Blackred",nil,50],
			["G_Sport_Checkered",nil,50],
			["G_Sport_Blackyellow",nil,50],
			["G_Sport_BlackWhite",nil,50],
			["G_Squares",nil,10],
			["G_Lowprofile",nil,30],
			["G_Combat",nil,100],
			["G_Balaclava_combat","Cagoule aveuglante",5000],
			["G_Balaclava_lowprofile",nil,650],
			["G_Balaclava_oli",nil,650],
			["G_Balaclava_blk",nil,650],
			["G_Diving",nil,200]
		];
	};
	
	//Vest
	case 3:
	{
		[
			["V_BandollierB_cbr",nil,700],
			["V_HarnessO_brn",nil,1200],
			["V_HarnessOGL_brn",nil,2000],
			["V_HarnessOSpec_brn",nil,2000],
			["V_PlateCarrierIA1_dgtl",nil,16000],
			["V_TacVest_oli",nil,15000],
			["V_TacVest_khk",nil,15000],
			["V_TacVest_camo",nil,15000],
			["V_RebreatherIR",nil,10000]
			
		];
	};
	
	//Backpacks
	case 4:
	{
		[
			["B_AssaultPack_cbr","Sac à Dos 44 Slots",2500],
			["cl3_civ_tacticalbelt","Ceinture 49 Slots",3000],
			["cl3_civ_tacticalbelt_black","Ceinture noir 49 Slots",3000],
			["B_FieldPack_ocamo","Sac à Dos 49 Slots",3000],
			["B_TacticalPack_oli","Sac à Dos 54 Slots",3500],
			["B_Kitbag_mcamo","Sac à Dos 59 Slots",4500],
			["B_Bergen_sgg","Sac à Dos 59 Slots",4500],
			["B_Kitbag_cbr","Sac à Dos 59 Slots",4500],
			["B_Carryall_oli","Sac à Dos 64 Slots",5000],
			["B_Carryall_khk","Sac à Dos 64 Slots",5000],
			["CL3_ParachutePack","Parachute",2500],
			["CL3_ParachutePackCptAmer","Parachute Captin America",2500],
			["CL3_ParachutePackPhyco","Parachute Psycho",2500],
			["CL3_ParachutePackAmerica","Parachute America",2500],
			["CL3_ParachutePackRedEye","Parachute Red Eye",2500],
			["CL3_ParachutePackToxic","Parachute Toxic",2500],
			["CL3_ParachutePackGoblin","Parachute Goblin",2500],
			["CL3_ParachutePackVista","Parachute Vista",2500],
			["CL3_ParachutePackBlueFire","Parachute Blue Fire",2500],
			["CL3_ParachutePackASU","Parachute ASU",2500]
		];
	};
};