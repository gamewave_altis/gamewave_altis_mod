/*

	File: fn_VehicleWeight.sqf

	Modif for GameWave

	

	Description:

	Base configuration for vehicle weight

*/
private["_vehicle","_weight","_used"];
_vehicle = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _vehicle) exitWith {};


_weight = -1;
_used = (_vehicle getVariable "Trunk") select 1;

switch ((typeOf _vehicle)) do

{

	case "Skyline_Mercedes_C63_01_F": {_weight = 70;}; 
	case "Skyline_Mercedes_C63_02_F": {_weight = 70;}; 
	
	//HELIDLC
		//REB

	case "O_Heli_Transport_04_bench_F": {_weight = 50;}; 
	case "O_Heli_Transport_04_covered_F": {_weight = 210;}; 
		//COP
	case "B_Heli_Transport_03_unarmed_F": {_weight = 100;}; 
		//CIV
	case "O_Heli_Transport_04_F": {_weight = 40;}; 
	case "C_Heli_Light_01_civil_F": {_weight = 30;}; 
	case "O_Heli_Transport_04_medevac_F": {_weight = 210;}; 
	case "O_Heli_Transport_04_repair_F": {_weight = 210;}; 
	
	
	//KARTDLC
	case "C_Kart_01_Blu_F": {_weight = 5;}; // Kart
	case "C_Kart_01_Fuel_F": {_weight = 5;}; // Kart
	case "C_Kart_01_Red_F": {_weight = 5;}; // Kart
	case "C_Kart_01_Vrana_F": {_weight = 5;}; // Kart


	case "B_Quadbike_01_F": {_weight = 30;}; // Quad

	case "C_Hatchback_01_F": {_weight = 70;}; // Voiture Hayon

	case "C_Hatchback_01_sport_F": {_weight = 70;}; // Voiture Hayon Sport	

	case "C_Offroad_01_F": {_weight = 95;}; //Pickup

	case "C_SUV_01_F": {_weight = 85;}; // SUV 

	

	case "C_Van_01_transport_F": {_weight = 200;}; // Camionnette

	case "C_Van_01_box_F": {_weight = 250;}; // Camionnette couvert 

	case "C_Van_01_Fuel_F": {_weight = 10;}; // Camion Citerne

	

	case "I_Truck_02_transport_F": {_weight = 285;}; // Zamak 

	case "I_Truck_02_covered_F": {_weight = 340;}; // Zamak Couvert	

	case "I_Truck_02_box_F": {_weight = 10;}; // Camion Pegasus 

	

	case "B_Truck_01_transport_F": {_weight = 575;}; // HEMTT 

	case "B_Truck_01_box_F": {_weight = 750;}; // HEMTT Couvert

	

	case "O_Truck_03_device_F": {_weight = 375;}; //Tempest

	

	case "B_G_Offroad_01_F": {_weight = 110;}; //Pickup rebelle

	case "B_G_Offroad_01_armed_F": {_weight = 90;}; //Pickup Arm�

	case "O_MRAP_02_F": {_weight = 60;}; // Ifrit

	

	case "B_MRAP_01_F": {_weight = 65;}; // Hunter

	case "B_MRAP_01_hmg_F": {_weight = 65;}; // Hunter Arm�	

	case "I_MRAP_03_F": {_weight = 60;}; // Strider

	case "B_Heli_Light_01_F": {_weight = 30;}; //MH-9 Hummingbird

	case "B_Heli_Attack_01_F": {_weight = 40;}; //Blackfoot

	case "O_Heli_Light_02_unarmed_F": {_weight = 70;}; // Po-30 Orca

	case "I_Heli_Transport_02_F": {_weight = 125;}; // Mohawk

	case "I_Heli_light_03_unarmed_F": {_weight = 40;}; //Hellcat

    case "B_Heli_Transport_01_F": {_weight = 40;}; //Ghost Hawk


	case "C_Rubberboat": {_weight = 90;}; // Petit bateau

	case "I_G_Boat_Transport_01_F": {_weight = 150;}; //Canot d'Assaut

	case "C_Boat_Civil_01_F": {_weight = 200;}; // Bateau Civil


	case "B_Boat_Transport_01_F": {_weight = 90;}; //Petit Bateau

	case "C_Boat_Civil_01_police_F": {_weight = 200;}; // Bateau Police

	case "B_Boat_Armed_01_minigun_F": {_weight = 150;}; // Bateau arme

	case "B_SDV_01_F": {_weight = 30;}; // Sous-Marin

	case "Land_CargoBox_V1_F": {_weight = 5000;};
	
	case "Box_IND_Grenades_F": {_weight = 375;};
	
	case "B_supplyCrate_F": {_weight = 750;};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule LOW
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_xlada_red": {_weight =70;};
case "cl3_xlada_green": {_weight =70;};
case "cl3_xlada_blue": {_weight =70;};
case "cl3_xlada_classic": {_weight =70;};

case "cl3_lada": {_weight =60;};
case "cl3_lada_red": {_weight =60;};
case "cl3_lada_white": {_weight =60;};

case "cl3_volha": {_weight =70;};
case "cl3_volha_black": {_weight =70;};
case "cl3_volha_grey": {_weight =70;};

case "cl3_440cuda": {_weight =70;};
case "cl3_440cuda_black": {_weight =70;};
case "cl3_440cuda_black1": {_weight =70;};
case "cl3_440cuda_blu_ltn": {_weight =70;};
case "cl3_440cuda_blue": {_weight =70;};
case "cl3_440cuda_blue_flame": {_weight =70;};
case "cl3_440cuda_emt_chief": {_weight =70;};
case "cl3_440cuda_flannery08": {_weight =70;};
case "cl3_440cuda_flannery12": {_weight =70;};
case "cl3_440cuda_flannery14": {_weight =70;};
case "cl3_440cuda_flannery27": {_weight =70;};
case "cl3_440cuda_flannery51": {_weight =70;};
case "cl3_440cuda_flannery55": {_weight =70;};
case "cl3_440cuda_flannery69": {_weight =70;};
case "cl3_440cuda_flannery70": {_weight =70;};
case "cl3_440cuda_green": {_weight =70;};
case "cl3_440cuda_green_Logo": {_weight =70;};
case "cl3_440cuda_yellow": {_weight =70;};
case "cl3_440cuda_yellow_stripe": {_weight =70;};

case "cl3_bounder_beige": {_weight =130;};

case "cl3_civic_vti": {_weight =65;};
case "cl3_civic_vti_aqua": {_weight =65;};
case "cl3_civic_vti_babyblue": {_weight =65;};
case "cl3_civic_vti_babypink": {_weight =65;};
case "cl3_civic_vti_black": {_weight =65;};
case "cl3_civic_vti_blue": {_weight =65;};
case "cl3_civic_vti_burgundy": {_weight =65;};
case "cl3_civic_vti_camo": {_weight =65;};
case "cl3_civic_vti_camo_urban": {_weight =65;};
case "cl3_civic_vti_cardinal": {_weight =65;};
case "cl3_civic_vti_dark_green": {_weight =65;};
case "cl3_civic_vti_gold": {_weight =65;};
case "cl3_civic_vti_green": {_weight =65;};
case "cl3_civic_vti_grey": {_weight =65;};
case "cl3_civic_vti_lavender": {_weight =65;};
case "cl3_civic_vti_light_blue": {_weight =65;};
case "cl3_civic_vti_light_yellow": {_weight =65;};
case "cl3_civic_vti_lime": {_weight =65;};
case "cl3_civic_vti_marina_blue": {_weight =65;};
case "cl3_civic_vti_navy_blue": {_weight =65;};
case "cl3_civic_vti_orange": {_weight =65;};
case "cl3_civic_vti_purple": {_weight =65;};
case "cl3_civic_vti_red": {_weight =65;};
case "cl3_civic_vti_sand": {_weight =65;};
case "cl3_civic_vti_silver": {_weight =65;};
case "cl3_civic_vti_violet": {_weight =65;};
case "cl3_civic_vti_white": {_weight =65;};
case "cl3_civic_vti_yellow": {_weight =65;};

case "cl3_crown_victoria": {_weight =75;};
case "cl3_crown_victoria_aqua": {_weight =75;};
case "cl3_crown_victoria_babyblue": {_weight =75;};
case "cl3_crown_victoria_babypink": {_weight =75;};
case "cl3_crown_victoria_black": {_weight =75;};
case "cl3_crown_victoria_blue": {_weight =75;};
case "cl3_crown_victoria_burgundy": {_weight =75;};
case "cl3_crown_victoria_camo": {_weight =75;};
case "cl3_crown_victoria_camo_urban": {_weight =75;};
case "cl3_crown_victoria_cardinal": {_weight =75;};
case "cl3_crown_victoria_dark_green": {_weight =75;};
case "cl3_crown_victoria_gold": {_weight =75;};
case "cl3_crown_victoria_green": {_weight =75;};
case "cl3_crown_victoria_grey": {_weight =75;};
case "cl3_crown_victoria_lavender": {_weight =75;};
case "cl3_crown_victoria_light_blue": {_weight =75;};
case "cl3_crown_victoria_light_yellow": {_weight =75;};
case "cl3_crown_victoria_lime": {_weight =75;};
case "cl3_crown_victoria_marina_blue": {_weight =75;};
case "cl3_crown_victoria_navy_blue": {_weight =75;};
case "cl3_crown_victoria_orange": {_weight =75;};
case "cl3_crown_victoria_purple": {_weight =75;};
case "cl3_crown_victoria_red": {_weight =75;};
case "cl3_crown_victoria_sand": {_weight =75;};
case "cl3_crown_victoria_silver": {_weight =75;};
case "cl3_crown_victoria_violet": {_weight =75;};
case "cl3_crown_victoria_white": {_weight =75;};
case "cl3_crown_victoria_yellow": {_weight =75;};

case "cl3_defender_110": {_weight =110;};
case "cl3_defender_110_cammo": {_weight =110;};
case "cl3_defender_110_red": {_weight =110;};
case "cl3_defender_110_yellow": {_weight =110;};

case "cl3_discovery": {_weight =110;};
case "cl3_discovery_black": {_weight =110;};
case "cl3_discovery_blue": {_weight =110;};
case "cl3_discovery_darkorange": {_weight =110;};
case "cl3_discovery_gold": {_weight =110;};
case "cl3_discovery_green": {_weight =110;};
case "cl3_discovery_hellokitty": {_weight =110;};
case "cl3_discovery_joker": {_weight =110;};
case "cl3_discovery_pink": {_weight =110;};
case "cl3_discovery_silver": {_weight =110;};

case "cl3_golf_mk2": {_weight =65;};
case "cl3_golf_mk2_aqua": {_weight =65;};
case "cl3_golf_mk2_babyblue": {_weight =65;};
case "cl3_golf_mk2_babypink": {_weight =65;};
case "cl3_golf_mk2_black": {_weight =65;};
case "cl3_golf_mk2_blue": {_weight =65;};
case "cl3_golf_mk2_burgundy": {_weight =65;};
case "cl3_golf_mk2_camo": {_weight =65;};
case "cl3_golf_mk2_camo_urban": {_weight =65;};
case "cl3_golf_mk2_cardinal": {_weight =65;};
case "cl3_golf_mk2_dark_green": {_weight =65;};
case "cl3_golf_mk2_gold": {_weight =65;};
case "cl3_golf_mk2_green": {_weight =65;};
case "cl3_golf_mk2_grey": {_weight =65;};
case "cl3_golf_mk2_lavender": {_weight =65;};
case "cl3_golf_mk2_light_blue": {_weight =65;};
case "cl3_golf_mk2_light_yellow": {_weight =65;};
case "cl3_golf_mk2_lime": {_weight =65;};
case "cl3_golf_mk2_marina_blue": {_weight =65;};
case "cl3_golf_mk2_navy_blue": {_weight =65;};
case "cl3_golf_mk2_orange": {_weight =65;};
case "cl3_golf_mk2_purple": {_weight =65;};
case "cl3_golf_mk2_red": {_weight =65;};
case "cl3_golf_mk2_sand": {_weight =65;};
case "cl3_golf_mk2_silver": {_weight =65;};
case "cl3_golf_mk2_violet": {_weight =65;};
case "cl3_golf_mk2_white": {_weight =65;};
case "cl3_golf_mk2_yellow": {_weight =65;};

case "cl3_polo_gti": {_weight =65;};
case "cl3_polo_gti_aqua": {_weight =65;};
case "cl3_polo_gti_babyblue": {_weight =65;};
case "cl3_polo_gti_babypink": {_weight =65;};
case "cl3_polo_gti_black": {_weight =65;};
case "cl3_polo_gti_blue": {_weight =65;};
case "cl3_polo_gti_burgundy": {_weight =65;};
case "cl3_polo_gti_camo": {_weight =65;};
case "cl3_polo_gti_camo_urban": {_weight =65;};
case "cl3_polo_gti_cardinal": {_weight =65;};
case "cl3_polo_gti_dark_green": {_weight =65;};
case "cl3_polo_gti_gold": {_weight =65;};
case "cl3_polo_gti_green": {_weight =65;};
case "cl3_polo_gti_grey": {_weight =65;};
case "cl3_polo_gti_lavender": {_weight =65;};
case "cl3_polo_gti_light_blue": {_weight =65;};
case "cl3_polo_gti_light_yellow": {_weight =65;};
case "cl3_polo_gti_lime": {_weight =65;};
case "cl3_polo_gti_marina_blue": {_weight =65;};
case "cl3_polo_gti_navy_blue": {_weight =65;};
case "cl3_polo_gti_orange": {_weight =65;};
case "cl3_polo_gti_purple": {_weight =65;};
case "cl3_polo_gti_red": {_weight =65;};
case "cl3_polo_gti_sand": {_weight =65;};
case "cl3_polo_gti_silver": {_weight =65;};
case "cl3_polo_gti_violet": {_weight =65;};
case "cl3_polo_gti_white": {_weight =65;};
case "cl3_polo_gti_yellow": {_weight =65;};

case "beetle_custom": {_weight =95;};
case "beetle_bleufonce": {_weight =95;};
case "beetle_bleupetrole": {_weight =95;};
case "beetle_red": {_weight =95;};
case "beetle_vert": {_weight =95;};
case "beetle_violet": {_weight =95;};
case "beetle_white": {_weight =95;};
case "beetle_psycha": {_weight =95;};
case "beetle_psycha1": {_weight =95;};
case "beetle_coci": {_weight =95;};
case "beetle_camo": {_weight =95;};
case "beetle": {_weight =95;};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule MEDIUM
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "clpd_mondeo": {_weight =80;};
case "civ_mondeo_Aqua": {_weight =80;};
case "civ_mondeo_BabyBlue": {_weight =80;};
case "civ_mondeo_BabyPink": {_weight =80;};
case "civ_mondeo_Black": {_weight =80;};
case "civ_mondeo_Burgundy": {_weight =80;};
case "civ_mondeo_CardinalBurgundy": {_weight =80;};
case "civ_mondeo_DarkGreen": {_weight =80;};
case "civ_mondeo_Gold": {_weight =80;};
case "civ_mondeo_Green": {_weight =80;};
case "civ_mondeo_Grey": {_weight =80;};
case "civ_mondeo_Lavendel": {_weight =80;};
case "civ_mondeo_LightBlue": {_weight =80;};
case "civ_mondeo_LightYellow": {_weight =80;};
case "civ_mondeo_Lime": {_weight =80;};
case "civ_mondeo_MarineBlue": {_weight =80;};
case "civ_mondeo_NavyBlue": {_weight =80;};
case "civ_mondeo_Orange": {_weight =80;};
case "civ_mondeo_Pink": {_weight =80;};
case "civ_mondeo_Purple": {_weight =80;};
case "civ_mondeo_Red": {_weight =80;};
case "civ_mondeo_Silver": {_weight =80;};
case "civ_mondeo_SkyBlue": {_weight =80;};
case "civ_mondeo_Violet": {_weight =80;};
case "civ_mondeo_White": {_weight =80;};
case "civ_mondeo_Yellow": {_weight =80;};

case "cl3_challenger_2009": {_weight =70;};
case "cl3_challenger_2009_black": {_weight =70;};
case "cl3_challenger_2009_blue": {_weight =70;};
case "cl3_challenger_2009_green": {_weight =70;};
case "cl3_challenger_2009_red": {_weight =70;};
case "cl3_challenger_2009_silver": {_weight =70;};
case "cl3_challenger_2009_yellow": {_weight =70;};
case "cl3_challenger_aquablue": {_weight =70;};
case "cl3_challenger_beige": {_weight =70;};
case "cl3_challenger_beige2": {_weight =70;};
case "cl3_challenger_blue": {_weight =70;};
case "cl3_challenger_brown": {_weight =70;};
case "cl3_challenger_darksilver": {_weight =70;};
case "cl3_challenger_green": {_weight =70;};
case "cl3_challenger_lime": {_weight =70;};
case "cl3_challenger_orange": {_weight =70;};
case "cl3_challenger_orangeflame": {_weight =70;};
case "cl3_challenger_pink": {_weight =70;};
case "cl3_challenger_purple": {_weight =70;};
case "cl3_challenger_red": {_weight =70;};
case "cl3_challenger_yellow": {_weight =70;};

case "cl3_dodge_charger_patrol": {_weight =70;};
case "cl3_dodge_charger_cn": {_weight =70;};
case "cl3_dodge_charger_emt": {_weight =70;};
case "cl3_dodge_charger_emtcg": {_weight =70;};
case "cl3_dodge_charger_emtfd": {_weight =70;};
case "cl3_dodge_charger_emtmr": {_weight =70;};
case "cl3_dodge_charger_emtpa": {_weight =70;};
case "cl3_dodge_charger_etu": {_weight =70;};
case "cl3_dodge_charger_k9": {_weight =70;};
case "cl3_dodge_charger_patrol": {_weight =70;};
case "cl3_dodge_charger_patrol2": {_weight =70;};
case "cl3_dodge_charger_s_black": {_weight =70;};
case "cl3_dodge_charger_s_blue": {_weight =70;};
case "cl3_dodge_charger_s_camo": {_weight =70;};
case "cl3_dodge_charger_s_camourban": {_weight =70;};
case "cl3_dodge_charger_s_darkgreen": {_weight =70;};
case "cl3_dodge_charger_s_darkred": {_weight =70;};
case "cl3_dodge_charger_s_green": {_weight =70;};
case "cl3_dodge_charger_s_grey": {_weight =70;};
case "cl3_dodge_charger_s_lime": {_weight =70;};
case "cl3_dodge_charger_s_orange": {_weight =70;};
case "cl3_dodge_charger_s_pink": {_weight =70;};
case "cl3_dodge_charger_s_purple": {_weight =70;};
case "cl3_dodge_charger_s_red": {_weight =70;};
case "cl3_dodge_charger_s_white": {_weight =70;};
case "cl3_dodge_charger_s_yellow": {_weight =70;};

case "cl3_e60_m5": {_weight =70;};
case "cl3_e60_m5_aqua": {_weight =70;};
case "cl3_e60_m5_babyblue": {_weight =70;};
case "cl3_e60_m5_babypink": {_weight =70;};
case "cl3_e60_m5_black": {_weight =70;};
case "cl3_e60_m5_blue": {_weight =70;};
case "cl3_e60_m5_burgundy": {_weight =70;};
case "cl3_e60_m5_camo": {_weight =70;};
case "cl3_e60_m5_camo_urban": {_weight =70;};
case "cl3_e60_m5_cardinal": {_weight =70;};
case "cl3_e60_m5_dark_green": {_weight =70;};
case "cl3_e60_m5_gold": {_weight =70;};
case "cl3_e60_m5_green": {_weight =70;};
case "cl3_e60_m5_grey": {_weight =70;};
case "cl3_e60_m5_lavender": {_weight =70;};
case "cl3_e60_m5_light_blue": {_weight =70;};
case "cl3_e60_m5_light_yellow": {_weight =70;};
case "cl3_e60_m5_lime": {_weight =70;};
case "cl3_e60_m5_marina_blue": {_weight =70;};
case "cl3_e60_m5_navy_blue": {_weight =70;};
case "cl3_e60_m5_orange": {_weight =70;};
case "cl3_e60_m5_purple": {_weight =70;};
case "cl3_e60_m5_red": {_weight =70;};
case "cl3_e60_m5_sand": {_weight =70;};
case "cl3_e60_m5_silver": {_weight =70;};
case "cl3_e60_m5_violet": {_weight =70;};
case "cl3_e60_m5_white": {_weight =70;};
case "cl3_e60_m5_yellow": {_weight =70;};

case "cl3_e63_amg": {_weight =80;};
case "cl3_e63_amg_aqua": {_weight =80;};
case "cl3_e63_amg_babyblue": {_weight =80;};
case "cl3_e63_amg_babypink": {_weight =80;};
case "cl3_e63_amg_black": {_weight =80;};
case "cl3_e63_amg_blue": {_weight =80;};
case "cl3_e63_amg_burgundy": {_weight =80;};
case "cl3_e63_amg_camo": {_weight =80;};
case "cl3_e63_amg_camo_urban": {_weight =80;};
case "cl3_e63_amg_cardinal": {_weight =80;};
case "cl3_e63_amg_dark_green": {_weight =80;};
case "cl3_e63_amg_gold": {_weight =80;};
case "cl3_e63_amg_green": {_weight =80;};
case "cl3_e63_amg_grey": {_weight =80;};
case "cl3_e63_amg_lavender": {_weight =80;};
case "cl3_e63_amg_light_blue": {_weight =80;};
case "cl3_e63_amg_light_yellow": {_weight =80;};
case "cl3_e63_amg_lime": {_weight =80;};
case "cl3_e63_amg_marina_blue": {_weight =80;};
case "cl3_e63_amg_navy_blue": {_weight =80;};
case "cl3_e63_amg_orange":{_weight=80;};
case "cl3_e63_amg_purple":{_weight=80;};
case "cl3_e63_amg_red":{_weight=80;};
case "cl3_e63_amg_sand":{_weight=80;};
case "cl3_e63_amg_silver":{_weight=80;};
case "cl3_e63_amg_violet":{_weight=80;};
case "cl3_e63_amg_white": {_weight =80;};
case "cl3_e63_amg_yellow": {_weight =80;};

case "cl3_escalade": {_weight =110;};
case "cl3_escalade_aqua": {_weight =110;};
case "cl3_escalade_babyblue": {_weight =110;};
case "cl3_escalade_babypink": {_weight =110;};
case "cl3_escalade_black": {_weight =110;};
case "cl3_escalade_blue": {_weight =110;};
case "cl3_escalade_burgundy": {_weight =110;};
case "cl3_escalade_camo": {_weight =110;};
case "cl3_escalade_camo_urban": {_weight =110;};
case "cl3_escalade_cardinal": {_weight =110;};
case "cl3_escalade_cg": {_weight =110;};
case "cl3_escalade_cn": {_weight =110;};
case "cl3_escalade_dark_green": {_weight =110;};
case "cl3_escalade_etu": {_weight =110;};
case "cl3_escalade_fd": {_weight =110;};
case "cl3_escalade_gold": {_weight =110;};
case "cl3_escalade_green": {_weight =110;};
case "cl3_escalade_grey": {_weight =110;};
case "cl3_escalade_k9": {_weight =110;};
case "cl3_escalade_lavender": {_weight =110;};
case "cl3_escalade_light_blue": {_weight =110;};
case "cl3_escalade_light_yellow": {_weight =110;};
case "cl3_escalade_lime": {_weight =110;};
case "cl3_escalade_marina_blue": {_weight =110;};
case "cl3_escalade_mr": {_weight =110;};
case "cl3_escalade_navy_blue": {_weight =110;};
case "cl3_escalade_orange": {_weight =110;};
case "cl3_escalade_patrolbw": {_weight =110;};
case "cl3_escalade_patrolwb": {_weight =110;};
case "cl3_escalade_pd": {_weight =110;};
case "cl3_escalade_pm": {_weight =110;};
case "cl3_escalade_purple": {_weight =110;};
case "cl3_escalade_red": {_weight =110;};
case "cl3_escalade_sand": {_weight =110;};
case "cl3_escalade_silver": {_weight =110;};
case "cl3_escalade_traffic": {_weight =110;};
case "cl3_escalade_violet": {_weight =110;};
case "cl3_escalade_white": {_weight =110;};
case "cl3_escalade_yellow": {_weight =110;};
case "cl3_escalade_dep": {_weight =110;};

case "cl3_q7": {_weight =90;};
case "cl3_q7_aqua": {_weight =90;};
case "cl3_q7_babyblue": {_weight =90;};
case "cl3_q7_babypink": {_weight =90;};
case "cl3_q7_black": {_weight =90;};
case "cl3_q7_blue": {_weight =90;};
case "cl3_q7_burgundy": {_weight =90;};
case "cl3_q7_camo": {_weight =90;};
case "cl3_q7_camo_urban": {_weight =90;};
case "cl3_q7_cardinal": {_weight =90;};
case "cl3_q7_clpd_cn": {_weight =90;};
case "cl3_q7_clpd_etu": {_weight =90;};
case "cl3_q7_clpd_patrol": {_weight =90;};
case "cl3_q7_dark_green": {_weight =90;};
case "cl3_q7_gold": {_weight =90;};
case "cl3_q7_green": {_weight =90;};
case "cl3_q7_grey": {_weight =90;};
case "cl3_q7_lavender": {_weight =90;};
case "cl3_q7_light_blue": {_weight =90;};
case "cl3_q7_light_yellow": {_weight =90;};
case "cl3_q7_lime": {_weight =90;};
case "cl3_q7_marina_blue": {_weight =90;};
case "cl3_q7_navy_blue": {_weight =90;};
case "cl3_q7_orange": {_weight =90;};
case "cl3_q7_purple": {_weight =90;};
case "cl3_q7_red": {_weight =90;};
case "cl3_q7_sand": {_weight =90;};
case "cl3_q7_silver": {_weight =90;};
case "cl3_q7_violet": {_weight =90;};
case "cl3_q7_white": {_weight =90;};
case "cl3_q7_yellow": {_weight =90;};

case "cl3_s5": {_weight =80;};
case "cl3_s5_aqua": {_weight =80;};
case "cl3_s5_babyblue": {_weight =80;};
case "cl3_s5_babypink": {_weight =80;};
case "cl3_s5_black": {_weight =80;};
case "cl3_s5_blue": {_weight =80;};
case "cl3_s5_burgundy": {_weight =80;};
case "cl3_s5_camo": {_weight =80;};
case "cl3_s5_camo_urban": {_weight =80;};
case "cl3_s5_cardinal": {_weight =80;};
case "cl3_s5_dark_green": {_weight =80;};
case "cl3_s5_gold": {_weight =80;};
case "cl3_s5_green": {_weight =80;};
case "cl3_s5_grey": {_weight =80;};
case "cl3_s5_lavender": {_weight =80;};
case "cl3_s5_light_blue": {_weight =80;};
case "cl3_s5_light_yellow": {_weight =80;};
case "cl3_s5_lime": {_weight =80;};
case "cl3_s5_marina_blue": {_weight =80;};
case "cl3_s5_navy_blue": {_weight =80;};
case "cl3_s5_orange": {_weight =80;};
case "cl3_s5_purple": {_weight =80;};
case "cl3_s5_red": {_weight =80;};
case "cl3_s5_sand": {_weight =80;};
case "cl3_s5_silver": {_weight =80;};
case "cl3_s5_violet": {_weight =80;};
case "cl3_s5_white": {_weight =80;};
case "cl3_s5_yellow": {_weight =80;};

case "cl3_suv": {_weight =110;};
case "cl3_suv_black": {_weight =110;};
case "cl3_suv_emt": {_weight =110;};
case "cl3_suv_taxi": {_weight =110;};

case "cl3_taurus": {_weight =80;};
case "cl3_taurus_aqua": {_weight =80;};
case "cl3_taurus_babyblue": {_weight =80;};
case "cl3_taurus_babypink": {_weight =80;};
case "cl3_taurus_black": {_weight =80;};
case "cl3_taurus_blue": {_weight =80;};
case "cl3_taurus_burgundy": {_weight =80;};
case "cl3_taurus_camo": {_weight =80;};
case "cl3_taurus_camo_urban": {_weight =80;};
case "cl3_taurus_cardinal": {_weight =80;};
case "cl3_taurus_dark_green": {_weight =80;};
case "cl3_taurus_gold": {_weight =80;};
case "cl3_taurus_green": {_weight =80;};
case "cl3_taurus_grey": {_weight =80;};
case "cl3_taurus_lavender": {_weight =80;};
case "cl3_taurus_light_blue": {_weight =80;};
case "cl3_taurus_light_yellow": {_weight =80;};
case "cl3_taurus_lime": {_weight =80;};
case "cl3_taurus_marina_blue": {_weight =80;};
case "cl3_taurus_navy_blue": {_weight =80;};
case "cl3_taurus_orange": {_weight =80;};
case "cl3_taurus_purple": {_weight =80;};
case "cl3_taurus_red": {_weight =80;};
case "cl3_taurus_sand": {_weight =80;};
case "cl3_taurus_silver": {_weight =80;};
case "cl3_taurus_violet": {_weight =80;};
case "cl3_taurus_white": {_weight =80;};
case "cl3_taurus_yellow": {_weight =80;};

case "cl3_transit": {_weight =170;};
case "cl3_transit_aqua": {_weight =170;};
case "cl3_transit_babyblue": {_weight =170;};
case "cl3_transit_babypink": {_weight =170;};
case "cl3_transit_black": {_weight =170;};
case "cl3_transit_blue": {_weight =170;};
case "cl3_transit_burgundy": {_weight =170;};
case "cl3_transit_camo": {_weight =170;};
case "cl3_transit_camo_urban": {_weight =170;};
case "cl3_transit_cardinal": {_weight =170;};
case "cl3_transit_civ": {_weight =170;};
case "cl3_transit_cop": {_weight =170;};
case "cl3_transit_dark_green": {_weight =170;};
case "cl3_transit_gold": {_weight =170;};
case "cl3_transit_green": {_weight =170;};
case "cl3_transit_grey": {_weight =170;};
case "cl3_transit_lavender": {_weight =170;};
case "cl3_transit_light_blue": {_weight =170;};
case "cl3_transit_light_yellow": {_weight =170;};
case "cl3_transit_lime": {_weight =170;};
case "cl3_transit_marina_blue": {_weight =170;};
case "cl3_transit_navy_blue": {_weight =170;};
case "cl3_transit_orange": {_weight =170;};
case "cl3_transit_purple": {_weight =170;};
case "cl3_transit_red": {_weight =170;};
case "cl3_transit_sand": {_weight =170;};
case "cl3_transit_silver": {_weight =170;};
case "cl3_transit_violet": {_weight =170;};
case "cl3_transit_white": {_weight =170;};
case "cl3_transit_yellow": {_weight =170;};
case "cl3_transitemt": {_weight =170;};
case "cl3_transitk9": {_weight =170;};
case "cl3_transitNews": {_weight =170;};
case "cl3_transitpatrol": {_weight =170;};

case "cl3_z4_2008": {_weight =50;};
case "cl3_z4_2008_aqua": {_weight =50;};
case "cl3_z4_2008_babyblue": {_weight =50;};
case "cl3_z4_2008_babypink": {_weight =50;};
case "cl3_z4_2008_black": {_weight =50;};
case "cl3_z4_2008_blue": {_weight =50;};
case "cl3_z4_2008_burgundy": {_weight =50;};
case "cl3_z4_2008_camo": {_weight =50;};
case "cl3_z4_2008_camo_urban": {_weight =50;};
case "cl3_z4_2008_cardinal": {_weight =50;};
case "cl3_z4_2008_dark_green": {_weight =50;};
case "cl3_z4_2008_gold": {_weight =50;};
case "cl3_z4_2008_green": {_weight =50;};
case "cl3_z4_2008_grey": {_weight =50;};
case "cl3_z4_2008_lavender": {_weight =50;};
case "cl3_z4_2008_light_blue": {_weight =50;};
case "cl3_z4_2008_light_yellow": {_weight =50;};
case "cl3_z4_2008_lime": {_weight =50;};
case "cl3_z4_2008_marina_blue": {_weight =50;};
case "cl3_z4_2008_navy_blue": {_weight =50;};
case "cl3_z4_2008_orange": {_weight =50;};
case "cl3_z4_2008_purple": {_weight =50;};
case "cl3_z4_2008_red": {_weight =50;};
case "cl3_z4_2008_sand": {_weight =50;};
case "cl3_z4_2008_silver": {_weight =50;};
case "cl3_z4_2008_violet": {_weight =50;};
case "cl3_z4_2008_white": {_weight =50;};
case "cl3_z4_2008_yellow": {_weight =50;};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Civil Vehicule HIGH
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_458": {_weight =30;};
case "cl3_458_2tone1": {_weight =30;};
case "cl3_458_2tone2": {_weight =30;};
case "cl3_458_2tone3": {_weight =30;};
case "cl3_458_2tone4": {_weight =30;};
case "cl3_458_2tone5": {_weight =30;};
case "cl3_458_aqua": {_weight =30;};
case "cl3_458_babyblue": {_weight =30;};
case "cl3_458_babypink": {_weight =30;};
case "cl3_458_black": {_weight =30;};
case "cl3_458_blue": {_weight =30;};
case "cl3_458_burgundy": {_weight =30;};
case "cl3_458_camo": {_weight =30;};
case "cl3_458_camo_urban": {_weight =30;};
case "cl3_458_cardinal": {_weight =30;};
case "cl3_458_dark_green": {_weight =30;};
case "cl3_458_flame": {_weight =30;};
case "cl3_458_flame1": {_weight =30;};
case "cl3_458_flame2": {_weight =30;};
case "cl3_458_gold": {_weight =30;};
case "cl3_458_green": {_weight =30;};
case "cl3_458_grey": {_weight =30;};
case "cl3_458_lavender": {_weight =30;};
case "cl3_458_light_blue": {_weight =30;};
case "cl3_458_light_yellow": {_weight =30;};
case "cl3_458_lime": {_weight =30;};
case "cl3_458_marina_blue": {_weight =30;};
case "cl3_458_navy_blue": {_weight =30;};
case "cl3_458_orange": {_weight =30;};
case "cl3_458_purple": {_weight =30;};
case "cl3_458_red": {_weight =30;};
case "cl3_458_sand": {_weight =30;};
case "cl3_458_silver": {_weight =30;};
case "cl3_458_violet": {_weight =30;};
case "cl3_458_white": {_weight =30;};
case "cl3_458_yellow": {_weight =30;};

case "cl3_aventador_lp7004": {_weight =30;};
case "cl3_aventador_lp7004_2tone1": {_weight =30;};
case "cl3_aventador_lp7004_2tone2": {_weight =30;};
case "cl3_aventador_lp7004_2tone3": {_weight =30;};
case "cl3_aventador_lp7004_2tone4": {_weight =30;};
case "cl3_aventador_lp7004_2tone5": {_weight =30;};
case "cl3_aventador_lp7004_aqua": {_weight =30;};
case "cl3_aventador_lp7004_babyblue": {_weight =30;};
case "cl3_aventador_lp7004_babypink": {_weight =30;};
case "cl3_aventador_lp7004_black": {_weight =30;};
case "cl3_aventador_lp7004_blue": {_weight =30;};
case "cl3_aventador_lp7004_burgundy": {_weight =30;};
case "cl3_aventador_lp7004_camo": {_weight =30;};
case "cl3_aventador_lp7004_camo_urban": {_weight =30;};
case "cl3_aventador_lp7004_cardinal": {_weight =30;};
case "cl3_aventador_lp7004_dark_green": {_weight =30;};
case "cl3_aventador_lp7004_flame": {_weight =30;};
case "cl3_aventador_lp7004_flame1": {_weight =30;};
case "cl3_aventador_lp7004_flame2": {_weight =30;};
case "cl3_aventador_lp7004_gold": {_weight =30;};
case "cl3_aventador_lp7004_green": {_weight =30;};
case "cl3_aventador_lp7004_grey": {_weight =30;};
case "cl3_aventador_lp7004_lavender": {_weight =30;};
case "cl3_aventador_lp7004_light_blue": {_weight =30;};
case "cl3_aventador_lp7004_light_yellow": {_weight =30;};
case "cl3_aventador_lp7004_lime": {_weight =30;};
case "cl3_aventador_lp7004_marina_blue": {_weight =30;};
case "cl3_aventador_lp7004_navy_blue": {_weight =30;};
case "cl3_aventador_lp7004_orange": {_weight =30;};
case "cl3_aventador_lp7004_purple": {_weight =30;};
case "cl3_aventador_lp7004_red": {_weight =30;};
case "cl3_aventador_lp7004_sand": {_weight =30;};
case "cl3_aventador_lp7004_silver": {_weight =30;};
case "cl3_aventador_lp7004_violet": {_weight =30;};
case "cl3_aventador_lp7004_white": {_weight =30;};
case "cl3_aventador_lp7004_yellow": {_weight =30;};

case "cl3_carrera_gt": {_weight =30;};
case "cl3_carrera_gt_aqua": {_weight =30;};
case "cl3_carrera_gt_babyblue": {_weight =30;};
case "cl3_carrera_gt_babypink": {_weight =30;};
case "cl3_carrera_gt_black": {_weight =30;};
case "cl3_carrera_gt_blue": {_weight =30;};
case "cl3_carrera_gt_burgundy": {_weight =30;};
case "cl3_carrera_gt_camo": {_weight =30;};
case "cl3_carrera_gt_camo_urban": {_weight =30;};
case "cl3_carrera_gt_cardinal": {_weight =30;};
case "cl3_carrera_gt_dark_green": {_weight =30;};
case "cl3_carrera_gt_gold": {_weight =30;};
case "cl3_carrera_gt_green": {_weight =30;};
case "cl3_carrera_gt_grey": {_weight =30;};
case "cl3_carrera_gt_lavender": {_weight =30;};
case "cl3_carrera_gt_light_blue": {_weight =30;};
case "cl3_carrera_gt_light_yellow": {_weight =30;};
case "cl3_carrera_gt_lime": {_weight =30;};
case "cl3_carrera_gt_marina_blue": {_weight =30;};
case "cl3_carrera_gt_navy_blue": {_weight =30;};
case "cl3_carrera_gt_orange": {_weight =30;};
case "cl3_carrera_gt_purple": {_weight =30;};
case "cl3_carrera_gt_red": {_weight =30;};
case "cl3_carrera_gt_sand": {_weight =30;};
case "cl3_carrera_gt_silver": {_weight =30;};
case "cl3_carrera_gt_violet": {_weight =30;};
case "cl3_carrera_gt_white": {_weight =30;};
case "cl3_carrera_gt_yellow": {_weight =30;};

case "cl3_dbs_volante": {_weight =40;};
case "cl3_dbs_volante_aqua": {_weight =40;};
case "cl3_dbs_volante_babyblue": {_weight =40;};
case "cl3_dbs_volante_babypink": {_weight =40;};
case "cl3_dbs_volante_black": {_weight =40;};
case "cl3_dbs_volante_blue": {_weight =40;};
case "cl3_dbs_volante_burgundy": {_weight =40;};
case "cl3_dbs_volante_camo": {_weight =40;};
case "cl3_dbs_volante_camo_urban": {_weight =40;};
case "cl3_dbs_volante_cardinal": {_weight =40;};
case "cl3_dbs_volante_dark_green": {_weight =40;};
case "cl3_dbs_volante_flame": {_weight =40;};
case "cl3_dbs_volante_flame1": {_weight =40;};
case "cl3_dbs_volante_flame2": {_weight =40;};
case "cl3_dbs_volante_gold": {_weight =40;};
case "cl3_dbs_volante_green": {_weight =40;};
case "cl3_dbs_volante_grey": {_weight =40;};
case "cl3_dbs_volante_lavender": {_weight =40;};
case "cl3_dbs_volante_light_blue": {_weight =40;};
case "cl3_dbs_volante_light_yellow": {_weight =40;};
case "cl3_dbs_volante_lime": {_weight =40;};
case "cl3_dbs_volante_marina_blue": {_weight =40;};
case "cl3_dbs_volante_navy_blue": {_weight =40;};
case "cl3_dbs_volante_orange": {_weight =40;};
case "cl3_dbs_volante_purple": {_weight =40;};
case "cl3_dbs_volante_red": {_weight =40;};
case "cl3_dbs_volante_sand": {_weight =40;};
case "cl3_dbs_volante_silver": {_weight =40;};
case "cl3_dbs_volante_violet": {_weight =40;};
case "cl3_dbs_volante_white": {_weight =40;};
case "cl3_dbs_volante_yellow": {_weight =40;};

case "cl3_impreza_rally": {_weight =60;};
case "cl3_impreza_rally_aqua": {_weight =60;};
case "cl3_impreza_rally_babyblue": {_weight =60;};
case "cl3_impreza_rally_babypink": {_weight =60;};
case "cl3_impreza_rally_black": {_weight =60;};
case "cl3_impreza_rally_blue": {_weight =60;};
case "cl3_impreza_rally_burgundy": {_weight =60;};
case "cl3_impreza_rally_camo": {_weight =60;};
case "cl3_impreza_rally_camo_urban": {_weight =60;};
case "cl3_impreza_rally_cardinal": {_weight =60;};
case "cl3_impreza_rally_dark_green": {_weight =60;};
case "cl3_impreza_rally_flame": {_weight =60;};
case "cl3_impreza_rally_flame1": {_weight =60;};
case "cl3_impreza_rally_flame2": {_weight =60;};
case "cl3_impreza_rally_gold": {_weight =60;};
case "cl3_impreza_rally_green": {_weight =60;};
case "cl3_impreza_rally_grey": {_weight =60;};
case "cl3_impreza_rally_lavender": {_weight =60;};
case "cl3_impreza_rally_light_blue": {_weight =60;};
case "cl3_impreza_rally_light_yellow": {_weight =60;};
case "cl3_impreza_rally_lime": {_weight =60;};
case "cl3_impreza_rally_marina_blue": {_weight =60;};
case "cl3_impreza_rally_navy_blue": {_weight =60;};
case "cl3_impreza_rally_orange": {_weight =60;};
case "cl3_impreza_rally_purple": {_weight =60;};
case "cl3_impreza_rally_red": {_weight =60;};
case "cl3_impreza_rally_sand": {_weight =60;};
case "cl3_impreza_rally_silver": {_weight =60;};
case "cl3_impreza_rally_violet": {_weight =60;};
case "cl3_impreza_rally_white": {_weight =60;};
case "cl3_impreza_rally_yellow": {_weight =60;};

case "cl3_impreza_road": {_weight =60;};
case "cl3_impreza_road_aqua": {_weight =60;};
case "cl3_impreza_road_babyblue": {_weight =60;};
case "cl3_impreza_road_babypink": {_weight =60;};
case "cl3_impreza_road_black": {_weight =60;};
case "cl3_impreza_road_blue": {_weight =60;};
case "cl3_impreza_road_burgundy": {_weight =60;};
case "cl3_impreza_road_camo": {_weight =60;};
case "cl3_impreza_road_camo_urban": {_weight =60;};
case "cl3_impreza_road_cardinal": {_weight =60;};
case "cl3_impreza_road_dark_green": {_weight =60;};
case "cl3_impreza_road_flame": {_weight =60;};
case "cl3_impreza_road_flame1": {_weight =60;};
case "cl3_impreza_road_flame2": {_weight =60;};
case "cl3_impreza_road_gold": {_weight =60;};
case "cl3_impreza_road_green": {_weight =60;};
case "cl3_impreza_road_grey": {_weight =60;};
case "cl3_impreza_road_lavender": {_weight =60;};
case "cl3_impreza_road_light_blue": {_weight =60;};
case "cl3_impreza_road_light_yellow": {_weight =60;};
case "cl3_impreza_road_lime": {_weight =60;};
case "cl3_impreza_road_livery1": {_weight =60;};
case "cl3_impreza_road_livery2": {_weight =60;};
case "cl3_impreza_road_livery3": {_weight =60;};
case "cl3_impreza_road_livery4": {_weight =60;};
case "cl3_impreza_road_livery5": {_weight =60;};
case "cl3_impreza_road_marina_blue": {_weight =60;};
case "cl3_impreza_road_navy_blue": {_weight =60;};
case "cl3_impreza_road_orange": {_weight =60;};
case "cl3_impreza_road_purple": {_weight =60;};
case "cl3_impreza_road_red": {_weight =60;};
case "cl3_impreza_road_sand": {_weight =60;};
case "cl3_impreza_road_silver": {_weight =60;};
case "cl3_impreza_road_violet": {_weight =60;};
case "cl3_impreza_road_white": {_weight =60;};
case "cl3_impreza_road_yellow": {_weight =60;};

case "cl3_insignia": {_weight =80;};
case "cl3_insignia_aqua": {_weight =80;};
case "cl3_insignia_babyblue": {_weight =80;};
case "cl3_insignia_babypink": {_weight =80;};
case "cl3_insignia_black": {_weight =80;};
case "cl3_insignia_blue": {_weight =80;};
case "cl3_insignia_burgundy": {_weight =80;};
case "cl3_insignia_camo": {_weight =80;};
case "cl3_insignia_camo_urban": {_weight =80;};
case "cl3_insignia_cardinal": {_weight =80;};
case "cl3_insignia_dark_green": {_weight =80;};
case "cl3_insignia_gold": {_weight =80;};
case "cl3_insignia_green": {_weight =80;};
case "cl3_insignia_grey": {_weight =80;};
case "cl3_insignia_lavender": {_weight =80;};
case "cl3_insignia_light_blue": {_weight =80;};
case "cl3_insignia_light_yellow": {_weight =80;};
case "cl3_insignia_lime": {_weight =80;};
case "cl3_insignia_marina_blue": {_weight =80;};
case "cl3_insignia_navy_blue": {_weight =80;};
case "cl3_insignia_orange": {_weight =80;};
case "cl3_insignia_purple": {_weight =80;};
case "cl3_insignia_red": {_weight =80;};
case "cl3_insignia_sand": {_weight =80;};
case "cl3_insignia_silver": {_weight =80;};
case "cl3_insignia_violet": {_weight =80;};
case "cl3_insignia_white": {_weight =80;};
case "cl3_insignia_yellow": {_weight =80;};

case "cl3_lamborghini_gt1": {_weight =30;};
case "cl3_lamborghini_gt1_2tone1": {_weight =30;};
case "cl3_lamborghini_gt1_2tone2": {_weight =30;};
case "cl3_lamborghini_gt1_2tone3": {_weight =30;};
case "cl3_lamborghini_gt1_2tone4": {_weight =30;};
case "cl3_lamborghini_gt1_2tone5": {_weight =30;};
case "cl3_lamborghini_gt1_aqua": {_weight =30;};
case "cl3_lamborghini_gt1_babyblue": {_weight =30;};
case "cl3_lamborghini_gt1_babypink": {_weight =30;};
case "cl3_lamborghini_gt1_black": {_weight =30;};
case "cl3_lamborghini_gt1_blue": {_weight =30;};
case "cl3_lamborghini_gt1_burgundy": {_weight =30;};
case "cl3_lamborghini_gt1_camo": {_weight =30;};
case "cl3_lamborghini_gt1_camo_urban": {_weight =30;};
case "cl3_lamborghini_gt1_cardinal": {_weight =30;};
case "cl3_lamborghini_gt1_dark_green": {_weight =30;};
case "cl3_lamborghini_gt1_flame": {_weight =30;};
case "cl3_lamborghini_gt1_flame1": {_weight =30;};
case "cl3_lamborghini_gt1_flame2": {_weight =30;};
case "cl3_lamborghini_gt1_gold": {_weight =30;};
case "cl3_lamborghini_gt1_green": {_weight =30;};
case "cl3_lamborghini_gt1_grey": {_weight =30;};
case "cl3_lamborghini_gt1_lavender": {_weight =30;};
case "cl3_lamborghini_gt1_light_blue": {_weight =30;};
case "cl3_lamborghini_gt1_light_yellow": {_weight =30;};
case "cl3_lamborghini_gt1_lime": {_weight =30;};
case "cl3_lamborghini_gt1_marina_blue": {_weight =30;};
case "cl3_lamborghini_gt1_navy_blue": {_weight =30;};
case "cl3_lamborghini_gt1_orange": {_weight =30;};
case "cl3_lamborghini_gt1_purple": {_weight =30;};
case "cl3_lamborghini_gt1_red": {_weight =30;};
case "cl3_lamborghini_gt1_sand": {_weight =30;};
case "cl3_lamborghini_gt1_silver": {_weight =30;};
case "cl3_lamborghini_gt1_violet": {_weight =30;};
case "cl3_lamborghini_gt1_white": {_weight =30;};
case "cl3_lamborghini_gt1_yellow": {_weight =30;};

case "cl3_murcielago": {_weight =30;};
case "cl3_murcielago_2tone1": {_weight =30;};
case "cl3_murcielago_2tone2": {_weight =30;};
case "cl3_murcielago_2tone3": {_weight =30;};
case "cl3_murcielago_2tone4": {_weight =30;};
case "cl3_murcielago_2tone5": {_weight =30;};
case "cl3_murcielago_aqua": {_weight =30;};
case "cl3_murcielago_babyblue": {_weight =30;};
case "cl3_murcielago_babypink": {_weight =30;};
case "cl3_murcielago_black": {_weight =30;};
case "cl3_murcielago_blue": {_weight =30;};
case "cl3_murcielago_burgundy": {_weight =30;};
case "cl3_murcielago_camo": {_weight =30;};
case "cl3_murcielago_camo_urban": {_weight =30;};
case "cl3_murcielago_cardinal": {_weight =30;};
case "cl3_murcielago_dark_green": {_weight =30;};
case "cl3_murcielago_flame": {_weight =30;};
case "cl3_murcielago_flame1": {_weight =30;};
case "cl3_murcielago_flame2": {_weight =30;};
case "cl3_murcielago_gold": {_weight =30;};
case "cl3_murcielago_green": {_weight =30;};
case "cl3_murcielago_grey": {_weight =30;};
case "cl3_murcielago_lavender": {_weight =30;};
case "cl3_murcielago_light_blue": {_weight =30;};
case "cl3_murcielago_light_yellow": {_weight =30;};
case "cl3_murcielago_lime": {_weight =30;};
case "cl3_murcielago_marina_blue": {_weight =30;};
case "cl3_murcielago_navy_blue": {_weight =30;};
case "cl3_murcielago_orange": {_weight =30;};
case "cl3_murcielago_purple": {_weight =30;};
case "cl3_murcielago_red": {_weight =30;};
case "cl3_murcielago_sand": {_weight =30;};
case "cl3_murcielago_silver": {_weight =30;};
case "cl3_murcielago_violet": {_weight =30;};
case "cl3_murcielago_white": {_weight =30;};
case "cl3_murcielago_yellow": {_weight =30;};

case "cl3_r8_spyder": {_weight =30;};
case "cl3_r8_spyder_2tone1": {_weight =30;};
case "cl3_r8_spyder_2tone2": {_weight =30;};
case "cl3_r8_spyder_2tone3": {_weight =30;};
case "cl3_r8_spyder_2tone4": {_weight =30;};
case "cl3_r8_spyder_2tone5": {_weight =30;};
case "cl3_r8_spyder_aqua": {_weight =30;};
case "cl3_r8_spyder_babyblue": {_weight =30;};
case "cl3_r8_spyder_babypink": {_weight =30;};
case "cl3_r8_spyder_black": {_weight =30;};
case "cl3_r8_spyder_blue": {_weight =30;};
case "cl3_r8_spyder_burgundy": {_weight =30;};
case "cl3_r8_spyder_camo": {_weight =30;};
case "cl3_r8_spyder_camo_urban": {_weight =30;};
case "cl3_r8_spyder_cardinal": {_weight =30;};
case "cl3_r8_spyder_dark_green": {_weight =30;};
case "cl3_r8_spyder_flame": {_weight =30;};
case "cl3_r8_spyder_flame1": {_weight =30;};
case "cl3_r8_spyder_flame2": {_weight =30;};
case "cl3_r8_spyder_gold": {_weight =30;};
case "cl3_r8_spyder_green": {_weight =30;};
case "cl3_r8_spyder_grey": {_weight =30;};
case "cl3_r8_spyder_lavender": {_weight =30;};
case "cl3_r8_spyder_light_blue": {_weight =30;};
case "cl3_r8_spyder_light_yellow": {_weight =30;};
case "cl3_r8_spyder_lime": {_weight =30;};
case "cl3_r8_spyder_marina_blue": {_weight =30;};
case "cl3_r8_spyder_navy_blue": {_weight =30;};
case "cl3_r8_spyder_orange": {_weight =30;};
case "cl3_r8_spyder_purple": {_weight =30;};
case "cl3_r8_spyder_red": {_weight =30;};
case "cl3_r8_spyder_sand": {_weight =30;};
case "cl3_r8_spyder_silver": {_weight =30;};
case "cl3_r8_spyder_violet": {_weight =30;};
case "cl3_r8_spyder_white": {_weight =30;};
case "cl3_r8_spyder_yellow": {_weight =30;};

case "cl3_reventon": {_weight =30;};
case "cl3_reventon_2tone1": {_weight =30;};
case "cl3_reventon_2tone2": {_weight =30;};
case "cl3_reventon_2tone3": {_weight =30;};
case "cl3_reventon_2tone4": {_weight =30;};
case "cl3_reventon_2tone5": {_weight =30;};
case "cl3_reventon_aqua": {_weight =30;};
case "cl3_reventon_babyblue": {_weight =30;};
case "cl3_reventon_babypink": {_weight =30;};
case "cl3_reventon_black": {_weight =30;};
case "cl3_reventon_blue": {_weight =30;};
case "cl3_reventon_burgundy": {_weight =30;};
case "cl3_reventon_camo": {_weight =30;};
case "cl3_reventon_camo_urban": {_weight =30;};
case "cl3_reventon_cardinal": {_weight =30;};
case "cl3_reventon_clpd": {_weight =30;};
case "cl3_reventon_clpd_traf": {_weight =30;};
case "cl3_reventon_dark_green": {_weight =30;};
case "cl3_reventon_flame": {_weight =30;};
case "cl3_reventon_flame1": {_weight =30;};
case "cl3_reventon_flame2": {_weight =30;};
case "cl3_reventon_gold": {_weight =30;};
case "cl3_reventon_green": {_weight =30;};
case "cl3_reventon_grey": {_weight =30;};
case "cl3_reventon_lavender": {_weight =30;};
case "cl3_reventon_light_blue": {_weight =30;};
case "cl3_reventon_light_yellow": {_weight =30;};
case "cl3_reventon_lime": {_weight =30;};
case "cl3_reventon_marina_blue": {_weight =30;};
case "cl3_reventon_navy_blue": {_weight =30;};
case "cl3_reventon_orange": {_weight =30;};
case "cl3_reventon_purple": {_weight =30;};
case "cl3_reventon_red": {_weight =30;};
case "cl3_reventon_sand": {_weight =30;};
case "cl3_reventon_silver": {_weight =30;};
case "cl3_reventon_violet": {_weight =30;};
case "cl3_reventon_white": {_weight =30;};
case "cl3_reventon_yellow": {_weight =30;};

case "cl3_veyron": {_weight =30;};
case "cl3_veyron_black": {_weight =30;};
case "cl3_veyron_blk_wht": {_weight =30;};
case "cl3_veyron_brn_blk": {_weight =30;};
case "cl3_veyron_lblue_dblue": {_weight =30;};
case "cl3_veyron_lblue_lblue": {_weight =30;};
case "cl3_veyron_red_red": {_weight =30;};
case "cl3_veyron_wht_blu": {_weight =30;};
case "cl3_veyron_wht_lwht": {_weight =30;};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Motos
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_enduro": {_weight =10;};
case "cl3_enduro_aqua": {_weight =10;};
case "cl3_enduro_babyblue": {_weight =10;};
case "cl3_enduro_babypink": {_weight =10;};
case "cl3_enduro_black": {_weight =10;};
case "cl3_enduro_blue": {_weight =10;};
case "cl3_enduro_burgundy": {_weight =10;};
case "cl3_enduro_camo": {_weight =10;};
case "cl3_enduro_camo_urban": {_weight =10;};
case "cl3_enduro_cardinal": {_weight =10;};
case "cl3_enduro_dark_green": {_weight =10;};
case "cl3_enduro_emt": {_weight =10;};
case "cl3_enduro_flame": {_weight =10;};
case "cl3_enduro_flame1": {_weight =10;};
case "cl3_enduro_flame2": {_weight =10;};
case "cl3_enduro_gold": {_weight =10;};
case "cl3_enduro_green": {_weight =10;};
case "cl3_enduro_grey": {_weight =10;};
case "cl3_enduro_lavender": {_weight =10;};
case "cl3_enduro_light_blue": {_weight =10;};
case "cl3_enduro_light_yellow": {_weight =10;};
case "cl3_enduro_lime": {_weight =10;};
case "cl3_enduro_marina_blue": {_weight =10;};
case "cl3_enduro_navy_blue": {_weight =10;};
case "cl3_enduro_orange": {_weight =10;};
case "cl3_enduro_police": {_weight =10;};
case "cl3_enduro_purple": {_weight =10;};
case "cl3_enduro_red": {_weight =10;};
case "cl3_enduro_sand": {_weight =10;};
case "cl3_enduro_silver": {_weight =10;};
case "cl3_enduro_violet": {_weight =10;};
case "cl3_enduro_white": {_weight =10;};
case "cl3_enduro_yellow": {_weight =10;};

case "cl3_xr_1000": {_weight =20;};
case "cl3_xr_1000_aqua": {_weight =20;};
case "cl3_xr_1000_babyblue": {_weight =20;};
case "cl3_xr_1000_babypink": {_weight =20;};
case "cl3_xr_1000_black": {_weight =20;};
case "cl3_xr_1000_blue": {_weight =20;};
case "cl3_xr_1000_burgundy": {_weight =20;};
case "cl3_xr_1000_camo": {_weight =20;};
case "cl3_xr_1000_camo_urban": {_weight =20;};
case "cl3_xr_1000_cardinal": {_weight =20;};
case "cl3_xr_1000_dark_green": {_weight =20;};
case "cl3_xr_1000_emt": {_weight =20;};
case "cl3_xr_1000_flame": {_weight =20;};
case "cl3_xr_1000_flame1": {_weight =20;};
case "cl3_xr_1000_flame2": {_weight =20;};
case "cl3_xr_1000_gold": {_weight =20;};
case "cl3_xr_1000_green": {_weight =20;};
case "cl3_xr_1000_grey": {_weight =20;};
case "cl3_xr_1000_lavender": {_weight =20;};
case "cl3_xr_1000_light_blue": {_weight =20;};
case "cl3_xr_1000_light_yellow": {_weight =20;};
case "cl3_xr_1000_lime": {_weight =20;};
case "cl3_xr_1000_marina_blue": {_weight =20;};
case "cl3_xr_1000_navy_blue": {_weight =20;};
case "cl3_xr_1000_orange": {_weight =20;};
case "cl3_xr_1000_police": {_weight =20;};
case "cl3_xr_1000_purple": {_weight =20;};
case "cl3_xr_1000_red": {_weight =20;};
case "cl3_xr_1000_sand": {_weight =20;};
case "cl3_xr_1000_silver": {_weight =20;};
case "cl3_xr_1000_violet": {_weight =20;};
case "cl3_xr_1000_white": {_weight =20;};

case "cl3_chopper": {_weight =10;};
case "cl3_chopper_blue": {_weight =10;};
case "cl3_chopper_gold": {_weight =10;};
case "cl3_chopper_green": {_weight =10;};
case "cl3_chopper_red": {_weight =10;};
case "cl3_chopper_silver": {_weight =10;};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//AVIATION
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_GNT_C185_fuse1": {_weight =110;};
case "cl3_GNT_C185_fuse2": {_weight =110;};
case "cl3_GNT_C185_fuse3": {_weight =110;};
case "cl3_GNT_C185_fuse4": {_weight =110;};
case "cl3_GNT_C185_fuse5": {_weight =110;};
case "cl3_GNT_C185_fuse6": {_weight =110;};
case "C130J_Cargo": {_weight =800;};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DEPANNEUR
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


case "cl3_f150repo_black": {_weight =95;};
case "cl3_f150repo_blue": {_weight =95;};
case "cl3_f150repo_gray": {_weight =95;};
case "cl3_f150repo_green": {_weight =95;};
case "cl3_f150repo_orange": {_weight =95;};
case "cl3_f150repo_red": {_weight =95;};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//BUS
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "CL3_bus_cl_black": {_weight =190;};
case "CL3_bus_cl_blue": {_weight =190;};
case "CL3_bus_cl_green": {_weight =190;};
case "CL3_bus_cl_grey": {_weight =190;};
case "CL3_bus_cl_jail": {_weight =190;};
case "CL3_bus_cl_purple": {_weight =190;};
case "CL3_bus_cl_yellow": {_weight =190;};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Rigolol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

case "cl3_batmobile": {_weight =50;};

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Trucks
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
case "cl3_mackr_del": {_weight = 900;}; //Mack R
case "cl3_mackr_del_american": {_weight =900;};
case "cl3_mackr_del_black": {_weight =900;};
case "cl3_mackr_del_black_gold": {_weight =900;};
case "cl3_mackr_del_black_white": {_weight =900;};
case "cl3_mackr_del_blue": {_weight =900;};
case "cl3_mackr_del_brown_camo": {_weight =900;};
case "cl3_mackr_del_forest_camo": {_weight =900;};
case "cl3_mackr_del_green_white": {_weight =900;};
case "cl3_mackr_del_multi_color": {_weight =900;};
case "cl3_mackr_del_optimus": {_weight =900;};
case "cl3_mackr_del_orange_white": {_weight =900;};
case "cl3_mackr_del_purple_white": {_weight =900;};
case "cl3_mackr_del_silver": {_weight =900;};



};
if(isNil "_used") then {_used = 0};

[_weight,_used];
