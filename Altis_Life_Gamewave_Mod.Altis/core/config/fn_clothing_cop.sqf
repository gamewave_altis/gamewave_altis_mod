#include <macro.h>
/*
        File: fn_clothing_cop.sqf
        Author: Bryan "Tonic" Boardwine
       
        Description:
        Master config file for Cop clothing store.
*/
private["_filter","_ret"];
_filter = [_this,0,0,[0]] call BIS_fnc_param;
//Classname, Custom Display name (use nil for Cfg->DisplayName, price
 
//Shop Title Name
ctrlSetText[3103,"Magasin de tenues Police"];
 
_ret = [];
switch (_filter) do
{
        //Uniforms
        case 0:
        {
                        _ret pushBack["alfr_polopants_police","Uniforme de Recrue",25];
                        _ret pushBack["U_B_Wetsuit","Combinaison de plongée",5000];
                        _ret pushBack["CL3_emt_uniform","Combinaison anti-radiations",350];
                        _ret pushBack["cl3_c_poloshirtpants_clpdtraffic_uniform","Uniforme police blanc",25];
                if(__GETC__(life_coplevel) >= 2) then //Brigadier +
                {
                        _ret pushBack["CL3_mpg_uniform","Tenue de police",350];
                        _ret pushBack["CL3_emtCG_uniform","Tenue de police bleue",350];
						_ret pushBack["CL3_cn_uniform","Tenue des narcotiques",350];
						_ret pushBack["CL3_traffic_uniform","Tenue d'intervention",350];
                        _ret pushBack["cl3_c_poloshirtpants_clpdcom_uniform","Uniforme de comémoration",350];
                       
                };
                if(__GETC__(life_coplevel) >= 3) then //Sergent-Chef / Adjudant +
                {
                _ret pushBack["Trixie_Ghillie_Uniform_01","Ghillie complète Bois1",550];
                _ret pushBack["Trixie_Ghillie_Uniform_02","Ghillie complète Bois1",550];
                _ret pushBack["Trixie_Ghillie_Uniform_03","Ghillie complète Sable",550];
                _ret pushBack["U_B_FullGhillie_ard","Ghillie complète Aride",550];
                _ret pushBack["U_B_FullGhillie_sard","Ghillie complète Semie-Aride",550];
                _ret pushBack["U_B_FullGhillie_lsh","Ghillie complète Luxuriante",550];
                };
                       
        };
        //Hats
        case 1:
        {
                if(__GETC__(life_coplevel) == 1) then //Recrue
                {
                        _ret pushBack["cl3_Headgear_policehat_USA","Casquette de recrue",10];
                };
                if(__GETC__(life_coplevel) >= 2) then //Brigadier +
                {
                        _ret pushBack["H_MilCap_blue","Kepi Bleu",120];
                        _ret pushBack["H_Cap_police","Casquette de police",120];
                        _ret pushBack["cl3_Headgear_policecap",nil,100];
                        _ret pushBack["cl3_Headgear_policeetu",nil,100];
                        _ret pushBack["cl3_Headgear_policehat",nil,100];
                        _ret pushBack["cl3_Headgear_policehat2",nil,100];
                        _ret pushBack["cl3_Headgear_policehat3",nil,100];
                        _ret pushBack["cl3_Headgear_policehat_proby",nil,10];
                        _ret pushBack["cl3_Headgear_policehat_traffic",nil,10];
                        _ret pushBack["cl3_Headgear_policehat_trafficProby",nil,10];
                        _ret pushBack["cl3_Headgear_policehat_UK",nil,10];
                        _ret pushBack["cl3_Headgear_motorbike_helmet_police","Casque Motard",10];
                };
                if(__GETC__(life_coplevel) >= 3) then //Adjudant +
                {
                        _ret pushBack["H_Beret_brn_SF","Béret d'Adjudant",100];
                };
                if(__GETC__(life_coplevel) >= 4) then //Major +
                {
                        _ret pushBack["H_Cap_marshal","Casquette instructeur",10];
                        _ret pushBack["H_Beret_brn_SF","Béret d'officier",100];
						_ret pushBack["H_ShemagOpen_tan","Shemag de B.A.C.",75];
                };
                if(__GETC__(life_coplevel) >= 6) then //Commandant +
                {
                        _ret pushBack["H_Beret_Colonel","Béret de colonel/commandant",100];
 
                };
               
        };
       
        //Glasses
        case 2:
        {
                        _ret pushBack["G_Diving",nil,75];
                        _ret pushBack["G_Shades_Black",nil,75];
                        _ret pushBack["G_Shades_Blue",nil,75];
                        _ret pushBack["G_Sport_Blackred",nil,75];
                        _ret pushBack["G_Sport_Checkered",nil,75];
                        _ret pushBack["G_Sport_Blackyellow",nil,75];
                        _ret pushBack["G_Sport_BlackWhite",nil,75];
                        _ret pushBack["G_Squares",nil,75];
                        _ret pushBack["G_Lowprofile",nil,75];
                        _ret pushBack["G_Combat",nil,75];
                if(__GETC__(life_coplevel) >= 2) then //Brigadier +
                {
                _ret pushBack["G_Aviator",nil,75];
                _ret pushBack["G_Balaclava_blk","Cagoule de police",75];
                };
				if(__GETC__(life_coplevel) >= 4) then
				{ //Major +
				_ret pushBack["G_Bandanna_aviator","Cagoule de B.A.C.",75];
				};
        };
       
        //Vest
        case 3:
        {      
                        _ret pushBack["V_RebreatherB","Respirateur de police",5000];
                        _ret pushBack["V_Rangemaster_belt",nil,800];
 
       
                if(__GETC__(life_coplevel) == 1) then //Recrue
                {
                        _ret pushBack["cl3_police_vest_yellow","Gilet Pare-Balles de Recrue",500];
                       
                };
                if(__GETC__(life_coplevel) >= 2) then //Brigadier +
                {
                        _ret pushBack["V_TacVestIR_blk","Gilet Pare-Balles Police",1500];
                        _ret pushBack["cl3_police_vest_yellow","Gilet de circulation",500];
                        _ret pushBack["cl3_police_vest_black","Gilet Pare-Balles noir/blanc",500];
                        _ret pushBack["V_PlateCarrier1_blk","Gilet Tactique léger noir",1500];
                       
                };
                if(__GETC__(life_coplevel) >= 3) then //Sergent-Chef / Adjudant +
                {
                        _ret pushBack["V_PlateCarrier2_rgr","Gilet Tactique léger vert",1500];
                        _ret pushBack["V_Chestrig_blk","Chestrig",1500];
                        ret pushBack["cl3_police_PlateCarrier_ETU","Gilet Tactique d'Adjudant/Sergent-Chef",4500];
                };
                if(__GETC__(life_coplevel) > 6) then //Commandant +
                {
                        _ret pushBack["V_PlateCarrierSpec_blk","Gilet Tactique moyen noir",5000];
                        _ret pushBack["V_PlateCarrierGL_blk","Gilet Tactique lourd noir",50000];
                };
        };
       
        //Backpacks
        case 4:
        {
                        _ret pushBack["cl3_police_tacticalbelt","Ceinture de police",800];
                if(__GETC__(life_coplevel) >= 2) then //Brigadier +
                {
                        _ret pushBack["cl3_police_tacticalbelt",nil,800];
                        _ret pushBack["B_Kitbag_cbr",nil,800];
                        _ret pushBack["B_mas_Kitbag_black",nil,800];
                        _ret pushBack["B_FieldPack_cbr",nil,800];
                        _ret pushBack["B_AssaultPack_cbr",nil,800];
                        _ret pushBack["B_Bergen_sgg",nil,800];
                        _ret pushBack["B_Carryall_cbr",nil,800];
                        _ret pushBack["B_Carryall_oucamo",nil,800];
                        _ret pushBack["B_FieldPack_blk",nil,800];
                        _ret pushBack["B_Bergen_blk",nil,800];
                        _ret pushBack["B_OutdoorPack_blk",nil,800]
                };
        };
};
 
_ret;