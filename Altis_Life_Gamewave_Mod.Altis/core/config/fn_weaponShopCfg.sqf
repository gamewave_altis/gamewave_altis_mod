	#include <macro.h>
/*
        File: fn_weaponShopCfg.sqf
        Author: Bryan "Tonic" Boardwine
       
        Description: Vendeurs d'armes.
        Modifié par Nonoxs (ce mec est trop cool) pour Gamewave.
*/
private["_shop","_return"];
_shop = [_this,0,"",[""]] call BIS_fnc_param;
if(_shop == "") exitWith {closeDialog 0}; //Bad shop type passed.
_return = [];
switch(_shop) do
{
////////////////////////////////////////////////////////////////////////////////////////////////
///POLICE///////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
        case "cop_Armes":
        {
                switch(true) do
                {
                        case (playerSide != west): {_return = "Interdit aux civils !"};
                       
                                                default
                                                {
                                                        _return =
                                                        ["Armurerie", //Recrue
                                                        [                                                            
                                                                ["cl3_fingerprintscanner","*Lecteur d'empreintes digital",10],
                                                                ["cl3_taserM26_Yellow","Pistolet Taser",2000],
                                                                ["cl3_taserm26mag_mpx","Munition Taser",50],
                                                                ["cl_pepperspray","Spray au poivre",2000],
                                                                ["cl_PepperMag","Recharge spray au poivre",50],
                                                                ["RH_g18","Glock 18 9mm",20000],
                                                                ["RH_33Rnd_9x19_g18","Chargeur Glock 33x9mm",80],
                                                                ["hlc_smg_mp510","MP5 10mm",5000],
                                                                ["hlc_30Rnd_10mm_B_MP5","Chargeur MP5 30x10mm",100],
                                                                ["SMG_01_F","Vector .45",5000],
                                                                ["30Rnd_45ACP_Mag_SMG_01","Chargeur Vector 30x.45",100],
                                                                ["R3F_Famas_F1_HG","Famas F1",5000],
                                                                ["R3F_25Rnd_556x45_FAMAS","Chargeur Famas F1 25x5.56",100],
                                                                ["arifle_mas_arx_l","ARX160 Noir 5.56mm",5000],
                                                                ["R3F_Famas_G2_HG","Famas G2 5.56mm",5000],
                                                                ["R3F_Famas_surb_HG","Famas C 5.56mm",5000],
                                                                ["R3F_HK416M_HG","HK 416 5.56mm",5000],
                                                                ["RH_M4A1_ris","M4 RIS",5000],
                                                                ["RH_M4sbr_b","M4 Vltor",5000],
                                                                ["30Rnd_556x45_Stanag","Chargeur STANAG 30x5.56",100]          
                                                        ]
                                                        ];
                                                        if(__GETC__(life_donator) > 1) then //Recrue donateur
                                                        {
                                                                _return select 1 pushBack ["arifle_mas_arx_l_gl","ARX160 Noir + EGLM",6000];
                                                                _return select 1 pushBack ["R3F_HK416M_HG_DES","HK 416 14.5 Camo",5000];
                                                                _return select 1 pushBack ["R3F_Famas_F1_HG_DES","Famas F1 Camo",5000];
                                                                _return select 1 pushBack ["R3F_Famas_G2_HG_DES","Famas G2 Camo",5000];
                                                                _return select 1 pushBack ["R3F_Famas_surb_HG_DES","Famas C Camo",5000];
                                                                _return select 1 pushBack ["1Rnd_Smoke_Grenade_shell","*Grenade M203 Lacrymogene",1200];
                                                                _return select 1 pushBack ["1Rnd_SmokeYellow_Grenade_shell","Grenade M203 Jaune",500];
                                                                _return select 1 pushBack ["1Rnd_SmokeRed_Grenade_shell","Grenade M203 Rouge",500];
                                                                _return select 1 pushBack ["1Rnd_SmokeGreen_Grenade_shell","Grenade M203 Vert",500];
                                                                _return select 1 pushBack ["1Rnd_SmokePurple_Grenade_shell","Grenade M203 Violet",500];
                                                                _return select 1 pushBack ["1Rnd_SmokeBlue_Grenade_shell","Grenade M203 Bleu",500];
                                                                _return select 1 pushBack ["1Rnd_SmokeOrange_Grenade_shell","Grenade M203 Orange",500];
                                                        };
                                                        if(__GETC__(life_coplevel) >= 2) then //Brigadier/sergent
                                                        {
                                                                _return select 1 pushBack["arifle_MXC_Black_F","MXC Noir 6.5 mm",5000];
                                                                _return select 1 pushBack["arifle_MX_Black_F","MX Noir",5000];
                                                                _return select 1 pushBack["arifle_MXM_Black_F","MXM Noir 6.5 mm",5000];
                                                                _return select 1 pushBack["30Rnd_65x39_caseless_mag","Chargeur MX 6.5mm",100];
                                                                _return select 1 pushBack["R3F_Minimi_HG","Minimi 5.56mm",10000];  
																_return select 1 pushBack["R3F_200Rnd_556x45_MINIMI","Chargeur Minimi 200x5.56mm",100];
																_return select 1 pushBack["arifle_MX_SW_Black_F","MX SW 6.5mm",10000];
																_return select 1 pushBack["100Rnd_65x39_caseless_mag","Chargeur MX SW 100x6.5mm",500];	
																_return select 1 pushBack["R3F_M4S90","Benelli M4",10000];
																_return select 1 pushBack["R3F_7Rnd_M4S90","Chargeur Benneli 7rnds",100];
                                                                if(__GETC__(life_donator) > 1) then //Brigadier donateur
                                                                {
                                                                _return select 1 pushBack ["arifle_MX_GL_Black_F","MXM GL Noir 6.5 mm",5000];
                                                                };
                                                        };
                                                        if(__GETC__(life_coplevel) >= 3) then //adjudant/chef
                                                        {                      
                                                                _return select 1 pushBack["srifle_DMR_04_F","ASP-1 Kir 12.7mm",50000];
                                                                _return select 1 pushBack["10Rnd_127x54_Mag","Chargeur ASP-1 Kir 10x12.7mm",2000];
																_return select 1 pushBack["srifle_DMR_03_F","Mk-I 7.62mm (DLC)",10000];
                                                                _return select 1 pushBack["20Rnd_762x51_Mag","Chargeur Mk-I 20x7.62mm",500];
                                                                _return select 1 pushBack["srifle_EBR_F", "Mk18 7.62mm",10000];
                                                                _return select 1 pushBack["20Rnd_762x51_Mag","Chargeur Mk18 20x7.62mm",500];
                                                                _return select 1 pushBack["LMG_Zafir_F","Zaphir 7.62mm",10000];
                                                                _return select 1 pushBack["150Rnd_762x54_Box","Boite 7.62mm Zaphir",500];
                                                                _return select 1 pushBack["srifle_DMR_02_F","Mar10 .338",40000];
                                                                _return select 1 pushBack["10Rnd_338_Mag","Chargeur Mar10 10x.338",2000]; 
																	if(__GETC__(life_donator) > 1) then
																	{
																	_return select 1 pushBack ["srifle_DMR_03_woodland_F","Mk-I Boisé 7.62mm (DLC)",5000];
																	};	
                                                        };
                                                        if(__GETC__(life_coplevel) >= 4) then //major
                                                        {
                                                                _return select 1 pushBack["srifle_LRR_SOS_F","M320 .408",50000];
                                                                _return select 1 pushBack["7Rnd_408_Mag","Chargeur M320 7x.408",100];
                                                                _return select 1 pushBack["srifle_GM6_SOS_F","GM6 Lynx .50",70000];
                                                                _return select 1 pushBack["5Rnd_127x108_Mag","Chargeur Lynx 5x.50",100];
                                                                _return select 1 pushBack["MMG_02_black_F","SPMG .338",50000];                                                            
                                                                _return select 1 pushBack["130Rnd_338_Mag","Chargeur SPMG 130x.338",2000]; 
                                                                _return select 1 pushBack["srifle_DMR_05_blk_F","Cyrus 9.3mm",50000];                                                           
                                                                _return select 1 pushBack["10Rnd_93x64_DMR_05_Mag","Chargeur 10x9.3mm"];                                                              
                                                            if(__GETC__(life_donator) > 1) then //Major																													
																{
                                                                _return select 1 pushBack ["srifle_LRR_camo_F","M320 .408 Camo ss optique",40000];
                                                                _return select 1 pushBack ["srifle_GM6_camo_F","GM6 Lynx .50 Camo ss optique",50000];
                                                                _return select 1 pushBack ["srifle_DMR_02_camo_F","Mar10 Camo .338",40000];
                                                                _return select 1 pushBack ["MMG_02_camo_F","SPMG camo HEX",50000];                                                              

															   };    
                                                        };
                                                        if(__GETC__(life_coplevel) >= 5) then //lieut
                                                        {
                                                                 _return select 1 pushBack["MMG_01_tan_F","Navid 9.3mm",50000];
                                                                 _return select 1 pushBack["150Rnd_93x64_Mag","Chargeur Navid 150x9.3mm",2000];                                                                                                                     
													if(__GETC__(life_donator) > 1) then //lieut donna
															{

															};                                                               
                                                        };                            
                        };
                };
        };
 
        case "cop_equip_Armes":
        {
                switch(true) do
                {
                        case (playerSide != west): {_return = "Interdit aux civils !"};
                                                default
                                                {
                                                        _return =
                                                        ["Accessoires Armes",
                                                                        [                                                                      
                                                                        ["optic_Aco_smg",nil,2000],
                                                                        ["optic_Arco",nil,2000],
                                                                        ["muzzle_snds_M","Silencieux 5.56 mm",350],
                                                                        ["R3F_SILENCIEUX_HK416","Silencieux HK416 noir",350],
                                                                        ["R3F_SILENCIEUX_HK416_DES","Silencieux HK416 Camo",350],
                                                                        ["FHQ_optic_AIM","Aimpoint Comp",2000],
                                                                        ["FHQ_optic_AIM_tan","Aimpoint Comp Camo",2000],
                                                                        ["optic_Holosight",nil,2000],
                                                                        ["optic_MRCO",nil,2000],
                                                                        ["optic_Hamr",nil,2000],
                                                                        ["optic_DMS",nil,2000],
                                                                        ["optic_NVS",nil,2000],
                                                                        ["optic_SOS",nil,2000],
                                                                        ["optic_LRPS",nil,2000],
                                                                        ["RH_x300","Surefire X300/pistolet",2000],
                                                                        ["acc_pointer_IR",nil,2000],
                                                                        ["FHQ_acc_ANPEQ15","AN/PEQ-15",2000],
                                                                        ["FHQ_acc_ANPEQ15_black","AN/PEQ-15 BLACK",2000],
                                                                        ["acc_flashlight",nil,2000]
                                                                        ]
                                                        ];                                                  
                                                        if(__GETC__(life_coplevel) > 1) then
                                                        {                                              
                                                        _return select 1 pushBack["muzzle_snds_B","Silencieux 6.5 mm",400];
                                                        _return select 1 pushBack["bipod_01_F_blk","Bipied noir",2000];
                                                        _return select 1 pushBack["bipod_01_F_mtp","Bipied MC",2000];
                                                        _return select 1 pushBack["bipod_01_F_snd","Bipied Sand",2000];
                                                        _return select 1 pushBack["bipod_02_F_hex","Bipied Hex",2000];
                                                        _return select 1 pushBack["bipod_02_F_tan","Bipied Tan",2000];
                                                        _return select 1 pushBack["bipod_03_F_oli","Bipied Olive",2000];
                                                        _return select 1 pushBack["optic_AMS_base","AMS noir",2000];
                                                        _return select 1 pushBack["optic_AMS_khk","AMS Khaki",2000];
                                                        _return select 1 pushBack["optic_AMS_snd","AMS Sand",2000];
                                                        _return select 1 pushBack["optic_KHS_blk","Kahlia Noir",2000];
                                                        _return select 1 pushBack["optic_KHS_hex","Kahlia Hex",2000];
                                                        _return select 1 pushBack["optic_KHS_tan","Kahlia Tan",2000];
                                                        _return select 1 pushBack["optic_KHS_old","Kahlia old",2000]; 
                                                        };                                                          
                                                        if(__GETC__(life_coplevel) > 2) then
                                                        {                                              
                                                        _return select 1 pushBack["muzzle_snds_338_black","Silencieux .338",15000];
                                                        _return select 1 pushBack["muzzle_snds_93mmg_tan","Silencieux 9.3mm",25000];
                                                     
                                                        };    
 
                                       
                        };  
                };
        };
 
        case "cop_divers":
        {
                    switch(true) do
            {
                                case (playerSide != west): {_return = "Interdit aux civils !";};
                                default
                                {
                                                _return =
                                                ["Divers",
													[
													["ItemMap",nil,50],
													["ItemGPS",nil,1250],
													["ItemCompass",nil,80],
													["ItemWatch",nil,80],
													["Binocular",nil,150],
													["NVGoggles_OPFOR","JVN noires",2500],
													["Rangefinder",nil,3400],
													["Chemlight_red",nil,300],
													["Chemlight_yellow",nil,300],
													["Chemlight_green",nil,300],
													["Chemlight_blue",nil,300],
													["ToolKit",nil,2500],
													["H_PilotHelmetFighter_B","Masque a Gaz",500],
													["SmokeShell","*Grenade Lacrymogene",1000],
													["SmokeShellYellow","Fumigène - Couverture",1000],
													["cl3_coveralls_prisoner_uniform","Uniforme de détenu",10],
													["G_Balaclava_combat","*Cagoule aveuglante pour détenu",10],
													["ItemRadio","Téléphone", 2000]
													]
                                                ];
                                                if(__GETC__(life_donator) > 1) then
                                                {              
                                                //_return select 1 pushBack ["EXEMPLE","EXEMPLE",100];
                                                };    
                                                if(__GETC__(life_coplevel) > 1) then
                                                {
                                                 _return select 1 pushBack ["MiniGrenade","Flashbang",700];
                                                 _return select 1 pushBack ["SmokeShellRed","Fumigène - Ennemi",300];
                                                 _return select 1 pushBack ["SmokeShellGreen","Fumigène - Allié",300];
                                                 _return select 1 pushBack ["SmokeShellBlue","Fumigène - Extraction",300];
                                                        if(__GETC__(life_donator) > 1) then
                                                        {              
                                                        //_return select 1 pushBack ["EXEMPLE","EXEMPLE",100];
                                                        };    
                                                };
                                };
                        };
                };
////////////////////////////////////////////////////////////////////////////////////////////////
///REBELLE//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////              
 
        case "rebel":
        {
                        switch(true) do
                        {
                        case (playerSide != civilian): {_return = "Tu es Policier, tu n'as rien a faire avec moi"};
                        case (!license_civ_rebel): {_return = "Tu n'as pas d'entrainement Rebelle!"};
                        case (license_civ_rebel):
                                {
                                _return =
                                ["Magasin d'armes Rebelles",
                                                [
													["RH_g19t","Glock 19 Tan 9mm",4000],
													["RH_33Rnd_9x19_g18","Chargeur Glock 33x9mm",500],
													["RH_p226","Sig P226 9mm",5000],
													["RH_15Rnd_9x19_SIG","Chargeur P226 15x9mm",500],
													["RH_python","Colt Python",6000],                                                                      
													["RH_6Rnd_357_Mag","6 balles Python .357",200],                                                                    
													["RH_bull","Taurus Raging Bull",8200],                                                                    
													["RH_bullb","Taurus Raging Bull Noir",8900],                                                                    
													["RH_6Rnd_454_Mag","6 balles Taurus RH .454",200],
													["hgun_Pistol_heavy_01_F","4-five .45",10000],
													["11Rnd_45ACP_Mag","Chargeur 11x.45",500],
													["RH_muzi","Micro Uzi",20000],
													["RH_30Rnd_9x19_UZI","Chargeur Uzi 33x9mm",500],                                                                                                                              
													["arifle_SDAR_F","SDAR",25000],
													["20Rnd_556x45_UW_mag","Chargeur SDAR UW 20x5.56mm",500],
													["arifle_mas_aks74u","AKS74U 5.45mm",40000],
													["arifle_mas_ak74","AK74 5.45mm",50000],
													["arifle_mas_ak12_sf","AK12 SF 5.45mm",55000],
													["arifle_mas_aks_74_sf","AK74 SF 5.45mm",55000],
													["30Rnd_mas_545x39_mag","Chargeur AK74/AK12 30x5.45mm",500],
													["arifle_mas_l119","L119A1 5.56mm",55000],                                                                
													["arifle_mas_l119c","L119 CQB 5.56mm",55000],
													["30Rnd_mas_556x45_Stanag","Chargeur L119A1 30x5.56mm",500],
													["hlc_rifle_g3ka4","G3-A4 5.56mm",55000],
													["hlc_20rnd_762x51_b_G3","Chargeur G3 20x5.56mm",500],
													["arifle_Katiba_F","Katiba 6.5 mm",60000],
													["arifle_Katiba_C_F","Katiba Carbine 6.5 mm",60000],
													["30Rnd_65x39_caseless_green",nil,200],  
													["R3F_Minimi","Minimi 5.56mm",75000],
													["R3F_200Rnd_556x45_MINIMI","Chargeur Minimi 200x5.56mm",500],
													["LMG_mas_rpk_F","RPK 5.45mm",75000],
													["100Rnd_mas_545x39_mag","Chargeur RPK 100x5.45mm",500],                                                                                                                              
													["arifle_mas_mk17","MK17 #JimB 7.62",80000],
													["20Rnd_mas_762x51_Stanag","Chargeur mk17#JimB. 20x7.62mm",700],  
													["ov_arifle_a2sa58","SA-58 Camo 7.62mm",80000],
													["ov_30rnd_762x39_b_sa58","Chargeur SA58 30x7.62mm",500],
													["srifle_EBR_F", "Mk18 7.62mm", 80000],
													["srifle_DMR_03_tan_F","Mk-I 7.62mm Sable (DLC)",100000],
													["20Rnd_762x51_Mag","Chargeur Mk-I/MK18 20x7.62mm",500],
													["srifle_DMR_04_Tan_F","ASP-1 Kir 12.7 (DLC)",100000],
													["10Rnd_127x54_Mag","Chargeur ASP-1 Kir 10x12.7mm",500],
													["srifle_mas_svd","SVD Dragunov 7.62mm",115000],
													["10Rnd_mas_762x54_mag","Chargeur SVD 10x7.62mm",500],  
													["LMG_Mk200_F","MK200 6.5mm",125000],
													["200Rnd_65x39_cased_Box_Tracer","Chargeur mk200 Tracer 200x6.5mm",500],
													["200Rnd_65x39_cased_Box","Chargeur mk200 200x6.5mm",500],
													["srifle_mas_lrr","LRR AWM .338",150000],
													["10Rnd_mas_338_Stanag","Chargeur LRR 7x.338",2000],
													["srifle_DMR_02_sniper_F","MAR10 .338 (DLC)",185000],
													["10Rnd_338_Mag","Chargeur MAR10 10x.338",2000],
													["LMG_Zafir_F","Zaphir 7.62mm",290000],
													["150Rnd_762x54_Box","Chargeur Zaphir 150x7.62mm",500],
													["srifle_DMR_05_tan_f","Cyrus 9.3mm (DLC)",210000],
													["10Rnd_93x64_DMR_05_Mag","Chargeur Cyrus 10x9.3mm",2000],
													["MMG_02_sand_F","SPMG .338 (DLC)",325000],
													["130Rnd_338_Mag","Chargeur SPMG ",5000],                                                              
													["MMG_01_hex_F","Navid 9.3mm (DLC)",350000],
													["150Rnd_93x64_Mag","Chargeur Navid",5000],
													["DemoCharge_Remote_Mag","Pain de C4",320000],                                                                                                                          
                                                    ["SatchelCharge_Remote_Mag","Sacoche d'explosif ",550000],
													["muzzle_snds_H",nil,2000],
													["muzzle_snds_H_MG","Silencieux Mk200",2000],
													["muzzle_snds_338_green","Silencieux .338 SPMG/MAR10",15000],
													["muzzle_snds_93mmg_tan","Silencieux 9.3mm Navid/Cyrus",25000],
													["optic_ACO_grn",nil,2000],
													["optic_Holosight",nil,2000],
													["optic_Arco",nil,2000],
													["optic_DMS",nil,2000],
													["optic_Hamr",nil,2000],
													["optic_MRCO",nil,2000],
													["optic_SOS",nil,2000],
													["optic_LRPS",nil,2000],
													["bipod_01_F_blk","Bipied noir",2000],
													["bipod_01_F_mtp","Bipied MC",2000],
													["bipod_01_F_snd","Bipied Sand",2000],
													["bipod_02_F_hex","Bipied Hex",2000],
													["bipod_02_F_tan","Bipied Tan",2000],
													["bipod_03_F_oli","Bipied Olive",2000],
													["FHQ_optic_AIM","Aimpoint Comp",2000],
													["optic_mas_kobra","Kobra",2000],                            
													["optic_mas_PSO_eo","PCO + Holo",2000],
													["optic_AMS_base","AMS noir",2000],
													["optic_AMS_khk","AMS Khaki",2000],
													["optic_AMS_snd","AMS Sand",2000],
													["optic_KHS_blk","Kahlia Noir",2000],
													["optic_KHS_hex","Kahlia Hex",2000],
													["optic_KHS_tan","Kahlia Tan",2000],
													["optic_KHS_old","Kahlia old",2000],
													["RH_x2","Insight X2",2000],
													["acc_flashlight",nil,2000],
													["Rangefinder",nil,5000]                                                          
                                                ]
                                ];
                                                if(__GETC__(life_donator) > 1) then
                                                {
													_return select 1 pushBack ["RH_Deagleg","Desert Eagle Or",250000];
													_return select 1 pushBack ["RH_7Rnd_50_AE","Chargeur Desert Eagle .50",1000];
													_return select 1 pushBack ["arifle_mas_aks74u_c","AKS74U 5.45mm camo",40000];
													_return select 1 pushBack ["arifle_mas_ak_74m_c","AK74 5.45mm camo",50000];
													_return select 1 pushBack ["30Rnd_mas_545x39_mag","Chargeur AK74/AK12 30x5.45mm",500];
													_return select 1 pushBack ["arifle_mas_l119_d","L119A1 Des 5.56mm",55000];
													_return select 1 pushBack ["arifle_mas_l119_v","L119A1 Wood 5.56mm",55000];
													_return select 1 pushBack ["arifle_mas_l119c_d","L119 CQB Des 5.56mm",55000];
													_return select 1 pushBack ["arifle_mas_l119c_v","L119 CQB Wood 5.56mm",55000];
													_return select 1 pushBack ["30Rnd_mas_556x45_Stanag","Chargeur L119A1 30x5.56mm",500];
													_return select 1 pushBack ["R3F_M4S90","Benelli M4",60000];
													_return select 1 pushBack ["R3F_7Rnd_M4S90","Chargeur Benelli 7rnds",100];
													_return select 1 pushBack ["arifle_mas_akms_c","AKMS 7.62mm camo",70000];
													_return select 1 pushBack ["30Rnd_mas_762x39_mag","Chargeur AKMS 30x7.62mm",500];
													_return select 1 pushBack ["arifle_mas_asval","AS VAL 7.62mm",70000];
													_return select 1 pushBack ["20Rnd_mas_9x39_mag","Chargeur AS VAL 20x7.62mm",500];
													_return select 1 pushBack ["srifle_DMR_03_multicam_F","Mk-I 7.62mm Camo (DLC)",100000];
													_return select 1 pushBack ["20Rnd_762x51_Mag","Chargeur Mk-I 20x7.62mm",500];
													_return select 1 pushBack ["srifle_DMR_02_camo_F","MAR10 Camo vert",185000];
													_return select 1 pushBack ["10Rnd_338_Mag","Chargeur MAR10 10x.338",2000],
													_return select 1 pushBack ["MMG_02_camo_F","SMPG Camo MTP",235000];
													_return select 1 pushBack ["130Rnd_338_Mag","Chargeur SPMG ",5000];
													_return select 1 pushBack ["srifle_DMR_05_hex_F","Cyrus Camo HEX",210000];
													_return select 1 pushBack ["10Rnd_93x64_DMR_05_Mag","Chargeur Cyrus 10x9.3mm",2000];
													_return select 1 pushBack ["srifle_mas_ksvk","KSVK .50",300000];
													_return select 1 pushBack ["srifle_mas_ksvk_c","KSVK .50 Camo",300000];
													_return select 1 pushBack ["5Rnd_mas_127x108_mag","Chargeur KSVK 5x.50",2000];
													_return select 1 pushBack ["optic_mas_PSO_nv_day","Viseur Snipeur KSVK",2000];
													_return select 1 pushBack ["FHQ_optic_AIM_tan","Aimpoint Comp Camo",2000];
													_return select 1 pushBack ["optic_mas_PSO_eo_c","PCO + Holo camo",2000];
													_return select 1 pushBack ["bipod_01_F_blk","Bipied noir",2000];
													_return select 1 pushBack ["bipod_01_F_mtp","Bipied MC",2000];
													_return select 1 pushBack ["bipod_01_F_snd","Bipied Sand",2000];
													_return select 1 pushBack ["bipod_02_F_hex","Bipied Hex",2000];
													_return select 1 pushBack ["bipod_02_F_tan","Bipied Tan",2000];
													_return select 1 pushBack ["bipod_03_F_oli","Bipied Olive",2000];
													_return select 1 pushBack ["optic_NVS",nil,2000];
													_return select 1 pushBack ["V_PlateCarrierIA1_dgtl",nil,10000];
													_return select 1 pushBack ["V_PlateCarrierIA2_dgtl",nil,20000];
													_return select 1 pushBack ["V_PlateCarrierIAGL_dgtl","Gilet Pare-Balles LOURD",35000];
													_return select 1 pushBack ["H_HelmetSpecO_blk",nil,70000];
													_return select 1 pushBack ["H_HelmetO_ocamo",nil,70000];
													_return select 1 pushBack ["H_Beret_ocamo",nil,10000];
													_return select 1 pushBack ["H_PilotHelmetFighter_I","Masque a Gaz",10000];
                                                };    
                                };
                        };
                };
     
////////////////////////////////////////////////////////////////////////////////////////////////
///REPAIRE DE GANG//////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////              
        case "gang":
        {
                switch(true) do
                {
                                        case (playerSide != civilian): {_return = "Tu es Policier, tu n'as rien a faire avec moi"};
                    case (license_civ_rebel):
                                                {
                                                        _return =
                                                        ["Magasin d'armes Rebelles",
                                                        [
                                                        ["hgun_Pistol_Signal_F","Pistolet de détresse",2000],
                                                        ["6Rnd_RedSignal_F","Fusée éclairante Rouge",50],                                                                                            
                                                        ["RH_p226","Sig P226 9mm",15000],
                                                        ["RH_15Rnd_9x19_SIG","Chargeur P226 15x9mm",500],
                                                        ["RH_muzi","Micro Uzi",20000],
                                                        ["RH_30Rnd_9x19_UZI","Chargeur Uzi 33x9mm",500],
                                                        ["arifle_mas_bizon","PP19 Bizon 9mm",60000],
                                                        ["64Rnd_mas_9x18_mag","Chargeur Bizon 64x9mm",500],
                                                        ["arifle_mas_aks74u","AKS74U 5.45mm",80000],
                                                        ["arifle_mas_ak74","AK74 5.45mm",80000],
                                                        ["arifle_mas_ak12_sf","AK12 SF 5.45mm",85000],
                                                        ["30Rnd_mas_545x39_mag","Chargeur AK74/AK12 30x5.45mm",500],
                                                        ["arifle_SDAR_F",nil,25000],
                                                        ["20Rnd_556x45_UW_mag","Chargeur SDAR UW 20x5.56mm",500],
                                                        ["hlc_rifle_g3ka4","G3-A4 5.56mm",60000],
                                                        ["hlc_20rnd_762x51_b_G3","Chargeur G3 20x5.56mm",400],
                                                        ["LMG_mas_rpk_F","RPK 5.45mm",125000],
                                                        ["100Rnd_mas_545x39_mag","Chargeur RPK 100x5.45mm",500],
                                                        ["arifle_mas_akm","AKM 7.62mm",100000],
                                                        ["30Rnd_mas_762x39_mag","Chargeur AKM 30x7.62mm",500],
                                                        ["srifle_mas_svd","SVD Dragunov 7.62mm",170000],
                                                        ["10Rnd_mas_762x54_mag","Chargeur SVD 10x7.62mm",500],
                                                        ["Trixie_FNFAL_Rail","FAL 7.62mm",100000],
                                                        ["Trixie_FNFAL_Mag","Chargeur FAL 20x7.62mm",500],
                                                        ["srifle_mas_lrr","LRR AWM .338",230000],
                                                        ["10Rnd_mas_338_Stanag","Chargeur LRRM 7x.338",2000],
                                                        ["optic_DMS",nil,2000],
                                                        ["optic_LRPS",nil,2000],
                                                        ["optic_MRCO",nil,2000],
                                                        ["optic_ACO_grn",nil,3500],
                                                        ["FHQ_optic_AIM","Aimpoint Comp",4000],
                                                        ["optic_mas_kobra","Kobra",4000],
                                                        ["optic_Holosight",nil,3600],
                                                        ["optic_SOS",nil,2000],
                                                        ["optic_Arco",nil,2000],
                                                        ["optic_Hamr",nil,3500],
                                                        ["optic_mas_PSO_eo","PCO + Holo",10000],
                                                        ["RH_x2","Insight X2",1000],
                                                        ["acc_flashlight",nil,1000],
                                                        ["Rangefinder",nil,8000],
                                                        ["V_Rangemaster_belt",nil,2500]
                                                        ]
                                                        ];
                                                        if(__GETC__(life_donator) > 1) then
                                                        {
                                                        _return select 1 pushBack ["RH_Deagleg","Desert Eagle Or",250000];
                                                        _return select 1 pushBack ["arifle_mas_aks74u_c","AKS74U 5.45mm camo",40000];
                                                        _return select 1 pushBack ["arifle_mas_ak_74m_c","AK74 5.45mm camo",50000];
                                                        _return select 1 pushBack ["arifle_mas_akms_c","AKMS 7.62mm camo",90000];
                                                        _return select 1 pushBack ["arifle_mas_asval","AS VAL 7.62mm",100000];
                                                        _return select 1 pushBack ["R3F_M4S90","Benelli M4",100000];
                                                        _return select 1 pushBack ["srifle_mas_ksvk","KSVK .50",500000];
                                                        _return select 1 pushBack ["srifle_mas_ksvk_c","KSVK .50 Camo",500000];
                                                        _return select 1 pushBack ["optic_mas_PSO_nv_day","Viseur Snipeur KSVK",10000];
                                                        _return select 1 pushBack ["FHQ_optic_AIM_tan","Aimpoint Comp Camo",4000];
                                                        _return select 1 pushBack ["optic_mas_PSO_eo_c","PCO + Holo camo",10000];
                                                        _return select 1 pushBack ["RH_7Rnd_50_AE","Chargeur Desert Eagle .50",1000];
                                                        _return select 1 pushBack ["20Rnd_mas_9x39_mag","Chargeur AS VAL 20x7.62mm",500];
                                                        _return select 1 pushBack ["R3F_7Rnd_M4S90","Chargeur Benneli 7rnds",100];
                                                        _return select 1 pushBack ["5Rnd_mas_127x108_mag","Chargeur KSVK 5x.50",3000];
                                                        _return select 1 pushBack ["optic_NVS",nil,15000];
                                                        };    
                                                };
                               
                                        default {
                                                _return =
                                                ["Magasin de bandit",
                                                                [
                                                                ["arifle_SDAR_F","Fusil SDAR",50000],
                                                                ["cl3_crossbow","Arbalette",6000],
                                                                ["RH_g17","Glock 17 9mm",6500],
                                                                ["RH_g19","Glock 19 9mm",6500],
                                                                ["RH_m1911","Colt 1911 .45",12000],
                                                                ["hlc_smg_mp5a4","MP5 9mm",30000],
                                                                ["AG_MP9","MP9 9mm",30000],
                                                                ["SMG_01_F","Vector .45",40000],
                                                                ["Trixie_Enfield_Rail","Mk3 Lee Enfield",30000],
                                                                ["Trixie_CZ550","CZ 550",100000],
                                                                ["optic_ACO_grn_smg",nil,2500],
                                                                ["FHQ_optic_AIM","Aimpoint Comp",4000],
                                                                ["RH_x2","Insight X2",1000],
                                                                ["30Rnd_556x45_Stanag","Chargeur SDAR 30x5.56mm",500],
                                                                ["4Rnd_crossbow_mag","Carreau 4 fleches",50],
                                                                ["RH_33Rnd_9x19_g18","Chargeur Glock 33x9mm",500],
                                                                ["RH_7Rnd_45cal_m1911","Chargeur Colt 7x.45",500],
                                                                ["20Rnd_556x45_UW_mag","Chargeur SDAR UW 20x5.56mm",500],
                                                                ["hlc_30Rnd_9x19_B_MP5","Chargeur MP5 30x9mm",500],
                                                                ["30Rnd_9x21_Mag","Chargeur MP9 30x9mm",500],
                                                                ["30Rnd_45ACP_Mag_SMG_01","Chargeur Vector 30x.45",500],
                                                                ["Trixie_Enfield_Mag","Chargeur MK3 10x7.7mm",500],
                                                                ["Trixie_CZ550_Mag","Chargeur CZ550 5x6.6mm",500],
                                                                ["hgun_Pistol_Signal_F","Pistolet de détresse",2000],
                                                                ["6Rnd_RedSignal_F","Fusée éclairante Rouge",50],
                                                                ["V_TacVest_blk","Gilet Pare-Balles Noir",4500],
                                                                ["V_Press_F","Gilet Pare-Balles Presse",10000],
                                                                ["V_Rangemaster_belt",nil,2125]
                                                                ]
                                                ];
                                                                if(__GETC__(life_donator) > 1) then
                                                                {
                                                                //_return select 1 pushBack ["EXEMPLE","EXEMPLE",100];
                                                                };    
                                                };
                };
        };
 
////////////////////////////////////////////////////////////////////////////////////////////////
///DONATEURS////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
        case "donator":
        {
                switch(true) do
                {
                        case (__GETC__(life_donator) == 0): {_return = "Tu n'es pas donateur..."};
                                                case (license_civ_rebel): //Rebelle
                        {
                                                _return =
                                                                ["Magasin donateur rebelles",
                                                                [
                                                                ["RH_g19t","Glock 19 Tan 9mm",4000],
                                                                ["RH_33Rnd_9x19_g18","Chargeur Glock 33x9mm",500],
                                                                ["RH_p226","Sig P226 9mm",5000],
                                                                ["RH_15Rnd_9x19_SIG","Chargeur P226 15x9mm",500],
                                                                ["RH_muzi","Micro Uzi",20000],
                                                                ["RH_30Rnd_9x19_UZI","Chargeur Uzi 33x9mm",500],  
																["RH_Deagleg","Desert Eagle Or",250000],
                                                                ["RH_7Rnd_50_AE","Chargeur Desert Eagle .50",1000],                                                                                                                                    
                                                                ["arifle_SDAR_F","SDAR",25000],
                                                                ["20Rnd_556x45_UW_mag","Chargeur SDAR UW 20x5.56mm",500],                                                                                                                              
                                                                ["arifle_mas_aks74u","AKS74U 5.45mm",40000],
																["arifle_mas_aks74u_c","AKS74U 5.45mm camo",40000],                                                                
                                                                ["arifle_mas_ak74","AK74 5.45mm",50000],
																["arifle_mas_ak_74m_c","AK74 5.45mm camo",50000],
                                                                ["arifle_mas_ak12_sf","AK12 SF 5.45mm",55000],
                                                                ["arifle_mas_aks_74_sf","AK74 SF 5.45mm",55000],
                                                                ["30Rnd_mas_545x39_mag","Chargeur 30x5.45mm",500],                                                                                                                             
                                                                ["arifle_mas_l119","L119A1 5.56mm",55000],  
																["arifle_mas_l119_d","L119A1 Des 5.56mm",55000],
                                                                ["arifle_mas_l119_v","L119A1 Wood 5.56mm",55000],                                                                                                                              
                                                                ["arifle_mas_l119c","L119 CQB 5.56mm",55000],                                                                                                                          
                                                                ["arifle_mas_l119c_d","L119 CQB Des 5.56mm",55000],
                                                                ["arifle_mas_l119c_v","L119 CQB Wood 5.56mm",55000],
                                                                ["30Rnd_mas_556x45_Stanag","Chargeur L119A1 30x5.56mm",500],                                                                                                                           
                                                                ["hlc_rifle_g3ka4","G3-A4 5.56mm",55000],
                                                                ["hlc_20rnd_762x51_b_G3","Chargeur G3 20x5.56mm",500],                                                                                                                         
																["R3F_M4S90","Benelli M4",60000],
                                                                ["R3F_7Rnd_M4S90","Chargeur Benelli 7rnds",100],                                                                                                                               
                                                                ["arifle_Katiba_F","Katiba 6.5 mm",60000],
                                                                ["arifle_Katiba_C_F","Katiba Carbine 6.5 mm",60000],                                                                                                                           
                                                                ["30Rnd_65x39_caseless_green",nil,200],  
																["arifle_mas_akms_c","AKMS 7.62mm camo",70000],
																["30Rnd_mas_762x39_mag","Chargeur AKMS 30x7.62mm",500],
																["arifle_mas_asval","AS VAL 7.62mm",70000],
                                                                ["20Rnd_mas_9x39_mag","Chargeur AS VAL 20x7.62mm",500],                                                                                                                
                                                                ["R3F_Minimi","Minimi 5.56mm",70000],
                                                                ["R3F_200Rnd_556x45_MINIMI","Chargeur Minimi 200x5.56mm",500],
                                                                ["LMG_mas_rpk_F","RPK 5.45mm",70000],
                                                                ["100Rnd_mas_545x39_mag","Chargeur RPK 100x5.45mm",500],                                                                                                                                       
                                                                ["arifle_mas_mk17","MK17 #JimB 7.62",80000],
                                                                ["20Rnd_mas_762x51_Stanag","Chargeur mk17#JimB. 20x7.62mm",700],                                                                                                                               
                                                                ["ov_arifle_a2sa58","SA-58 Camo 7.62mm",80000],
                                                                ["ov_30rnd_762x39_b_sa58","Chargeur SA58 30x7.62mm",500],                                                                                                                              
                                                                ["srifle_EBR_F", "Mk18 7.62mm", 80000],
                                                                ["srifle_DMR_03_tan_F","Mk-I 7.62mm Sable (DLC)",100000],
                                                                ["srifle_DMR_03_multicam_F","Mk-I 7.62mm Camo (DLC)",100000],
                                                                ["20Rnd_762x51_Mag","Chargeur Mk-I/MK18 20x7.62mm",500],                                                                                                                               
                                                                ["srifle_mas_svd","SVD Dragunov 7.62mm",115000],
                                                                ["10Rnd_mas_762x54_mag","Chargeur SVD 10x7.62mm",500],                                                                                                                         
                                                                ["srifle_mas_lrr","LRR AWM .338",150000],
                                                                ["10Rnd_mas_338_Stanag","Chargeur LRRM 7x.338",2000],                                                                                                                          
                                                                ["srifle_mas_ksvk","KSVK .50",300000],
                                                                ["srifle_mas_ksvk_c","KSVK .50 Camo",300000],
                                                                ["5Rnd_mas_127x108_mag","Chargeur KSVK 5x.50",2000],
																["optic_mas_PSO_nv_day","Viseur Snipeur KSVK",2000],                                                                                                                   
																["optic_ACO_grn",nil,2000],
																["optic_Holosight",nil,2000],
																["optic_Arco",nil,2000],
																["optic_DMS",nil,2000],
																["optic_Hamr",nil,2000],
																["optic_MRCO",nil,2000],
																["optic_SOS",nil,2000],
																["optic_AMS_base","AMS noir",2000],
                                                                ["optic_AMS_khk","AMS Khaki",2000],
                                                                ["optic_AMS_snd","AMS Sand",2000],
                                                                ["optic_KHS_blk","Kahlia Noir",2000],
                                                                ["optic_KHS_hex","Kahlia Hex",2000],
                                                                ["optic_KHS_tan","Kahlia Tan",2000],
                                                                ["optic_KHS_old","Kahlia old",2000],
																["optic_LRPS",nil,2000],
																["optic_NVS",nil,2000],
																["optic_mas_kobra","Kobra",2000],
																["FHQ_optic_AIM","Aimpoint Comp",2000],
																["FHQ_optic_AIM_tan","Aimpoint Comp Camo",2000],
																["optic_mas_PSO_eo","PCO + Holo",2000],
																["optic_mas_PSO_eo_c","PCO + Holo camo",2000],
																["RH_x2","Insight X2",2000],
                                                                ["acc_flashlight",nil,2000],
																["bipod_01_F_blk","Bipied noir",2000],
                                                                ["bipod_01_F_mtp","Bipied MC",2000],
                                                                ["bipod_01_F_snd","Bipied Sand",2000],
                                                                ["bipod_02_F_hex","Bipied Hex",2000],
                                                                ["bipod_02_F_tan","Bipied Tan",2000],
                                                                ["bipod_03_F_oli","Bipied Olive",2000],
																["muzzle_snds_H",nil,2000],                                                                                                                                                                                                                                                    
																["Rangefinder",nil,5000],
                                                                ["V_PlateCarrierIA1_dgtl",nil,10000],
                                                                ["V_PlateCarrierIA2_dgtl",nil,20000],
                                                                ["V_PlateCarrierIAGL_dgtl","Gilet Pare-Balles LOURD",35000],
                                                                ["H_HelmetSpecO_blk",nil,70000],
                                                                ["H_HelmetO_ocamo",nil,70000],
                                                                ["H_Beret_ocamo",nil,10000],
                                                                ["V_Rangemaster_belt",nil,2500],
                                                                ["H_PilotHelmetFighter_I","Masque a Gaz",10000]                                                                                                                                                                                                                                                  
                                                                ]
                                ];
                        };
                                               
                        case (!license_civ_rebel): //Civil
                        {
                                                _return =
                                ["Magasin donateur civils",
                                [
									["RH_m9","Berreta M9",4000],
									["RH_15Rnd_9x19_M9","Chargeur M9 12x9mm",200],
									["RH_tec9","TEC-9",15000],
									["RH_32Rnd_9x19_tec","Chargeur TEC9 32x9mm",500],
									["RH_ttracker_g","Taurus Or",112500],
									["RH_6Rnd_45ACP_Mag","Chargeur Taurus 6x.45",500],
									["hlc_smg_mp5sd5","MP5 SD5 9mm",20000],
									["hlc_30Rnd_9x19_B_MP5","Chargeur MP5 30x9mm",500],
									["arifle_SDAR_F",nil,20000],
									["30Rnd_556x45_Stanag","Chargeur SDAR 30x5.56mm",500],
									["20Rnd_556x45_UW_mag","Chargeur SDAR 20x5.56 UW",500],                                                                                                                                
									["arifle_mas_bizon","PP19 Bizon 9mm",50000],
									["64Rnd_mas_9x18_mag","Chargeur Bizon 64x9mm",500],                                                                                                                                    
									["arifle_mas_g36c","G36 5.56mm",60000],
									["30Rnd_mas_556x45_Stanag","Chargeur G36 30x5.56mm",500],                                                                                                                                              
									["RH_PDW","KAC PDW 5.56mm",60000],
									["RH_30Rnd_6x35_mag","Chargeur KAC 30x5.56mm",500],                                                                                                                                            
									["arifle_mas_ak_74m","AK 74M 5.45mm",60000],
									["arifle_mas_ak_74m_c","AK 74M 5.45mm",60000],
									["30Rnd_mas_545x39_mag","Chargeur AK74/AK12 30x5.45mm",500],                                                                                                                                           
									["R3F_M4S90","Benelli M4",85000],
									["arifle_Katiba_F","Katiba 6.5 mm",80000],
									["arifle_Katiba_C_F","Katiba Carbine 6.5 mm",80000],
									["R3F_7Rnd_M4S90","Chargeur Benneli 7rnds",100],
									["Trixie_FNFAL_Rail","FAL 7.62mm",100000],
									["Trixie_FNFAL_Mag","Chargeur FAL 20x7.62mm",500],                                                                                                                                             
									["arifle_mas_akm","AKM 7.62mm",100000],
									["30Rnd_mas_762x39_mag","Chargeur AKM 30x7.62mm",500],                                                                                                                                         
									["Trixie_CZ750_Black","CZ 750 7.62mm",100000],
									["Trixie_CZ750_Ghillie","CZ 750 Camo 7.62mm",100000],
									["Trixie_10x762_Mag","Chargeur CZ750 10x7.62mm",500],
									["srifle_DMR_06_olive_F","M14 7.62mm",125000],
									["srifle_DMR_06_camo_F","M14 7.62mm camo",125000],
									["20Rnd_762x51_Mag","Chargeur M14 20x7.62mm",100],                                                                                                                        
									["optic_ACO_grn",nil,2000],
									["optic_Holosight",nil,2000],
									["optic_Arco",nil,2000],
									["optic_DMS",nil,2000],
									["optic_Hamr",nil,2000],
									["optic_MRCO",nil,2000],
									["optic_SOS",nil,2000],
									["optic_AMS_base","AMS noir",2000],
									["optic_AMS_khk","AMS Khaki",2000],
									["optic_AMS_snd","AMS Sand",2000],
									["optic_KHS_blk","Kahlia Noir",2000],
									["optic_KHS_hex","Kahlia Hex",2000],
									["optic_KHS_tan","Kahlia Tan",2000],
									["optic_KHS_old","Kahlia old",2000],
									["optic_LRPS",nil,2000],
									["optic_NVS",nil,2000],
									["optic_mas_kobra","Kobra",2000],
									["FHQ_optic_AIM","Aimpoint Comp",2000],
									["FHQ_optic_AIM_tan","Aimpoint Comp Camo",2000],
									["optic_mas_PSO_eo","PCO + Holo",2000],
									["optic_mas_PSO_eo_c","PCO + Holo camo",2000],
									["RH_x2","Insight X2",2000],
									["acc_flashlight",nil,2000],
									["bipod_01_F_blk","Bipied noir",2000],
									["bipod_01_F_mtp","Bipied MC",2000],
									["bipod_01_F_snd","Bipied Sand",2000],
									["bipod_02_F_hex","Bipied Hex",2000],
									["bipod_02_F_tan","Bipied Tan",2000],
									["bipod_03_F_oli","Bipied Olive",2000],    
									["V_Rangemaster_belt",nil,2000],                                                                                                                                                                                                                                                                                                                                           
									["V_TacVest_blk","Gilet Pare-Balles Noir",4500],
									["V_PlateCarrierIA2_dgtl","Gilet Pare-Balles Moyen",35000],                                                                        
									["H_PilotHelmetFighter_I","Masque a Gaz",75000],
									["Rangefinder",nil,5000]
                                ]
                                ];                            
                        };    
                };
        };
////////////////////////////////////////////////////////////////////////////////////////////////
///MEDECIN//////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
        case "med_basic":
        {
                                _return =
                ["Magasin Médecin d'Altis",
                        [
                        ["hgun_Pistol_Signal_F","Pistolet de détresse",2000],
                        ["6Rnd_GreenSignal_F","Fusée éclairante Verte",200],
                        ["Binocular",nil,150],
                        ["ItemMap",nil,50],
                        ["ItemGPS",nil,1250],
                        ["ToolKit",nil,2500],
                        ["ItemCompass",nil,80],
                        ["ItemWatch",nil,80],
                        ["FirstAidKit",nil,1500],
                        ["Medikit",nil,15000],
                        ["NVGoggles",nil,10000],
                        ["Chemlight_red",nil,300],
                        ["Chemlight_yellow",nil,300],
                        ["Chemlight_green",nil,300],
                        ["Chemlight_blue",nil,300],
                        ["ItemRadio","Téléphone", 1000]
                        ]
                ];
					if(__GETC__(life_donator) > 1) then
					{
					//_return select 1 pushBack ["EXEMPLE","EXEMPLE",100];
					};    
	};
               
////////////////////////////////////////////////////////////////////////////////////////////////
///MERCENAIRE///////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
        case "merc_weapon":
        {
                        _return =
            ["Armes & viseurs mercenaire",
                    [
                    ["hgun_Pistol_heavy_02_Yorris_F","Pistolet Taser",6500],
                    ["cl_FlashLight","Lampe Torche/Maglite",10],
                                        ["cl3_fingerprintscanner","*Lecteur d'empreintes digital",10],
                    ["RH_g19t","Glock 19 Tan 9mm",6500],
                    ["RH_fnp45","FNP.45",15000],
                    ["RH_g18","Glock 18 9mm",20000],
                    ["hlc_smg_mp5k_PDW","MP5-K 9mm",30000],
                    ["arifle_mas_mk16","MK16 CQB 5.56mm",15000],
                    ["arifle_mas_mk16_l","MK16 5.56mm",15000],
                    ["RH_M4A1_ris","M4 RIS",60000],
                    ["RH_M4sbr_b","M4 Vltor",60000],
                    ["arifle_mas_aks_74_sf","AK74 SF 5.45mm",70000],
                    ["ov_arifle_a2sa58","SA-58 Camo 7.62mm",80000],
                    ["R3F_M4S90","Benelli M4",100000],
                    ["arifle_mas_mk17","MK17 7.62mm",100000],
                    ["hlc_rifle_m14sopmod","M14 Troy 7.62mm",100000],
                    ["R3F_Minimi","Minimi 5.56mm",125000],
                    ["srifle_LRR_SOS_F",nil,230000],
                    ["optic_DMS",nil,2000],
                    ["optic_LRPS",nil,2000],
                    ["optic_MRCO",nil,2000],
                    ["optic_ACO_grn",nil,3500],
                    ["RH_t1","Aimpoint T1 / M4",3600],
                    ["FHQ_optic_AIM","Aimpoint Comp",3600],
                    ["FHQ_optic_ACOG","ACOG TA31 + Holo",3600],
                    ["RH_ta31rco","ACOG TA31 / M4",3600],
                    ["optic_Holosight",nil,3600],
                    ["optic_SOS",nil,2000],
                    ["optic_Arco",nil,2000],
                    ["optic_Hamr",nil,3500],
                    ["RH_m6x","Insight M6X",1000],
                    ["acc_flashlight",nil,1000],
					["bipod_01_F_blk","Bipied noir",1000],
					["bipod_01_F_mtp","Bipied MC",1000],
					["bipod_01_F_snd","Bipied Sand",1000],
					["bipod_02_F_hex","Bipied Hex",1000],
                    ["bipod_02_F_tan","Bipied Tan",1000],
                    ["bipod_03_F_oli","Bipied Olive",1000],
                    ["muzzle_snds_M","Silencieux 5.56 mm",5000],
                    ["RH_qdss_nt4","Silencieux M4 noir",5000],
                    ["RH_sfm952v","Surefire M952 / M4",1000],
                    ["RH_m4covers_s","Cover rails / M4",10],
                    ["RH_m4covers_f","Cover rails full / M4",10]
                    ]
           ];
                                        if(__GETC__(life_donator) > 1) then
                                        {                                      
                    _return select 1 pushBack ["arifle_mas_mk16_gl","MK16 CQB + EGLM",80000];
                    _return select 1 pushBack ["RH_M4A1_ris_M203","M4 RIS + M203",100000];
                    _return select 1 pushBack ["RH_M4sbr","M4 Vltor TAN",60000];
                    _return select 1 pushBack ["RH_M4sbr_g","M4 Vltor OD",60000];
                    _return select 1 pushBack ["srifle_LRR_camo_F","M320 .408 Camo ss optique",200000];
                    _return select 1 pushBack ["R3F_PGM_Hecate_II","PGM Hecate II .50",400000];
                    _return select 1 pushBack ["R3F_PGM_Hecate_II_DES","PGM Hecate II Camo .50",400000];
                    _return select 1 pushBack ["R3F_PGM_Hecate_II_POLY","PGM Hecate II Noir .50",400000];
                    _return select 1 pushBack ["FHQ_optic_AIM_tan","Aimpoint Comp Camo",3600];
                    _return select 1 pushBack ["RH_sfm952v_tan","Surefire M952 TAN / M4",1000];
                    _return select 1 pushBack ["muzzle_snds_L","Silencieux 9 mm ",5000];
                    _return select 1 pushBack ["muzzle_snds_H","Silencieux 6.5 mm",5000];
                    _return select 1 pushBack ["muzzle_snds_H_MG","Silencieux LMG 6.5 mm",5000];
                                        _return select 1 pushBack ["muzzle_snds_B","Silencieux 6.5 mm",5000];
                                        };
        };
 
       
        case "merc_ammo":
        {
                        _return =
                        ["Munitions",
                                        [
										["6Rnd_45ACP_Cylinder","Munition Taser",500],
										["RH_33Rnd_9x19_g18","Chargeur Glock 33x9mm",500],
										["RH_15Rnd_45cal_fnp","Chargeur FNP 15x.45",500],
										["hlc_30Rnd_9x19_B_MP5","Chargeur MP5 30x9mm",500],
										["30Rnd_mas_556x45_Stanag","Chargeur MK16 30x5.56mm",500],
										["30Rnd_556x45_Stanag","Chargeur M4 30x5.56mm",500],
										["30Rnd_mas_545x39_mag","Chargeur AK 30x5.45mm",500],
										["R3F_7Rnd_M4S90","Chargeur Benneli 7rnds",100],
										["ov_30rnd_762x39_b_sa58","Chargeur SA58 30x7.62mm",500],
										["20Rnd_mas_762x51_Stanag","Chargeur MK17 20x7.62mm",500],
										["hlc_20Rnd_762x51_B_M14","Chargeur M14 20x7.62mm",500],
										["R3F_200Rnd_556x45_MINIMI","Chargeur Minimi 200x5.56mm",500],
										["7Rnd_408_Mag","Chargeur M320 7x.408",1500],
										["MiniGrenade","Flashbang",10000],
										["cl3_coveralls_prisoner_uniform","Uniforme de détenu",10],
										["G_Balaclava_combat","*Cagoule aveuglante pour détenu",10],
										["cl3_police_vest_etu","Gilet protection détenu",10],
										["SmokeShell","*Grenade Lacrymogene",2000]
                                        ]
                        ];
                        if(__GETC__(life_donator) > 1) then
                                        {
                                                  _return select 1 pushBack ["R3F_7Rnd_127x99_PGM","Chargeur PGM",500];
                                                  _return select 1 pushBack ["1Rnd_Smoke_Grenade_shell","*Grenade EGLM Lacrymogene",2500];
                                                  _return select 1 pushBack ["1Rnd_SmokeYellow_Grenade_shell","Grenade EGLM Jaune",1000];
                                                  _return select 1 pushBack ["1Rnd_SmokeRed_Grenade_shell","Grenade EGLM Rouge",1000];
                                                  _return select 1 pushBack ["1Rnd_SmokeGreen_Grenade_shell","Grenade EGLM Vert",1000];
                                                  _return select 1 pushBack ["1Rnd_SmokePurple_Grenade_shell","Grenade EGLM Violet",1000];
                                                  _return select 1 pushBack ["1Rnd_SmokeBlue_Grenade_shell","Grenade EGLM Bleu",1000];
                                                  _return select 1 pushBack ["1Rnd_SmokeOrange_Grenade_shell","Grenade EGLM Orange",1000];
                                        };                    
        };
 
        case "merc_tools":
        {
                _return =
                        ["Outils",
						 [
						 ["Binocular",nil,150],
						 ["ItemMap",nil,50],
						 ["ItemGPS",nil,1250],
						 ["ToolKit",nil,2500],
						 ["Rangefinder",nil,5600],
						 ["NVGoggles_INDEP",nil,10000],
						 ["Chemlight_red",nil,300],
						 ["Chemlight_yellow",nil,300],
						 ["Chemlight_green",nil,300],
						 ["Chemlight_blue",nil,300],
						 ["Chemlight_blue",nil,300],
						 ["ItemRadio", "Téléphone", 3000]
						 ]
                        ];
                                if(__GETC__(life_donator) > 1) then
                                {
                                //_return select 1 pushBack ["EXEMPLE","EXEMPLE",100];
                                };    
                       
        };    
////////////////////////////////////////////////////////////////////////////////////////////////
///CIVIL////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
        case "genstore":
        {
                _return =
            ["Altis magasin général",
                    [
                    ["Binocular",nil,150],
                    ["ItemMap",nil,50],
                    ["ItemGPS",nil,1250],
                    ["ToolKit",nil,2500],
                    ["NVGoggles",nil,10000],
                    ["Chemlight_red",nil,300],
                    ["Chemlight_yellow",nil,300],
                    ["Chemlight_green",nil,300],
                    ["Chemlight_blue",nil,300],
                    ["ItemRadio","Téléphone", 2500]
                    ]
            ];
				if(__GETC__(life_donator) > 1) then
				{
				//_return select 1 pushBack ["EXEMPLE","EXEMPLE",100];
				};    
        };
               
                case "civil_tool":
        {
                _return =
            ["Outils",
                        [
                        ["cl_axe",nil,3000],
                        ["cl_shovel",nil,5000],
                        ["cl_pick_axeweap",nil,5000],
                        ["geiger","Détecteur de Radiation",25000],
                        ["alfr_coveralls_antirad",nil,15000]
                        ]
                ];
					if(__GETC__(life_donator) > 1) then
					{
					//_return select 1 pushBack ["U_VirtualMan_F",nil,329688];
					};                            
	};
               
               
                case "gun":
        {
                switch(true) do
        {
                        case (playerSide != civilian): {_return = "Ce n'est pas un shop pour les Flics!"};
                        case (!license_civ_gun): {_return = "Il te faut le Permis port d'armes!"};
                        default {
                        _return =
                        ["Magasin d'armes légales",
                                        [
                                        ["hgun_Pistol_Signal_F","Pistolet de détresse",2000],
                                        ["6Rnd_RedSignal_F","Fusée éclairante Rouge",50],
                                        ["RH_g17","Glock 17 9mm",5000],
                                        ["RH_g19","Glock 19 9mm",5000],
                                        ["RH_33Rnd_9x19_g18","Chargeur Glock 33x9mm",500],
                                        ["cl3_crossbow","Arbalète",5000],
                                        ["4Rnd_crossbow_mag","Carreau 4 fleches",50],
                                        ["RH_m1911","Colt 1911 .45",10000],
                                        ["RH_7Rnd_45cal_m1911","Chargeur Colt 7x.45",500],
										["arifle_SDAR_F","Fusil SDAR",25000],
                                        ["30Rnd_556x45_Stanag","Chargeur SDAR 30x5.56mm",500],
                                        ["20Rnd_556x45_UW_mag","Chargeur SDAR 20x5.56 UW",500],
                                        ["Trixie_Enfield_Rail","Mk3 Lee Enfield",25000],
                                        ["Trixie_Enfield_Mag","Chargeur MK3 10x7.7mm",500],
                                        ["hlc_smg_mp5a4","MP5 9mm",30000],
                                        ["AG_MP9","MP9 9mm",30000],                                                                    
                                        ["30Rnd_9x21_Mag","Chargeur MP9 30x9mm",500],
										["hlc_smg_mp5k_PDW","MP5-K 9mm",35000],
                                        ["hlc_30Rnd_9x19_B_MP5","Chargeur MP5 30x9mm",500],
                                        ["SMG_01_F","Vector .45",40000],
                                        ["30Rnd_45ACP_Mag_SMG_01","Chargeur Vector 30x.45",500],
                                        ["Trixie_CZ550","CZ 550",80000],
                                        ["Trixie_CZ550_Mag","Chargeur CZ550 5x6.6mm",500],                                        
                                        ["optic_ACO_grn_smg",nil,2000],
                                        ["FHQ_optic_AIM","Aimpoint Comp",2000],
                                        ["RH_x2","Insight X2",2000],
										["V_Rangemaster_belt",nil,2000],
                                        ["V_TacVest_blk","Gilet Pare-Balles Noir",4500],
                                        ["V_Press_F","Gilet Pare-Balles Presse",10000]
                                       
                                        ]
                        ];
                                                        if(__GETC__(life_donator) > 1) then
                                                        {
                                                        //_return select 1 pushBack ["EXEMPLE","EXEMPLE",100];
                                                        };    
                        };
                };
                };
   
        case "gunblack":
        {
                switch(true) do
        {
                        case (playerSide != civilian): {_return = "Ce n'est pas un shop pour les Flics!"};
                        default {
                                        _return =    
                                        ["Marché Noir",
                                                        [
														["RH_m9","Berreta M9",4000],
														["RH_15Rnd_9x19_M9","Chargeur M9 12x9mm",200],                                                                      
														["RH_python","Colt Python",6000],                                                                      
														["RH_6Rnd_357_Mag","6 balles Python .357",200],                                                                    
														["RH_bull","Taurus Raging Bull",8200],                                                                    
														["RH_bullb","Taurus Raging Bull Noir",8900],                                                                    
														["RH_6Rnd_454_Mag","6 balles Taurus RH .454",200],                                                                    
														["RH_tec9","TEC-9",20000],
														["RH_32Rnd_9x19_tec","Chargeur TEC9 32x9mm",500],
														["RH_ttracker_g","Taurus Or",150000],
														["RH_6Rnd_45ACP_Mag","Chargeur Taurus 6x.45",500],
														["arifle_SDAR_F",nil,20000],
														["30Rnd_556x45_Stanag","Chargeur SDAR 30x5.56mm",500],
														["20Rnd_556x45_UW_mag","Chargeur SDAR 20x5.56mm Sous Marine",500],                                                                                                                                             
														["hlc_smg_mp5sd5","MP5 SD5 9mm",30000],
														["hlc_30Rnd_9x19_B_MP5","Chargeur MP5 30x9mm",500],                                                                                                                                                                                                                    
														["arifle_mas_bizon","PP19 Bizon 9mm",50000],
														["64Rnd_mas_9x18_mag","Chargeur Bizon 64x9mm",500],
														["arifle_mas_mk16_l","MK16 5.56mm",60000],
														["arifle_mas_mk16","MK16 CQB 5.56mm",60000],                                                                      
														["30Rnd_mas_556x45_Stanag","Chargeur MK16 30x5.56mm",500],
														["arifle_mas_g36c","G36 5.56mm",60000],
														["30Rnd_mas_556x45_Stanag","Chargeur G36 30x5.56mm",500],
														["RH_PDW","KAC PDW 5.56mm",60000],
														["RH_30Rnd_6x35_mag","Chargeur KAC 30x5.56mm",500],
														["arifle_mas_ak_74m","AK 74M 5.45mm",60000],
														["30Rnd_mas_545x39_mag","Chargeur 30x5.45mm",500],
														["arifle_Katiba_F","Katiba 6.5 mm",80000],
														["arifle_Katiba_C_F","Katiba Carbine 6.5 mm",80000],
														["30Rnd_65x39_caseless_green",nil,200],                                                                                                                                                
														["R3F_M4S90","Benelli M4",100000],
														["R3F_7Rnd_M4S90","Chargeur Benneli 7rnds",100],
														["Trixie_FNFAL_Rail","FAL 7.62mm",100000],
														["Trixie_FNFAL_Mag","Chargeur FAL 20x7.62mm",500],
														["Trixie_CZ750_Black","CZ 750 7.62mm",100000],
														["Trixie_10x762_Mag","Chargeur CZ750 10x7.62mm",500],                                                                                                                                          
														["arifle_mas_akm","AKM 7.62mm",125000],
														["30Rnd_mas_762x39_mag","Chargeur AKM 30x7.62mm",500],
														["srifle_DMR_06_olive_F","M14 7.62mm (DLC)",125000],
														["20Rnd_762x51_Mag","Chargeur M14 20x7.62mm",100],
														["optic_ACO_grn",nil,2000],
														["optic_Holosight",nil,2000],
														["optic_Arco",nil,2000],
														["optic_DMS",nil,2000],
														["optic_Hamr",nil,2000],
														["optic_MRCO",nil,2000],
														["optic_SOS",nil,2000],
														["optic_LRPS",nil,2000],
														["optic_mas_kobra","Kobra",2000],
														["FHQ_optic_AIM","Aimpoint Comp",2000],
														["optic_mas_PSO_eo","PCO + Holo",2000],
														["RH_x2","Insight X2",2000],
														["acc_flashlight",nil,2000],
														["bipod_01_F_blk","Bipied noir",2000],
														["bipod_01_F_mtp","Bipied MC",2000],
														["bipod_01_F_snd","Bipied Sand",2000],
														["bipod_02_F_hex","Bipied Hex",2000],
														["bipod_02_F_tan","Bipied Tan",2000],
														["bipod_03_F_oli","Bipied Olive",2000],
														["Rangefinder",nil,10000],
														["V_Rangemaster_belt",nil,2000],
														["V_TacVest_blk","Gilet Pare-Balles Noir",4500],        
														["cl3_Headgear_spliff","Spliff",4000],   
														["G_mas_wpn_bala_gog_b",nil,500],
														["G_mas_wpn_bala_mask_b",nil,500],
														["G_mas_wpn_bala_b",nil,500],
														["G_mas_wpn_bala_t",nil,500],
														["G_mas_wpn_shemag_r",nil,500],
														["G_mas_wpn_shemag_mask",nil,500],
														["G_mas_wpn_shemag_w",nil,500],
														["G_mas_wpn_shemag",nil,500],
														["G_mas_wpn_wrap_mask_b",nil,500],
														["G_mas_wpn_wrap_mask_g",nil,500],
														["G_mas_wpn_wrap_mask_t",nil,500],
														["G_mas_wpn_wrap",nil,500],
														["G_mas_wpn_wrap_c",nil,500],
														["G_mas_wpn_wrap_gog",nil,500],														
														["G_Bandanna_tan",nil,650],
														["G_Bandanna_aviator",nil,650],
														["G_Bandanna_beast",nil,650],
														["G_Bandanna_oli",nil,650],
														["G_Bandanna_khk",nil,650],
														["G_Bandanna_sport",nil,650],
														["G_Bandanna_shades",nil,650],                                                                      
														["G_Balaclava_combat","*Cagoule aveuglante",5000]
                                                        ]
                                        ];
                                                if(__GETC__(life_donator) > 1) then
                                                {
                                                _return select 1 pushBack ["srifle_DMR_06_camo_F","M14 7.62mm camo (DLC)",125000];
                                                //_return select 1 pushBack ["U_VirtualMan_F",nil,329688];
                                                };                            
                        };
                };
                };
////////////////////////////////////////////////////////////////////////////////////////////////
///FUN//////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////
                case "paintball":
        {
                _return =  
            ["Paintball shop",
                [
                ["U_B_Protagonist_VR","Tenue équipe 1",10000],
                ["U_I_Protagonist_VR","Tenue équipe 2",10000],
                ["U_O_Protagonist_VR","Tenue équipe 3",10000],
                ["V_Rangemaster_belt",nil,2500],
                ["cal68_angel_dye",nil,15000],
                ["cal68_angel_kitty",nil,15000],
                ["cal68_angel_rasta",nil,15000],
                ["cal68_egosl",nil,15000],
                ["cal68_invert_mini_dye",nil,15000],
                ["cal68_50FP",nil,100],
                ["NVGoggles",nil,10000],
                ["FirstAidKit",nil,1500]
                ]
            ];
                                        if(__GETC__(life_donator) > 1) then
                                        {
                                        //_return select 1 pushBack ["EXEMPLE","EXEMPLE",100];
                                        };    
        };
               
                case "admin":
        {
                _return =  
            ["Admin shop",
                [
                ["U_VirtualMan_F","Tenue invisible",1],
                ["alfr_poloshirt_admin","Tee shirt admin",1]
                ]
            ];
                                if(__GETC__(life_donator) > 1) then
                                {
                                //_return select 1 pushBack ["EXEMPLE","EXEMPLE",100];
                                };    
        };
       
};
///BACKUP
//["CL_hammer",nil,15000],
//["cl_fishing_rod",nil,15000],
//["cl_bigredkey",nil,15000],
//["cl_bin_shield",nil,15000]
//["cl_picket_mlnw",nil,15000],
//["cl_picket_ftp",nil,15000],
//["cl_picket_rtp",nil,15000],
 
_return;