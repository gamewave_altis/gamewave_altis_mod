#include <macro.h>
/*
        File: fn_adminMenu.sqf
        Author: Bryan "Tonic" Boardwine
        Modified by Olek
       
        Description:
        Opens the admin menu, sorry nothing special in here. Take a look for yourself.
*/
if(__GETC__(life_adminlevel) < 1) exitWith {closeDialog 0;};
private["_display","_list","_side"];
disableSerialization;
waitUntil {!isNull (findDisplay 2900)};
_display = findDisplay 2900;
_list = _display displayCtrl 2902;
if(__GETC__(life_adminlevel) < 1) exitWith {closeDialog 0;};
//Purge List
lbClear _list;
 
{
        switch(side _x) do
        {
                case west: {
                        _list lbAdd format["%1%2",">", _x getVariable["realname",name _x]];
                };     
               
                case civilian : {
                        _list lbAdd format["%1%2","", _x getVariable["realname",name _x]];
                };
               
                default {
                        _list lbAdd format["%1-%2","Inconnu", _x getVariable["realname",name _x]];
                };
        };
_list lbSetdata [(lbSize _list)-1,str(_x)];
} foreach playableUnits;
lbsort [_display displayCtrl 2902,"ASC"];
if(__GETC__(life_adminlevel) < 1) exitWith {closeDialog 0;};
if(__GETC__(life_adminlevel) < 3)then {
ctrlShow [666, false];
ctrlShow [667, false];
};

