/*
	File: fn_jail.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Starts the initial process of jailing.
*/
private["_bad","_unit","_time","_itemName","_oldVal","_handle"];
_unit = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
hint format["%1", _unit];
if(isNull _unit) exitWith {}; //Dafuq?
if(_unit != player) exitWith {}; //Dafuq?
if(life_is_arrested) exitWith {}; //Dafuq i'm already arrested
_bad = [_this,1,false,[false]] call BIS_fnc_param;
_time = [_this,2,15,[0]] call BIS_fnc_param;

player setVariable["restrained",false,true];
player setVariable["CopRestrain",false,true];
player setVariable["Escorting",false,true];
player setVariable["transporting",false,true];

titleText["Vous êtes en état d'arrestation. Votre temps de prison est sauvegardé sur le serveur","PLAIN"];
//hint "Pour avoir été en état d'arrestation vous avez perdu aucune license, mais attention la prochaine fois !";
player setPos (getMarkerPos "jail_marker");

if(_bad) then
{
	waitUntil {alive player};
	sleep 1;
};

//Check to make sure they goto check
if(player distance (getMarkerPos "jail_marker") > 40) then
{
	player setPos (getMarkerPos "jail_marker");
};

//[1] call life_fnc_removeLicenses;
//if(life_inv_heroinu > 0) then {[false,"heroinu",life_inv_heroinu] call life_fnc_handleInv;};
//if(life_inv_heroinp > 0) then {[false,"heroinp",life_inv_heroinp] call life_fnc_handleInv;};
//if(life_inv_coke > 0) then {[false,"cocaine",life_inv_coke] call life_fnc_handleInv;};
//if(life_inv_cokep > 0) then {[false,"cocainep",life_inv_cokep] call life_fnc_handleInv;};
//if(life_inv_turtle > 0) then {[false,"turtle",life_inv_turtle] call life_fnc_handleInv;};
//if(life_inv_cannabis > 0) then {[false,"cannabis",life_inv_cannabis] call life_fnc_handleInv;};
//if(life_inv_marijuana > 0) then {[false,"marijuana",life_inv_marijuana] call life_fnc_handleInv;};
//if(life_inv_uranium > 0) then {[false,"uranium",life_inv_uranium] call life_fnc_handleInv;};
//if(life_inv_uraniump > 0) then {[false,"uraniump",life_inv_uraniump] call life_fnc_handleInv;};
//if(life_inv_phos > 0) then {[false,"phos",life_inv_phos] call life_fnc_handleInv;};
//if(life_inv_soude > 0) then {[false,"soude",life_inv_soude] call life_fnc_handleInv;};
//if(life_inv_meth > 0) then {[false,"meth",life_inv_meth] call life_fnc_handleInv;};

//Delete tous les items // LUCEL MOFO
{
_itemName = [_x,1] call life_fnc_varHandle;
diag_log format["fn_jail _itemName %1",_itemName];
_oldVal = missionNamespace getVariable ([_itemName,0] call life_fnc_varHandle);
diag_log format["fn_jail _oldVal %1",_oldVal];
if(_oldVal > 0) then {[false,_itemName,_oldVal] call life_fnc_handleInv;};
}forEach life_inv_items;

//_handle = [] spawn life_fnc_stripDownPlayer;
//waitUntil {scriptDone _handle};


[true,"pain",3] call life_fnc_handleInv;
[true,"verredeau",3] call life_fnc_handleInv;

life_is_arrested = true;

removeAllWeapons player;
{player removeMagazine _x} foreach (magazines player);
removeUniform player;
removeVest player;
removeBackpack player;
removeGoggles player;
removeHeadGear player;
player forceAddUniform "cl3_coveralls_maxprisoner_uniform";

[[player,_bad,_time],"life_fnc_jailSys",false,false] spawn life_fnc_MP;
[5] call SOCK_fnc_updatePartial;
