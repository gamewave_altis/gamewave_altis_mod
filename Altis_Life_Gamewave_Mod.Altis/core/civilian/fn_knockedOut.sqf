/*
	File: fn_knockedOut.sqf
	Original Author: Bryan "Tonic" Boardwine
	Edit : Altislife.fr / Prospere
*/
private["_target","_who","_obj","_reallife","_life","_degat","_realDamage","_backflip","_direction"];
_target = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
_who = [_this,1,"",[""]] call BIS_fnc_param;
_hasrifle = [_this,2,false,[false]] call BIS_fnc_param;
if(isNull _target) exitWith {};
if(_target != player) exitWith {};
if(_who == "") exitWith {};
life_knockout = true;

//Life loss
_reallife = getDammage _target;
_life = (-(_reallife -1)*100);
_degat = 15;

//diag_log format ["_reallife %1", _reallife];
//diag_log format ["_life %1", _life];
//diag_log format ["_hasrifle %1", _hasrifle];

//Ko avec rifle OU vie inférieur a 65
if((_hasrifle) || (_life <= 65)) then {
	if (_hasrifle) then {
			player playmoveNow "cl3_anim_knockout2in";
		}else{
			player playmoveNow "cl3_anim_knockout1in";
	};

	//Dégat
	if(!(_life <= _degat)) then {
	_life = _life - _degat;
	_realDamage = (-(_life/100) +1);
	diag_log format ["_realDamage: %1",_realDamage];
	player setDamage _realDamage;
	}else{
	[player,player] call FAR_Player_Unconscious;
	};
	
	sleep 2.2;

	player playMoveNow "Incapacitated";
	//_direction = getDir player;
	//_backflip = _direction + 180;
	//if(_backflip > 360) then { 
	//	_backflip = _backflip - 360;
	//	diag_log format ["_backflip %1", _backflip];
	//};
	//player setDir _backflip;
	
	titleText[format["Quelqu'un t'a assomé.",_who],"PLAIN"];
	_obj = "Land_ClutterCutter_small_F" createVehicle (getPosATL player);
	_obj setPosATL (getPosATL player);
	player attachTo [_obj,[0,0,0]];



	sleep 8;
	if((_target getVariable "restrained")) then {
		
			_target allowDamage true;
			disableUserInput false;
			detach _target;
			deleteVehicle _obj;
			_target setVariable["robbed",FALSE,TRUE];
			sleep 4; 
			life_knockout = false;
		}
		else
		{
			_target playMoveNow "amovppnemstpsraswrfldnon";
			_target allowDamage true;
			disableUserInput false;
			detach _target;
			deleteVehicle _obj;
			_target setVariable["robbed",FALSE,TRUE];
			sleep 4; 
			life_knockout = false;
		};
	};

//Trop de vie pour KO
if(_life > 65 && !(_hasrifle)) then {
	_rng = round(random 6);
	diag_log format ["_rng %1", _rng];
	Switch (_rng) do
		{
			case 0: {player playMoveNow "cl3_anim_receive1a";};
			case 1: {player playMoveNow "cl3_anim_receive1b";};
			case 2: {player playMoveNow "cl3_anim_receive2a";};
			case 3: {player playMoveNow "cl3_anim_receive2b";};
			case 4: {player playMoveNow "cl3_anim_receive3a";};
			case 5: {player playMoveNow "cl3_anim_receive3c";};
			case 6: {player playMoveNow "cl3_anim_receive4";};
		};
	_life = _life - _degat;
	_realDamage = (-(_life/100) +1);
	diag_log format ["_realDamage: %1",_realDamage];
	player setDamage _realDamage;	
	life_knockout = false;	
};
