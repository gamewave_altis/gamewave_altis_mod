/*
	File: fn_sirenLights.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Lets play a game! Can you guess what it does? I have faith in you, if you can't
	then you have failed me and therefor I lose all faith in humanity.. No pressure.
*/
if((time - life_delay) < 3) exitWith {};
life_delay = time;
private["_vehicle"];
_vehicle = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _vehicle) exitWith {}; //Bad entry!
diag_log format ["DepSirenLights %1", _vehicle];
if(!(typeOf _vehicle in [
			"C_Offroad_01_F",
			"cl3_escalade_dep"
])) exitWith {}; //Last chance check to prevent something from defying humanity and creating a monster.

diag_log format["DepSirenLights %1", _vehicle];
_trueorfalse = _vehicle getVariable["lightsdep",FALSE];
diag_log format["DepSirenLights _trueorfalse %1", _trueorfalse];
if(_trueorfalse) then {
	_vehicle setVariable["lightsdep",FALSE,TRUE];
	diag_log format["DepSirenLights _trueorfalse %1 FALSE TRUE", _trueorfalse];
} else {
	_vehicle setVariable["lightsdep",TRUE,TRUE];
	diag_log format["DepSirenLights _trueorfalse %1 TRUE TRUE", _trueorfalse];
	[[_vehicle,0.22],"life_fnc_depLights",true,false] call life_fnc_MP;
};