/*
	File: fn_knockoutAction.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Knocks out the target.
	Edit : Altislife.fr / Prospere
*/
private["_target","_currentWeapon","_hasrifle"];
_target = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if((player getVariable "restrained")||(player getVariable "Escorting")||(player getVariable "Surrender")) exitWith {};
//Check armes
_currentWeapon = currentWeapon player;
if ("cl_shovel" in [_currentWeapon])exitWith{};
if ("CL_hammer" in [_currentWeapon])exitWith{};
if ("cl_pick_axeweap" in [_currentWeapon])exitWith{};
if ("geiger" in [_currentWeapon])exitWith{};
if ("cl_picket_mlnw" in [_currentWeapon])exitWith{};
if ("cl_picket_ftp" in [_currentWeapon])exitWith{};
if ("cl_picket_rtp" in [_currentWeapon])exitWith{};
if ("cl_bigredkey" in [_currentWeapon])exitWith{};
if ("cl_fishing_rod" in [_currentWeapon])exitWith{};
//Check fusil
_hasrifle = if((currentWeapon player == primaryWeapon player)) then {true} else {false};

//diag_log format [" _hasrifle %1", _hasrifle];
//diag_log format [" _currentWeapon %1", _currentWeapon];

if(isNull _target) exitWith {};
if(!isPlayer _target) exitWith {};
if(player distance _target > 4) exitWith {};

//Start Knock
life_knockout = true;
if(_hasrifle) then {
diag_log format [" TRUE exec anim rifle _hasrifle %1", _hasrifle];
	player playmoveNow "cl3_anim_weaponhit";
	sleep 1;
	[[player,"Punch",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;	
	[[_target,name player,_hasrifle],"life_fnc_knockedOut",_target,false] spawn life_fnc_MP;
	
} else {
diag_log format [" FALSE exec anim classic _hasrifle %1", _hasrifle];
	player playmoveNow "cl3_anim_punchrandom";
	sleep 0.5;
	[[player,"Punch",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;	
	[[_target,name player,_hasrifle],"life_fnc_knockedOut",_target,false] spawn life_fnc_MP;
};
//Cooldown
sleep 3;
life_knockout = false;

