/*
	File: fn_copLights.sqf
	Author: mindstorm, modified by Adanteh
	Link: http://forums.bistudio.com/showthread.php?157474-Offroad-Police-sirens-lights-and-underglow
	
	Description:
	Adds the light effect to cop vehicles, specifically the offroad.
*/
private ["_vehicle","_lightYellow","_lightleft","_lightright","_brightness","_attach"];
diag_log format["depLights"];
_vehicle = _this select 0;
diag_log format["depLights _vehicle %1", _vehicle];
if (isNil "_vehicle" || isNull _vehicle || !(_vehicle getVariable "lightsdep")) exitWith {};

switch (typeOf _vehicle) do {
	case "C_Offroad_01_F": { _attach = [[-0.37, 0.0, 0.56], [0.37, 0.0, 0.56]]; };
	case "cl3_escalade_dep": { _attach = [[-0.37, 0.0, 0.56], [0.37, 0.0, 0.56]]; };
};

_lightYellow = [20, 15, 0.1];

_lightleft = createVehicle ["#lightpoint", getPos _vehicle, [], 0, "CAN_COLLIDE"];
sleep 0.2;
_lightleft setLightColor _lightYellow;
_lightleft setLightBrightness 0;
_lightleft lightAttachObject [_vehicle, _attach select 0];
_lightleft setLightAttenuation [0.1, 0, 50, 50,0.5,3];
_lightleft setLightIntensity 5;
_lightleft setLightFlareSize 2;
_lightleft setLightFlareMaxDistance 5;
_lightleft setLightUseFlare true;
_lightleft setLightDayLight true;

_lightright = createVehicle ["#lightpoint", getPos _vehicle, [], 0, "CAN_COLLIDE"];
sleep 0.2;
_lightright setLightColor _lightYellow;
_lightright setLightBrightness 0;
_lightright lightAttachObject [_vehicle, _attach select 1];
_lightright setLightAttenuation [0.1, 0, 50, 50,0.5,3];
_lightright setLightIntensity 5;
_lightright setLightFlareSize 2;
_lightright setLightFlareMaxDistance 5;
_lightright setLightUseFlare true;
_lightright setLightDayLight true;

if (sunOrMoon < 1) then {
	_brightness = 1;
} else {
	_brightness = 5;
};

_lightYellow = true;  
while {(alive _vehicle)} do { 
	if (!(_vehicle getVariable "lightsdep")) exitWith {};
	if (_lightYellow) then {  
		_lightYellow = false;  
		_lightright setLightBrightness 0;  
		sleep 0.2;
		_lightleft setLightBrightness _brightness;  
	} else {  
		_lightYellow = true;  
		_lightleft setLightBrightness 0;  
		sleep 0.2;
		_lightright setLightBrightness _brightness;  
	};
	sleep 0.5;  
};  
deleteVehicle _lightleft;
deleteVehicle _lightright;
