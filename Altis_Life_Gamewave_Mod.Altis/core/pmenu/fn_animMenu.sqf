#include <macro.h>
/*
	File: fn_animMenu.sqf
	Prospere
	Altislife.fr / gamewave.fr
*/
private["_display","_anims","_name"];
disableSerialization;

waitUntil {!isNull (findDisplay 60000)};
_display = findDisplay 60000;
_anims = _display displayCtrl 60001;
lbClear _anims;
if(player getVariable ["restrained",false]) exitWith {closeDialog 0;};
if(player getVariable ["surrender",false]) exitWith {closeDialog 0;};
if(player getVariable ["escorting",false]) exitWith {closeDialog 0;};
if(animationState player == "incapacitated") exitWith {closeDialog 0;};
if((player getVariable "restrained")||(player getVariable "Escorting")||(player getVariable "Surrender")) exitWith {closeDialog 0;};
if(life_istazed) exitWith {closeDialog 0;};
if(life_action_inUse) exitWith {closeDialog 0;};	
if(!alive player) exitWith {closeDialog 0;};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Animation disponible
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
alfr_animlist = [
["Acts_StandingSpeakingUnarmed","Gestuel de discours"],
["cl3_nightclubdance","Night Club Danse"],
["cl3_russiandance","Danse Russe"],
["cl3_hiphopdance","Danse HipHop"],
["cl3_dubstepPop","Pop Danse"],
["cl3_dubstepdance","DubStep Danse"],
["AmovPercMstpSnonWnonDnon_exerciseKata","Karate"],
["AmovPercMstpSnonWnonDnon_exercisePushup","Serie de pompes"],
["AmovPercMstpSnonWnonDnon_exercisekneeBendB","Flexion rapide"],
["AmovPercMstpSnonWnonDnon_exercisekneeBendA","Flexion lente"],
["AmovPercMstpSnonWnonDnon_Scared","A genoux"],
["AmovPercMstpSnonWnonDnon_Scared2","Face Palm à genoux"]

];
	if(__GETC__(life_coplevel) > 1) then 
	{	
	alfr_animlist pushBack["CL3_anim_halt","Halt !"];
	alfr_animlist pushBack["Acts_ShowingTheRightWay_loop","Circulez ! circulez !"];
	};
	
	if(__GETC__(life_donator) > 1) then	
	{
	alfr_animlist pushBack["CL3_anim_halt","Halt !"];
	alfr_animlist pushBack["cl3_crazydrunkdance","Danse de l'homme bourré"];
	alfr_animlist pushBack["cl3_robotdance","Danse du Robot"];
	alfr_animlist pushBack["cl3_Stephan","Danse Stephan"];
	alfr_animlist pushBack["cl3_DuoIvan","Danse Duo A"];
	alfr_animlist pushBack["cl3_DuoStephan","Danse Duo B"];
	alfr_animlist pushBack["Acts_EpicSplit","Grand écart"];
	alfr_animlist pushBack["Acts_Kore_IdleNoWeapon_loop","Main sur les hanches"];
	alfr_animlist pushBack["Acts_Kore_PointingForward","Main sur les hanches - Pointé"];
	alfr_animlist pushBack["Acts_Kore_Introducing","Main sur les hanches - Présentation"];
	alfr_animlist pushBack["cl3_anims_wank","Masturbation"];
	alfr_animlist pushBack["Acts_SittingWounded_breath","Sieste"];
	alfr_animlist pushBack["Acts_SittingWounded_wave","Sieste salutation"];
	alfr_animlist pushBack["Acts_NavigatingChopper_Loop","Rifle - Faire signe d'avancé"];
	alfr_animlist pushBack["Acts_SignalToCheck","Rifle - Mouvement de bras"];
	alfr_animlist pushBack["Acts_PercMstpSlowWrflDnon_handup1","Rifle - Saluer de loin"];
	alfr_animlist pushBack["Acts_PercMstpSlowWrflDnon_handup1b","Rifle - Saluer"];
	alfr_animlist pushBack["Acts_PercMstpSlowWrflDnon_handup2","Rifle - Faire signe de stoper"];
	alfr_animlist pushBack["Acts_PercMstpSlowWrflDnon_handup2c","Rifle - Halt !"];
	alfr_animlist pushBack["Acts_ShowingTheRightWay_loop","Rifle - Circulez ! circulez !"];
	
	};	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
{
_name = _x select 1;
diag_log format["_name %1", _name];
_anim = _x select 0;
_anims lbAdd format["%1",_name];
_anims lbSetData [(lbSize _anims)-1,_anim];
}
foreach alfr_animlist;


if(((lbSize _anims)-1) == -1) then
{
	_anims lbAdd "Vous n'avez pas d'animation disponible.";
	_anims lbSetData [(lbSize _anims)-1,str(ObjNull)];
};