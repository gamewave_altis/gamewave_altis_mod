/*
	File: fn_playAnim.sqf
	Prospere
	Altislife.fr / Gamewave

*/
if((time - life_action_delay) < 3) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_dialog","_list","_sel","_animation","_loop"];
disableSerialization;
_unit = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
_run = [_this,1,true,[false]] call BIS_fnc_param;
if(isNull _unit) exitWith {}; 
if(local _unit && !_run) exitWith {}; 

_dialog = findDisplay 60000;
_list = _dialog displayCtrl 60001;
_sel = lbCurSel _list;
if(_sel == -1) exitWith {hint "Pas d'animation séléctionné";};
_animation = _list lbData _sel;

if(vehicle _unit != _unit) exitWith{hint "Vous devez être en dehors d'un véhicule"};
//diag_log format["_animation %1", _animation];
//diag_log format["animationState %1", animationState _unit];
closeDialog 0;
life_action_inUse = true;
_loop = 0;
_Oldanim = animationState _unit;
while {true} do
{
	diag_log format["WHILE animationState %1", animationState _unit];
	if(animationState _unit != _animation) then {
		_loop = _loop + 0.5;
		if(_loop >= 1) exitWith {};
		[[_unit,_animation],"life_fnc_animSync",true,false] call life_fnc_MP;
		sleep 0.5;
		_unit switchMove _animation;
		_unit playMoveNow _animation;
		diag_log format["INSIDE IF ANIMATIONSTATE %1", animationState _unit];

	};
	sleep 1;
	if(_loop >= 1 OR !alive _unit) exitWith {};
	if((_unit getVariable "restrained")||(_unit getVariable "Escorting")||(_unit getVariable "Surrender")) exitWith {};
	if(animationState _unit == "incapacitated") exitWith {};
	if(life_istazed) exitWith {}; //Tazed
	if(life_knockout) exitWith {}; //knockout
	if(life_interrupted) exitWith {}; //lucel
};

_unit switchMove _Oldanim;
[[_unit,_Oldanim],"life_fnc_animSync",true,false] call life_fnc_MP;
//_unit switchMove "stop";
//_unit playMoveNow "stop";
if(!alive _unit OR life_istazed) exitWith {life_action_inUse = false;};
if((_unit getVariable["restrained",false])) exitWith {life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; life_action_inUse = false;};

life_action_inUse = false;