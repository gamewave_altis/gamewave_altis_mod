#include <macro.h>
/*
	File: fn_jobMenu.sqf
	Prospere
	Altislife.fr / gamewave.fr
*/
private["_display","_jobs","_jobsList","_jobAllowed","_jobname","_job"];
disableSerialization;

waitUntil {!isNull (findDisplay 60000)};
_display = findDisplay 60000;
_jobs = _display displayCtrl 60001;
lbClear _jobs;
_jobsList = [];
_jobAllowed = ["license_civ_medic","license_civ_taxi","license_civ_dep"];

{
	if(missionNamespace getVariable (_x select 0) && _x select 1 == "civ") then
	{
		if ((_x select 0) in _jobAllowed)then{
		_jobsList pushBack (_x select 0);
		};
	};
}foreach life_licenses;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//LUCEL DIAG
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
//diag_log format["_jobsList %1",_jobsList];
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		

{
	_job = _x;
	_jobname = [_x] call life_fnc_varToStr;
	_jobs lbAdd (_jobname);
	_jobs lbSetData [(lbSize _jobs)-1,_job];
}
foreach _jobsList;


if(((lbSize _jobs)-1) == -1) then
{
	_jobs lbAdd "Vous n'avez pas de métier";
	_jobs lbSetData [(lbSize _jobs)-1,str(ObjNull)];
};