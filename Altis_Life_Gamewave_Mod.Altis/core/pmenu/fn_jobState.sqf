/*
	File: fn_jobState.sqf
	Prospere
	Altislife.fr / Gamewave

*/
private["_state","_dialog","_list","_sel","_job"];
disableSerialization;
_state = _this select 0;
_dialog = findDisplay 60000;
_list = _dialog displayCtrl 60001;
_sel = lbCurSel _list;
if(_sel == -1) exitWith {hint "Pas de métier séléctionné";};
_job = _list lbData _sel;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//LUCEL DIAG
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
diag_log format["_job %1", _job];
diag_log format["_state %1", _state];
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////		
switch (_job) do 
{
	case "license_civ_medic": 
	{
	
	medicservice = _state;

	if(_state) then {
	hint parseText format["<t size='3'><t color='#00FF00'>Médecin : </t></t> <br/><t size='1.5'>Vous avez <t color='#00FF00'>activé</t> votre téléphone professionnel.</t>"];
	systemChat format["[Métier] Vous venez de prendre votre service de médecin"];
	}
	else
	{
	hint parseText format["<t size='3'><t color='#00FF00'>Médecin : </t></t> <br/><t size='1.5'>Vous avez <t color='#FF0000'>désactivé</t> votre téléphone professionnel, vous ne pouvez plus recevoir de demande téléphonique</t>"];
	systemChat format["[Métier] Vous venez de stopper votre service de médecin"];
	};
	};
	
	case "license_civ_taxi": 
	{
	taxiservice = _state;
	if(_state) then {
	hint parseText format["<t size='3'><t color='#00FF00'>Taxi : </t></t> <br/><t size='1.5'>Vous avez <t color='#00FF00'>activé</t> votre téléphone professionnel.</t>"];
	systemChat format["[Métier] Vous venez de prendre votre service de Taxi"];
	}
	else
	{
	hint parseText format["<t size='3'><t color='#00FF00'>Taxi : </t></t> <br/><t size='1.5'>Vous avez <t color='#FF0000'>désactivé</t> votre téléphone professionnel, vous ne pouvez plus recevoir de demande téléphonique</t>"];
	systemChat format["[Métier] Vous venez de stopper votre service de Taxi"];
	};
	};	
	
	case "license_civ_dep": 
	{
	depservice = _state;
	if(_state) then {
	hint parseText format["<t size='3'><t color='#00FF00'>Dépanneur : </t></t> <br/><t size='1.5'>Vous avez <t color='#00FF00'>activé</t> votre téléphone professionnel.</t>"];
	systemChat format["[Métier] Vous venez de prendre votre service de Dépanneur"];
	}
	else
	{
	hint parseText format["<t size='3'><t color='#00FF00'>Dépanneur : </t></t> <br/><t size='1.5'>Vous avez <t color='#FF0000'>désactivé</t> votre téléphone professionnel, vous ne pouvez plus recevoir de demande téléphonique</t>"];
	systemChat format["[Métier] Vous venez de stopper votre service de Dépanneur"];
	};
	};
};