/*
	File: fn_useItem.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main function for item effects and functionality through the player menu.
*/
if((time - life_action_delay) < 1) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_item"];
disableSerialization;
if((lbCurSel 2005) == -1) exitWith {hint "Il faut deja choisir un objet !";};
_item = lbData[2005,(lbCurSel 2005)];

switch (true) do
{
	case (_item == "water" or _item == "coffee" or _item == "verredeau"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			player setFatigue 0;
		};
		
	};
	
	case (_item == "boltcutter"): {
		[cursorTarget] spawn life_fnc_boltcutter;
		closeDialog 0;
	};
	
	case (_item == "blastingcharge"): {
		player reveal fed_bank;
		(group player) reveal fed_bank;
		[cursorTarget] spawn life_fnc_blastingCharge;
				closeDialog 0;
	};
	
	case (_item == "scannerlucel"): {
		[cursorTarget] spawn life_fnc_scannerlucel;
				closeDialog 0;
	};
	
	case (_item == "defusekit"): {
		[cursorTarget] spawn life_fnc_defuseKit;
				closeDialog 0;
	};
	
	case (_item in ["storagesmall","storagebig"]): {
		[_item] call life_fnc_storageBox;
				closeDialog 0;
	};
	
	case (_item == "redgull"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			player setFatigue 0;
			[] spawn
			{
				life_redgull_effect = time;
				titleText["Tu peux courrir comme un fou pendant 5 minutes ! Pas le temps de niaiser","PLAIN"];
				player enableFatigue false;
				waitUntil {!alive player OR ((time - life_redgull_effect) > (5 * 60))};
				player enableFatigue true;
			};
		};
	};
	
	case (_item == "spikeStrip"):
	{
		if(!isNull life_spikestrip) exitWith {hint "Tu as déjà une herse de déployée."};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_spikeStrip;
					closeDialog 0;
		};
	};
	
	case (_item == "mauer"):
	{
		if(!isNull life_mauer) exitWith {hint "Vous avez déja déployé votre mur."};
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_mauer;
					closeDialog 0;
		};
	};

	case (_item == "fuelF"):
	{
		if(vehicle player != player) exitWith {hint "Tu ne peux pas faire ça !"};
		[] spawn life_fnc_jerryRefuel;
	};
	
	case (_item == "biere"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			[] spawn life_fnc_beer;
		};
	};
	case (_item == "meth"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if (vehicle player == player) THEN {[[getPlayerUID player,name player,"1050"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;};
			life_thirst = 100;
			life_hunger = 100;
			player setFatigue 0;
			player setDamage (damage player + 0.05);
			[] spawn life_fnc_meth;
		};
	};
	
	case (_item == "marijuana"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if (vehicle player == player) THEN {[[getPlayerUID player,name player,"1050"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;};
			life_thirst = 100;
			life_hunger = 100;
			player setFatigue 0;
			[] spawn life_fnc_weed;
		};
	};
	case (_item == "cannabis"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			if (vehicle player == player) THEN {[[getPlayerUID player,name player,"1050"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;};
			life_thirst = 100;
			life_hunger = 100;
			player setFatigue 0;
			[] spawn life_fnc_weed;
		};
	};
		case (_item == "uranium"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_uranium;
		};
	};
		case (_item == "uraniump"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			[] spawn life_fnc_uraniump;
		};
	};
	case (_item == "heroinp"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			life_hunger = 100;
			if (vehicle player == player) THEN {[[getPlayerUID player,name player,"1050"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;};
			player setFatigue 0;
			player setDamage (damage player + 0.04);
			[] spawn life_fnc_hero;
		};
	};
	case (_item == "cocainep"):
	{
		if(([false,_item,1] call life_fnc_handleInv)) then
		{
			life_thirst = 100;
			life_hunger = 100;
			if (vehicle player == player) THEN {[[getPlayerUID player,name player,"1050"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;};
			player setDamage (damage player + 0.03);
			player setFatigue 0;
			[] spawn life_fnc_coke;
		};
	};
	case (_item == "lockpick"):
	{
		[] spawn life_fnc_lockpick;
	};	
	// TRACEUR
	case (_item == "trackerlucel"):
	{
		[cursorTarget] spawn life_fnc_trackerlucel;
	};
	
	case (_item in ["apple","rabbit","salema","ornate","mackerel","tuna","mullet","catshark","turtle","turtlesoup","donuts","tbacon","peach","pain"]):
	{
		[_item] call life_fnc_eatFood;
	};
	
	case "fishing":
	{
		[] spawn fnc_fishing;
	};
	
	case (_item == "pickaxe"):
	{
		[] spawn life_fnc_pickAxeUse;
	};
	//House placement coffre
	case (_item in ["storage1","storage2"]):
        {
		[_item] call life_fnc_placeStorage;
     };	
	
	default
	{
		hint "Cet objet n'est pas consommable !";
	};
};
	
[] call life_fnc_p_updateMenu;
[] call life_fnc_hudUpdate;