//player addEventHandler["FiredNear", {_this call life_fnc_lacrymo}];
private["_unit","_firer","_distance","_weapon","_muzzle","_mode","_ammo"];

_unit = _this select 0; //obj
_firer = _this select 1; //obj
_distance = _this select 2; //number
_weapon = _this select 3; //string
_muzzle = _this select 4; //string
_mode = _this select 5; //mode
_ammo = _this select 6; //string
diag_log format["_unit %1 %",_unit];
diag_log format["_firer %1 %",_firer];
diag_log format["_weapon %1 %",_weapon];
diag_log format["_distance %1 %",_distance];
diag_log format["_muzzle %1 %",_muzzle];
diag_log format["_mode %1 %",_mode];
if(_weapon != "Throw") exitWith{};
if(_distance > 100) exitWith{};
_activate = if((_weapon == "Throw")) then {true} else {false};

While{_activate} do //boucle while = caca
{
    "dynamicBlur" ppEffectEnable true; // enables ppeffect
    "dynamicBlur" ppEffectAdjust [0]; // enables normal vision
    "dynamicBlur" ppEffectCommit 15; // time it takes to normal
    resetCamShake; // resets the shake
    20 fadeSound 1;     //fades the sound back to normal


    waituntil{
    ((nearestObject [getpos player, "SmokeShell","1Rnd_Smoke_Grenade_shell"]) distance player < 10)
    and
    (getpos (nearestObject [getpos player, "SmokeShell","1Rnd_Smoke_Grenade_shell"]) select 2 < 0.5)
    };

    if (headgear player != "H_PilotHelmetFighter_B"
    && headgear player != "H_PilotHelmetFighter_O"
    && headgear player != "H_PilotHelmetFighter_I"
	) then
    {
		titleText ["DU GAZ LACRYMOGENE MEC! COURS!", "PLAIN"];
        "dynamicBlur" ppEffectEnable true; // enables ppeffect
        "dynamicBlur" ppEffectAdjust [20]; // intensity of blur
        "dynamicBlur" ppEffectCommit 3; // time till vision is fully blurred
        enableCamShake true;     // enables camera shake
        addCamShake [10, 45, 10];    // sets shakevalues
        player setFatigue 1; // sets the fatigue to 100%
        5 fadeSound 0.1;     // fades the sound to 10% in 5 seconds

    };
    
   // sleep 5;
    waituntil{
    ((nearestObject [getpos player, "SmokeShell","1Rnd_Smoke_Grenade_shell"]) distance player > 10)
    and
    (getpos (nearestObject [getpos player, "SmokeShell","1Rnd_Smoke_Grenade_shell"]) select 2 < 0.5)
    };
	_activate = false;
};