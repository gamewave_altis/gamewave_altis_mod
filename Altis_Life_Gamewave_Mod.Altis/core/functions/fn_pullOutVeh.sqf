/*
	File: fn_pullOutVeh.sqf
	Author: Bryan "Tonic" Boardwine
	Traduction : Gamewave
*/
if(playerSide == west && !(player getVariable "restrained")) exitWith {};



if(player getVariable "restrained") then
{
	detach player;
	player setVariable["Escorting",false,true];
	player setVariable["transporting",false,true];
	player action ["Eject", vehicle player];
	titleText["Vous vous êtes fait éjècter du vehicule.","PLAIN"];
	titleFadeOut 4;
	
	player switchMove "AmovPercMstpSnonWnonDnon";
	player playMoveNow "AmovPercMstpSnonWnonDnon";
	player playMove "AmovPercMstpSnonWnonDnon_Ease";
	sleep 1;
}
else
{
	player action ["Eject", vehicle player];
	titleText["Vous vous êtes fait éjècter du vehicule.","PLAIN"];
	titleFadeOut 4;
};

















	


