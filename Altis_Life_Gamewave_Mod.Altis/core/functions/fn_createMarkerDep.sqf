/*
Create DepMarker
Lucel
Altislife.fr
*/
if(!(license_civ_dep)) exitWith {};
Private["_playerName","_pos","_marker","_text"];
if(isServer) exitWith {};
_playerName = [_this,0,"",[""]] call BIS_fnc_param;
_prefix = [_this,2,"-DEPANNAGE-",["-DEPANNAGE-"]] call BIS_fnc_param;
_pos = _this select 1;
_text = format["%2 %1 -DEPANNAGE-", _playerName, _prefix];
_playerName = format["%2%1", _playerName, _prefix];
deleteMarkerLocal _playerName;
_marker = createMarkerLocal [_playerName, _pos];
_marker setMarkerShapeLocal "ICON";
_marker setMarkerTypeLocal "mil_dot";
_marker setMarkerColorLocal "ColorWhite";
_marker setMarkerTextLocal _text;
diag_log format ["MARKERDEP || playername: %1 _pos :%2 _marker : %3", _playerName, _pos, _marker];
sleep 300;
deleteMarkerLocal _playerName;
