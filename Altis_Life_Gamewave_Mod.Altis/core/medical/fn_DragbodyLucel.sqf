private ["_target"];
_target = cursorTarget;
diag_log format ["------------Dragbodylucel _Target: %1", _target];

FAR_isDragging = true;

_target enableSimulation true;

_target attachTo [player, [0, 1.1, 0.092]];

_target setVariable["currentlyEscorting",true,true];
player reveal _target;
_target setDir 180;
_target setVariable ["FAR_isDragged", 1, true];
	
player playMoveNow "AcinPknlMstpSrasWrflDnon";
	
// Rotation fix
FAR_isDragging_EH = _target;
publicVariable "FAR_isDragging_EH";
		
// Wait until release action is used
waitUntil 
{ 
	!alive player || !alive _target || !FAR_isDragging || _target getVariable "FAR_isDragged" == 0 
};

// Handle release action
