/*
	File: fn_sirenLights.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Lets play a game! Can you guess what it does? I have faith in you, if you can't
	then you have failed me and therefor I lose all faith in humanity.. No pressure.
*/
if((time - life_delay) < 3) exitWith {};
life_delay = time;
private["_vehicle"];
_vehicle = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _vehicle) exitWith {}; //Bad entry!
if(!(typeOf _vehicle in ["C_SUV_01_F",
			"C_Hatchback_01_F",
			"C_Hatchback_01_sport_F",
			"B_Heli_Light_01_F",
			"cl3_suv_emt",
			"cl3_escalade_pm",
			"cl3_transitemt",
			"cl3_dodge_charger_emtpa",
			"O_Heli_Light_02_unarmed_F"
])) exitWith {}; //Last chance check to prevent something from defying humanity and creating a monster.

_trueorfalse = _vehicle getVariable["lightsmedic",FALSE];

if(_trueorfalse) then {
	_vehicle setVariable["lightsmedic",FALSE,TRUE];
} else {
	_vehicle setVariable["lightsmedic",TRUE,TRUE];
	[[_vehicle,0.22],"life_fnc_medicLights",true,false] call life_fnc_MP;
};