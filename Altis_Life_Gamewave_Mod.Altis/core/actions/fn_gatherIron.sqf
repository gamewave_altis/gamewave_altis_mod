if(life_action_inUse) exitWith {};
if((animationState player) == "CL3_anim_WeaponHit") exitWith {};
if((time - life_action_delay) < 3) exitWith {};
life_action_delay = time;
private["_sum","_rand"];

_rand=floor(random 5)+1;
_sum = ["ironore",_rand,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
if (!("cl_pick_axeweap" in weapons player)) exitWith {hint "Vous avez besoin d'une pioche pour effectuer cette action.";life_action_inUse = false;};
if(_sum > 0) then
{
	life_action_inUse = true;
	titleText["Récolte de Fer...","PLAIN"];
	titleFadeOut 5;
	[[player, "mining",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;
	for "_i" from 0 to 2 do
	{
		player playmoveNow "CL3_anim_WeaponHit";
		waitUntil{animationState player != "CL3_anim_WeaponHit";};
		sleep 1.5;
		if (_i == 2) then {life_action_inUse = false;};
	};
	if(([true,"ironore",_sum] call life_fnc_handleInv)) then
	{
		titleText[format["Vous avez collecté %1 fer",_sum],"PLAIN"];
	};
};