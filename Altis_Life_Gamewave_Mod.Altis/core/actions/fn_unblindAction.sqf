//Altislife.fr
//Lucel
//Yianas
private["_unit","_upp","_ui","_progress","_pgText","_cP"];
_unit = cursorTarget;

life_interrupted = false;
if(isNull _unit) exitWith {};
if(player distance _unit > 5 || !alive player || !alive _unit) exitWith {hint "Rapproche toi de lui pour lui enlever la cagoule"};
//hint "Fouille...";

	life_action_inUse = true;
	_upp = "Retirer cagoule";
	//[[player, "Fouille",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;
	//Setup our progress bar.
	disableSerialization;
	5 cutRsc ["life_progress","PLAIN"];
	_ui = uiNameSpace getVariable "life_progress";
	_progress = _ui displayCtrl 38201;
	_pgText = _ui displayCtrl 38202;
	_pgText ctrlSetText format["%2 (1%1)...","%",_upp];
	_progress progressSetPosition 0.01;
	_cP = 0.01;
	while{true} do
	{
		if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
		};
		sleep 0.09;
		_cP = _cP + 0.05;
		_progress progressSetPosition _cP;
		_pgText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_upp];
		if(_cP >= 1) exitWith {};
		if(player distance _unit > 5) exitWith {};
		if(!alive player) exitWith {};
		if(life_interrupted) exitWith {};

	};
	life_action_inUse = false;
	player playActionNow "stop";
	5 cutText ["","PLAIN"];
	if(!alive player) exitWith {};
	if(life_interrupted) exitWith {life_interrupted = false; titleText["Action annulé","PLAIN"]; life_action_inUse = false;};

_unit setVariable["imBlind",false,true];
life_action_inUse = true;
	
	