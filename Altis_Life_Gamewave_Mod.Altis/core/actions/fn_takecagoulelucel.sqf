/*
takecagoulelucel.sqf
think & tweak bylecul
altislife.fr
*/

private["_unit","_headgear","_goggles","_headgeartobackpack","_gogglestobackpack"];
_unit = cursorTarget;
_headgeartobackpack = headgear _unit;
_gogglestobackpack = Goggles _unit;

player playMove "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";
waitUntil{animationState player != "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";};
sleep 1;
[[2,format["Quelqu'un vient de t'arracher ton masque...", name _unit, name player]],"life_fnc_broadcast",_unit,false] spawn life_fnc_MP;
titleText[format["Vous avez arraché le masque de cette personne..."],"PLAIN"];

if(headgear _unit in alfr_headgear) then {
removeHeadGear _unit;
player addItemToBackpack _headgeartobackpack;
}
else
{
	if(Goggles _unit in alfr_googles) then {
	removeGoggles _unit;
	player addItemToBackpack _gogglestobackpack;
};
};

