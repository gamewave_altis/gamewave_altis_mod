/*
fn_promoteMerc.sqf
lucel
*/
systemChat format["Vous avez été promu Mercenaire. Félicitation !"];
hint parseText format["<t size='3'><t color='#00FF00'>Mercenaire</t></t> <br/><t size='1.5'>Félicitation vous avez obtenu votre accréditation de Mercenaire !</t>"];
		
license_civ_taxi = false;
license_civ_dep = false;
license_civ_medic = false;
license_civ_rebel = false;
license_civ_merc = true;