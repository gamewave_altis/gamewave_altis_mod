/*
	File: fn_pumpRepair.sqf
	
	Description:
	Quick simple action that is only temp.
*/
private["_method"];
if(life_flouze < 500) then
{
	if(life_dabflouze < 500) exitWith {_method = 0;};
	_method = 2;
}
	else
{
	_method = 1;
};

switch (_method) do
{
	case 0: {hint "Tu n'as pas 500€ !"};
	case 1: {vehicle player setDamage 0; life_flouze = life_flouze - 500; hint "Tu as réparé ton véhicule pour 500€";};
	case 2: {vehicle player setDamage 0; life_dabflouze = life_dabflouze - 500; hint "Tu as réparé ton véhicule pour 500€";};
};