if(life_action_inUse) exitWith {};
if((animationState player) == "cl3_anim_shovel") exitWith {};
if((time - life_action_delay) < 3) exitWith {};
life_action_delay = time;
private["_sum","_rand"];

_rand=floor(random 3)+2;
_sum = ["rock",_rand,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
if (!("cl_shovel" in weapons player)) exitWith {hint "Vous avez besoin d'une pelle pour effectuer cette action.";life_action_inUse = false;};
if(_sum > 0) then
{
	life_action_inUse = true;
	titleText["Récolte du gravier...","PLAIN"];
	titleFadeOut 5;
	[[player, "mining",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;
	for "_i" from 0 to 2 do
	{
		player playmoveNow "cl3_anim_shovel";
		//[[player,"cl3_anim_shovel"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		waitUntil{animationState player != "cl3_anim_shovel";};

		if (_i == 2) then {life_action_inUse = false;};
	};
	if(([true,"rock",_sum] call life_fnc_handleInv)) then
	{
		titleText[format["Vous avez collecté %1 kg de gravier",_sum],"PLAIN"];
	};
}
else
{
	hint "Ton inventaire est plein !";
};