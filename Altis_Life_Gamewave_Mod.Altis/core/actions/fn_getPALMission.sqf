/*

	File: fn_getPALMission.sqf
	Author: For GameWave
	Donne une mission de Retour Palettes Pegasus avec control de véhicule
	
*/
	
	
private["_nearVehicles","_pal","_target","_vehicle","_color","_lieu"];

	_nearVehicles = nearestObjects[getPos (_this select 0),["Car","Air","Ship"],20]; //Recherche de vehicules dans les 20 m
	
if(count _nearVehicles > 0) then
	{
		{
			if(!isNil "_vehicle") exitWith {}; //Evite les boucles.
				_vehData = _x getVariable["vehicle_info_owners",[]];

			if(count _vehData  > 0) then
			{
				_vehOwner = (_vehData select 0) select 0;
				if((getPlayerUID player) == _vehOwner) exitWith
				{
					_vehicle = _x;
				};
			};
		} foreach _nearVehicles;
	};

if(isNil "_vehicle") exitWith {["MessagePNJ",["T'es venu à pied? Rapproches ton camion qu'on le charge..."]] call BIS_fnc_showNotification };
if(isNull _vehicle) exitWith {};

if (typeOf _vehicle != "I_Truck_02_box_F") then 
	{
		["MessagePNJ",["Hey, il te faut le camion Pegasus"]] call BIS_fnc_showNotification 
	};
		
_veh = _vehicle;
_color = [(typeOf _veh),(_veh getVariable "Life_VEH_color")] call life_fnc_vehicleColorStr;

if (typeOf _vehicle == "I_Truck_02_box_F") then 
	{
		["MessagePNJ",["Viens avec le Camion Pegasus..."]] call BIS_fnc_showNotification; 
	};	

	
if (typeOf _vehicle == "I_Truck_02_box_F") then 
{
	_target = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;

		if(str(_target) in life_trp_points) then
		{
			_pal = "Pegasus_airport";
		}
		else
		{
			["MessagePNJ",["J'ai rien pour toi..."]] call BIS_fnc_showNotification; 
		};

	life_pal_start = _target;
	life_delivery_in_progress = true;
	life_pal_point = call compile format["%1",_pal];

	_pal = [_pal,"_"," "] call KRON_Replace;
	_lieu = _pal;	
		life_cur_task = player createSimpleTask [format["Retour_Palettes_%1",life_pal_point]];
		life_cur_task setSimpleTaskDescription [format["Rapporte ces palettes à la Centrale %1 (GPS: 153.161)",_lieu],"Mission de Livraison Pegasus",""];
		life_cur_task setTaskState "Assigned";
		player setCurrentTask life_cur_task;
		
		["DeliveryAssigned",["Rapporte ces palettes à la Centrale (GPS: 153.161)"]] call BIS_fnc_showNotification;
		
					
};

[] spawn
	{
		waitUntil {!life_delivery_in_progress OR !alive player};
		if(!alive player) then
		{
			life_cur_task setTaskState "Failed";
			player removeSimpleTask life_cur_task;
			["DeliveryFailed",["Tu es mort... Tu as perdu ta Mission Pegasus."]] call BIS_fnc_showNotification;
			life_delivery_in_progress = false;
			life_pal_point = nil;
		};
	};

