/*
	File: fn_serviceTruck.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main functionality for the service truck.
	*Needs to be revised for new system and flow*
*/
if((time - life_action_delay) < 15) exitWith {hint "Doucement ! Un client à la fois !"};
life_action_delay = time;
life_interrupted = false;
if(life_action_inUse) exitWith {};

private["_nearby","_distance","_title","_progressBar","_cP","_titleText","_badDistance","_uid","_name","_pid","_unit","_index"];

_nearby = nearestObjects[(vehicle player),["Car","Ship","Air","Motorbike"],10];
if(count (_nearby) > 1) then
{

		_vehicle = _nearby select 1;
		_title = format["Ravitaillement sur %1",getText(configFile >> "CfgVehicles" >> (typeOf _vehicle) >> "displayName")];
		life_action_inUse = true; //Lock out other actions
		//Setup the progress bar
		disableSerialization;
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNamespace getVariable "life_progress";
		_progressBar = _ui displayCtrl 38201;
		_titleText = _ui displayCtrl 38202;
		_titleText ctrlSetText format["%2 (1%1)...","%",_title];
		_progressBar progressSetPosition 0.01;
		_cP = 0.1;
		//_cP = 0.01;
		[[player, "RepairTruck",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;
		while {true} do
		{

			sleep 0.25;
			_cP = _cP + 0.1;
			_progressBar progressSetPosition _cP;
			_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
			if(_cP >= 1 OR !alive player) exitWith {};
			if(life_interrupted) exitWith {};
			if((vehicle player) distance _vehicle > 10) exitWith {_badDistance = true;};
		};
		//Kill the UI display and check for various states
		5 cutText ["","PLAIN"];
		if(!isNil "_badDistance") exitWith {titleText["Vous êtes trop loin du véhicule","PLAIN"]; life_action_inUse = false;};
		if(life_interrupted) exitWith {life_interrupted = false; titleText["Action annulé","PLAIN"]; life_action_inUse = false;};



		_name = getText(configFile >> "CfgVehicles" >> (typeOf _vehicle) >> "displayName");
		//titleText[format["Ravitaillement du %1 en cours...",_name],"PLAIN"];
		//[[player, "RepairTruck",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;
		//titleFadeOut 12;
		//sleep 15;
		//if((vehicle player) distance _vehicle > 10) exitWith {titleText["Vehicule trop loin...","PLAIN"];};
		titleText[format["Tu as ravitaillé un %1",_name],"PLAIN"];
	if(!local _vehicle) then
	{
		[{_vehicle setFuel 1;},"BIS_fnc_spawn",_vehicle,false] spawn life_fnc_MP;
	}
		else
	{
		_vehicle setFuel 1;
	};
	life_action_inUse = false; //Lock out other actions
};


