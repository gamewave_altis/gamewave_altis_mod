if(life_action_inUse) exitWith {};
if((animationState player) == "cl3_anim_shovel") exitWith {};
if((time - life_action_delay) < 3) exitWith {};
life_action_delay = time;
private["_sum","_rand"];

_rand=floor(random 4)+2;
_sum = ["salt",_rand,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
if (!("cl_shovel" in weapons player)) exitWith {hint "Vous avez besoin d'une pelle pour effectuer cette action.";life_action_inUse = false;};
if(_sum > 0) then
{
	life_action_inUse = true;
	titleText["Récolte de sel...","PLAIN"];
	titleFadeOut 5;
	[[player, "mining",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;
	for "_i" from 0 to 1 do
	{
		player playmoveNow "cl3_anim_shovel";
		waitUntil{animationState player != "cl3_anim_shovel";};
		sleep 1.5;
		if (_i == 1) then {life_action_inUse = false;};
	};
	if(([true,"salt",_sum] call life_fnc_handleInv)) then
	{
		titleText[format["Vous avez collecté %1 kilos de sel",_sum],"PLAIN"];
	};
}
else
{
	hint "Ton inventaire est plein !";
};