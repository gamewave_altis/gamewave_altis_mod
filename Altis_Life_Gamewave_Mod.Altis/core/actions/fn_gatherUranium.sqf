if(life_action_inUse) exitWith {};
if((animationState player) == "CL3_anim_Gathering4") exitWith {};
if((time - life_action_delay) < 3) exitWith {};
life_action_delay = time;
private["_sum","_rand"];

_rand=floor(random 8)+2;
_sum = ["Uranium",_rand,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
if(_sum > 0) then
{
	life_action_inUse = true;
	titleText["Extraction d'Uranium...","PLAIN"];
	titleFadeOut 5;
	for "_i" from 0 to 2 do
	{
		player playMove "CL3_anim_Gathering4";
		waitUntil{animationState player != "CL3_anim_Gathering4";};
		sleep 2.5;
		if (_i == 1) then {life_action_inUse = false;};
	};
	if(([true,"uranium",_sum] call life_fnc_handleInv)) then
	{
		titleText[format["Vous avez récolté %1 d'Uranium",_sum],"PLAIN"];
	};
}
else
{
	hint "Ton inventaire est plein !";
};