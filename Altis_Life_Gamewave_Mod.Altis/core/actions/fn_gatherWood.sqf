if(life_action_inUse) exitWith {};
if((animationState player) == "AwopPercMstpSgthWnonDnon_throw") exitWith {};
if((time - life_action_delay) < 3) exitWith {};
life_action_delay = time;
private["_sum","_rand"];

_rand=floor(random 4)+2;
_sum = ["salt",_rand,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
if (!("cl_axe" in weapons player)) exitWith {hint "Vous avez besoin d'une hâche pour effectuer cette action.";life_action_inUse = false;};
if(_sum > 0) then
{
	life_action_inUse = true;
	titleText["Coupage du bois...","PLAIN"];
	titleFadeOut 5;
	[[player, "mining",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;
	for "_i" from 0 to _rand do
	{
		player playmoveNow "AwopPercMstpSgthWnonDnon_throw";
		waitUntil{animationState player != "AwopPercMstpSgthWnonDnon_throw";};
		sleep 2.5;
		if (_i == _rand) then {life_action_inUse = false;};
	};
	if(([true,"wood",_sum] call life_fnc_handleInv)) then
	{
		titleText[format["Vous avez coupé %1 bûches",_sum],"PLAIN"];
	};
}
else
{
	hint "Ton inventaire est plein !";
};