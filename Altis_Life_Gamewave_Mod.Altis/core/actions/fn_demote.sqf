/*
fn_promote.sqf
lucel
*/
private["_unit","_id","_name"];
_unit = lbData[2902,lbCurSel (2902)];
_unit = call compile format["%1", _unit];

//diag_log format["PROMOTE"];

// Check Block
if(isNull _unit) exitWith {};
if(isNil "_unit") exitwith {}; 
if(!(_unit isKindOf "Man")) exitWith {};
if(!isPlayer _unit) exitWith {}; 
if(side _unit != civilian) exitWith {}; 
if(isNull _unit) exitWith {}; 
_name = name _unit;
//diag_log format["CHECK END"];
//[[_unit,false],"life_fnc_promoteMerc",_unit,false] spawn life_fnc_MP;
	_action = [
				format["Etes vous sur de vouloir retirer la licence mercenaire de <t color='#00FF00'> %1</t><br/>",_name],
				"Mercenaire",
				"Oui",
				"Non"
			] call BIS_fnc_guiMessage;


	if(_action) then {
		[[_unit,false],"life_fnc_demoteMerc",_unit,false] spawn life_fnc_MP;

	} else {
		closedialog 0;
	};	


