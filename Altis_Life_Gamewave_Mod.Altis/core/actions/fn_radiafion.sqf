/*
While Damage on time
Tipix for altislife.fr
Edit by Lucel
*/
if(isServer) exitWith {};

private ["_nearPercent","_random","_casqueAntirad","_combiAntirad","_geiger"];
	//_casqueAntirad = ["H_PilotHelmetFighter_I"];
	_combiAntirad = ["alfr_coveralls_antirad","CL3_emt_uniform","cl3_pilot_CGP_uniform"];
	while {true} do {
		sleep 5;
		if (isNil "ZoneDeRadiaFion") exitwith{};
		if !(alive player) exitwith{};
		diag_log format["WHILE TRUE"];
		while {alive player} do {
		diag_log format["WHILE RADIAZONE"];
			if !(alive player) exitwith{};
			if (isNil "ZoneDeRadiaFion") exitwith{};
			if (!isNil "ZoneDeRadiaFion") then {
				_nearPercent = [ZoneDeRadiaFion, player] call CalculateNearNess;
					_gun = handgunWeapon player;
					if (_gun == "geiger") then {
					_geiger = true;
					}else{
					_geiger = false;
					};
				diag_log format["IN RADIAZONE %1 %",_nearPercent];
				diag_log format["_gun %1 %",_gun];
				diag_log format["GEIGER %1",_geiger];
				
				//Entre 0 et 50%
				if (
				(_nearPercent > 25) && (_nearPercent < 50)
				&& !(uniform player in _combiAntirad)
				
				) then {
					"chromAberration" ppEffectEnable true;
					"radialBlur" ppEffectEnable true;
					enableCamShake true;
					"FilmGrain" ppEffectEnable true;
					"dynamicBlur" ppEffectEnable true;
					"chromAberration" ppEffectAdjust [0,0,true];
					"chromAberration" ppEffectCommit 2;   
					"radialBlur" ppEffectAdjust [0,0,0,0];
					"radialBlur" ppEffectCommit 2;
					"dynamicBlur" ppEffectAdjust [0];
					"dynamicBlur" ppEffectCommit 2; 
					"FilmGrain" ppEffectAdjust [0, 0, 0, 1, 1, true];
					"FilmGrain" ppEffectCommit 2; 
					addcamShake[0,2,0];
				};
				if (_geiger) then {
					if (((_nearPercent > 1) && (_nearPercent < 50))) then {
						_random = (round(random 2));
						diag_log format["BIPBIP %1",_geiger];
						diag_log format["GEIGER %1",_geiger];
						if (_random == 0) then 
						{
							player say3D "lowRadiaFion";
						};
						if (_random == 1) then 
						{
							player say3D "lowRadiaFion1";
						};
						if (_random == 2) then 
						{
							player say3D "lowRadiaFion2";
						};
					};
				};
				
				//Entre 50 et 75%
				if (
				(_nearPercent > 50) && (_nearPercent < 75)
				&& !(uniform player in _combiAntirad)	
				) then {
					player setDamage (damage player + (_nearPercent/3000));
					"chromAberration" ppEffectEnable true;
					"radialBlur" ppEffectEnable true;
					enableCamShake true;
					"FilmGrain" ppEffectEnable true;
					"dynamicBlur" ppEffectEnable true;
					"chromAberration" ppEffectAdjust [0.01,0.01,true];
					"chromAberration" ppEffectCommit 2;   
					"radialBlur" ppEffectAdjust [0.20,0.20,0.6,0.6];
					"radialBlur" ppEffectCommit 2;
					"dynamicBlur" ppEffectAdjust [1];
					"dynamicBlur" ppEffectCommit 2; 
					"FilmGrain" ppEffectAdjust [0.1, 10, 5, 1, 1, true];
					"FilmGrain" ppEffectCommit 2; 
					addcamShake[4,2,4];
				};
				if (_geiger) then {
					if (((_nearPercent > 50) && (_nearPercent < 75))) then {
						_random = (round(random 2));
						diag_log format["BIPBIP %1",_geiger];
						diag_log format["GEIGER %1",_geiger];
						if (_random == 0) then 
						{
							player say3D "medRadiaFion";
						};
						if (_random == 1) then 
						{
							player say3D "medRadiaFion1";
						};
						if (_random == 2) then 
						{
							player say3D "medRadiaFion2";
						};
					};
				};
				
				//Entre 75 et 95%
				if (
				(_nearPercent > 75) && (_nearPercent < 95)	
				&& !(uniform player in _combiAntirad)	
				) then {
					player setDamage (damage player + (_nearPercent/2500));
					"chromAberration" ppEffectEnable true;
					"radialBlur" ppEffectEnable true;
					enableCamShake true;
					"FilmGrain" ppEffectEnable true;
					"dynamicBlur" ppEffectEnable true;
					"chromAberration" ppEffectAdjust [0.02,0.02,true];
					"chromAberration" ppEffectCommit 2;   
					"radialBlur" ppEffectAdjust [0.4,0.4,0.50,0.50];
					"radialBlur" ppEffectCommit 2;
					"dynamicBlur" ppEffectAdjust [2];
					"dynamicBlur" ppEffectCommit 2; 
					"FilmGrain" ppEffectAdjust [0.3, 15, 5, 1, 1, true];
					"FilmGrain" ppEffectCommit 2; 
					addcamShake[6,2,6];
				};
				if (_geiger) then {
					if (((_nearPercent > 75) && (_nearPercent < 95))) then {
						_random = (round(random 2));
						diag_log format["BIPBIP %1",_geiger];
						diag_log format["GEIGER %1",_geiger];
						if (_random == 0) then 
						{
							player say3D "highRadiaFion";
						};
						if (_random == 1) then 
						{
							player say3D "highRadiaFion1";
						};
						if (_random == 2) then 
						{
							player say3D "highRadiaFion2";
						};
					};
				};
				//Supérieur à 95%
				if ((_nearPercent > 95) 				
				&& !(uniform player in _combiAntirad)	
					) then {
					player setDamage (damage player + (_nearPercent/2000));
					"chromAberration" ppEffectEnable true;
					"radialBlur" ppEffectEnable true;
					enableCamShake true;
					"FilmGrain" ppEffectEnable true;
					"dynamicBlur" ppEffectEnable true;
					"chromAberration" ppEffectAdjust [0.03,0.03,true];
					"chromAberration" ppEffectCommit 2;   
					"radialBlur" ppEffectAdjust  [1,1,0.50,0.50];
					"radialBlur" ppEffectCommit 2;
					"dynamicBlur" ppEffectAdjust [3];
					"dynamicBlur" ppEffectCommit 2; 
					"FilmGrain" ppEffectAdjust [0.5, 20, 5, 1, 1, true];
					"FilmGrain" ppEffectCommit 2; 
					addcamShake[7,2,7];
				};
				if (_geiger) then {
					if ((_nearPercent > 95)) then {
						_random = (round(random 2));
						diag_log format["BIPBIP %1",_geiger];
						diag_log format["GEIGER %1",_geiger];
						if (_random == 0) then 
						{
							player say3D "highRadiaFion";
						};
						if (_random == 1) then 
						{
							player say3D "highRadiaFion1";
						};
						if (_random == 2) then 
						{
							player say3D "highRadiaFion2";
						};	
					};
				};
			} else {
			"chromAberration" ppEffectEnable false;
			"radialBlur" ppEffectEnable false;
			"FilmGrain" ppEffectEnable false;
			"dynamicBlur" ppEffectEnable false;	
			};
		   sleep 2;
		};
	};
