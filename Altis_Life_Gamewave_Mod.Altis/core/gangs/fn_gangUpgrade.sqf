#include <macro.h>
/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Determinds the upgrade price and blah
*/
private["_maxMembers","_slotUpgrade","_upgradePrice"];
_maxMembers = grpPlayer getVariable ["gang_maxMembers",8];
_slotUpgrade = _maxMembers + 4;
_upgradePrice = round(_slotUpgrade * (__GETC__(life_gangUpgradeBase)) / (__GETC__(life_gangUpgradeMultipler)));

_action = [
	format["Vous êtes sur le point de mettre à niveau votre gang<br/><br/>Nombre de membres max : %1<br/>Après : %2<br/>Prix: <t color='#8cff9b'>%3€</t>",_maxMembers,_slotUpgrade,[_upgradePrice] call life_fnc_numberText],
	"Augmentation de la taille du gang",
	"Achat",
	"Annuler"
] call BIS_fnc_guiMessage;

if(_action) then {
	if(life_dabflouze < _upgradePrice) exitWith {
		hint parseText format[
			"Pas assez d'argent sur votre compte pour augmenter la capacité maximum de votre gang.<br/><br/>Coût: <t color='#8cff9b'>%1€</t><br/>Manque: <t color='#FF0000'>%2€</t>",
			[life_dabflouze] call life_fnc_numberText,
			[_upgradePrice - life_dabflouze] call life_fnc_numberText
		];
	};
	__SUB__(life_dabflouze,_upgradePrice);
	grpPlayer setVariable["gang_maxMembers",_slotUpgrade,true];
	hint parseText format["Vous avez mis a niveau %1 pour une capacité de %2 maximum. Coût : <t color='#8cff9b'>%3€</t>",_maxMembers,_slotUpgrade,[_upgradePrice] call life_fnc_numberText];
	[[2,grpPlayer],"TON_fnc_updateGang",false,false] spawn life_fnc_MP;
} else {
	hint "Mise à niveau annulé.";
};