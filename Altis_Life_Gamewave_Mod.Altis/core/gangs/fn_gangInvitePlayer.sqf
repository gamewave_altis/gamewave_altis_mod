#include <macro.h>
/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Starts the invite process?
*/
private["_unit"];
disableSerialization;

if((lbCurSel 2632) == -1) exitWith {hint "Vous devez selectioner quelqu'un"};
_unit = call compile format["%1",getSelData(2632)];
if(isNull _unit) exitWith {}; //Bad unit?
if(_unit == player) exitWith {hint "Vous ne pouvez pas vous kick."};
if(!isNil {(group _unit) getVariable "gang_name"}) exitWith {hint "This player is already in a gang"}; //Added

if(count(grpPlayer getVariable ["gang_members",8]) == (grpPlayer getVariable ["gang_maxMembers",8])) exitWith {hint "Your gang has reached its maximum allowed slots, please upgrade your gangs slot limit."};

_action = [
	format["Vous êtes sur le point d'inviter %1 dans votre gang, En acceptant les membres auront accès au compte bancaire groupé.",_unit getVariable ["realname",name _unit]],
	"Envoi Invitation Gang",
	"Oui",
	"Non"
] call BIS_fnc_guiMessage;

if(_action) then {
	[[profileName,grpPlayer],"life_fnc_gangInvite",_unit,false] spawn life_fnc_MP;
	_members = grpPlayer getVariable "gang_members";
    _members pushBack getPlayerUID _unit;

	grpPlayer setVariable["gang_members",_members,true];
    hint format["Vous avez envoyé une invitation à %1",_unit getVariable["realname",name _unit]];
} else {
	hint "Invitation annulé";
};