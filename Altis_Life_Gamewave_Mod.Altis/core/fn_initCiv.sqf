#include <macro.h>
/*
	File: fn_initCiv.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Initializes the civilian.
*/
private["_spawnPos"];

civ_spawn_1 = nearestObjects[getMarkerPos  "civ_spawn_1", ["Land_u_House_Big_01_V1_F","Land_u_House_Small_02_V1_F","Land_u_House_Big_02_V1_F","Land_u_House_Small_01_V1_F"],250];
civ_spawn_2 = nearestObjects[getMarkerPos  "civ_spawn_2", ["Land_u_House_Big_01_V1_F","Land_u_House_Small_02_V1_F","Land_u_House_Big_02_V1_F","Land_u_House_Small_01_V1_F"],250];
civ_spawn_3 = nearestObjects[getMarkerPos  "civ_spawn_3", ["Land_u_House_Big_01_V1_F","Land_u_House_Small_02_V1_F","Land_u_House_Big_02_V1_F","Land_u_House_Small_01_V1_F"],250];
civ_spawn_4 = nearestObjects[getMarkerPos  "civ_spawn_4", ["Land_u_House_Big_01_V1_F","Land_u_House_Small_02_V1_F","Land_u_House_Big_02_V1_F","Land_u_House_Small_01_V1_F"],250];
civ_spawn_5 = nearestObjects[getMarkerPos  "civ_spawn_5", ["Land_u_House_Big_01_V1_F","Land_u_House_Small_02_V1_F","Land_u_House_Big_02_V1_F","Land_u_House_Small_01_V1_F"],250];

waitUntil {!(isNull (findDisplay 46))};

if((str(player) in ["civ_70","civ_71","civ_72","civ_73","civ_74","civ_75","civ_76","civ_77","civ_78","civ_79","civ_80","civ_81","civ_82","civ_83","civ_84","civ_85","civ_86","civ_87","civ_88","civ_89","civ_90","civ_91","civ_92","civ_93","civ_94","civ_95","civ_96","civ_97","civ_98","civ_99","civ_100","civ_101","civ_102","civ_103","civ_104","civ_105","civ_106","civ_107","civ_108","civ_109","civ_110","civ_111","civ_112","civ_113","civ_114","civ_115","civ_116","civ_118","civ_119","civ_120","civ_121","civ_122","civ_123","civ_124","civ_125","civ_126","civ_127","civ_128","civ_129","civ_130"])) then {
	if((__GETC__(life_donator) <= 1)) then {
		["NotWhitelisted",false,true] call BIS_fnc_endMission;
		sleep 35;
	};
};


//if((__GETC__(life_adminlevel) > 1)) then {
//[] execVM "admintools\loop.sqf";
//[] execVM "admintools\activate.sqf";
//};

if(life_is_arrested) then
{
	life_is_arrested = false;
	[player,true] spawn life_fnc_jail;
}
else
{
	if (life_alive) then
	{
		player setPos [life_bidet select 0, life_bidet select 1, (life_bidet select 2) +1];
		
	}
	else
	{
		[] call life_fnc_spawnMenu;
		waitUntil{!isNull (findDisplay 38500)}; //Wait for the spawn selection to be open.
		waitUntil{isNull (findDisplay 38500)}; //Wait for the spawn selection to be done.
	};
};
player addRating 9999999;

if(license_civ_rebel)then{
license_civ_taxi = false;
license_civ_dep = false;
license_civ_medic = false;
license_civ_merc = false;
};
if(license_civ_merc)then{
license_civ_taxi = false;
license_civ_dep = false;
license_civ_medic = false;
license_civ_rebel = false;
};

[] call life_fnc_zoneCreator;
//[] call life_fnc_equipGear;
//[] call life_fnc_initHouses;
//[] call life_fnc_updateClothing;


	
[] call life_fnc_updateClothing;
[[getPlayerUID player],"TON_fnc_loadBounties",false,false] spawn life_fnc_MP;
