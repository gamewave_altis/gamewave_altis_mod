/*
    File: fn_updateRequest.sqf
    Author: Tonic
*/
private["_packet","_array","_flag"];
_packet = [getPlayerUID player,(profileName),playerSide,life_flouze,life_dabflouze];
_array = [];
_flag = switch(playerSide) do {case west: {"cop"}; case civilian: {"civ"}; case independent: {"med"};};
{
	if(_x select 1 == _flag) then
	{
		_array set[count _array,[_x select 0,(missionNamespace getVariable (_x select 0))]];
	};
} foreach life_licenses;

_packet set[count _packet,_array];

//[] call life_fnc_saveGear;
//_packet set[count _packet, life_gear];

switch (playerSide) do {
	case west: {
			//_packet set[count _packet,cop_gear];
			[] call life_fnc_saveGear;
			_packet set[count _packet, life_gear];
			_packet set[9, getPos player];
			_packet set[10, alive player];
			_packet set[11,life_in_rea];
			_packet set[12,life_hunger];
			_packet set[13,life_thirst];
			_packet set[14,life_damage];
			_packet set[15,life_epargne];
			_packet set[16,life_restrained];
			
		};
};
diag_log format ["UPDATEREQUEST || %1", _packet];

[_packet,"DB_fnc_updateRequestCop",false,false] spawn life_fnc_MP;