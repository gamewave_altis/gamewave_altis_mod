#include <macro.h>
/*
	File: fn_vehicleShopBuy.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Does something with vehicle purchasing.
*/
private["_mode","_spawnPoints","_className","_basePrice","_colorIndex","_spawnPoint","_vehicle","_mod","_realprice"];
_mode = _this select 0;
if((lbCurSel 2302) == -1) exitWith {hint "Vous n'avez selectionné aucun véhicule."};
_className = lbData[2302,(lbCurSel 2302)];
_vIndex = lbValue[2302,(lbCurSel 2302)];
_vehicleList = [life_veh_shop select 0] call life_fnc_vehicleListCfg;
 
_basePrice = (_vehicleList select _vIndex) select 1;

_colorIndex = lbValue[2304,(lbCurSel 2304)];
_mod = true;
//////////////////////////////
///Reduction Donateur -25%///
//////////////////////////////
if(__GETC__(life_donator) > 1) then {
_basePrice = round(_basePrice * 0.75)
};
//Series of checks (YAY!)
if(_basePrice < 0) exitWith {}; //Bad price entry
if(life_flouze < _basePrice) exitWith {hint format["Désolé mais tu n'as pas assez d'argent.\n\nIl te manque: %1€",[_basePrice - life_flouze] call life_fnc_numberText];};
if(!([_className] call life_fnc_vehShopLicenses)) exitWith {hint "Vous n'avez pas le bon permis."};


_spawnPoints = life_veh_shop select 1;
_spawnPoint = "";

if((life_veh_shop select 0) == "med_air_hs") then {
	if(count(nearestObjects[(getMarkerPos _spawnPoints),["Air"],35]) == 0) exitWith {_spawnPoint = _spawnPoints};
} else {
	//Check if there is multiple spawn points and find a suitable spawnpoint.
	if(typeName _spawnPoints == typeName []) then {
		//Find an available spawn point.
		{if(count(nearestObjects[(getMarkerPos _x),["Car","Ship","Air","Motorcycle"],5]) == 0) exitWith {_spawnPoint = _x};} foreach _spawnPoints;
	} else {
		if(count(nearestObjects[(getMarkerPos _spawnPoints),["Car","Ship","Air","Motorcycle"],5]) == 0) exitWith {_spawnPoint = _spawnPoints};
	};
};
diag_log format ["Shopflag ? %1 _spawnPoints ? %2 ",life_veh_shop select 0, life_veh_shop select 1];

diag_log format ["vehicleshopBuy-- _basePrice %1, _colorIndex %2, _classname %3, _spawnPoints %4",_basePrice, _colorIndex, _classname, _spawnPoints];

if(_spawnPoint == "") exitWith {hint "Il y'a un véhicule qui gène sur le point de vente...";};

life_flouze = life_flouze - _basePrice;
hint format["Vous avez acheté un %1 pour %2€",getText(configFile >> "CfgVehicles" >> _className >> "displayName"),[_basePrice] call life_fnc_numberText];

//Spawn the vehicle and prep it.
if((life_veh_shop select 0) == "med_air_hs") then {
	_vehicle = createVehicle [_className,[0,0,999],[], 0, "NONE"];
	waitUntil {!isNil "_vehicle"}; //Wait?
	_vehicle allowDamage false;
	_hs = nearestObjects[getMarkerPos _spawnPoint,["Land_Hospital_side2_F"],50] select 0;
    _vehicle setPosATL (_hs modelToWorld [-0.4,-4,12.65]);
	_vehicle lock 2;
	[[_vehicle,_colorIndex],"life_fnc_colorVehicle",true,false] spawn life_fnc_MP;
	[_vehicle] call life_fnc_clearVehicleAmmo;
    [[_vehicle,"trunk_in_use",false,true],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
    [[_vehicle,"vehicle_info_owners",[[getPlayerUID player,profileName]],true],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
	[[_vehicle,"vehicle_info_tracker",[[],true]],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
	[[_vehicle,"vehicle_info_c4",[[],true]],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
	[[_vehicle,"vehicle_info_side",[[life_veh_shop select 0],true]],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
	//_vehicle disableTIEquipment true; //No Thermals.. They're cheap but addictive.
} else {

	_modArray = [_className] call life_fnc_vehicleColorMod;
	if(count _modArray == 0) then {_mod = false};
	if(isNil "_modArray") then {_mod = false};
	if (_mod) then {
	diag_log format ["VEHMOD TRUE"];

		_vehicleMod = _modArray select _colorIndex;
		diag_log format ["_vehicleMod %1",_vehicleMod];
		_vehicle = createVehicle [_vehicleMod select 0, (getMarkerPos _spawnPoint), [], 0, "NONE"];
			waitUntil {!isNil "_vehicle"}; //Wait?
		_vehicle allowDamage false; //Temp disable damage handling..
		_vehicle lock 2;
		_vehicle setVectorUp (surfaceNormal (getMarkerPos _spawnPoint));
		_vehicle setDir (markerDir _spawnPoint);
		_vehicle setPos (getMarkerPos _spawnPoint);
		
		[[_vehicle,_colorIndex],"life_fnc_colorVehicle",true,false] spawn life_fnc_MP;
		
		[_vehicle] call life_fnc_clearVehicleAmmo;
		[[_vehicle,"trunk_in_use",false,true],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
		[[_vehicle,"vehicle_info_owners",[[getPlayerUID player,profileName]],true],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
		[[_vehicle,"vehicle_info_tracker",[[],true]],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
		[[_vehicle,"vehicle_info_c4",[[],true]],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
		[[_vehicle,"vehicle_info_side",[[life_veh_shop select 0],true]],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
		_vehicle setVariable["dbAssur",true,true];
	};
	
	if (!_mod) then {
		_vehicle = createVehicle [_className, (getMarkerPos _spawnPoint), [], 0, "NONE"];
		waitUntil {!isNil "_vehicle"}; //Wait?
		_vehicle allowDamage false; //Temp disable damage handling..
		_vehicle lock 2;
		_vehicle setVectorUp (surfaceNormal (getMarkerPos _spawnPoint));
		_vehicle setDir (markerDir _spawnPoint);
		_vehicle setPos (getMarkerPos _spawnPoint);
		
		[[_vehicle,_colorIndex],"life_fnc_colorVehicle",true,false] spawn life_fnc_MP;
		
		[_vehicle] call life_fnc_clearVehicleAmmo;
		[[_vehicle,"trunk_in_use",false,true],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
		[[_vehicle,"vehicle_info_owners",[[getPlayerUID player,profileName]],true],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
		[[_vehicle,"vehicle_info_tracker",[[],true]],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
		[[_vehicle,"vehicle_info_c4",[[],true]],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
		[[_vehicle,"vehicle_info_side",[[life_veh_shop select 0],true]],"TON_fnc_setObjVar",false,false] spawn life_fnc_MP;
		_vehicle setVariable["dbAssur",true,true];
		
		//_vehicle disableTIEquipment true; //No Thermals.. They're cheap but addictive.
		
		//if(_className in ["O_MRAP_02_F","B_MRAP_01_F","I_MRAP_03_F","B_MRAP_01_hmg_F"]) then {
		//_vehicle addEventHandler["handleDamage",{_this call life_fnc_damageLucel;}];
		//};
		//_vehicle addEventHandler ["GetIn",{_this spawn life_fnc_getIn;}];
	};
};

//Side Specific actions.
switch(playerSide) do {
	case west: {
		[_vehicle,"cop_offroad",true] spawn life_fnc_vehicleAnimate;
		if(_className in [
		"C_Hatchback_01_F",
		"C_Hatchback_01_sport_F",
		"C_Offroad_01_F",
		"B_G_Offroad_01_armed_F",
		"C_SUV_01_F",
		"B_Heli_Light_01_F",
		"B_Heli_Transport_01_F",
		"I_Heli_light_03_unarmed_F",
		"I_MRAP_03_hmg_F",
		"I_MRAP_03_F",
		"B_APC_Wheeled_01_cannon_F",
		"B_MRAP_01_hmg_F",
		"B_MRAP_01_F",
		"B_Quadbike_01_F",
		"cl3_xr_1000_police",
		"cl3_reventon_clpd",
		"cl3_escalade_patrolbw",
		"cl3_dodge_charger_patrol",
		"cl3_dodge_charger_patrol2",
		"cl3_transit_black",
		"cl3_suv_black",
		"CL3_bus_cl_jail",
		"cl3_reventon_clpd_traf",
		"cl3_dodge_charger_patrol2",
		"O_Heli_Transport_04_F",
        "O_Heli_Transport_04_bench_F",
        "B_Heli_Transport_03_unarmed_F"		
		] && playerSide == west) then {
		_vehicle setVariable["lights",false,true];
		_vehicle setVariable["SirenCop",false,true];
		};

	};
	
case civilian: {
		//Hm9 : Désactivation du banc
		if((life_veh_shop select 2) == "civ" && {_className == "B_Heli_Light_01_F"}) then {
			[_vehicle,"civ_littlebird",true] spawn life_fnc_vehicleAnimate;
		};
		//Pickup Depanneur : Ajout des outils + gyro
		if((life_veh_shop select 2) == "dep" && {_className == "C_Offroad_01_F"} && _colorIndex == 9) then {
			_vehicle setVariable["lightsDep",false,true];
			diag_log ["DEPBUY _vehicle %1", _vehicle];
			[_vehicle,"service_truck",true] spawn life_fnc_vehicleAnimate;
		};	
		//Pegasus : Désactivation de la fonction réparation du camion
		if((life_veh_shop select 2) == "civ" && {_className == "I_Truck_02_box_F"}) then {
			_vehicle setRepairCargo 0
		};
		
		//Light pour Medic :
		if((life_veh_shop select 2) == "medic") then {
		_vehicle setVariable["lightsMedic",false,true];
		_vehicle setVariable["SirenMedic",false,true];
		};	
		
		

		//if(_className == "C_Offroad_01_F" && _basePrice == 15002) then {
		//[_vehicle,"med_offroad",true] spawn life_fnc_vehicleAnimate;
		//
		
	};
	
	case independent: {
		[_vehicle,"med_offroad",true] spawn life_fnc_vehicleAnimate;
	};
};

_vehicle allowDamage true;
//life_vehicles set[count life_vehicles,_vehicle]; //Add err to the chain.
life_vehicles pushBack _vehicle;

[[getPlayerUID player,playerSide,_vehicle,1],"TON_fnc_keyManagement",false,false] spawn life_fnc_MP;

[[(getPlayerUID player),playerSide,_vehicle,_colorIndex],"TON_fnc_vehicleCreate",false,false] spawn life_fnc_MP;


//if(!(_className in ["B_G_Offroad_01_armed_F","B_MRAP_01_hmg_F"])) then {};

//[] call SOCK_fnc_updateRequest; //Sync silently because it's obviously silently..
//[[getPlayerUID player,playerSide,life_flouze,0],"DB_fnc_updatePartial",false,false] spawn life_fnc_MP; // Sync life_flouze

[0] call SOCK_fnc_updatePartial;

closeDialog 0; //Exit the menu.
diag_log format ["vehicleshopBuy-- _basePrice %1, _colorIndex %2, _classname %3, _spawnPoints %4, _vehicle %5", _basePrice, _colorIndex, _classname, _spawnPoints, _vehicle];
diag_log format["END VEHSHOPBUY"];
true;