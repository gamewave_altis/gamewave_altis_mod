/*
	File: fn_virt_menu.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Initialize the virtual shop menu.
*/
private["_shop"];
_shop = _this select 3;
if(isNil {_shop}) exitWith {};
life_shop_type = _shop;
life_shop_npc = _this select 0;
if(_shop == "cop" && playerSide != west) exitWith {hint "Tu n'es pas policier !"};

if(_shop == "heroin" 
|| _shop == "oil"
|| _shop == "glass"
|| _shop == "iron"
|| _shop == "diamond"
|| _shop == "salt"
|| _shop == "cement"
|| _shop == "gold"
|| _shop == "wongs"
|| _shop == "wood"
) then {
createDialog "shops_menu_eco";
[] call life_fnc_virt_updateEco;
}
else
{
createDialog "shops_menu";
[] call life_fnc_virt_update;
};