/*
Altislife.fr by lucel
*/
private["_dialog","_licName","_licTxt","_control","_licenses"];
_civ = [_this,0,"",[""]] call BIS_fnc_param;
_licenses = [_this,1,[],[[]]] call BIS_fnc_param;
disableSerialization;
createDialog "licenseRemove";	
ctrlEnable[1016,true];
_control = ((findDisplay 8888) displayCtrl 2005);
{
	_licName = _x select 0;
	_licTxt = [_x select 0] call life_fnc_varToStr;
	_control lbAdd (_licTxt);
	_control lbSetData [(lbSize _control)-1,_licName];
		
}foreach _licenses;

if(count _licenses == 0) then {
_control lbAdd format["Pas de permis"];
ctrlEnable[1016,false];
};
