/*

	Player clicked arrest/ok

*/

private ["_time"];

//if(playerSide != west) exitWith {};
if(isNil "life_pInact_curTarget") exitWith {hint "Erreur."};

//Get minutes
_time = ctrlText 1400;


if(! ([_time] call TON_fnc_isnumber)) exitWith
{
	hint "Vous devez entrer un chiffre.";
};

_time = parseNumber _time; //requested number
_time = round _time;

if(_time < 5 || _time > 60) exitWith { hint hint "Veuillez mettre un chiffre entre 5 et 60"; };

closeDialog 0; 
[life_pInact_curTarget, _time] call life_fnc_arrestAction;