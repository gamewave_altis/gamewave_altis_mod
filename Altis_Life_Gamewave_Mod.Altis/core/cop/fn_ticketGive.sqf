/*
	File: fn_ticketGive.sqf
	Author: Bryan "Tonic" Boardwine
	Traduction : Gamewave
	
	Description:
	Gives a ticket to the targeted player.
*/
private["_val"];
if(isNil {life_ticket_unit}) exitWith {hint "Person to ticket is nil"};
if(isNull life_ticket_unit) exitWith {hint "Person to ticket doesn't exist."};
_val = ctrlText 2652;
if(!([_val] call TON_fnc_isnumber)) exitWith {hint "Vous devez rentrer des chiffres pour mettre une amende"};
if((parseNumber _val) > 500000) exitWith {hint "Les amendes de peuvent pas dépasser les 200.000€"};
[[0,format["%1 donne une amende de %2€ a %3",name player,[(parseNumber _val)] call life_fnc_numberText,name life_ticket_unit]],"life_fnc_broadcast",true,false] spawn life_fnc_MP;
[[player,(parseNumber _val)],"life_fnc_ticketPrompt",life_ticket_unit,false] spawn life_fnc_MP;
closeDialog 0;