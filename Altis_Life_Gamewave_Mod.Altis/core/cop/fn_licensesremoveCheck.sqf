/*
Altislife.fr by lucel
*/
private["_cop","_licenses"];
_cop = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _cop) exitWith {}; //Bad entry
_licenses = [];
//Licenses
{
	if(missionNamespace getVariable (_x select 0) && _x select 1 == "civ") then
	{
		_licenses pushBack _x;
	};
		
} foreach life_licenses;

[[name player,_licenses],"life_fnc_licensesRemove",_cop,FALSE] spawn life_fnc_MP;
		
