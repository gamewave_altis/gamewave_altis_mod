/*
	File: fn_copLoadout.sqf
	Author: Bryan "Tonic" Boardwine
	Edited: Itsyuka
	
	Description:
	Loads the cops out with the default gear.
*/
private["_handle"];
_handle = [] spawn life_fnc_stripDownPlayer;
waitUntil {scriptDone _handle};
player setVariable["imMedicBro",true,true];
//Load player with default cop gear.
player addUniform "CL3_emtMR_uniform";
player addVest "V_TacVest_blk_POLICE";
player addHeadgear "H_Cap_police";
player addBackpack "B_FieldPack_blk";

player addWeapon "hlc_smg_mp510";
player addPrimaryWeaponItem "optic_ACO_grn_smg";
player addWeapon "cl3_taserM26_Yellow";
player addWeapon "Rangefinder";

player addItemToBackpack "Medikit";
player addItemToBackpack "Toolkit";
player addItemToBackpack "FirstAidKit";
player addItemToBackpack "optic_Holosight";
player addItemToVest "hlc_30Rnd_10mm_B_MP5";
player addItemToVest "hlc_30Rnd_10mm_B_MP5";
player addItemToVest "hlc_30Rnd_10mm_B_MP5";
player addItemToVest "hlc_30Rnd_10mm_B_MP5";
player addItemToVest "hlc_30Rnd_10mm_B_MP5";
player addItemToVest "SmokeShell";
player addItemToVest "SmokeShell";
player addItemToVest "SmokeShell";
player addItemToVest "Chemlight_green";
player addItemToVest "Chemlight_green";
player addItemToVest "Chemlight_green";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";
player addItemToUniform "cl3_taserm26mag_mpx";

/* ITEMS */
player addItem "ItemMap";
player addItem "NVGoggles_OPFOR";
player assignItem "NVGoggles_OPFOR";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemWatch";
player assignItem "ItemWatch";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemRadio";
player assignItem "ItemRadio";

//[[player,0,"texture\skins\medic_uniform.jpg"],"life_fnc_setTexture",true,false] spawn life_fnc_MP;

//[] call life_fnc_saveGear;


