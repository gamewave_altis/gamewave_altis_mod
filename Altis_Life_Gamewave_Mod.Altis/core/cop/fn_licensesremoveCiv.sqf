/*
Altislife.fr by lucel
*/
private["_licDelete","_cop","_licDelete"];
_licDelete = [_this,1,"",[[]]] call BIS_fnc_param;
_cop = [_this,0,Objnull,[Objnull]] call BIS_fnc_param;
_licTxt = [_licDelete] call life_fnc_varToStr;
switch (_licDelete) do 
{
	case "license_civ_driver": 
	{
	license_civ_driver = false;
	};

	case "license_civ_medic": 
	{
	license_civ_medic = false;
	};
	
	case "license_civ_taxi": 
	{
	license_civ_taxi = false;
	};	
	
	case "license_civ_dep": 
	{
	license_civ_dep = false;
	};
	
	case "license_civ_truck": 
	{
	license_civ_truck = false;
	};
	
	case "license_civ_air": 
	{
	license_civ_air = false;
	};
	
	case "license_civ_gun": 
	{
	license_civ_gun = false;
	};	
	case "license_civ_boat": 
	{
	license_civ_boat = false;
	};
};
[2] call SOCK_fnc_updatePartial;
hint parseText format["<t size='3'><t color='#FF0000'>Permis : </t></t> <br/><t size='1.5'>La police vient de vous retirer votre permis de type : <t color='#00FF00'>%1</t></t>",_licTxt];
systemChat format["Vous venez de perdre votre permis de type : %1",_licTxt];