/*
	File: fn_copLoadout.sqf
	Author: Bryan "Tonic" Boardwine
	Edited: Itsyuka
	
	Description:
	Loads the cops out with the default gear.
*/
private["_handle"];
_handle = [] spawn life_fnc_stripDownPlayer;
waitUntil {scriptDone _handle};

//Load player with default cop gear.
player addUniform "cl3_c_poloshirtpants_clpdpatrol_uniform";
player addVest "V_TacVest_blk_POLICE";
player addHeadgear "H_Cap_police";

player addWeapon "cl3_taserM26_Yellow";
player addMagazine "cl3_taserm26mag_mpx";
player addMagazine "cl3_taserm26mag_mpx";
player addMagazine "cl3_taserm26mag_mpx";
player addMagazine "cl3_taserm26mag_mpx";
player addMagazine "cl3_taserm26mag_mpx";
player addMagazine "cl3_taserm26mag_mpx";

/* ITEMS */
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemWatch";
player assignItem "ItemWatch";
player addItem "ItemGPS";
player assignItem "ItemGPS";
player addItem "ItemRadio";
player assignItem "ItemRadio";

[] call life_fnc_saveGear;


