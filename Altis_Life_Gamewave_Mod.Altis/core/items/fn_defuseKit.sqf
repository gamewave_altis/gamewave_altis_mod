/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Defuses blasting charges for the cops?
*/
private["_vault","_ui","_title","_progressBar","_cP","_titleText","_bank","_veh"];
if((player getVariable "restrained")) exitWith {hint "Vous êtes attaché !";closeDialog 0;closeDialog 0;};
if((player getVariable "Surrender")) exitWith {hint "Vous n'avez pas les mains libres !";closeDialog 0;closeDialog 0;};
_bank = false;
_veh = false;
_vault = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
diag_log format["_vault %1",_vault];
_isVehicle = if((_vault isKindOf "landVehicle") OR (_vault isKindOf "Ship") OR (_vault isKindOf "Air")) then {true} else {false};
if(isNull _vault) exitWith {};
if(typeOf _vault != "Land_CargoBox_V1_F" && !_isVehicle) exitWith {};
if(typeOf _vault == "Land_CargoBox_V1_F") then {
_bank = true;
};
if(_isVehicle) then {
_veh = true;
_trackerList = _vault getVariable "vehicle_info_c4";
};
diag_log format["_vault %1",_vault];
if((!(_vault getVariable["chargeplaced",false]) && !(_vault getVariable["c4detected",false]))) exitWith {hint "Pas de charge explosif détecté..."};


life_action_inUse = true;
//Setup the progress bar
disableSerialization;
_title = "Désamorsage...";
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNamespace getVariable "life_progress";
_progressBar = _ui displayCtrl 38201;
_titleText = _ui displayCtrl 38202;
_titleText ctrlSetText format["%2 (1%1)...","%",_title];
_progressBar progressSetPosition 0.01;
if(_veh)then{
_cP = 0.1;
}else{
_cP = 0.01;
};
while {true} do
{
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player switchMove "AinvPknlMstpSnonWnonDnon_medic_1";
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	sleep 0.26;
	if(isNull _ui) then {
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNamespace getVariable "life_progress";
		_progressBar = _ui displayCtrl 38201;
		_titleText = _ui displayCtrl 38202;
	};
	_cP = _cP + .0035;
	_progressBar progressSetPosition _cP;
	_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
	if(_cP >= 1 OR !alive player) exitWith {};
	if(life_interrupted) exitWith {};
	if(life_istazed) exitWith {player switchMove "";}; //Tazed
};

//Kill the UI display and check for various states
5 cutText ["","PLAIN"];
player playActionNow "stop";
if(!alive player) exitWith {life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; titleText["Action annulé","PLAIN"]; life_action_inUse = false;};

life_action_inUse = false;

if(_veh) then {
_vault setVariable["vehicle_info_c4",[],true];
_vault setVariable["c4detected",false];
[[4,"<t size='1.5'><t color='#00FF00'>Charge explosif désamorcée.</t></t> <br/><t size='1'>Vous avez réussi le désamorsage de la charge explosif. Félicitation.</t>"],"life_fnc_broadcast",player,false] spawn life_fnc_MP;
};
if(_bank) then {
_vault setVariable["chargeplaced",false,true];
[[2,"La charge explosif est désamorcée."],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
};