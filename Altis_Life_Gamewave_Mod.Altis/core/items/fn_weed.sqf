private ["_hndlBlur","_hndlChromAberr","_hndlClrInversion","_hndlWetDist","_blur","_color","_chroma","_wetpower","_wetblur","_time","_loop","_timefade","_timecommit","_text"];
closeDialog 0;

sleep 10;

//Activate ppEffects we need
_hndlBlur = ppEffectCreate ["DynamicBlur", 200];
_hndlClrInversion = ppEffectCreate ["ColorInversion", 1000];
_hndlChromAberr = ppEffectCreate ["ChromAberration", 1001];
_hndlWetDist = ppEffectCreate ["WetDistortion", 1002];
_hndlBlur ppEffectEnable true;
_hndlClrInversion ppEffectEnable true;
_hndlChromAberr ppEffectEnable true;
_hndlWetDist ppEffectEnable true;
enableCamShake true;

_time = 150; // 90 sec.
_timefade = 100; // fade out in sec (remove time of commit )
_timecommit = 10; // fade to 0
_blur=0.25; //blur power
_color=0.05; // color power
_chroma=0.02; // chromaberr power
_wetpower=0.3; // wetdist power
_wetblur=1; // wetdist blur
_loop=0;
_text = true;
for "_i" from 0 to _time do
{
	_loop=_loop + 1;
	_hndlBlur ppEffectAdjust [_blur];
	_hndlBlur ppEffectCommit 0;
	
	_hndlClrInversion ppEffectAdjust [_color,0.0,_color];
	_hndlClrInversion ppEffectCommit 0;
	
	_hndlChromAberr ppEffectAdjust [_chroma,_chroma,_chroma];
	_hndlChromAberr ppEffectCommit 0;
	
	_hndlWetDist ppEffectAdjust [_wetblur,_wetpower,_wetpower, 4.1, 3.7, 4.5, 2.85, 0.02, 0.02, 0.01, 0.001, 0.0001, 0.5, 1000, 200.0];
	_hndlWetDist ppEffectCommit 0;
	if (_loop > (_time-_timefade)) then {
		_blur = _blur - (_blur/(_time*(_time/_timefade)));
		_color = _color - (_color/(_time*(_time/_timefade)));
		_chroma = _chroma - (_chroma/(_time*(_time/_timefade)));
		_wetpower = _wetpower - (_wetpower/(_time*(_time/_timefade)));
		_wetblur = _wetblur - (_wetblur/(_time*(_time/_timefade)));
	};
	if ((player == driver(vehicle player))&&(vehicle player != player)&&(_text)) then {
		hint "La drogue au volant c'est mal";
		[[getPlayerUID player,name player,"502D"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
		_text=false;
	};
	if(!alive player) exitWith {_text=false;};
    sleep 1;
};
_text = false;
_hndlBlur ppEffectAdjust [0];
_hndlBlur ppEffectCommit _timecommit;

_hndlClrInversion ppEffectAdjust [0,0,0];
_hndlClrInversion ppEffectCommit _timecommit;

_hndlChromAberr ppEffectAdjust [0,0,0];
_hndlChromAberr ppEffectCommit _timecommit;

_hndlWetDist ppEffectAdjust [0,0,0,4.1, 3.7, 4.5, 2.85, 0.02, 0.02, 0.01, 0.001, 0.0001, 0.5, 1000, 200.0];
_hndlWetDist ppEffectCommit _timecommit;
addcamShake[0,0,0];
player setDamage (damage player - 0.1);
sleep 10;
//Stop effects
ppEffectDestroy [_hndlBlur,_hndlClrInversion,_hndlChromAberr,_hndlWetDist];
sleep 6;

//Deactivate ppEffects
_hndlBlur ppEffectEnable false;
_hndlClrInversion ppEffectEnable false;
_hndlChromAberr ppEffectEnable false;
_hndlWetDist ppEffectEnable false;
resetCamShake;