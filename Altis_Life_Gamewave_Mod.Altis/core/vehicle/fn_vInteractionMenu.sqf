/*
	File: fn_vInteractionMenu.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Replaces the mass addactions for various vehicle actions
*/
#define Btn1 37450
#define Btn2 37451
#define Btn3 37452
#define Btn4 37453
#define Btn5 37454
#define Btn6 37455
#define Btn7 37456
#define Btn8 37457
#define Btn9 37458
#define Btn10 37459
#define Btn11 37460
#define Btn12 37461
#define Title 37401
private["_display","_curTarget","_Btn1","_Btn2","_Btn3","_Btn4","_Btn5","_Btn6","_Btn7","_Btn8","_Btn9","_Btn10","_Btn11","_Btn12","_uid","_owners"];
if(!dialog) then {
	createDialog "vInteraction_Menu";
};
disableSerialization;
_curTarget = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
_uid = getPlayerUID player;
if(isNull _curTarget) exitWith {closeDialog 0;}; //Bad target
if((player getVariable "restrained")) exitWith {hint "Vous êtes attaché !";closeDialog 0;closeDialog 0;};
if((player getVariable "Surrender")) exitWith {hint "Vous n'avez pas les mains libres !";closeDialog 0;closeDialog 0;};
_isVehicle = if((_curTarget isKindOf "landVehicle") OR (_curTarget isKindOf "Ship") OR (_curTarget isKindOf "Air")) then {true} else {false};
    diag_log format ["_isVehicle %1",_isVehicle];
_isBox = if((_curTarget isKindOf "Box_IND_Grenades_F") OR (_curTarget isKindOf "B_supplyCrate_F")) then {true} else {false};
    diag_log format ["_isBox %1",_isBox];
if(!_isVehicle && !_isBox) exitWith {closeDialog 0;};
_display = findDisplay 37400;
_Btn1 = _display displayCtrl Btn1;
_Btn2 = _display displayCtrl Btn2;
_Btn3 = _display displayCtrl Btn3;
_Btn4 = _display displayCtrl Btn4;
_Btn5 = _display displayCtrl Btn5;
_Btn6 = _display displayCtrl Btn6;
_Btn7 = _display displayCtrl Btn7;
_Btn8 = _display displayCtrl Btn8;
_Btn9 = _display displayCtrl Btn9;
_Btn10 = _display displayCtrl Btn10;
_Btn11 = _display displayCtrl Btn11;
_Btn12 = _display displayCtrl Btn12;
life_vInact_curTarget = _curTarget;
 //diag_log format ["_owners %1",_owners];
//if(isNil _owners)exitWith {};

//diag_log format ["Owners %1", _owners];
//Set Repair Action
if(_isVehicle) then {
_owners = _curTarget getVariable "vehicle_info_owners";

_Btn1 ctrlSetText localize "STR_vInAct_Repair";
_Btn1 buttonSetAction "[life_vInact_curTarget] spawn life_fnc_repairTruck;";

if("ToolKit" in (items player) && (damage _curTarget < 1)) then {_Btn1 ctrlEnable true;} else {_Btn1 ctrlEnable false;};


if(playerSide == west) then {
	_Btn2 ctrlSetText localize "STR_vInAct_Registration";
	_Btn2 buttonSetAction "[life_vInact_curTarget] spawn life_fnc_searchVehAction;";
	
	_Btn3 ctrlSetText localize "STR_vInAct_SearchVehicle";
	_Btn3 buttonSetAction "[life_vInact_curTarget] spawn life_fnc_vehInvSearch;";
	
	_Btn4 ctrlSetText localize "STR_vInAct_PullOut";
	_Btn4 buttonSetAction "[life_vInact_curTarget] spawn life_fnc_pulloutAction;";
	if(count crew _curTarget == 0) then {_Btn4 ctrlEnable false;};
	
	_Btn5 ctrlSetText localize "STR_vInAct_Impound";
	_Btn5 buttonSetAction "[life_vInact_curTarget] spawn life_fnc_impoundAction;";
	
	if(_curTarget isKindOf "Ship") then {
		_Btn6 ctrlSetText localize "STR_vInAct_PushBoat";
		_Btn6 buttonSetAction "[] spawn life_fnc_pushObject; closeDialog 0;";
		if(_curTarget isKindOf "Ship" && {local _curTarget} && {count crew _curTarget == 0}) then { _Btn6 ctrlEnable true;} else {_Btn6 ctrlEnable false};
		} else {
			_Btn6 ctrlSetText localize "STR_vInAct_Unflip";
			_Btn6 buttonSetAction "life_vInact_curTarget setPos [getPos life_vInact_curTarget select 0, getPos life_vInact_curTarget select 1, (getPos life_vInact_curTarget select 2)+0.5]; closeDialog 0;";
			if(count crew _curTarget == 0 && {canMove _curTarget}) then { _Btn6 ctrlEnable false;} else {_Btn6 ctrlEnable true;};
		};
	_Btn7 ctrlSetText "FORCER CONDUITE";
	_Btn7 buttonSetAction "player moveInDriver life_vInact_curTarget; closeDialog 0;";
	if((count crew _curTarget) > 0)then
	{_Btn7 ctrlEnable true;} else {_Btn7 ctrlEnable false;};	
	
	_Btn8 ctrlSetText "FORCER PASSAGER";
	_Btn8 buttonSetAction "player moveInCargo life_vInact_curTarget; closeDialog 0;";
	if(
		(count crew _curTarget) > 0
	)then 
	{_Btn8 ctrlEnable true;} else {_Btn8 ctrlEnable false;};
	
	if(typeOf (_curTarget) in ["C_Kart_01_Blu_F","C_Kart_01_Red_F","C_Kart_01_Fuel_F","C_Kart_01_Vrana_F","B_Heli_Transport_03_F","B_Heli_Transport_03_unarmed_F","O_Heli_Transport_04_F","O_Heli_Transport_04_ammo_F","O_Heli_Transport_04_bench_F","O_Heli_Transport_04_box_F","O_Heli_Transport_04_covered_F","O_Heli_Transport_04_fuel_F","O_Heli_Transport_04_medevac_F","O_Heli_Transport_04_repair_F"]) then {
    _Btn9 ctrlSetText "Place passager (DLC)";
    _Btn9 buttonSetAction "player moveInCargo life_vInact_curTarget; closeDialog 0;";
    _Btn10 ctrlSetText "Place tireur (DLC)";
    _Btn10 buttonSetAction "player moveInGunner life_vInact_curTarget; closeDialog 0;";
    _Btn11 ctrlSetText  "Place conducteur (DLC)";
    _Btn11 buttonSetAction "player moveInDriver life_vInact_curTarget; closeDialog 0;";
		if(locked _curTarget == 0) then {
			_Btn9 ctrlEnable true;
			_Btn10 ctrlEnable true;
			_Btn11 ctrlEnable true;		
			_Btn9 ctrlshow true;
			_Btn10 ctrlshow true;
			_Btn11 ctrlshow true;

			} else {
			_Btn9 ctrlEnable false;
			_Btn10 ctrlEnable false;
			_Btn11 ctrlEnable false;
			_Btn9 ctrlshow false;
			_Btn10 ctrlshow false;
			_Btn11 ctrlshow false;

			};
	};	

_Btn12 ctrlshow false;

} else {

_Btn10 ctrlshow false;
_Btn11 ctrlshow false;
_Btn12 ctrlshow false;

if(_curTarget isKindOf "Ship") then {
		_Btn2 ctrlSetText localize "STR_vInAct_PushBoat";
		_Btn2 buttonSetAction "[] spawn life_fnc_pushObject; closeDialog 0;";
			if(_curTarget isKindOf "Ship" && {local _curTarget} && {count crew _curTarget == 0}) then { 
				_Btn2 ctrlEnable true;
				} else {
				_Btn2 ctrlEnable false;

				};
		}else{
	_Btn2 ctrlSetText "Dépanneur | Retourner véhicule";
	_Btn2 buttonSetAction "life_vInact_curTarget setPos [getPos life_vInact_curTarget select 0, getPos life_vInact_curTarget select 1, (getPos life_vInact_curTarget select 2)+0.5]; closeDialog 0;";
		if((license_civ_dep)) then {
			_Btn2 ctrlEnable true;
			} else {
			_Btn2 ctrlEnable false;
		};
};	
_Btn3 ctrlShow false;
if(typeOf _curTarget == "O_Truck_03_device_F") then {
	_Btn3 ctrlSetText localize "STR_vInAct_DeviceMine";
	_Btn3 buttonSetAction "[life_vInact_curTarget] spawn life_fnc_deviceMine";
		if(!isNil {(_curTarget getVariable "mining")} OR !local _curTarget && {_curTarget in life_vehicles}) then {
				_Btn3 ctrlEnable false;
				} else {
					if (fuel _curTarget == 0) then {
						_Btn3 ctrlShow false;
						_Btn3 ctrlEnable false;
					}else{
						_Btn3 ctrlEnable true;
						_Btn3 ctrlShow true;
					};
		};
};

	_Btn4 ctrlSetText localize "STR_vInAct_PullOut";
	_Btn4 buttonSetAction "[life_vInact_curTarget] spawn life_fnc_pulloutActionCiv; closeDialog 0;";
		if((currentWeapon player == primaryWeapon player OR currentWeapon player == handgunWeapon player) 
		&& currentWeapon player != "" 
		&& (locked _curTarget == 0)
		&& (count crew _curTarget) > 0)
		then {
			_Btn4 ctrlEnable true;
			} else {
			_Btn4 ctrlEnable false;
		};
	
	_Btn5 ctrlSetText "FORCER CONDUITE";
	_Btn5 buttonSetAction "player moveInDriver life_vInact_curTarget; closeDialog 0;";
	if(
		(locked _curTarget == 0 || ([_uid,name player] in _owners))
		&& (count crew _curTarget) > 0
	)then 
	{
if(count _owners == 0) exitWith{};
	_Btn5 ctrlEnable true;
	} else {
	_Btn5 ctrlEnable false;
	};

	_Btn6 ctrlSetText "FORCER PASSAGER";
	_Btn6 buttonSetAction "player moveInCargo life_vInact_curTarget; closeDialog 0;";
	
	if(
		(locked _curTarget == 0 || ([_uid,name player] in _owners))
		&& (count crew _curTarget) > 0
	)then 
	{
if(count _owners == 0) exitWith{};
	_Btn6 ctrlEnable true;
	} else {
	_Btn6 ctrlEnable false;

	};

if(typeOf (_curTarget) in ["C_Kart_01_Blu_F","C_Kart_01_Red_F","C_Kart_01_Fuel_F","C_Kart_01_Vrana_F","B_Heli_Transport_03_F","B_Heli_Transport_03_unarmed_F","O_Heli_Transport_04_F","O_Heli_Transport_04_ammo_F","O_Heli_Transport_04_bench_F","O_Heli_Transport_04_box_F","O_Heli_Transport_04_covered_F","O_Heli_Transport_04_fuel_F","O_Heli_Transport_04_medevac_F","O_Heli_Transport_04_repair_F"]) then {
    _Btn7 ctrlSetText "Place passager (DLC)";
    _Btn7 buttonSetAction "player moveInCargo life_vInact_curTarget; closeDialog 0;";
    _Btn8 ctrlSetText "Place tireur (DLC)";
    _Btn8 buttonSetAction "player moveInGunner life_vInact_curTarget; closeDialog 0;";
    _Btn9 ctrlSetText  "Place conducteur (DLC)";
    _Btn9 buttonSetAction "player moveInDriver life_vInact_curTarget; closeDialog 0;";
		if(locked _curTarget == 0) then {
			_Btn7 ctrlEnable true;
			_Btn8 ctrlEnable true;
			_Btn9 ctrlEnable true;
			_Btn7 ctrlshow true;
			_Btn8 ctrlshow true;
			_Btn9 ctrlshow true;

			} else {
			_Btn7 ctrlEnable false;
			_Btn8 ctrlEnable false;
			_Btn9 ctrlEnable false;
			_Btn7 ctrlshow false;
			_Btn8 ctrlshow false;
			_Btn9 ctrlshow false;

			};
		};	
	};
};

if(_isBox) then {
	if(typeOf (_curTarget) in ["B_supplyCrate_F","Box_IND_Grenades_F"]) then {
        _Btn1 ctrlSetText "Récupérer coffre";
        _Btn1 buttonSetAction "[life_vInact_curTarget] spawn life_fnc_takeBox; closeDialog 0;";
        _Btn1 ctrlShow true;
	};		
	_Btn2 ctrlShow false;
	_Btn3 ctrlShow false;
	_Btn4 ctrlShow false;
	_Btn5 ctrlShow false;	
	_Btn6 ctrlShow false;
	_Btn7 ctrlShow false;
	_Btn8 ctrlShow false;
	_Btn9 ctrlShow false;
	_Btn10 ctrlshow false;
	_Btn11 ctrlshow false;
	_Btn12 ctrlshow false;
		
};