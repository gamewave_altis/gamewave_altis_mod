/*
Navicat MySQL Data Transfer

Source Server         : Arma3
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : arma3life

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2015-09-09 20:06:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `economy`
-- ----------------------------
DROP TABLE IF EXISTS `economy`;
CREATE TABLE `economy` (
  `numero` int(12) NOT NULL AUTO_INCREMENT,
  `ressource` varchar(32) NOT NULL,
  `sellprice` int(100) NOT NULL DEFAULT '0',
  `buyprice` int(100) NOT NULL DEFAULT '0',
  `varprice` int(100) NOT NULL,
  `minprice` int(100) NOT NULL,
  `maxprice` int(100) NOT NULL,
  `factor` enum('0','1','2','3','4','5','6','7') NOT NULL DEFAULT '0',
  `shoptype` text NOT NULL,
  `total_vendu` int(100) NOT NULL,
  PRIMARY KEY (`numero`),
  UNIQUE KEY `ressource` (`ressource`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of economy
-- ----------------------------
INSERT INTO `economy` VALUES ('1', 'heroinp', '6616', '0', '1', '2000', '50000', '2', 'heroin', '199551');
INSERT INTO `economy` VALUES ('2', 'marijuana', '6234', '0', '1', '2000', '50000', '2', 'heroin', '197221');
INSERT INTO `economy` VALUES ('3', 'meth', '10696', '0', '1', '2000', '50000', '2', 'heroin', '193594');
INSERT INTO `economy` VALUES ('4', 'cocainep', '7016', '0', '1', '2000', '50000', '2', 'heroin', '198959');
INSERT INTO `economy` VALUES ('5', 'oilp', '4129', '0', '1', '500', '10000', '3', 'oil', '99759');
INSERT INTO `economy` VALUES ('6', 'glass', '1838', '0', '1', '500', '10000', '3', 'glass', '99411');
INSERT INTO `economy` VALUES ('7', 'iron_r', '1851', '0', '1', '500', '10000', '3', 'iron', '99491');
INSERT INTO `economy` VALUES ('8', 'diamondc', '5032', '0', '1', '500', '10000', '3', 'diamond', '100215');
INSERT INTO `economy` VALUES ('9', 'salt_r', '2427', '0', '1', '500', '10000', '3', 'salt', '101462');
INSERT INTO `economy` VALUES ('10', 'copper_r', '4698', '0', '1', '500', '10000', '3', 'iron', '97872');
INSERT INTO `economy` VALUES ('11', 'cement', '4573', '0', '1', '500', '10000', '3', 'cement', '99447');
INSERT INTO `economy` VALUES ('12', 'goldbar', '70000', '0', '1', '1', '100000', '1', 'gold', '3825');
INSERT INTO `economy` VALUES ('13', 'turtle', '8000', '0', '1', '2000', '50000', '4', 'wongs', '81102');
INSERT INTO `economy` VALUES ('14', 'woodp', '1800', '0', '1', '500', '10000', '3', 'wood', '5');
INSERT INTO `economy` VALUES ('15', 'uranium', '5000', '0', '1', '500', '500000', '2', 'uranium', '0');

-- ----------------------------
-- Table structure for `gangs`
-- ----------------------------
DROP TABLE IF EXISTS `gangs`;
CREATE TABLE `gangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `owner` varchar(32) DEFAULT NULL,
  `name` varchar(32) DEFAULT NULL,
  `members` text,
  `maxmembers` int(2) DEFAULT '8',
  `bank` int(100) DEFAULT '0',
  `active` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of gangs
-- ----------------------------

-- ----------------------------
-- Table structure for `houses`
-- ----------------------------
DROP TABLE IF EXISTS `houses`;
CREATE TABLE `houses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` varchar(32) NOT NULL,
  `pos` varchar(64) DEFAULT NULL,
  `inventory` text,
  `containers` text,
  `owned` tinyint(4) DEFAULT '0',
  `timer` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`,`pid`),
  KEY `pid` (`pid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- ----------------------------
-- Records of houses
-- ----------------------------

-- ----------------------------
-- Table structure for `logdeath`
-- ----------------------------
DROP TABLE IF EXISTS `logdeath`;
CREATE TABLE `logdeath` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `Killer` text NOT NULL,
  `Killed` text NOT NULL,
  `Type` text NOT NULL,
  `heure` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `uidKiller` text NOT NULL,
  `uidKilled` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of logdeath
-- ----------------------------
INSERT INTO `logdeath` VALUES ('1', 'Derek Tom', 'Derek Tom', '', '2015-05-11 15:47:22', '76561197960498085', '76561197960498085');
INSERT INTO `logdeath` VALUES ('2', 'Derek Tom', 'Derek Tom', '', '2015-05-11 17:44:25', '76561197960498085', '76561197960498085');
INSERT INTO `logdeath` VALUES ('3', 'Derek Tom', 'Derek Tom', '', '2015-05-11 17:53:38', '76561197960498085', '76561197960498085');
INSERT INTO `logdeath` VALUES ('4', 'Tibo dis tib', 'Tibo dis tib', '', '2015-05-11 19:09:16', '76561198131657786', '76561198131657786');
INSERT INTO `logdeath` VALUES ('5', 'Tibo dis tib', 'Tibo dis tib', '', '2015-05-11 19:14:09', '76561198131657786', '76561198131657786');
INSERT INTO `logdeath` VALUES ('6', 'Tibo dis tib', 'Tibo dis tib', '', '2015-05-17 00:20:53', '76561198131657786', '76561198131657786');
INSERT INTO `logdeath` VALUES ('7', 'Error: No vehicle', 'Derek Tom', '', '2015-05-26 15:07:04', '', '76561197960498085');
INSERT INTO `logdeath` VALUES ('8', 'Error: No vehicle', 'Derek Tom', '', '2015-06-03 13:46:55', '', '76561197960498085');
INSERT INTO `logdeath` VALUES ('9', 'Derek Tom', 'Derek Tom', '', '2015-06-14 13:49:33', '76561197960498085', '76561197960498085');
INSERT INTO `logdeath` VALUES ('10', 'Derek Tom', 'Derek Tom', '', '2015-06-21 22:57:36', '76561197960498085', '76561197960498085');
INSERT INTO `logdeath` VALUES ('11', 'Error: No vehicle', 'Derek Tom', '', '2015-06-21 23:05:21', '', '76561197960498085');
INSERT INTO `logdeath` VALUES ('12', 'Derek Tom', 'Derek Tom', '', '2015-06-27 19:47:22', '76561197960498085', '76561197960498085');
INSERT INTO `logdeath` VALUES ('13', 'Derek Tom', 'Derek Tom', '', '2015-06-30 04:40:49', '76561197960498085', '76561197960498085');
INSERT INTO `logdeath` VALUES ('14', 'Derek Tom', 'Derek Tom', '', '2015-07-16 16:18:39', '76561197960498085', '76561197960498085');
INSERT INTO `logdeath` VALUES ('15', 'Derek Tom', 'Derek Tom', '', '2015-07-16 16:26:12', '76561197960498085', '76561197960498085');
INSERT INTO `logdeath` VALUES ('16', 'Derek Tom', 'Derek Tom', '', '2015-07-16 16:31:47', '76561197960498085', '76561197960498085');
INSERT INTO `logdeath` VALUES ('17', 'Derek Tom', 'Derek Tom', '', '2015-07-20 01:41:53', '76561197960498085', '76561197960498085');

-- ----------------------------
-- Table structure for `messages`
-- ----------------------------
DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `uid` int(12) NOT NULL AUTO_INCREMENT,
  `fromID` varchar(50) NOT NULL,
  `toID` varchar(50) NOT NULL,
  `message` text,
  `fromName` varchar(32) NOT NULL,
  `toName` varchar(32) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of messages
-- ----------------------------
INSERT INTO `messages` VALUES ('1', '76561198131657786', '76561197960498085', '\"caca\"', 'Tibo dis tib', 'Derek Tom', '2015-05-17 00:22:20');

-- ----------------------------
-- Table structure for `players`
-- ----------------------------
DROP TABLE IF EXISTS `players`;
CREATE TABLE `players` (
  `uid` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `playerid` varchar(50) NOT NULL,
  `cash` int(100) DEFAULT '0',
  `bankacc` int(100) DEFAULT '0',
  `coplevel` enum('0','1','2','3','4','5','6','7') NOT NULL DEFAULT '0',
  `cop_licenses` text,
  `civ_licenses` text,
  `cop_gear` text,
  `arrested` tinyint(1) NOT NULL DEFAULT '0',
  `aliases` text NOT NULL,
  `adminlevel` enum('0','1','2','3') NOT NULL DEFAULT '0',
  `donatorlvl` enum('0','1','2','3','4','5') NOT NULL DEFAULT '0',
  `civ_gear` text,
  `blacklist` tinyint(1) NOT NULL DEFAULT '0',
  `timestamp` int(11) DEFAULT NULL,
  `worldpos` text,
  `alive` tinyint(1) NOT NULL,
  `duredon` int(1) NOT NULL DEFAULT '0',
  `in_rea` tinyint(1) NOT NULL DEFAULT '0',
  `manger` int(4) NOT NULL DEFAULT '100',
  `boire` int(4) NOT NULL DEFAULT '100',
  `vie` int(4) NOT NULL DEFAULT '100',
  `epargne` int(100) DEFAULT NULL,
  `restrained` tinyint(1) NOT NULL DEFAULT '0',
  `last_update` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `bounties` text,
  `jail_time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `playerid` (`playerid`),
  KEY `name` (`name`),
  KEY `blacklist` (`blacklist`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of players
-- ----------------------------
INSERT INTO `players` VALUES ('2', 'Derek Tom', '76561197960498085', '1745000', '25680438', '7', '\"[[`license_cop_air`,1],[`license_cop_swat`,0],[`license_cop_cg`,0]]\"', '\"[[`license_civ_driver`,1],[`license_civ_air`,1],[`license_civ_heroin`,1],[`license_civ_marijuana`,1],[`license_civ_gang`,0],[`license_civ_boat`,1],[`license_civ_oil`,1],[`license_civ_houblon`,1],[`license_civ_meth`,1],[`license_civ_uranium`,1],[`license_civ_dive`,1],[`license_civ_truck`,1],[`license_civ_piz`,0],[`license_civ_gla`,0],[`license_civ_gun`,1],[`license_civ_rebel`,1],[`license_civ_coke`,0],[`license_civ_diamond`,1],[`license_civ_copper`,1],[`license_civ_iron`,1],[`license_civ_sand`,1],[`license_civ_salt`,1],[`license_civ_cement`,1],[`license_civ_dep`,0],[`license_civ_home`,1],[`license_civ_taxi`,0],[`license_civ_medic`,0],[`license_civ_merc`,0],[`license_civ_wood`,1]]\"', '\"[`cl3_c_poloshirtpants_clpdpatrol_uniform`,`V_TacVest_blk_POLICE`,``,``,`H_Cap_police`,[`ItemMap`,`ItemCompass`,`ItemWatch`,`ItemGPS`],`srifle_LRR_SOS_F`,`cl3_taserM26_Yellow`,[],[`cl3_taserm26mag_mpx`,`cl3_taserm26mag_mpx`,`cl3_taserm26mag_mpx`,`cl3_taserm26mag_mpx`],[],[],[],[`cl3_taserm26mag_mpx`,`cl3_taserm26mag_mpx`],[``,``,`optic_SOS`,``],[``,``,``,``],[]]\"', '0', '\"[`Derek Tom`]\"', '3', '5', '\"[`U_C_Commoner1_1`,``,``,``,``,[`ItemMap`,`ItemCompass`,`ItemWatch`,`ItemRadio`],``,``,[],[],[],[],[],[],[``,``,``,``],[``,``,``,``],[`life_inv_boltcutter`]]\"', '0', null, '\'[3291.62,12962.2,0.00143528]\'', '1', '0', '0', '100', '90', '100', '0', '0', '2015-07-30 18:18:16', '[\"Derek Tom\",\"76561197960498085\",[\"Braquage de banque\"],500000]', '0');
INSERT INTO `players` VALUES ('3', 'Tibo dis tib', '76561198131657786', '0', '26500', '0', '\"[]\"', '\"[[`license_civ_driver`,0],[`license_civ_air`,0],[`license_civ_heroin`,0],[`license_civ_marijuana`,0],[`license_civ_gang`,0],[`license_civ_boat`,0],[`license_civ_oil`,0],[`license_civ_houblon`,0],[`license_civ_meth`,0],[`license_civ_uranium`,0],[`license_civ_dive`,0],[`license_civ_truck`,0],[`license_civ_piz`,0],[`license_civ_gla`,0],[`license_civ_gun`,0],[`license_civ_rebel`,0],[`license_civ_coke`,0],[`license_civ_diamond`,0],[`license_civ_copper`,0],[`license_civ_iron`,0],[`license_civ_sand`,0],[`license_civ_salt`,0],[`license_civ_cement`,0],[`license_civ_dep`,0],[`license_civ_home`,0],[`license_civ_taxi`,0],[`license_civ_medic`,0],[`license_civ_merc`,0],[`license_civ_wood`,0]]\"', '\"[]\"', '0', '\"[`Tibo dis tib`]\"', '0', '5', '\"[``,``,``,``,``,[],``,``,[],[],[],[],[],[],[``,``,``,``],[``,``,``,``],[]]\"', '0', null, '\'[3321.35,12952.5,0.110151]\'', '0', '0', '0', '100', '100', '100', '0', '0', '2015-05-17 00:20:54', null, '0');
INSERT INTO `players` VALUES ('4', 'Derek Tom', '_SP_PLAYER_', '0', '25000', '0', '\"[]\"', '\"[[`license_civ_driver`,1],[`license_civ_air`,1],[`license_civ_heroin`,1],[`license_civ_marijuana`,1],[`license_civ_gang`,0],[`license_civ_boat`,1],[`license_civ_oil`,1],[`license_civ_houblon`,1],[`license_civ_meth`,1],[`license_civ_uranium`,1],[`license_civ_dive`,1],[`license_civ_truck`,1],[`license_civ_piz`,0],[`license_civ_gla`,0],[`license_civ_gun`,1],[`license_civ_rebel`,0],[`license_civ_coke`,0],[`license_civ_diamond`,1],[`license_civ_copper`,1],[`license_civ_iron`,1],[`license_civ_sand`,1],[`license_civ_salt`,1],[`license_civ_cement`,1],[`license_civ_dep`,1],[`license_civ_home`,1],[`license_civ_taxi`,1],[`license_civ_medic`,1],[`license_civ_merc`,0],[`license_civ_wood`,1]]\"', '\"[]\"', '0', '\"[`Derek Tom`]\"', '0', '5', '\"[`U_C_Poloshirt_redwhite`,``,``,``,``,[`ItemMap`,`ItemCompass`,`ItemWatch`,`ItemRadio`],``,``,[],[],[],[],[],[],[``,``,``,``],[``,``,``,``],[]]\"', '0', null, '\'[3281.04,13097.1,0.549184]\'', '1', '0', '0', '100', '100', '100', '0', '0', '2015-05-17 16:25:31', null, '0');

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------

-- ----------------------------
-- Table structure for `vehicles`
-- ----------------------------
DROP TABLE IF EXISTS `vehicles`;
CREATE TABLE `vehicles` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `side` varchar(15) NOT NULL,
  `classname` varchar(32) NOT NULL,
  `type` varchar(12) NOT NULL,
  `pid` varchar(32) NOT NULL,
  `alive` tinyint(1) NOT NULL DEFAULT '1',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `plate` int(20) NOT NULL,
  `color` int(20) NOT NULL,
  `inventory` varchar(500) NOT NULL,
  `assur` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `side` (`side`),
  KEY `pid` (`pid`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of vehicles
-- ----------------------------
INSERT INTO `vehicles` VALUES ('1', 'civ', 'cl3_440cuda_flannery12', 'Car', '76561197960498085', '1', '0', '457555', '7', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('2', 'civ', 'cl3_civic_vti_dark_green', 'Car', '76561197960498085', '1', '0', '972608', '9', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('3', 'civ', 'cl3_civic_vti_green', 'Car', '76561197960498085', '1', '0', '685290', '11', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('4', 'cop', 'C_Hatchback_01_sport_F', 'Car', '76561197960498085', '1', '0', '72093', '7', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('5', 'cop', 'B_Heli_Light_01_F', 'Air', '76561197960498085', '1', '0', '880539', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('6', 'cop', 'B_G_Offroad_01_armed_F', 'Car', '76561197960498085', '1', '0', '384881', '1', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('7', 'cop', 'C_Offroad_01_F', 'Car', '76561197960498085', '1', '0', '156869', '7', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('8', 'cop', 'cl3_dodge_charger_patrol2', 'Car', '76561197960498085', '1', '0', '507612', '9', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('9', 'cop', 'cl3_dodge_charger_patrol', 'Car', '76561197960498085', '1', '0', '427847', '8', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('10', 'cop', 'B_MRAP_01_F', 'Car', '76561197960498085', '1', '0', '258990', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('11', 'cop', 'B_MRAP_01_F', 'Car', '76561197960498085', '1', '0', '476529', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('12', 'cop', 'B_MRAP_01_F', 'Car', '76561197960498085', '1', '0', '586375', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('13', 'cop', 'B_MRAP_01_F', 'Car', '76561197960498085', '1', '0', '353265', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('14', 'cop', 'cl3_dodge_charger_k9', 'Car', '76561197960498085', '1', '0', '180088', '7', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('15', 'cop', 'cl3_transit_black', 'Car', '76561197960498085', '1', '0', '202819', '3', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('16', 'cop', 'B_G_Offroad_01_armed_F', 'Car', '76561197960498085', '1', '0', '883152', '1', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('17', 'civ', 'C_Offroad_01_F', 'Car', '76561197960498085', '1', '0', '887339', '9', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('18', 'cop', 'B_Quadbike_01_F', 'Car', '76561197960498085', '1', '0', '996270', '9', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('19', 'civ', 'C_Hatchback_01_F', 'Car', '76561197960498085', '1', '0', '795059', '9', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('20', 'civ', 'IVORY_CRJ200_1', 'Air', '76561197960498085', '1', '0', '512209', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('21', 'cop', 'B_Quadbike_01_F', 'Car', '76561197960498085', '1', '0', '587768', '9', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('22', 'cop', 'C_Offroad_01_F', 'Car', '76561197960498085', '1', '0', '105700', '7', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('23', 'cop', 'C_SUV_01_F', 'Car', '76561197960498085', '1', '0', '621469', '4', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('24', 'cop', 'B_MRAP_01_F', 'Car', '76561197960498085', '1', '0', '86402', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('25', 'cop', 'cl3_xr_1000_police', 'Car', '76561197960498085', '1', '0', '31914', '24', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('26', 'cop', 'cl3_escalade_patrolbw', 'Car', '76561197960498085', '1', '0', '809473', '26', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('27', 'cop', 'cl3_dodge_charger_patrol2', 'Car', '76561197960498085', '1', '0', '696228', '9', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('28', 'cop', 'cl3_transitpatrol', 'Car', '76561197960498085', '1', '0', '247228', '32', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('29', 'cop', 'C_Hatchback_01_sport_F', 'Car', '76561197960498085', '1', '0', '958895', '7', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('30', 'cop', 'cl3_reventon_clpd', 'Car', '76561197960498085', '1', '0', '27474', '14', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('31', 'civ', 'beetle_psycha1', 'Car', '76561197960498085', '1', '0', '932715', '7', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('32', 'civ', 'C_Hatchback_01_sport_F', 'Car', '76561197960498085', '1', '0', '931495', '8', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('33', 'civ', 'C_Hatchback_01_F', 'Car', '76561197960498085', '1', '0', '416026', '9', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('34', 'civ', 'cl3_bike_bmx_Road2tone1', 'Motorcycle', '76561197960498085', '1', '0', '325392', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('35', 'civ', 'cl3_bike_mountain2tone1', 'Motorcycle', '76561197960498085', '1', '0', '434772', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('36', 'civ', 'cl3_bike_Road2tone1', 'Motorcycle', '76561197960498085', '1', '0', '523772', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('37', 'civ', 'C_Hatchback_01_sport_F', 'Car', '76561197960498085', '1', '0', '191217', '11', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('38', 'civ', 'B_Truck_01_box_F', 'Car', '76561197960498085', '1', '0', '745308', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('39', 'civ', 'Skyline_Mercedes_C63_01_F', 'Car', '76561197960498085', '1', '0', '793700', '3', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('40', 'civ', 'Skyline_Mercedes_C63_02_F', 'Car', '76561197960498085', '1', '0', '357949', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('41', 'civ', 'Skyline_Mercedes_C63_02_F', 'Car', '76561197960498085', '1', '0', '45009', '2', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('42', 'civ', 'Skyline_Mercedes_C63_02_F', 'Car', '76561197960498085', '1', '0', '119902', '4', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('43', 'civ', 'Skyline_Mercedes_C63_02_F', 'Car', '76561197960498085', '1', '0', '78666', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('44', 'civ', 'Skyline_Mercedes_C63_02_F', 'Car', '76561197960498085', '1', '0', '925666', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('45', 'civ', 'Skyline_Mercedes_C63_01_F', 'Car', '76561197960498085', '1', '0', '6355', '0', '\"[]\"', '1');
INSERT INTO `vehicles` VALUES ('46', 'civ', 'Skyline_Mercedes_C63_01_F', 'Car', '76561197960498085', '1', '0', '794911', '0', '\"[]\"', '1');

-- ----------------------------
-- Procedure structure for `cacaresetLifePosition`
-- ----------------------------
DROP PROCEDURE IF EXISTS `cacaresetLifePosition`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `cacaresetLifePosition`()
BEGIN
	UPDATE players SET `alive`= 0;
	UPDATE players SET `in_rea`= 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `deleteDeadVehicles`
-- ----------------------------
DROP PROCEDURE IF EXISTS `deleteDeadVehicles`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteDeadVehicles`()
BEGIN
	DELETE FROM `vehicles` WHERE `alive` = 0 AND `assur` = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `deleteHouses`
-- ----------------------------
DROP PROCEDURE IF EXISTS `deleteHouses`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteHouses`()
BEGIN
UPDATE players SET donatorlvl='5';
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `deleteOldGangs`
-- ----------------------------
DROP PROCEDURE IF EXISTS `deleteOldGangs`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteOldGangs`()
BEGIN
    DELETE FROM `gangs` WHERE `active` = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `deleteOldHouses`
-- ----------------------------
DROP PROCEDURE IF EXISTS `deleteOldHouses`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deleteOldHouses`()
BEGIN
  DELETE FROM `houses` WHERE `owned` = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `Function lucel`
-- ----------------------------
DROP PROCEDURE IF EXISTS `Function lucel`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `Function lucel`()
BEGIN
UPDATE players SET `bankacc`= 99999999;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `GiveMoneyToAll`
-- ----------------------------
DROP PROCEDURE IF EXISTS `GiveMoneyToAll`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `GiveMoneyToAll`()
BEGIN
UPDATE players SET `bankacc`= 99999999;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `resetAssurVehicles`
-- ----------------------------
DROP PROCEDURE IF EXISTS `resetAssurVehicles`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `resetAssurVehicles`()
BEGIN
	UPDATE vehicles SET `alive`= 1 WHERE `assur`= 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `resetCopCash`
-- ----------------------------
DROP PROCEDURE IF EXISTS `resetCopCash`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `resetCopCash`()
BEGIN
	UPDATE players SET `bankacc`= 1250000 WHERE `bankacc`> 1250000 AND `coplevel`= 2;
	UPDATE players SET `bankacc`= 2850000 WHERE `bankacc`> 2850000 AND `coplevel`= 3;
	UPDATE players SET `bankacc`= 3500000 WHERE `bankacc`> 3500000 AND `coplevel`= 4;
	UPDATE players SET `bankacc`= 4700000 WHERE `bankacc`> 4700000 AND `coplevel`= 5;
	UPDATE players SET `bankacc`= 5900000 WHERE `bankacc`> 5900000 AND `coplevel`= 6;
	UPDATE players SET `bankacc`= 8500000 WHERE `bankacc`> 8500000 AND `coplevel`= 7;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `resetLifeVehicles`
-- ----------------------------
DROP PROCEDURE IF EXISTS `resetLifeVehicles`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `resetLifeVehicles`()
BEGIN
	UPDATE vehicles SET `active`= 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `TruncateTable`
-- ----------------------------
DROP PROCEDURE IF EXISTS `TruncateTable`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `TruncateTable`()
BEGIN
Truncate table vehicles;
Truncate table players;
Truncate table users;
Truncate table gangs;
Truncate table houses;
Truncate table messages;
Truncate table logdeath;
END
;;
DELIMITER ;
