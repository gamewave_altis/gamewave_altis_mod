#define __CONST__(var1,var2) var1 = compileFinal (if(typeName var2 == "STRING") then {var2} else {str(var2)})
DB_Async_Active = false;
DB_Async_ExtraLock = false;
life_server_isReady = false;
publicVariable "life_server_isReady";
[] execVM "\life_server\functions.sqf";
[] execVM "\life_server\eventhandlers.sqf";

//I am aiming to confuse people including myself, ignore the ui checks it's because I test locally.
_extDB = false;

//Only need to setup extDB once.
if(isNil {uiNamespace getVariable "life_sql_id"}) then {
	life_sql_id = round(random(9999));
	__CONST__(life_sql_id,life_sql_id);
	uiNamespace setVariable ["life_sql_id",life_sql_id];

	//extDB Version
	_result = "extDB" callExtension "9:VERSION";
	diag_log format ["extDB: Version: %1", _result];
	if(_result == "") exitWith {};
	if ((parseNumber _result) < 19) exitWith {diag_log "Error: extDB version 19 or Higher Required";};

	//Initialize the database
	_result = "extDB" callExtension "9:DATABASE:altisliferpg";
	if(_result != "[1]") exitWith {diag_log "extDB: Error with Database Connection";};
	_result = "extDB" callExtension format["9:ADD:DB_RAW_V2:%1",(call life_sql_id)];
	if(_result != "[1]") exitWith {diag_log "extDB: Error with Database Connection";};
	"extDB" callExtension "9:LOCK";
	_extDB = true;
	diag_log "extDB: Connected to Database";
} else {
	life_sql_id = uiNamespace getVariable "life_sql_id";
	__CONST__(life_sql_id,life_sql_id);
	_extDB = true;
	diag_log "extDB: Still Connected to Database";
};

//Broadbase PV to Clients, to warn about extDB Error.
//	exitWith to stop trying to run rest of Server Code
if (!_extDB) exitWith {
	life_server_extDB_notLoaded = true;
	publicVariable "life_server_extDB_notLoaded";
	diag_log "extDB: Error checked extDB/logs for more info";
};

//Run procedures for SQL cleanup on mission start.
["CALL resetLifeVehicles",1] spawn DB_fnc_asyncCall;
["CALL resetCopCash",1] spawn DB_fnc_asyncCall;
["CALL deleteDeadVehicles",1] spawn DB_fnc_asyncCall;
["CALL resetLifePosition",1] spawn DB_fnc_asyncCall;
["CALL resetAssurVehicles",1] spawn DB_fnc_asyncCall;
["CALL deleteOldHouses",1] spawn DB_fnc_asyncCall;
["CALL deleteOldGangs",1] spawn DB_fnc_asyncCall; //Maybe delete old gangs
["CALL deleteHouses",1] spawn DB_fnc_asyncCall; //Delete house par rapport a la date


//Cr�ation des champs ill�gaux
[] execVM "\life_server\mapping\dealer_1.sqf";
[] execVM "\life_server\mapping\dealer_2.sqf";
[] execVM "\life_server\mapping\dealer_3.sqf";

[] execVM "\life_server\mapping\meth_1.sqf";
[] execVM "\life_server\mapping\meth_2.sqf";

[] execVM "\life_server\mapping\pavot_1.sqf";
[] execVM "\life_server\mapping\pavot_2.sqf";

[] execVM "\life_server\mapping\weed_1.sqf";
[] execVM "\life_server\mapping\weed_2.sqf";

[] execVM "\life_server\mapping\coca_1.sqf";
[] execVM "\life_server\mapping\coca_2.sqf";

[] execVM "\life_server\mapping\uranium_1.sqf";
[] execVM "\life_server\mapping\uranium_2.sqf";
[] execVM "\life_server\mapping\uranium_3.sqf";

[] execVM "\life_server\mapping\heroin_process.sqf";
[] execVM "\life_server\mapping\coke_process.sqf";
[] execVM "\life_server\mapping\meth_process.sqf";
[] execVM "\life_server\mapping\weed_process.sqf";
[] execVM "\life_server\mapping\uranium_process.sqf";



[] execVM "\life_server\pnj.sqf";

life_adminlevel = 0;
life_medicLevel = 0;
life_coplevel = 0;

//Null out harmful things for the server.
__CONST__(JxMxE_PublishVehicle,"No");

//[] execVM "\life_server\fn_initHC.sqf";

life_radio_west = radioChannelCreate [[0, 0.95, 1, 0.8], "Side Channel", "%UNIT_NAME", []];
//life_radio_civ = radioChannelCreate [[0, 0.95, 1, 0.8], "Side Channel", "%UNIT_NAME", []];
//life_radio_indep = radioChannelCreate [[0, 0.95, 1, 0.8], "Side Channel", "%UNIT_NAME", []];

serv_sv_use = [];

fed_bank setVariable["safe",(count playableUnits),true];
cop_jail_spawn = true;
publicVariable "cop_jail_spawn";
//General cleanup for clients disconnecting.
addMissionEventHandler ["HandleDisconnect",{_this call TON_fnc_clientDisconnect; false;}]; //Do not second guess this, this can be stacked this way.

[] spawn TON_fnc_cleanup_veh;
[] spawn TON_fnc_cleanup_obj;
life_gang_list = [];
publicVariable "life_gang_list";
life_wanted_list = [];
client_session_list = [];
RscSpectator_allowFreeCam = false;

[] execFSM "\life_server\cleanup.fsm";
[] execVM "\life_server\extras\cleaner.sqf";


[] spawn
{
	private["_logic","_queue"];
	while {true} do
	{
		sleep (30 * 60);
		_logic = missionnamespace getvariable ["bis_functions_mainscope",objnull];
		_queue = _logic getvariable "BIS_fnc_MP_queue";
		_logic setVariable["BIS_fnc_MP_queue",[],TRUE];
	};
};

[] spawn TON_fnc_federalUpdate;

[] spawn
{
	while {true} do
	{
		sleep (30 * 60);
		{
			_x setVariable["sellers",[],true];
		} foreach [Dealer_1,Dealer_2,Dealer_3];
	};
};

//Strip NPC's of weapons
{
	if(!isPlayer _x) then {
		_npc = _x;
		{
			if(_x != "") then {
				_npc removeWeapon _x;
			};
		} foreach [primaryWeapon _npc,secondaryWeapon _npc,handgunWeapon _npc];
	};
} foreach allUnits;

[] spawn TON_fnc_initHouses;


//Lockup the dome
private["_dome","_rsb"];
_dome = nearestObject [[16019.5,16952.9,0],"Land_Dome_Big_F"];
_rsb = nearestObject [[16019.5,16952.9,0],"Land_Research_house_V1_F"];

for "_i" from 1 to 3 do {_dome setVariable[format["bis_disabled_Door_%1",_i],1,true]; _dome animate [format["Door_%1_rot",_i],0];};
_rsb setVariable["bis_disabled_Door_1",1,true];
_rsb allowDamage false;
_dome allowDamage false;




//Delete objets sur la map
_kiosks_kavala = nearestObject [[3691.12,12880.1,-1.31576],"Land_Kiosk_gyros_F"];
hideObjectGlobal _kiosks_kavala;
_kiosks_kavala enableSimulation false;
_kiosks_kavala allowDamage false;

_chantier_kavala = nearestObject [[3445.17,13188.1,0.00677299],"Land_Unfinished_Building_01_F"];
hideObjectGlobal _chantier_kavala;
_chantier_kavala enableSimulation false;
_chantier_kavala allowDamage false;

_chantier_kavala2 = nearestObject [[3425.9,13176.4,-0.0091486],"Land_Unfinished_Building_02_F"];
hideObjectGlobal _chantier_kavala2;
_chantier_kavala2 enableSimulation false;
_chantier_kavala2 allowDamage false;

_pyrgos_high = nearestObject [[16634.6,12805.7,-0.633558],"Land_Offices_01_V1_F"];
hideObjectGlobal _pyrgos_high;
_pyrgos_high enableSimulation false;
_pyrgos_high allowDamage false;

_pyrgos_low = nearestObject [[16545.8,12784.5,-0.485485],"Land_Offices_01_V1_F"];
hideObjectGlobal _pyrgos_low;
_pyrgos_low enableSimulation false;
_pyrgos_low allowDamage false;  

_pyrgos_medium = nearestObject [[16586.6,12834.5,-0.638582],"Land_Offices_01_V1_F"];
hideObjectGlobal _pyrgos_medium;
_pyrgos_medium enableSimulation false;
_pyrgos_medium allowDamage false;

_sofialow_shop = nearestObject [[25837.1,21428.8,0.00998306],"Land_i_House_Small_03_V1_F"]; 
hideObjectGlobal _sofialow_shop;
_sofialow_shop enableSimulation false;
_sofialow_shop allowDamage false;

_kavala_dep_A = nearestObject [[4178.86,12849,0.119112],"Land_i_Shed_Ind_F"]; 
hideObjectGlobal _kavala_dep_A;
_kavala_dep_A enableSimulation false;
_kavala_dep_A allowDamage false; 

_kavala_dep_B = nearestObject [[4190.31,12823.5,3.8147e-006],"Land_i_Barracks_V2_F"]; 
hideObjectGlobal _kavala_dep_B;
_kavala_dep_B enableSimulation false;
_kavala_dep_B allowDamage false;

_kavala_dep_C = nearestObject [[4227.29,12834,-0.0276737],"Land_cmp_Shed_F"]; 
hideObjectGlobal _kavala_dep_C;
_kavala_dep_C enableSimulation false;
_kavala_dep_C allowDamage false; 

_kavala_dep_D = nearestObject [[4222.81,12818.5,0.00365257],"Land_cmp_Tower_F"]; 
hideObjectGlobal _kavala_dep_D;
_kavala_dep_D enableSimulation false;
_kavala_dep_D allowDamage false;

_kavala_dep_E = nearestObject [[4226.7,12822.2,0.00794792],"Land_cmp_Tower_F"]; 
hideObjectGlobal _kavala_dep_E;
_kavala_dep_E enableSimulation false;
_kavala_dep_E allowDamage false;
 
_kavala_dep_F = nearestObject [[4210.57,12810.3,0.00194359],"Land_spp_Transformer_F"]; 
hideObjectGlobal _kavala_dep_F;
_kavala_dep_F enableSimulation false;
_kavala_dep_F allowDamage false; 

_kavala_dep_G = nearestObject [[4148.56,12859.4,-0.255894],"Land_Shed_Small_F"]; 
hideObjectGlobal _kavala_dep_G;
_kavala_dep_G enableSimulation false;
_kavala_dep_G allowDamage false; 

_kavala_place_A = nearestObject [[3685.83,13120.3,0.153654],"Land_u_Shop_02_V1_F"]; 
hideObjectGlobal _kavala_place_A;
_kavala_place_A enableSimulation false;
_kavala_place_A allowDamage false;

_kavala_place_B = nearestObject [[3686.43,13101.7,-0.111379],"Land_u_Shop_02_V1_F"];
hideObjectGlobal _kavala_place_B;
_kavala_place_B enableSimulation false;
_kavala_place_B allowDamage false;

_kavala_place_C = nearestObject [[3647.7,13077.4,0.115539],"Land_u_Shop_02_V1_F"];
hideObjectGlobal _kavala_place_C;
_kavala_place_C enableSimulation false;
_kavala_place_C allowDamage false;   

//ATIRA
_atira_cloth = nearestObject [[14027.4,18704.1,0.119514],"Land_i_Shop_02_V2_F"]; 
hideObjectGlobal _atira_cloth;
_atira_cloth enableSimulation false;
_atira_cloth allowDamage false;

_atira_lic = nearestObject [[14035.9,18707.9,-0.0275879],"Land_i_House_Big_02_V2_F"];
hideObjectGlobal _atira_lic;
_atira_lic enableSimulation false;
_atira_lic allowDamage false;

_atira_general = nearestObject [[14042.5,18788.6,-0.697803],"Land_u_Shop_02_V1_F"];
hideObjectGlobal _atira_general;
_atira_general enableSimulation false;
_atira_general allowDamage false;

//SOFIA
_sofia_lic = nearestObject [[25780.8,21353.1,-0.0311451],"Land_i_Shop_02_V3_F"];  
hideObjectGlobal _sofia_lic;  
_sofia_lic enableSimulation false;  
_sofia_lic allowDamage false;

_sofia_general = nearestObject [[25732.3,21404.2,-0.0519295],"Land_u_House_Big_01_V1_F"];
hideObjectGlobal _sofia_general;
_sofia_general enableSimulation false;
_sofia_general allowDamage false;

_sofia_cloth = nearestObject [[25665.5,21297.9,-0.0387974],"Land_i_Shop_02_V3_F"];
hideObjectGlobal _sofia_cloth;
_sofia_cloth enableSimulation false;
_sofia_cloth allowDamage false;

//_objA = (getmarkerpos "delete_obj_A") nearestObject 1120919;
//_objA hideObjectGlobal true;
//_objA enableSimulation false;
//_objA allowDamage false;


life_server_isReady = true;
publicVariable "life_server_isReady";