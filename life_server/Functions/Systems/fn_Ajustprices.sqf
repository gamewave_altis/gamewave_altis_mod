/*
	File: fn_Ajustprices.sqf
	Author: worldtrade1101
	
	Description:
	Sends a request to update and adjust the price of stuff in the DB.
*/

//[[0,player,life_shop_type,_amount,_price,_var],"TON_fnc_Adjustprices",false,false] spawn life_fnc_MP;
private["_type","_side","_data","_unit","_ret","_tickTime","_queryResult","_var","_price","_amount"];
_type = [_this,0,0,[0]] call BIS_fnc_param;
_unit = [_this,1,ObjNull,[ObjNull]] call BIS_fnc_param;
_data = [_this,2,"",[""]] call BIS_fnc_param;
_amount = [_this,3,0,[0]] call BIS_fnc_param;
_price = [_this,4,0,[0]] call BIS_fnc_param;
_var = [_this,5,"",[""]] call BIS_fnc_param;


//Error checks
//we randomize the thing to not update every single transaction
diag_log format ["%1   %2    %3    %4      %5      %6",_unit,_type,_data,_amount,_price,_var];

_query = format["UPDATE economy SET total_vendu = (total_vendu + %2) WHERE ressource = '%1'",_var, _amount];
waitUntil {sleep (random 0.3); !DB_Async_Active};
_tickTime = diag_tickTime;
_queryResult = [_query,1] call DB_fnc_asyncCall;
diag_log "------------- Client Query Request -------------";
diag_log format["QUERY: %1",_query];
diag_log format["Time to complete: %1 (in seconds)",(diag_tickTime - _tickTime)];
diag_log format["Result: %1",_queryResult];
diag_log "------------------------------------------------";


//645 "civ_2   0    diamond    1      760      diamondc"
if( (_data == "") OR (isNull _unit)) exitWith
{
diag_log "data ou type ou unit null";
};

_unit = owner _unit; //for hack purpose!

//we randomize the thing to not update every single transaction
_randomnumber = random 100;

if (_randomnumber < 60) exitwith {diag_log "This transaction doesn't update the price!"};

_query = switch (_data) do {
		case "heroin" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='2' AND factor != '0' ",_data];};
		case "wongs" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='2' AND factor != '0' ",_data];};
		case "oil" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='3' AND factor != '0' ",_data];};
		case "diamond" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='3' AND factor != '0' "];};
		case "iron" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='3' AND factor != '0' ",_data];};
		case "glass" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='3' AND factor != '0' ",_data];};
		case "salt" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='3' AND factor != '0' ",_data];};
		case "cement" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='3' AND factor != '0' ",_data];};
		case "wood" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='3' AND factor != '0' ",_data];};
		case "gold" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE shoptype='%1' AND factor != '0' ",_data];};
		//case "market" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='1' AND factor != '0' ",_data];};
		//case "rebel" :{ format["SELECT ressource, buyprice, sellprice FROM economy WHERE shoptype='%1' OR shoptype='market' ",_data];};
		//case "fishmarket" :{format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE factor='4' AND factor != '0' ",_data];};
		//case "cop" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE shoptype='%1' AND factor != '0' ",_data];};
		//case "gang" :{ format["SELECT ressource, buyprice, sellprice, varprice, minprice, maxprice FROM economy WHERE shoptype='%1' AND factor != '0' ",_data];};
		default {"Error"};
};




if(_query == "Error") exitWith {
	
	
	diag_log "error";
};



waitUntil{sleep (random 0.3); !DB_Async_Active};
_tickTime = diag_tickTime;
_queryResult = [_query,2,true] call DB_fnc_asyncCall;

diag_log "------------- Client Query Request -------------";
diag_log format["QUERY: %1",_query];
diag_log format["Time to complete: %1 (in seconds)",(diag_tickTime - _tickTime)];
diag_log format["Result: %1",_queryResult];
diag_log "------------------------------------------------";
_sellingfactor =((count _queryResult)-1);
_query ="";
{
//diag_log format ["test : %1 (%2)", _this , typeName _this];
//diag_log format ["var : %1 (%2)", _var , typeName _var];
	_ressource = _x select 0;
	//diag_log format ["ressource : %1 (%2)", _ressource  , typeName _ressource ];
	_buyprice =  (_x select 1);
	_sellprice =  (_x select 2);
	_varprice =  (_x select 3);
	_minprice = (_x select 4);
	_maxprice = (_x select 5);

	if (_ressource == _var) then { //C'est l'item vendu ou achete
		if (_type == 0) then {//si on vend l'item
			if (_buyprice != 0) then {if( (_buyprice - (_varprice * _amount)) > _minprice) then {_buyprice= _buyprice - (_varprice * _amount);};};
			if ((_sellprice - (_varprice * _amount *_sellingfactor)) > _minprice) then {_sellprice = _sellprice - (_varprice * _amount *_sellingfactor);};
			if (_buyprice != 0) then {if ((_sellprice >= _buyprice)) then {_buyprice=_sellprice + 15};};
		} else {//si on achete l'item
			if (_buyprice != 0) then {if( (_buyprice + (_varprice * _amount)) < (_maxprice+15)) then {_buyprice = _buyprice + (_varprice * _amount);};};
			if ((_sellprice + (_varprice * _amount)) < _maxprice) then {_sellprice = _sellprice + (_varprice * _amount);};
		};

	} else {
		if (_type == 0) then {//si on a vendu un autre item on augmente le rpix
			if (_buyprice != 0) then {if( (_buyprice + (_varprice * _amount)) < (_maxprice)) then {_buyprice = _buyprice + (_varprice * _amount);};};
			if ((_sellprice + (_varprice * _amount)) < _maxprice) then {_sellprice = _sellprice + (_varprice * _amount);};

		} else { //si on achete un autre item on baise le prix
			if (_buyprice != 0) then {if( (_buyprice - (_varprice * _amount)) > _minprice ) then {_buyprice= _buyprice - (_varprice * _amount);};};
			if ((_sellprice - (_varprice * _amount)) > _minprice) then {_sellprice = _sellprice - (_varprice * _amount);};
		};
	};

	_query =format["UPDATE economy SET buyprice='%1', sellprice='%2' WHERE ressource='%3'",_buyprice,_sellprice,_ressource];
	//_query = format["UPDATE economy SET total_vendu = (total_vendu + %2) WHERE ressource = '%1'",_var, _amount];
	waitUntil {sleep (random 0.3); !DB_Async_Active};
_queryResult = [_query,1] call DB_fnc_asyncCall;
	diag_log "------------- Client Query Request -------------";
diag_log format["QUERY: %1",_query];
diag_log format["Time to complete: %1 (in seconds)",(diag_tickTime - _tickTime)];
diag_log format["Result: %1",_queryResult];
diag_log "------------------------------------------------";
}foreach _queryResult;
