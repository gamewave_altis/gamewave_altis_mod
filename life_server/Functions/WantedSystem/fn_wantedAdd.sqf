/*
	File: fn_wantedAdd.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Adds or appends a unit to the wanted list.
*/
private["_uid","_type","_index","_data","_crimes","_val","_customBounty","_name"];
_uid = [_this,0,"",[""]] call BIS_fnc_param;
_name = [_this,1,"",[""]] call BIS_fnc_param;
_type = [_this,2,"",[""]] call BIS_fnc_param;
_customBounty = [_this,3,-1,[0]] call BIS_fnc_param;
if(_uid == "" OR _type == "" OR _name == "") exitWith {}; //Bad data passed.

//What is the crime?
switch(_type) do
{
	case "187V": {_type = ["Meurtre avec un Véhicule",180000]};
	case "187": {_type = ["Meurtre",200000]};
	case "901": {_type = ["Echappé de Prison",10000]};
	case "261": {_type = ["Viol",5000]};
	case "261A": {_type = ["Tentative de Viol",3000]};
	case "215": {_type = ["Tentative de Vol de Véhicule",20000]};
	case "213": {_type = ["Utilisation Illégale d'Explosifs",10000]};
	case "211B": {_type = ["Petit Vol d'argent",25000]};
	case "211A": {_type = ["Vol d'argent",75000]};
	case "211": {_type = ["Grand vol d'argent",125000]};
	case "207": {_type = ["Enlèvement",3500]};
	case "207A": {_type = ["Tentative d'Enlèvement",2000]};
	case "487": {_type = ["Vol de Véhicule",50000]};
	case "480": {_type = ["Délit de fuite",1300]};
	case "481": {_type = ["Possession de drogues",1000]};
	case "502": {_type = ["Conduite sous l'emprise d'alcool",7000]};
	case "502D": {_type = ["Conduite sous l'emprise de drogues",10000]};
	case "1050": {_type = ["Utilisation de drogues",5000]};
	case "482": {_type = ["Intention de Distribuer de la Drogue",5000]};
	case "483": {_type = ["Trafic de Drogue",50000]};
	case "500": {_type = ["Facture depanneur impayée",5000]};
	case "600": {_type = ["Honoraire médecin impayée",5000]};
	case "700": {_type = ["Facture taxi impayée",5000]};
	case "800": {_type = ["Excès de vitesse en ville > 55km/h",2500]};
	case "801": {_type = ["Excès de vitesse en ville > 100km/h",5000]};
	case "802": {_type = ["Excès de vitesse hors-ville > 130km/h",2500]};
	case "803": {_type = ["Excès de vitesse hors-ville > 200km/h",2500]};
	case "459": {_type = ["Braquage de banque",500000]};
	case "460": {_type = ["Attaque de prison",200000]};
	default {_type = [];};
};

if(count _type == 0) exitWith {}; //Not our information being passed...
//Is there a custom bounty being sent? Set that as the pricing.
if(_customBounty != -1) then {_type set[1,_customBounty];};
//Search the wanted list to make sure they are not on it.
_index = [_uid,life_wanted_list] call TON_fnc_index;

if(_index != -1) then
{
	_data = life_wanted_list select _index;
	_crimes = _data select 2;
	_crimes pushBack (_type select 0);
	_val = _data select 3;
	life_wanted_list set[_index,[_name,_uid,_crimes,(_type select 1) + _val]];
	[[_name,_uid,_crimes,(_type select 1) + _val],_uid] spawn TON_fnc_saveBounties;
}
	else
{
	life_wanted_list pushBack [_name,_uid,[(_type select 0)],(_type select 1)];
	[[_name,_uid,[(_type select 0)],(_type select 1)],_uid] spawn TON_fnc_saveBounties;
};