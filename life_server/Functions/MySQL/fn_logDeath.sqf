/*
	LogDeath
	think & tweak by lecul / altislife.fr
*/
private["_unit","_source","_type","_uidKiller","_uidKilled"];
_unit = [_this,0,"",[""]] call BIS_fnc_param;
_source = [_this,1,"",[""]] call BIS_fnc_param;
_type = [_this,2,"",[""]] call BIS_fnc_param;
_uidKiller = [_this,3,"",[""]] call BIS_fnc_param;
_uidKilled = [_this,4,"",[""]] call BIS_fnc_param;

_query = format["INSERT INTO logDeath (Killer, Killed, Type, uidKiller, uidKilled) VALUES ('%1', '%2', '%3', '%4', '%5')",_source,_unit,_type,_uidKiller,_uidKilled];	
waitUntil {sleep (random 0.3); !DB_Async_Active};
[_query,1] call DB_fnc_asyncCall;