/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Takes partial data of a player and updates it, this is meant to be
	less network intensive towards data flowing through it for updates.
*/
private["_uid","_side","_value","_mode","_query","_value2","_alive","_position","_in_rea","_boire","_manger","_vie","_restrain"];
_uid = [_this,0,"",[""]] call BIS_fnc_param;
_side = [_this,1,sideUnknown,[civilian]] call BIS_fnc_param;
_mode = [_this,3,-1,[0]] call BIS_fnc_param;

if(_uid == "" OR _side == sideUnknown) exitWith {}; //Bad.
_query = "";

switch(_mode) do {
	case 0: {
		_value = [_this,2,0,[0]] call BIS_fnc_param;
		_value = [_value] call DB_fnc_numberSafe;
		_query = format["UPDATE players SET cash='%1' WHERE playerid='%2'",_value,_uid];
	};
	
	case 1: {
		_value = [_this,2,0,[0]] call BIS_fnc_param;
		_value = [_value] call DB_fnc_numberSafe;
		_query = format["UPDATE players SET bankacc='%1' WHERE playerid='%2'",_value,_uid];
	};
	
	case 2: {
		_value = [_this,2,[],[[]]] call BIS_fnc_param;
		//Does something license related but I can't remember I only know it's important?
		for "_i" from 0 to count(_value)-1 do {
			_bool = [(_value select _i) select 1] call DB_fnc_bool;
			_value set[_i,[(_value select _i) select 0,_bool]];
		};
		_value = [_value] call DB_fnc_mresArray;
		switch(_side) do {
			case west: {_query = format["UPDATE players SET cop_licenses='%1' WHERE playerid='%2'",_value,_uid];};
			case civilian: {_query = format["UPDATE players SET civ_licenses='%1' WHERE playerid='%2'",_value,_uid];};
			case independent: {_query = format["UPDATE players SET med_licenses='%1' WHERE playerid='%2'",_value,_uid];};
		};
	};
	// UPDATEGEAR
	case 3: {
		_value = [_this,2,[],[[]]] call BIS_fnc_param;
		_value = [_value] call DB_fnc_mresArray;
		switch(_side) do {
			case west: {_query = format["UPDATE players SET cop_gear='%1' WHERE playerid='%2'",_value,_uid];};
			case civilian: {_query = format["UPDATE players SET civ_gear='%1' WHERE playerid='%2'",_value,_uid];};
			//case independent: {_query = format["UPDATE players SET med_gear='%1' WHERE playerid='%2'",_value,_uid];};
		};
	};
	
	case 4: {
		_value = [_this,2,false,[true]] call BIS_fnc_param;
		_value = [_value] call DB_fnc_bool;
		_query = format["UPDATE players SET alive='%1' WHERE playerid='%2'",_value,_uid];
	};
	
	case 5: {
		_value = [_this,2,false,[true]] call BIS_fnc_param;
		_value = [_value] call DB_fnc_bool;
		_query = format["UPDATE players SET arrested='%1' WHERE playerid='%2'",_value,_uid];
	};
	
	// UPDATE THUNAS !
	case 6: {
		_value = [_this,2,0,[0]] call BIS_fnc_param; //life_flouze
		_value2 = [_this,4,0,[0]] call BIS_fnc_param; //life_dabflouze
		_value = [_value] call DB_fnc_numberSafe;
		_value2 = [_value2] call DB_fnc_numberSafe;
		_query = format["UPDATE players SET cash='%1', bankacc='%3' WHERE playerid='%2'",_value,_uid,_value2];
	};

	// UPDATE ALIVE / POS / REA / BOIRE / MANGER / VIE / RESTRAIN
	//[[getPlayerUID player,playerSide,alive player,7,getPos player,life_in_rea,life_hunger,life_thirst,life_damage,life_restrained],"DB_fnc_updatePartial",false,false] spawn life_fnc_MP;
	case 7: {
		_alive = [_this,2,false] call BIS_fnc_param;
		_alive = [_alive] call DB_fnc_bool;
		_position = [_this,4,[]] call BIS_fnc_param;
		_in_rea = [_this,5,false] call BIS_fnc_param;
		_in_rea = [_in_rea] call DB_fnc_bool;
		_boire = _this select 7;
		_manger = _this select 6;
		_vie = _this select 8;
		_restrain = [_this,9,false] call BIS_fnc_param;
		_restrain = [_restrain] call DB_fnc_bool;
		_query = format["UPDATE players SET alive='%1', worldpos=""'%3'"", in_rea='%4', manger='%5', boire='%6', vie='%7', restrained='%8' WHERE playerid='%2'",_alive,_uid,_position,_in_rea,_manger,_boire,_vie,_restrain];
	};
	
	case 8: {
		_array = [_this,2,[],[[]]] call BIS_fnc_param;
		[_uid,_side,_array,0] call TON_fnc_keyManagement;
	};
	
	//CIV AUTOSAVE
	case 9: {
		_gear = [_this,2,[],[[]]] call BIS_fnc_param;
		_gear = [_gear] call DB_fnc_mresArray;
		_flouze = [_this,4,0,[0]] call BIS_fnc_param; //life_flouze
		_dabflouze = [_this,5,0,[0]] call BIS_fnc_param; //life_dabflouze
		_flouze = [_flouze] call DB_fnc_numberSafe;
		_dabflouze = [_dabflouze] call DB_fnc_numberSafe;
		_alive = [_this,6,false] call BIS_fnc_param;
		_alive = [_alive] call DB_fnc_bool;
		_position = [_this,7,[]] call BIS_fnc_param;
		_in_rea = [_this,8,false] call BIS_fnc_param;
		_in_rea = [_in_rea] call DB_fnc_bool;
		_manger = _this select 9;
		_boire = _this select 10;
		_vie = _this select 11;
		_restrain = [_this,12,false] call BIS_fnc_param;
		_restrain = [_restrain] call DB_fnc_bool;
		_query = format["UPDATE players SET civ_gear='%1', cash='%3', bankacc='%4', alive='%5', worldpos=""'%6'"", in_rea='%7', manger='%8', boire='%9', vie='%10', restrained='%11' WHERE playerid='%2'",_gear,_uid,_flouze,_dabflouze,_alive,_position,_in_rea,_manger,_boire,_vie,_restrain];
	};
};

if(_query == "") exitWith {};
waitUntil {!DB_Async_Active};
[_query,1] call DB_fnc_asyncCall;