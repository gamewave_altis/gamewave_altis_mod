private ["_objs","_marker"];

_marker = createMarker ["radiaFion_1",[9241.04,8758.73,2.12782]];
_objs = [
	["Land_Factory_Main_ruins_F",[9246.77,8751.57,-4.90079],190.789,[[-0.187186,-0.982324,0],[-0,0,1]],false],
	["Land_spp_Tower_dam_F",[9247.45,8773.8,-19.3606],16.0789,[[0.276961,0.960881,0],[0,0,1]],false],
	["Land_dp_bigTank_ruins_F",[9220.74,8752.64,-3.77442],233.283,[[-0.801598,-0.597864,0],[-0,0,1]],false],
	["Land_Factory_Tunnel_F",[9241.83,8744.07,-3.36529],188.875,[[-0.154271,-0.988029,0],[-0,0,1]],false],
	["Land_Device_disassembled_F",[9241.04,8758.73,2.12782],8.561,[[0.148862,0.988858,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9138.01,8800.95,0],81.9259,[[0.990087,0.140454,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9215.86,8640.94,0],40.9629,[[0.655571,0.755134,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9140.19,8785.59,0],81.9259,[[0.990087,0.140454,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9142.37,8770.23,0],81.9259,[[0.990087,0.140454,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9144.54,8754.87,0],81.9259,[[0.990087,0.140454,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9148.9,8724.14,0],81.9259,[[0.990087,0.140454,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9153.26,8693.42,0],81.9259,[[0.990087,0.140454,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9157.62,8662.7,0],81.9259,[[0.990087,0.140454,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9161.97,8631.97,0],81.9259,[[0.990087,0.140454,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9166.33,8601.25,0],81.9259,[[0.990087,0.140454,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9183.48,8588.01,0],337.796,[[-0.377909,0.925843,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9212.21,8599.74,-7.62939e-006],337.796,[[-0.377909,0.925843,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9240.94,8611.47,-7.62939e-006],337.796,[[-0.377909,0.925843,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9269.67,8623.19,-7.62939e-006],337.796,[[-0.377909,0.925843,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9298.4,8634.92,-7.62939e-006],337.796,[[-0.377909,0.925843,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9327.13,8646.65,-7.62939e-006],337.796,[[-0.377909,0.925843,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9355.85,8658.37,-7.62939e-006],337.796,[[-0.377909,0.925843,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9370.22,8664.24,-7.62939e-006],337.796,[[-0.377909,0.925843,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9398.95,8675.96,-7.62939e-006],337.796,[[-0.377909,0.925843,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9413.31,8681.83,-7.62939e-006],337.796,[[-0.377909,0.925843,0],[0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9421.96,8701.69,-7.62939e-006],260.158,[[-0.985281,-0.17094,0],[-0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9419.31,8716.98,-7.62939e-006],260.158,[[-0.985281,-0.17094,0],[-0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9414,8747.55,-7.62939e-006],260.158,[[-0.985281,-0.17094,0],[-0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9411.35,8762.84,-7.62939e-006],260.158,[[-0.985281,-0.17094,0],[-0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9406.05,8793.41,-7.62939e-006],260.158,[[-0.985281,-0.17094,0],[-0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9403.39,8808.69,-7.62939e-006],260.158,[[-0.985281,-0.17094,0],[-0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9398.09,8839.27,-7.62939e-006],260.158,[[-0.985281,-0.17094,0],[-0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9395.44,8854.55,-7.62939e-006],260.158,[[-0.985281,-0.17094,0],[-0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9390.13,8885.13,-8.10623e-006],260.158,[[-0.985281,-0.17094,0],[-0,0,1]],false],
	["Land_Sign_WarningUnexplodedAmmo_F",[9384.83,8915.7,-8.10623e-006],260.158,[[-0.985281,-0.17094,0],[-0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
		_obj enableSimulation false;
		_obj allowDamage false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
				_obj enableSimulation false;
		_obj allowDamage false;
	};
} foreach _objs;