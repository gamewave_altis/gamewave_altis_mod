private ["_objs","_objs2"];
//_marker = createMarker ["Dealer_1",[5004.25,11170.4,0]];
_objs = [
	["",[15557.4,16289,0],156.359,[[0.401001,-0.916078,0],[0,-0,1]],false]
];

{
	private ["_obj"];
	_obj = nearestObject [[8485.12,25264.9,0.00105286],"cl3_c_poloshirtpants_1_uniform"];

		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;
		_obj forceAddUniform "alfr_coveralls_antirad";
		_obj addHeadgear "H_PilotHelmetFighter_B";
		_obj addGoggles "G_Sport_Blackred";
		_obj setFace "WhiteHead_18";


		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj allowDamage false;
		_obj enableSimulation false;


} foreach _objs;


_objs2 = [
	["Land_Cargo20_brick_red_F",[15556.3,16286.5,0],357.324,[[-0.0466895,0.998909,0],[0,0,1]],false],
	["Land_WaterTank_F",[15555,16288.7,0],357.324,[[-0.0466895,0.998909,0],[0,0,1]],false],
	["Land_PressureWasher_01_F",[15554.9,16290.1,0],87.1636,[[0.998775,0.0494841,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
				_obj allowDamage false;
		_obj enableSimulation false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
				_obj allowDamage false;
		_obj enableSimulation false;
	};
} foreach _objs2;