private ["_objs","_marker"];
_marker = createMarker ["heroin_2",[5208.3,21055.9,0]];
_objs = [
	["Land_TouristShelter_01_F",[5205.68,21048,0.217209],273.097,[[-0.998539,0.0540327,0],[0,0,1]],false],
	["Land_TouristShelter_01_F",[5206.02,21054.3,0.153641],273.097,[[-0.998539,0.0540327,0],[0,0,1]],false],
	["Land_TouristShelter_01_F",[5206.36,21060.5,0],273.097,[[-0.998539,0.0540327,0],[0,0,1]],false],
	["Land_TouristShelter_01_F",[5206.7,21066.7,0],273.097,[[-0.998539,0.0540327,0],[0,0,1]],false],
	["Land_TouristShelter_01_F",[5210.95,21066.4,0.111679],93.0994,[[0.998537,-0.0540693,0],[0,-0,1]],false],
	["Land_TouristShelter_01_F",[5210.61,21060.2,0.140472],93.0994,[[0.998537,-0.0540693,0],[0,-0,1]],false],
	["Land_TouristShelter_01_F",[5210.27,21054,0.182419],93.0994,[[0.998537,-0.0540693,0],[0,-0,1]],false],
	["Land_TouristShelter_01_F",[5209.93,21047.7,0.111679],93.0994,[[0.998537,-0.0540693,0],[0,-0,1]],false],
	["cl_p_papaver_EP1",[5210.58,21047.6,0],3.06264,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5210.75,21050.7,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5210.91,21053.7,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5211.07,21056.7,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5211.23,21059.7,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5211.39,21062.8,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5211.65,21065.3,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5206.17,21069,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5206.03,21066.8,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5205.87,21063.8,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5205.71,21060.8,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5205.55,21057.7,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5205.39,21054.7,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5205.22,21051.7,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5211.74,21068,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5205.06,21048.6,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5207.85,21048.5,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5208.15,21054.1,0],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5208.82,21060.4,-0.0714722],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[5209.22,21066.7,0.00813293],3.06265,[[0.0534279,0.998572,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
		_obj enableSimulation false;
		_obj allowDamage false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		_obj enableSimulation false;
		_obj allowDamage false;
	};
} foreach _objs;