private ["_objs","_marker"];
_marker = createMarker ["radiaFion_2",[23557.5,24557.6,-0.434181]];
_objs = [
	["F35B_wreck",[23557.5,24557.6,-0.434181],228.062,[[-0.743869,-0.668325,0],[-0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
		_obj enableSimulation false;
		_obj allowDamage false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		_obj enableSimulation false;
		_obj allowDamage false;
	};
} foreach _objs;