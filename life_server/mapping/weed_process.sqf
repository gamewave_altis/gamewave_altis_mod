private ["_objs","_objs2"];
//_marker = createMarker ["Dealer_1",[5004.25,11170.4,0]];
_objs = [
	["",[20591.3,16036.4,0.243074],338.974,[[-0.358797,0.933416,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = nearestObject [[8483.09,25262,0.00141907],"cl3_c_poloshirtpants_1_uniform"];

		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;
		_obj forceAddUniform "alfr_poloshirt_legalize";
		_obj addHeadgear "H_Booniehat_oli";
		_obj addGoggles "SFG_Tac_BeardD";
		_obj setFace "PersianHead_A3_01";


		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj allowDamage false;
		_obj enableSimulation false;


} foreach _objs;


_objs2 = [
	["Land_WoodenTable_large_F",[20593.2,16037.3,0.180002],333.622,[[-0.444297,0.895879,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
				_obj allowDamage false;
		_obj enableSimulation false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
				_obj allowDamage false;
		_obj enableSimulation false;
	};
} foreach _objs2;