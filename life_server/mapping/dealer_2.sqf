private ["_objs"];
//_marker = createMarker ["Dealer_2",[21862,20890.3,0]];
_objs = [
	["",[21846.6,20924.4,0.249623],33.2598,[[0.548436,0.836192,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = nearestObject [[8476.43,25253.5,0.0014267],"cl3_c_poloshirtpants_1_uniform"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);

		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;
		_obj forceAddUniform "alfr_poloshirt_legalize";
		_obj addGoggles "G_Aviator";
		_obj setFace "AfricanHead_01";
		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj allowDamage false;
		_obj enableSimulation false;
		
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		
		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;
		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj forceAddUniform "alfr_poloshirt_legalize";
		_obj addGoggles "G_Aviator";
		_obj setFace "WhiteHead_01";


		_obj allowDamage false;
		_obj enableSimulation false;
		

	};
} foreach _objs;