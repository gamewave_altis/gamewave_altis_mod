private ["_objs","_marker"];
_marker = createMarker ["heroin_1",[9489.86,7510.73,0]];
_objs = [
	["cl_p_papaver_EP1",[9493.57,7510.44,0],0,[[0,1,0],[0,0,1]],false],
	["CamoNet_OPFOR_big_F",[9489.86,7510.73,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9491.78,7510.44,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9489.99,7510.44,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9488.2,7510.44,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9488.2,7508.41,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9489.99,7508.41,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9491.78,7508.41,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9493.57,7508.41,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9493.57,7506.38,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9491.78,7506.38,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9489.99,7506.38,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9488.2,7506.38,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9488.2,7504.35,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9489.99,7504.35,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9491.78,7504.35,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9493.57,7504.35,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9493.57,7502.32,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9491.78,7502.32,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9489.99,7502.32,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_papaver_EP1",[9488.2,7502.32,0],0,[[0,1,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
				_obj enableSimulation false;
		_obj allowDamage false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
				_obj enableSimulation false;
		_obj allowDamage false;
	};
} foreach _objs;