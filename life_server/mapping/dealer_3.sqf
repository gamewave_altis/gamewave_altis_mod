private ["_objs"];
//_marker = createMarker ["Dealer_3",[15525.6,15761,14.2853]];
_objs = [
	["",[16856.7,16190.7,0],337.827,[[-0.377407,0.926047,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = nearestObject [[8473.76,25252.3,0.00143433],"cl3_c_poloshirtpants_1_uniform"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);

		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;
		_obj forceAddUniform "alfr_poloshirt_happy";
		_obj addHeadgear "cl3_Headgear_paperbaghappy";
		_obj setFace "AfricanHead_01";
		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj allowDamage false;
		_obj enableSimulation false;

	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		
		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;
		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj forceAddUniform "alfr_poloshirt_happy";
		_obj addHeadgear "cl3_Headgear_paperbaghappy";
		_obj setFace "AfricanHead_01";
		
		_obj allowDamage false;
		_obj enableSimulation false;
		

	};
} foreach _objs;

