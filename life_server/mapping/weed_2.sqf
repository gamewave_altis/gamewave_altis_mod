/*
Cr�ation du champs de cannabis
www.altislife.fr
*/

private ["_objs","_marker"];
_marker = createMarker ["weed_2",[26232.1,20391.6,0]];
_objs = [
	["CamoNet_INDP_big_Curator_F",[26232.1,20391.6,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26229.7,20393.1,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26231.6,20393.1,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26233.4,20393.1,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26235.3,20393.1,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26237.1,20393.1,0],0,[[0,1,0],[0,0,1]],false],	
	["cl_p_fiberPlant_EP1",[26234.4,20392.2,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26230.6,20392.2,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26231.6,20391.3,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26234.4,20391.3,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26235.3,20391.3,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26236.2,20391.3,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26237.1,20390.4,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26235.3,20390.4,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26231.6,20390.4,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26229.7,20390.4,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26231.6,20389.4,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26233.4,20389.4,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26236.2,20389.4,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26237.1,20389.4,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26236.2,20388.5,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26235.3,20388.5,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26233.4,20388.5,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26230.6,20388.5,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26229.7,20388.5,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26230.6,20387.6,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26231.6,20387.6,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26234.4,20387.6,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26236.2,20387.6,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[26237.1,20387.6,0],0,[[0,1,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
		_obj enableSimulation false;
		_obj allowDamage false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		_obj enableSimulation false;
		_obj allowDamage false;
	};
} foreach _objs;