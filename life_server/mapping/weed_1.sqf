/*
Cr�ation du champs de cannabis
www.altislife.fr
*/

private ["_objs","_marker"];
_marker = createMarker ["weed_1",[18348.9,7906.76,0]];
_objs = [
	["CamoNet_INDP_big_F",[18348.9,7906.76,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18345.3,7908.25,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18349,7908.25,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18350.8,7908.25,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18351.7,7907.33,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18349.9,7907.33,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18346.2,7907.33,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18347.1,7906.4,0],0,[[0,1,0],[0,0,1]],false],	
	["cl_p_fiberPlant_EP1",[18350.8,7906.4,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18351.7,7905.48,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18348,7905.48,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18346.2,7905.48,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18345.3,7904.55,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18347.1,7904.55,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18349,7904.55,0],0,[[0,1,0],[0,0,1]],false],	
	["cl_p_fiberPlant_EP1",[18351.7,7903.62,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18349.9,7903.62,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18345.3,7902.7,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18347.1,7902.7,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18350.8,7902.7,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18351.7,7901.77,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18349.9,7901.77,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18346.2,7901.77,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18344.3,7901.77,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18344.3,7903.62,0],0,[[0,1,0],[0,0,1]],false],
	["cl_p_fiberPlant_EP1",[18344.3,7907.33,0],0,[[0,1,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
		_obj enableSimulation false;
		_obj allowDamage false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		_obj enableSimulation false;
		_obj allowDamage false;
	};
} foreach _objs;