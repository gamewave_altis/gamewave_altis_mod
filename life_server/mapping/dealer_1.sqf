private ["_objs"];
//_marker = createMarker ["Dealer_1",[5004.25,11170.4,0]];
_objs = [
	["",[5004.25,11170.4,0],0,[[0,1,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = nearestObject [[8473.31,25254.5,0.0014267],"cl3_c_poloshirtpants_1_uniform"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
		
		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;
		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj forceAddUniform "cl3_suit_brown_blue_black";
		_obj addGoggles "G_Aviator";
		_obj setFace "AfricanHead_01";
		_obj allowDamage false;
		_obj enableSimulation false;

	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		
		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;
		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj forceAddUniform "cl3_suit_brown_blue_black";
		_obj addGoggles "G_Aviator";
		_obj setFace "AfricanHead_01";
		
		_obj allowDamage false;
		_obj enableSimulation false;

	};
} foreach _objs;