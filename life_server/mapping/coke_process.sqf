private ["_objs","_objs2"];
//_marker = createMarker ["Dealer_1",[5004.25,11170.4,0]];

_objs = [
	["",[12083.8,22797.4,0.683942],321.388,[[-0.624042,0.781391,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = nearestObject [[8482.03,25260.7,0.00141907],"cl3_c_poloshirtpants_1_uniform"];

		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);

		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;

		_obj forceAddUniform "A3L_Farmer_Outfit";
		_obj addHeadgear "H_Hat_brown";
		_obj addGoggles "G_mas_wpn_wrap_mask_c";

		_obj setFace "WhiteHead_12";
		_obj setSpeaker "Male01ENG";


		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj allowDamage false;
		_obj enableSimulation false;

} foreach _objs;


_objs2 = [
	["Land_u_Shed_Ind_F",[12083.2,22806.3,1.17808],145.779,[[0.562384,-0.826876,0],[0,-0,1]],false],
	["Land_Shed_Small_F",[12087.1,22793.1,-0.0304623],55.9886,[[0.828927,0.559357,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
				_obj allowDamage false;
		_obj enableSimulation false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
				_obj allowDamage false;
		_obj enableSimulation false;
	};
} foreach _objs2;