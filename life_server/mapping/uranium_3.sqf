private ["_objs","_marker"];
_marker = createMarker ["radiaFion_3",[4008.45,18361.4,0]];
_objs = [
	["Land_Wreck_Plane_Transport_01_F",[4008.45,18361.4,0],226.775,[[-0.728667,-0.684868,0],[-0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
		_obj enableSimulation false;
		_obj allowDamage false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		_obj enableSimulation false;
		_obj allowDamage false;
	};
} foreach _objs;