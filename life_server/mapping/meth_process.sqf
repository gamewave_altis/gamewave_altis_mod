private ["_objs","_objs2"];
//_marker = createMarker ["Dealer_1",[5004.25,11170.4,0]];
_objs = [
	["",[15186.2,11075.6,0.387671],357.04,[[-0.0516302,0.998666,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = nearestObject [[8484.14,25263.5,0.00141907],"cl3_c_poloshirtpants_1_uniform"];

		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;
		_obj forceAddUniform "cl3_suit_black_black_black";
		_obj addHeadgear "H_Hat_brown";
		_obj addGoggles "G_Sport_Blackred";
		_obj setFace "GreekHead_A3_03";



		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj allowDamage false;
		_obj enableSimulation false;

} foreach _objs;

_objs2 = [
	["Land_Cargo20_sand_F",[15197.4,11058.7,-0.208894],306.463,[0,0,1],true],
	["Land_Cargo10_military_green_F",[15183.3,11043.2,0.0103235],196.398,[0,0,1],true],
	["Land_Cargo40_grey_F",[15169.1,11067.8,0.00168037],298.566,[0,0,1],true],
	["Land_i_Shed_Ind_F",[15185,11060.9,0.142681],301.768,[[-0.85019,0.526477,0],[0,0,1]],false],
	["Land_Cargo40_grey_F",[15172.9,11065.7,-0.348742],300.86,[0,0,1],true],
	["WaterPump_01_forest_F",[15185.7,11063.5,0.307993],0,[[0,1,0],[0,0,1]],false],
	["Fridge_01_open_F",[15185.6,11077.2,0.369942],353.883,[[-0.106555,0.994307,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
				_obj allowDamage false;
		_obj enableSimulation false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
				_obj allowDamage false;
		_obj enableSimulation false;
	};
} foreach _objs2;