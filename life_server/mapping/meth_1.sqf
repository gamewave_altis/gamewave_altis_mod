//soude
private ["_objs","_marker"];
_marker = createMarker ["meth_1",[13589.5,12191.9,-2.00272e-005]];
_objs = [
	["Land_Portable_generator_F",[13586.6,12192.1,0.014246],295.057,[[-0.905884,0.423527,0],[0,0,1]],false],
	["Land_PortableLight_double_F",[13587.1,12193.1,-0.0284119],292.622,[[-0.923065,0.384645,0],[0,0,1]],false],
	["Land_WeldingTrolley_01_F",[13584.1,12194,0],293.77,[[-0.915169,0.40307,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13592.4,12194,0],265.058,[[-0.996282,-0.0861491,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13592.2,12193.5,-9.53674e-007],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13592,12192.9,-9.53674e-007],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13591.7,12192.3,-9.53674e-007],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13591.5,12191.7,-1.90735e-006],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13591.2,12191.2,-1.90735e-006],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13591,12190.6,-2.86102e-006],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.8,12190,-3.8147e-006],293.77,[[-0.915169,0.403069,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.5,12189.4,-3.8147e-006],300.278,[[-0.863586,0.504201,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.3,12188.9,-4.76837e-006],216.056,[[-0.588569,-0.808447,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13589.7,12189.1,-4.76837e-006],258.933,[[-0.981402,-0.191964,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13590,12189.7,-5.72205e-006],325.162,[[-0.571253,0.820774,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.2,12190.2,-6.67572e-006],280.754,[[-0.982437,0.186592,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.4,12190.8,-7.62939e-006],278.457,[[-0.989127,0.147067,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.7,12191.4,-7.62939e-006],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.9,12192,-8.58307e-006],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13591.1,12192.6,-9.53674e-006],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13591.4,12193.1,-1.04904e-005],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13591.6,12193.7,-1.04904e-005],7.27377,[[0.126611,0.991952,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13591.8,12194.3,-1.14441e-005],201.891,[[-0.372838,-0.927896,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13591.3,12194.5,-1.14441e-005],225.626,[[-0.714794,-0.699336,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13591,12193.9,-1.23978e-005],343.538,[[-0.283375,0.959009,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.8,12193.4,-1.23978e-005],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.6,12192.8,-1.23978e-005],71.9723,[[0.950907,0.309477,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.3,12192.2,-1.23978e-005],191.554,[[-0.200297,-0.979735,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13590.1,12191.6,-1.33514e-005],33.3063,[[0.549115,0.835747,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13589.9,12191.1,-1.43051e-005],266.206,[[-0.997809,-0.0661633,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13589.6,12190.5,-1.52588e-005],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13589.4,12189.9,-1.52588e-005],318.654,[[-0.660602,0.750737,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13589.1,12189.3,-1.62125e-005],299.13,[[-0.873519,0.486791,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13588.6,12189.6,-1.71661e-005],187.726,[[-0.134436,-0.990922,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13588.8,12190.1,-1.71661e-005],294.536,[[-0.909701,0.415263,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13589,12190.7,-1.81198e-005],286.114,[[-0.960713,0.277543,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13589.3,12191.3,-1.90735e-005],303.724,[[-0.831723,0.55519,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13589.5,12191.9,-2.00272e-005],232.9,[[-0.797584,-0.603207,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13589.8,12192.4,-2.00272e-005],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590,12193,-2.00272e-005],270.418,[[-0.999973,0.00728697,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.2,12193.6,-2.09808e-005],52.8307,[[0.796854,0.604172,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.5,12194.2,-2.19345e-005],268.886,[[-0.999811,-0.0194389,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13590.7,12194.8,-2.28882e-005],293.387,[[-0.917842,0.396946,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13590.1,12195,-2.28882e-005],109.107,[[0.94491,-0.327331,0],[0,-0,1]],false],
	["Land_MetalBarrel_F",[13589.9,12194.4,-2.38419e-005],192.32,[[-0.213371,-0.976971,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13589.6,12193.8,-2.47955e-005],214.907,[[-0.572246,-0.820082,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13589.4,12193.3,-2.57492e-005],44.4084,[[0.699768,0.71437,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13589.2,12192.7,-2.67029e-005],219.501,[[-0.636091,-0.771614,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13588.9,12192.1,-2.67029e-005],42.8771,[[0.680428,0.732815,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13588.7,12191.5,-2.76566e-005],285.731,[[-0.962546,0.271117,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13588.5,12191,-2.86102e-005],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13588.2,12190.4,-2.95639e-005],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13588,12189.8,-2.95639e-005],248.596,[[-0.931031,-0.364939,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13587.4,12190,-2.95639e-005],275.011,[[-0.996177,0.0873552,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13587.7,12190.6,-3.05176e-005],292.239,[[-0.925614,0.378469,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13587.9,12191.2,-3.14713e-005],245.151,[[-0.907416,-0.420234,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13588.1,12191.8,-3.14713e-005],233.666,[[-0.805574,-0.592495,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13588.4,12192.3,-3.24249e-005],226.775,[[-0.728667,-0.684868,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13588.6,12192.9,-3.33786e-005],216.821,[[-0.599319,-0.80051,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13588.8,12193.5,-3.43323e-005],47.4711,[[0.736936,0.675962,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13589.1,12194.1,-3.43323e-005],207.633,[[-0.46381,-0.885935,0],[-0,0,1]],false],
	["Land_MetalBarrel_F",[13589.3,12194.7,-3.52859e-005],331.671,[[-0.474542,0.880233,0],[0,0,1]],false],
	["Land_MetalBarrel_F",[13589.5,12195.2,-3.62396e-005],206.485,[[-0.445959,-0.895053,0],[-0,0,1]],false],
	["Land_PortableLight_double_F",[13592.8,12191,-0.0284109],115.371,[[0.903553,-0.428476,0],[0,-0,1]],false],
	["Land_StallWater_F",[13589.3,12214.2,0],16.8446,[[0.289777,0.957094,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13585.8,12202.1,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13586.6,12204.5,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13587.4,12206.9,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13588.2,12209.2,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13589.1,12211.6,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13586.6,12212.5,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13585.7,12210.1,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13584.9,12207.7,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13584.1,12205.3,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13581.6,12206.2,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13582.4,12208.6,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13583.2,12211,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13584.1,12213.3,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13591.6,12210.8,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13590.8,12208.4,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13589.9,12206,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13589.1,12203.6,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13588.3,12201.2,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13583.3,12202.9,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13580.8,12203.8,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["Land_EngineCrane_01_F",[13588.1,12187.6,0],202.656,[[-0.385204,-0.922831,0],[-0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13590.8,12200.4,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13591.6,12202.8,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13592.4,12205.2,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13593.3,12207.5,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false],
	["cl_b_AmygdalusN1s_EP1",[13594.1,12209.9,-0.0118322],288.793,[[-0.946686,0.322157,0],[0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
		_obj enableSimulation false;
		_obj allowDamage false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		_obj enableSimulation false;
		_obj allowDamage false;
	};
} foreach _objs;