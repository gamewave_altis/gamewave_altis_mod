//phosphore
private ["_objs","_marker"];
_marker = createMarker ["meth_2",[13236.1,7385.44,-3.37413]];
_objs = [
	["Land_Sea_Wall_F",[13265.3,7431.71,3.59835],122.262,[[0.845618,-0.533789,0],[0,-0,1]],false],
	["Land_Sea_Wall_F",[13278.6,7457.07,5.14708],122.262,[[0.845618,-0.533789,0],[0,-0,1]],false],
	["Land_Factory_Conv2_F",[13256.6,7418.2,0.289053],300.034,[[-0.865727,0.500517,0],[0,0,1]],false],
	["Land_Factory_Tunnel_F",[13236.1,7385.44,-3.37413],211.462,[[-0.521925,-0.852991,0],[-0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
		_obj enableSimulation false;
		_obj allowDamage false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		_obj enableSimulation false;
		_obj allowDamage false;
	};
} foreach _objs;