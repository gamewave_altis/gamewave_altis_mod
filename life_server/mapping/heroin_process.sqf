private ["_objs"];
//_marker = createMarker ["Dealer_1",[5004.25,11170.4,0]];
_objs = [
	["",[7244.94,10996.4,0.196019],102.599,[[0.975922,-0.218121,0],[0,-0,1]],false]
];

{
	private ["_obj"];
	_obj = nearestObject [[8481.12,25259.2,0.00143433],"cl3_c_poloshirtpants_1_uniform"];

		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		
		removeAllWeapons _obj;
		removeAllItems _obj;
		removeAllAssignedItems _obj;
		removeUniform _obj;
		removeVest _obj;
		removeBackpack _obj;
		removeHeadgear _obj;
		removeGoggles _obj;
		_obj forceAddUniform "cl3_suit_white_white_black";
		_obj addHeadgear "H_Hat_brown";
		_obj addGoggles "SFG_Tac_BeardD";
		_obj setFace "WhiteHead_12";
		_obj setSpeaker "Male01ENG";

		_obj playmovenow "amovpercmstpsnonwnondnon";
		_obj allowDamage false;
		_obj enableSimulation false;
		
} foreach _objs;

_objs2 = [
	["Land_i_Shed_Ind_F",[7248.04,10995.9,0.12115],191.416,[[-0.197923,-0.980218,0],[-0,0,1]],false]
];

{
	private ["_obj"];
	_obj = createVehicle [_x select 0, [0,0,0], [], 0, "CAN_COLLIDE"];
	if (_x select 4) then {
		_obj setDir (_x select 2);
		_obj setPos (_x select 1);
		_obj allowDamage false;
		_obj enableSimulation false;
	} else {
		_obj setPosATL (_x select 1);
		_obj setVectorDirAndUp (_x select 3);
		_obj allowDamage false;
		_obj enableSimulation false;
	};
} foreach _objs2;